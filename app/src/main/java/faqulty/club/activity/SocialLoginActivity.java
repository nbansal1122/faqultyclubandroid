package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import faqulty.club.LoginActivity;
import faqulty.club.R;
import faqulty.club.SignUpActivity;
import faqulty.club.Util.FBLoginUtil;
import faqulty.club.Util.GoogleUtil;
import faqulty.club.VerifyMobile;
import faqulty.club.models.SocialLoginModel;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by nbansal2211 on 19/09/16.
 */
public class SocialLoginActivity extends BaseActivity {
    private FBLoginUtil fbLoginUtil;
    private UserDetailsApi user = new UserDetailsApi();;
    private GoogleUtil googleUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_login);
        initGoogle();
        setOnClickListener(R.id.btn_signup, R.id.btn_login, R.id.btn_fb_login, R.id.btn_gplus_login);
    }

    private void initGoogle() {
        googleUtil = GoogleUtil.getInstance(this);
        googleUtil.setListener(new GoogleUtil.GoogleLoginCallBack() {
            @Override
            public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                if (googleSignInAccount != null) {
                    if (googleSignInAccount.getDisplayName() != null)
                        user.setName(googleSignInAccount.getDisplayName());
                    if (googleSignInAccount.getEmail() != null)
                        user.setEmail(googleSignInAccount.getEmail());
                    //// TODO: 22-09-2016 get user image from this
                }
                initiateSocialLogin("google");
            }

            @Override
            public void onFailed() {
                showToast("Unable to fetch data");
            }
        });
    }

    private void getFBData() {
        fbLoginUtil = new FBLoginUtil(this, new FBLoginUtil.FBLoginCallback() {
            @Override
            public void onSuccess(Bundle bundle) {
                Preferences.saveData(Preferences.LOGIN_KEY, true);
                if (bundle != null) {
                    if (bundle.getString("name") != null && bundle.getString("last_name") != null)
                        user.setName(bundle.getString("name") + " " + bundle.getString("last_name"));
                    if (bundle.getString("gender") != null)
                        user.setGender(bundle.getString("gender"));
                    if (bundle.getString("email") != null)
                        user.setEmail(bundle.getString("email"));
                }
                initiateSocialLogin("facebook");
            }

            @Override
            public void onFailure() {
                showToast("Unable to fetch data");
            }
        });
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_signup:
                moveToSignUp();
                finish();
                break;
            case R.id.btn_login:
                moveToLogin();
                finish();
                break;
            case R.id.btn_fb_login:
                getFBData();
                fbLoginUtil.initiateFbLogin();
                break;
            case R.id.btn_gplus_login:
                googleUtil.signInWithGoogle();
                break;
        }
    }

    private void initiateSocialLogin(String source) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SOCIAL_LOGIN);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(SocialLoginModel.class);
        httpParamObject.setJson(getJsonData(source).toString());
        executeTask(AppConstants.TASKCODES.SOCAIL_LOGIN,httpParamObject);
    }

    private JSONObject getJsonData(String source) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("source",source);
            jsonObject.put("email",user.getEmail());
            jsonObject.put("name",user.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("social signup",jsonObject.toString());
        return jsonObject;
    }


    private void moveToSignUp() {
        startNextActivity(SignUpActivity.class);
        finish();
    }

    private void moveToLogin() {
        startNextActivity(LoginActivity.class);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==AppConstants.REQUEST_CODES.GOOGLE_SIGHN_IN){
            googleUtil.onActivityResult(data);
            return;
        }
        if(resultCode!=RESULT_OK){
            return;
        }
        fbLoginUtil.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASKCODES.SOCAIL_LOGIN:
                SocialLoginModel socialLoginModel = (SocialLoginModel) response;
                if(response!=null){
                    if(socialLoginModel.getStatus()){
                        user = socialLoginModel.getData().getUser();
                        Preferences.saveData(AppConstants.PREF_KEYS.USER_TOKEN,socialLoginModel.getData().getToken());
                        Preferences.saveData(AppConstants.PREF_KEYS.USER_ID,socialLoginModel.getData().getUser().getId());
                        Preferences.saveData(Preferences.LOGIN_KEY, true);
                        if(user.getPhoneVerified()==0){
                            startNextActivity(VerifyMobile.class);
                            finish();
                        }
                        else{
                            Intent intent=new Intent(this,HomeActivity.class);
                            intent.putExtra(AppConstants.BUNDLE_KEYS.TYPE,1);
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        showToast("Oops there seems to be an error in linking to your social account!!");
                    }
                }
        }
    }
}
