package faqulty.club.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import faqulty.club.AttendenceActivity;
import faqulty.club.R;
import faqulty.club.autoadapters.MyFilterAdapter;
import faqulty.club.models.Students;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class CreateGroupActivity extends BaseActivity implements MyFilterAdapter.OnListClickListener, AdapterView.OnItemClickListener {

    private static final int REQ_ADD_CUSTOM = 2;
    private ArrayList<Students> studentList = new ArrayList<>();
    private MyFilterAdapter listAdapter;
    private ArrayList<Students> productFilter;
    ArrayList<String> nameArray = new ArrayList<String>();
    private List<Students> selectedStudentList = new ArrayList<>();

    private ListView lv_search_result;
    private SearchView searchView;
    private int counter = 0;
    private View footerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_class);
        initToolBar(getString(R.string.title_new_group));
        getSupportActionBar().setSubtitle(counter + " Selected");
        lv_search_result = (ListView) findViewById(R.id.lv_search_students);
        lv_search_result.addFooterView(getFooterView());
        productFilter = new ArrayList<>(studentList);
        searchView = (SearchView) findViewById(R.id.et_search);
        listAdapter = new MyFilterAdapter(this, R.layout.row_new_class, studentList, productFilter, this);
        lv_search_result.setAdapter(listAdapter);
        lv_search_result.setOnItemClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        searchData();
    }

    private View getFooterView() {
        return footerView = LayoutInflater.from(this).inflate(R.layout.footer_new_class, null);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_check:
                startNextActivity(AttendenceActivity.class);
                break;
            case R.id.menu_done:
                getSelectedStudents();
                Intent data = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.SELECTEDSTUDENTS, (Serializable) selectedStudentList);
                data.putExtras(bundle);
                setResult(RESULT_OK, data);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getSelectedStudents() {
        for (Students student : studentList) {
            if (student.isSelect()) {
                selectedStudentList.add(student);
            }
        }

    }


    private void searchData() {
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.et_search);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//
//        } else {
//            searchView.setBackgroundColor(getResourceColor(R.color.white));
//        }
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setTextColor(getResourceColor(R.color.black));
        searchAutoComplete.setHintTextColor(getResourceColor(R.color.et_gray));
        searchAutoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

    private void updateToolbar() {
        for (Students student : studentList) {
            if (student.isSelect()) {
                counter = counter++;
                selectedStudentList.add(student);
            }
        }
        getSupportActionBar().setSubtitle(counter + "Selected");
    }


    @Override
    public void onEdit(Students students) {
        updateListProduct(students);
    }

    private void updateListProduct(Students students) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, students);
        Intent intent = new Intent(this, CreateGroupActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQ_ADD_CUSTOM);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Students student = studentList.get(position);
        if (student.isSelect()) {
            student.setSelect(false);
            selectedStudentList.remove(student);
        } else {
            student.setSelect(true);
            selectedStudentList.add(student);
        }
        listAdapter.notifyDataSetChanged();
        updateToolbar();
    }
}
