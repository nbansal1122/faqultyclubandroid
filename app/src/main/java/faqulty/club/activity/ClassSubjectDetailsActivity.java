package faqulty.club.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.transaction.Invoice;
import faqulty.club.models.creategroup.CreateGroupResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;

public class ClassSubjectDetailsActivity extends BaseActivity {
    LayoutInflater inflater;
    LinearLayout ll_enrolled_students;
    List<Invoice> studentList = new ArrayList<>();
    TextView tv_add_student;
    private CreateGroupResponse group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_subject_details);
        group = CreateGroupResponse.getInstance();
        initToolBar(group.getCreateGroupData().getName().toString());
        tv_add_student = (TextView) findViewById(R.id.tv_add_student_to_class);
        initData();
        ll_enrolled_students = (LinearLayout) findViewById(R.id.ll_enrolled_students);
        inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setData();
    }


    private void setData() {
        for (int y = 0; y < studentList.size(); y++) {
            Invoice details = studentList.get(y);
            View view = inflater.inflate(R.layout.row_enrolled_students, null);
            setText(details.getInvoiceNum(), R.id.tv_name_of_student, view);
            ll_enrolled_students.addView(view);
        }
        if (!TextUtils.isEmpty(group.getCreateGroupData().getPupilInfo().getBillingCycle()))
            setText(group.getCreateGroupData().getPupilInfo().getBillingCycle().toString(), R.id.tv_billing_info);
        if (!TextUtils.isEmpty(group.getCreateGroupData().getPupilInfo().getTuitionType()))
            setText(group.getCreateGroupData().getPupilInfo().getTuitionType().toString(), R.id.tv_tution_type);
        if (!TextUtils.isEmpty(group.getCreateGroupData().getPupilInfo().getBillingCycle()))
            setText("Rs." + group.getCreateGroupData().getPupilInfo().getFee().toString() + " per Hour", R.id.tv_billing_info);
        if (group.getCreateGroupData().getPupilInfo().getClassDurationHours()!=null &&
        group.getCreateGroupData().getPupilInfo().getClassDurationMins()!=null)
            setText(group.getCreateGroupData().getPupilInfo().getClassDurationHours() + "Hr " +
                    group.getCreateGroupData().getPupilInfo().getClassDurationMins() + "Min"  , R.id.tv_billing_info);
    }

    private void initData() {
        studentList.clear();
        for (int x = 0; x < 4; x++) {
            Invoice studentDetails = new Invoice();
            studentDetails.setInvoiceNum("Invoice" + x);
            studentDetails.setImageNext(R.mipmap.rightarrowgrey);
            studentList.add(studentDetails);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.class_subject_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_attendence:
            case R.id.add_students:
            case R.id.delete_class:
            case R.id.change_tuition_fee:

        }
        return super.onOptionsItemSelected(item);
    }
}
