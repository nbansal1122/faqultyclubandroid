package faqulty.club.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import com.squareup.picasso.Picasso;

import java.io.File;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.Util;

public class LogoPreviewActivity extends BaseActivity {

    private String brand;
    private String waterMark;
    private String logoPath;
    private String logo;

    public static void showPreview(String brandName, String watermark, String logoPath, String logoUrl, Context context) {
        Intent intent = new Intent(context, LogoPreviewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("brand", brandName);
        bundle.putString("water_mark", watermark);
        bundle.putString("logo_path", logoPath);
        bundle.putString("logo_url", logoUrl);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_preview);
        initToolBar("Preview");
        setText(waterMark, R.id.tv_water_mark);
        setText(brand, R.id.tv_brand);
        if (!TextUtils.isEmpty(logoPath)) {
            Uri uri = Uri.fromFile(new File(logoPath));
            ImageView imageView = (ImageView) findViewById(R.id.iv_logo);
            imageView.setImageURI(uri);
        }
        setText(waterMark, R.id.tv_water_mark);
        setText(brand, R.id.tv_brand);
        ImageView imageView = (ImageView) findViewById(R.id.iv_logo);
        if (!TextUtils.isEmpty(logo)) {
            Picasso.with(this).load(Util.getCompleteUrl(logo)).into(imageView);
        } else if (!TextUtils.isEmpty(logoPath)) {
            Uri uri = Uri.fromFile(new File(logoPath));
            imageView.setImageURI(uri);
        }
        setQuestions();
    }

    private void setQuestions() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_dummy_questions);
        setQuestion("1.", "If a line segment AB is divided in the ratio 5 : 7, the first step is to draw a ray AX, so that &ang;BAX is an acute angle and then at equal distances point are marked on the ray AX such that the minimum number of these points is<br/>(A) 8<br/>(B) 10<br/>(C) 11<br/>(D) 12", linearLayout, layoutInflater);
        setQuestion("2.", "When a line segment AB is divided in the ratio 4 : 7, a ray AX is drawn such that &ang;BAX is an acute angle and then points A<sub>1</sub>, A<sub>2</sub>, A<sub>3</sub>, &hellip; are located at equal distances on the ray AX then the point B is joined to<br/>(A) A<sub>12</sub><br/>(B) A<sub>11</sub><br/>(C) A<sub>10</sub><br/>(D) A<sub>9</sub>", linearLayout, layoutInflater);
        setQuestion("3.", "Given a&#9651;ABC, in order to draw another triangle similar to it, with sides <math> <semantics> <mrow> <mfrac> <mn>3</mn> <mn>7</mn> </mfrac> </mrow> </semantics> </math> of the corresponding sides of &#9651;ABC, the first step is to draw a ray BX such that &ang;CBX is an acute angle and X lies on the opposite side of A with respect to BC. The second step is to locate points B<sub>1</sub>, B<sub>2</sub>, B<sub>3</sub>, &hellip; on BX at equal distances, and the final step is to join<br/>(A) B<sub>10</sub> to C<br/>(B) B<sub>3</sub> to C<br/>(C) B<sub>7</sub> to C<br/>(D) B<sub>4</sub> to C", linearLayout, layoutInflater);
        setQuestion("4.", "Given a&#9651;ABC, another triangle similar to it having sides <math> <semantics> <mrow> <mfrac> <mn>8</mn> <mn>5</mn> </mfrac> </mrow> </semantics> </math> of the corresponding sides of &#9651;ABC can be constructed by drawing a ray BX such that &ang;CBX is an acute angle and X is on the opposite side of A with respect to BC. The minimum number of points to be located at equal distances on ray BX is<br/>(A) 5<br/>(B) 8<br/>(C) 13<br/>(D) 3", linearLayout, layoutInflater);
        setQuestion("5.", "If pair of tangents to a circle which are inclined to each other at an angle of 60&deg; are to be drawn, then those tangents should be drawn at end points of the two radii of the circle, the angle between whose is<br/>(A) 135&deg;<br/>(B) 90&deg;<br/>(C) 60&deg;<br/>(D) 120&deg;", linearLayout, layoutInflater);
    }

    private void setQuestion(String counter, String question, LinearLayout linearLayout, LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.row_logo_question, linearLayout, false);
        setText(question, R.id.tv_logo_text, view);
        setText(counter, R.id.tv_counter, view);
        linearLayout.addView(view);
    }

    @Override
    public void setText(String text, int textViewId, View v) {
        TextView textView = (TextView) v.findViewById(textViewId);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(Html.fromHtml(text));
        }

    }

    @Override
    protected void loadBundle(Bundle bundle) {
        brand = bundle.getString("brand");
        waterMark = bundle.getString("water_mark");
        logoPath = bundle.getString("logo_path");
        logo = bundle.getString("logo_url");
    }
}
