package faqulty.club.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.WebTemplate;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

import static faqulty.club.R.mipmap.template;

public class WebsiteTemplateActivity extends BaseActivity implements CustomListAdapterInterface {

    List<WebTemplate> templateList = new ArrayList<>();
    private CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website_template);
        initToolBar("Website Template");
        TextView tvEmptyListview = (TextView) findViewById(R.id.tv_empty_listview);
        ListView template = (ListView) findViewById(R.id.lv_website_template);
        customListAdapter = new CustomListAdapter(this, R.layout.row_website_template, templateList, this);
        template.setAdapter(customListAdapter);
        template.setEmptyView(tvEmptyListview);
        //setData();
    }

    private void setData() {
        for (int x = 0; x < 10; x++) {
            WebTemplate webTemplate = new WebTemplate();
            webTemplate.setTemplateTitle("Template Edu");
            webTemplate.setTemplateSubtitle("Highlights Educationals Details");
            webTemplate.setTemplateImage(template);
            templateList.add(webTemplate);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        WebTemplate webTemplateList = templateList.get(position);
        holder.tvTitle.setText(webTemplateList.getTemplateTitle());
        holder.tvSubtitle.setText(webTemplateList.getTemplateSubtitle());
        holder.ivTImage.setImageResource(webTemplateList.getTemplateImage());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {
                    case 0:
                        startNextActivity(PamphletPreviewActivity.class);
                }
            }
        });
        return convertView;
    }

    class Holder {
        TextView tvTitle, tvSubtitle;
        ImageView ivTImage;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.template_title);
            tvSubtitle = (TextView) view.findViewById(R.id.subtitle_template);
            ivTImage = (ImageView) view.findViewById(R.id.template_image);
        }
    }

}
