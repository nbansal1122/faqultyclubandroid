package faqulty.club.activity;

import android.os.Bundle;

import faqulty.club.R;

import simplifii.framework.activity.BaseActivity;

/**
 * Created by Neeraj Yadav on 9/26/2016.
 */

public class BasicDetailActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_details);
    }
}
