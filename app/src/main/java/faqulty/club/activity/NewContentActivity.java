package faqulty.club.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;
import faqulty.club.models.BaseApi;

import simplifii.framework.requestmodels.SelectContent;

import faqulty.club.models.content.ContentData;
import faqulty.club.models.content.ContentUploadResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class NewContentActivity extends BaseActivity {
    private Uri uri;
    private SelectContent selectContent;
    private ContentData contentData;
    ImageView imageView;
    ImageView playContent;
    ExposureVideoPlayer videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_content);
        videoView = (ExposureVideoPlayer) findViewById(R.id.videoView_content);
        imageView = (ImageView) findViewById(R.id.imageView_content);
        playContent = (ImageView) findViewById(R.id.iv_play_content);
        loadBundle(getIntent().getExtras());
        if (selectContent != null) {
            initToolBar("New Content");
            setDummyView();
        } else if (contentData != null) {
            initToolBar("Content");
            setContentData();
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        Object content = bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (content instanceof SelectContent) {
            selectContent = (SelectContent) content;
        } else if (content instanceof ContentData) {
            contentData = (ContentData) content;
        } else if (content instanceof ArrayList) {
            ArrayList<SelectContent> selectContents = (ArrayList<SelectContent>) content;
            if (selectContents != null && selectContents.size() > 0) {
                selectContent = selectContents.get(0);
            }
        }

    }

    private void setContentData() {
        String url = contentData.getContentUrl();
        showVisibility(R.id.iv_delete_content);
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(this).load(Util.getCompleteUrl(url) + "&thumb=true").into(playContent);
        }
        setOnClickListener(R.id.iv_delete_content);
        setDataBasedOnMimeType();
        setEditText(R.id.et_desc, contentData.getDescription() + "");
    }


    private void setDataBasedOnMimeType() {
        ContentData data = contentData;
        String mimeType = data.getMimeType();
        videoView.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.contains("pdf")) {
                PDFViewer.startActivity(this, data.getContentUrl());
            } else if (mimeType.contains("image")) {
                playContent.setVisibility(View.GONE);
                Picasso.with(this).load(Util.getCompleteUrl(data.getContentUrl())).into(imageView);
            } else if (mimeType.contains("video") || mimeType.contains("audio")) {
                videoView.setVisibility(View.VISIBLE);
                videoView.setVideoSource(Util.getCompleteUrl(data.getContentUrl()));
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_delete_content:
                deleteContent();
                break;
        }
    }

    private void deleteContent() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(Util.getFormattedTutorUrl(AppConstants.PAGE_URL.TUTOR_CONTENT) + "/" + contentData.getId());
        obj.setDeleteMethod();
        obj.setJSONContentType();
        obj.setJson("");
        obj.setClassType(BaseApi.class);
        executeTask(AppConstants.TASKCODES.DELETE_TASK, obj);
    }

    private void setDummyView() {
        String filePath = selectContent.getFilePath();
        if(TextUtils.isEmpty(filePath)){
            showToast("Cannot get file");
            return;
        }
        File file = new File(filePath);
        uri = Uri.fromFile(file);
        switch (selectContent.getFileType()) {
            case AppConstants.FILE_TYPES.IMAGE:
                imageView.setImageURI(uri);
                videoView.setVisibility(View.GONE);
                playContent.setVisibility(View.GONE);
                break;
            case AppConstants.FILE_TYPES.VIDEO:
                videoView.setVisibility(View.VISIBLE);
                videoView.setVideoSource(uri);
                imageView.setVisibility(View.GONE);
                break;
            case AppConstants.FILE_TYPES.AUDIO:
                videoView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                playContent.setVisibility(View.GONE);
                videoView.setVideoSource(uri);
                break;
            case AppConstants.FILE_TYPES.PDF:
                imageView.setImageURI(uri);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Object content = getIntent().getExtras().getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (content instanceof SelectContent) {
            getMenuInflater().inflate(R.menu.save_content, menu);
            return true;
        } else if (content instanceof ArrayList) {
            getMenuInflater().inflate(R.menu.save_content, menu);
            return true;
        } else {
            getMenuInflater().inflate(R.menu.menu_update, menu);
            return true;
        }
    }

    private void uploadContent(SelectContent content) {
        File file = new File(content.getFilePath());
        if (file.exists()) {
            FileParamObject obj = new FileParamObject(file, file.getName(), "file");
            obj.setClassType(ContentUploadResponse.class);
            obj.setUrl(Util.getFormattedTutorUrl(AppConstants.PAGE_URL.TUTOR_CONTENT));
            obj.addParameter("description", content.getDesc());
            obj.setPostMethod();
            executeTask(AppConstants.TASKCODES.FILE_UPLOAD, obj);
        }

    }

    //    {
//        "description": "This is updated description",
//            "contentUrl": "/egest?file=b487b936548e6116cc67d184025935e5.png",
//            "mimetype": "image/png"
//    }
    private void updateContent() {
        String contentUrl = contentData.getContentUrl();
        JSONObject obj = new JSONObject();
        try {
            obj.put("contentUrl", contentUrl);
            obj.put("mimetype", contentData.getMimeType());
            obj.put("description", getEditText(R.id.et_desc));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpParamObject paramObject = new HttpParamObject();
        paramObject.setPutMethod();
        paramObject.setClassType(BaseApi.class);
        paramObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.UPDATE_CONTENT, (contentData.getId())));
        paramObject.setJSONContentType();
        paramObject.setJson(obj.toString());
        executeTask(AppConstants.TASKCODES.UPDATE_CONTENT, paramObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.FILE_UPLOAD:
                ContentUploadResponse fileUploadResponse = (ContentUploadResponse) response;
                if (fileUploadResponse != null && fileUploadResponse.getData() != null) {
                    Intent i = new Intent();
                    i.putExtra(AppConstants.BUNDLE_KEYS.KEY_MESSAGE, AppConstants.BUNDLE_KEYS.UPDATE);
                    setResult(RESULT_OK, i);
                    finish();
                }
                break;
            case AppConstants.TASKCODES.DELETE_TASK:
            case AppConstants.TASKCODES.UPDATE_CONTENT: {
                BaseApi api = (BaseApi) response;
                if (api != null) {
                    if (!TextUtils.isEmpty(api.getMsg()))
                        showToast(api.getMsg());
                    Intent i = new Intent();
                    i.putExtra(AppConstants.BUNDLE_KEYS.KEY_MESSAGE, AppConstants.BUNDLE_KEYS.UPDATE);
                    setResult(RESULT_OK, i);
                    finish();
                }
            }
            break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_content:
                String desc = getEditText(R.id.et_desc);
                selectContent.setDesc(desc);
                uploadContent(selectContent);
                break;
            case R.id.update_content:
                updateContent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}