package faqulty.club.activity;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.creategroup.Contacts;
import faqulty.club.models.creategroup.PhoneContact;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Dhillon on 03-12-2016.
 */

public class AddFromContact extends BaseActivity implements CustomListAdapterInterface, PermissionListener, LoaderManager.LoaderCallbacks<Cursor>, SearchView.OnQueryTextListener {
    private SearchView svContacts;
    private ListView lvContacts;
    private ArrayList<PhoneContact> selectedContacts = new ArrayList<>();
    private ArrayList<PhoneContact> allContacts = new ArrayList<>();
    private ArrayList<PhoneContact> copyAllContacts = new ArrayList<>();
    private CustomListAdapter contactAdapter;
    private Contacts contacts = new Contacts();
    private List<PhoneContact> savedContacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contacts);
        initToolBar(getString(R.string.add_from_contacts));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT)) {
            savedContacts = (ArrayList<PhoneContact>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        }
        new TedPermission(this)
                .setPermissions(Manifest.permission.READ_CONTACTS)
                .setPermissionListener(this).check();

        svContacts = (SearchView) findViewById(R.id.sv_search);
        svContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                svContacts.setIconified(false);
            }
        });
        svContacts.setOnQueryTextListener(this);
        svContacts.setQueryHint(getString(R.string.search_here));
        lvContacts = (ListView) findViewById(R.id.lv_contacts);
        if (savedInstanceState != null && savedContacts.size() > 0) {
            getSupportActionBar().setSubtitle(savedContacts.size() + " selected");
        }
        contactAdapter = new CustomListAdapter(this, R.layout.row_add_contact, allContacts, this);
        lvContacts.setAdapter(contactAdapter);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final PhoneContact phoneContact = allContacts.get(position);
        if (phoneContact != null) {
            if (!TextUtils.isEmpty(phoneContact.getName()))
                holder.tvName.setText(phoneContact.getName());
            else
                holder.tvName.setText("Unnamed contact");

            if (!TextUtils.isEmpty(phoneContact.getNumber()))
                holder.tvNumber.setText(phoneContact.getNumber());

            if (!TextUtils.isEmpty(phoneContact.getPhoto())) {
                Picasso.with(this).load(phoneContact.getPhoto()).placeholder(R.mipmap.accountselected).into(holder.ivContact);
            } else {
                holder.ivContact.setImageResource(R.mipmap.accountselected);
            }
            holder.cbSelected.setChecked(phoneContact.isSelected());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (phoneContact.isSelected()) {
                        phoneContact.setSelected(false);
                        selectedContacts.remove(phoneContact);
                    } else {
                        phoneContact.setSelected(true);
                        selectedContacts.add(phoneContact);
                    }
                    getSupportActionBar().setSubtitle(selectedContacts.size() + " selected");
                    contactAdapter.notifyDataSetChanged();
                }
            });
        }
        return convertView;
    }

    @Override
    public void onPermissionGranted() {
        getSupportLoaderManager().initLoader(1, null, this);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> arrayList) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.tv_done:
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                removeRepeated(selectedContacts);
                contacts.setPhoneContact(selectedContacts);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.SELECTEDCONTACTS, contacts);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void removeRepeated(ArrayList<PhoneContact> selectedContacts) {
        PhoneContact repeatedContact = null;
        for (PhoneContact phoneContact : selectedContacts) {
            List<PhoneContact> contactList = new ArrayList<>(selectedContacts);
            contactList.remove(phoneContact);
            for (PhoneContact c : contactList) {
                if (c.getNumber().equals(phoneContact.getNumber())) {
                    repeatedContact = phoneContact;
                    break;
                }
            }
        }
        if (repeatedContact != null) {
            selectedContacts.remove(repeatedContact);
            removeRepeated(selectedContacts);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(this, CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        cursor.moveToFirst();
        allContacts.clear();
        while (!cursor.isAfterLast()) {
            final String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            final String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            final String imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
            PhoneContact phoneContact = new PhoneContact(name, number, imageUri);
            checkSelected(phoneContact);
            allContacts.add(phoneContact);
            cursor.moveToNext();
        }


        Collections.sort(allContacts, new Comparator<PhoneContact>() {
            public int compare(PhoneContact s1, PhoneContact s2) {
                if (s1.getName() != null && s2.getName() != null) {
                    return s1.getName().compareTo(s2.getName());
                } else {
                    return s1.getNumber().compareTo(s2.getNumber());
                }
            }
        });
        copyAllContacts.addAll(allContacts);
        contactAdapter.notifyDataSetChanged();
        getSupportActionBar().setSubtitle(selectedContacts.size() + " selected");
    }

    private void checkSelected(PhoneContact phoneContact) {
        if (savedContacts != null && savedContacts.size() > 0) {
            for (PhoneContact contact : savedContacts) {
                if (phoneContact.getNumber().equals(contact.getNumber())) {
                    phoneContact.setSelected(true);
                    selectedContacts.add(phoneContact);
                    break;
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (s.length() > 0 && copyAllContacts.size() > 0) {
            filterList(s.toString());
        } else {
            allContacts.clear();
            allContacts.addAll(copyAllContacts);
            contactAdapter.notifyDataSetChanged();
        }
        return false;
    }

    private void filterList(String s) {
        allContacts.clear();
        for (PhoneContact c : copyAllContacts) {
            if (c.getName().toLowerCase().contains(s.toLowerCase()) || c.getNumber().contains(s)) {
                allContacts.add(c);
            }
        }
        contactAdapter.notifyDataSetChanged();
    }


    class Holder {
        TextView tvName, tvNumber;
        CircleImageView ivContact;
        CheckBox cbSelected;
        FrameLayout layOver;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name_invitation);
            tvNumber = (TextView) view.findViewById(R.id.tv_number_invitation);
            ivContact = (CircleImageView) view.findViewById(R.id.iv_invitation_pic);
            cbSelected = (CheckBox) view.findViewById(R.id.cb_send_invitation);
            layOver = (FrameLayout) view.findViewById(R.id.lay_over);
        }
    }
}
