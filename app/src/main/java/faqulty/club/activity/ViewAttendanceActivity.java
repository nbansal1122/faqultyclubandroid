package faqulty.club.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import faqulty.club.AttendenceActivity;
import faqulty.club.R;
import faqulty.club.models.BaseApi;
import faqulty.club.models.attendance.AttendanceResponse;
import faqulty.club.models.tutorstudent.StudentBaseClass;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by Dhillon on 15-12-2016.
 */

public class ViewAttendanceActivity extends BaseActivity {
    private LinearLayout llAttendance;
    private LayoutInflater layoutInflater;
    private StudentBaseClass student;
    private List<AttendanceResponse> attendanceResponseList;
    private boolean isGroupStudent;
    private String toDate, fromDate;
    private boolean isDeleteoptionMenu;
    private Menu menu;
    private List<AttendanceResponse> selectedItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);
        initViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            student = (StudentBaseClass) bundle.getSerializable(AppConstants.BUNDLE_KEYS.STUDENT);
            isGroupStudent = bundle.getBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT);
            fromDate = bundle.getString(AppConstants.BUNDLE_KEYS.FROM_DATE);
            toDate = bundle.getString(AppConstants.BUNDLE_KEYS.TO_DATE);
        }


    }


    private void initViews() {
        initToolBar(getString(R.string.view_attendance));
        attendanceResponseList = new ArrayList<>();
        llAttendance = (LinearLayout) findViewById(R.id.ll_attendance);
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAttendanceData();
    }

    private void getAttendanceData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        String attendanceUrl = String.format(AppConstants.PAGE_URL.GETATTENDANCE, String.valueOf(student.getId()), String.valueOf(Preferences.getUserId()));
        httpParamObject.setUrl(attendanceUrl);
        httpParamObject.addParameter("offset", "0");
        httpParamObject.addParameter("count", "100");
        httpParamObject.setClassType(AttendanceResponse.class);
        executeTask(AppConstants.TASKCODES.GETATTENDANCE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GETATTENDANCE:
                attendanceResponseList = (List<AttendanceResponse>) response;
                if (attendanceResponseList != null) {
                    createAttendanceMap();
                    if (!TextUtils.isEmpty(fromDate) && !TextUtils.isEmpty(toDate)) {
                        attendanceResponseList = filterAttandence(attendanceResponseList);
                    }
                    setAttendanceData();
                }
                break;
            case AppConstants.TASKCODES.MULTI_SESSION_DELETE:
                getAttendanceData();
                break;
        }

    }

    private ArrayList<AttendanceResponse> filterAttandence(List<AttendanceResponse> attendanceResponseList) {
        ArrayList<AttendanceResponse> attendanceResponses = new ArrayList<>();
        Date fromDatePick = Util.convertStringToDate(fromDate, "dd-MM-yyyy");
        Date toDatePick = Util.convertStringToDate(toDate, "dd-MM-yyyy");
        for (AttendanceResponse attendanceResponse : attendanceResponseList) {
            String createdOn = attendanceResponse.getCreatedOn();
            Date date = Util.convertStringToDate(createdOn, "dd MMM yyyy");
            if (date.getTime() >= fromDatePick.getTime() && date.getTime() <= toDatePick.getTime()) {
                attendanceResponses.add(attendanceResponse);
            }
        }
        return attendanceResponses;
    }

    private HashMap<String, List<AttendanceResponse>> mapAttendanceData = new HashMap<>();

    private void createAttendanceMap() {
        mapAttendanceData.clear();
        for (int i = 0; i < attendanceResponseList.size(); i++) {
            AttendanceResponse response = attendanceResponseList.get(i);
            List<AttendanceResponse> mapList;
            if (mapAttendanceData.containsKey(response.getCreatedOn())) {
                mapList = mapAttendanceData.get(response.getCreatedOn());
            } else {
                mapList = new ArrayList<>();
            }
            mapList.add(response);
            response.setSessionName(mapList.size() + "");
            mapAttendanceData.put(response.getCreatedOn(), mapList);
        }
    }

    public void setPerformance(int performance, ImageView ivPerformance) {
        // 0 poor, average, good
        if (performance == 0) {
            ivPerformance.setImageResource(R.mipmap.sad_icon);
        } else if (performance == 1) {
            ivPerformance.setImageResource(R.mipmap.ic_performance_neutral_selected);
        } else if (performance == 2) {
            ivPerformance.setImageResource(R.mipmap.smile_icon);
        }
    }

    private void setAttendanceData() {
        llAttendance.removeAllViews();
        if (null != attendanceResponseList && attendanceResponseList.size() > 0) {
            for (final AttendanceResponse ar : attendanceResponseList) {
                final View view = layoutInflater.inflate(R.layout.row_attendance_list, null);
                TextView tvDate, tvSession, tvHours;
                ImageView ivPerformance;
                tvDate = (TextView) view.findViewById(R.id.tv_date);
                tvSession = (TextView) view.findViewById(R.id.tv_session);
                tvHours = (TextView) view.findViewById(R.id.tv_hours);
                ivPerformance = (ImageView) view.findViewById(R.id.iv_performance);

                if (!TextUtils.isEmpty(ar.getCreatedOn()))
                    tvDate.setText(Util.convertDateFormat(ar.getCreatedOn(), "dd MMM yyyy", "dd MMM"));

                if (ar.getAttendance() != null)
                    tvSession.setText(ar.getSessionName());

                if (ar.getDurationHours() != null && ar.getDurationMins() != null)
                    tvHours.setText(padValue(ar.getDurationHours()) + ":" + padValue(ar.getDurationMins()));

                if (ar.getPerformance() != null) {
                    setPerformance(ar.getPerformance(), ivPerformance);
                }
                if (selectedItems.size() > 0) {
                    if (selectedItems.contains(ar)) {
                        view.setBackgroundColor(getResourceColor(R.color.et_gray));
                    } else {
                        view.setBackgroundColor(getResourceColor(R.color.white));
                    }
                }
                view.setTag(ar);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isGroupStudent) {
                            AttendanceResponse ar = (AttendanceResponse) v.getTag();
                            if (selectedItems.size() > 0) {
                                setSelect(ar);
                            } else {
                                Bundle b = new Bundle();
                                b.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT, student);
                                b.putString(AppConstants.BUNDLE_KEYS.SESSION_DATE, ar.getCreatedOn());
                                Intent i = new Intent(ViewAttendanceActivity.this, AttendenceActivity.class);
                                i.putExtras(b);
                                startActivityForResult(i, AppConstants.REQUEST_CODES.VIEW_ATTENDANCE);
                            }
                        }
                    }
                });

                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        AttendanceResponse ar = (AttendanceResponse) v.getTag();
                        setSelect(ar);
                        return true;
                    }
                });

                llAttendance.addView(view);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        MenuItem itemDelete = menu.findItem(R.id.action_delete);
        if (selectedItems.size() > 0) {
            itemDelete.setVisible(true);
        } else {
            itemDelete.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteAttandence();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAttandence() {
        if (!Util.isConnectingToInternet(this)) {
            Toast.makeText(this, "Please Connect to Internet..!", Toast.LENGTH_SHORT).show();
            return;
        } else {
            if (selectedItems.size() > 0) {
                List<HttpParamObject> list = new ArrayList<>();
                for (AttendanceResponse response : selectedItems) {
                    list.add(getParamObject(response.getSessionId()));
                }
                executeTask(AppConstants.TASKCODES.MULTI_SESSION_DELETE, list);
            } else {
                showToast(R.string.message_delete_session);
            }
        }

    }

    private HttpParamObject getParamObject(int sessionId) {
        HttpParamObject obj = new HttpParamObject();
        obj.setDeleteMethod();
        obj.setClassType(BaseApi.class);
        obj.setJSONContentType();
        obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE + "?sessionId=" + sessionId);
        return obj;
    }

    private void setSelect(AttendanceResponse ar) {
        if (!selectedItems.contains(ar)) {
            selectedItems.add(ar);
        } else {
            selectedItems.remove(ar);
        }
        setAttendanceData();
        invalidateOptionsMenu();
    }

    private String padValue(int value) {
        if (value < 10) {
            return "0" + value;
        }
        return value + "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getAttendanceData();
    }
}
