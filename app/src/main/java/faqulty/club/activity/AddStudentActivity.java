package faqulty.club.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import faqulty.club.R;
import simplifii.framework.activity.BaseActivity;

public class AddStudentActivity extends BaseActivity {

    Button btn_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        initToolBar("Add Student");
        btn_add= (Button) findViewById(R.id.btn_add_student);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(CreateNewClassActivity.class );
            }
        });
    }
}
