package faqulty.club.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import faqulty.club.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class PDFViewer extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfviewer);
        initToolBar("PDF");
        WebView webView = (WebView) findViewById(R.id.contentViewer);
        String url = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.PDF_URL);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
    }

    public static void startActivity(Context context, String contentUrl) {
        contentUrl=AppConstants.PAGE_URL.PHOTO_URL+contentUrl;
        Intent i = new Intent(context, PDFViewer.class);
        i.putExtra(AppConstants.BUNDLE_KEYS.PDF_URL, contentUrl);
        context.startActivity(i);
    }
}
