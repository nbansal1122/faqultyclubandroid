package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.invoice.InvoiceResponse;
import faqulty.club.models.tutorstudent.Invoice;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class SendReminderInvoice extends BaseActivity {
    TextView tv_from, tv_to;
    private int mYear, mMonth, mDay;
    private int studentId;
    private long fromDate, toDate;
    private InvoiceResponse invoiceResponse;
    private boolean isEdit;
    private Invoice invoice;
    private int resultCode = RESULT_CANCELED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_reminder_invoice);
        tv_from = (TextView) findViewById(R.id.tv_from_date);
        tv_to = (TextView) findViewById(R.id.tv_to_date);
        setViewsIfEditingInvoice();
        setOnClickListener(R.id.btn_payment_received, R.id.btn_send_reminder);
    }

    private void setViewsIfEditingInvoice() {
        if (isEdit) {
            String dateFrom = Util.convertDateFormat(invoice.getDateFrom(), Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.INVOICE_EDIT_FORMAT);
            String dateTo = Util.convertDateFormat(invoice.getDateTo(), Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.INVOICE_EDIT_FORMAT);
            tv_from.setText(dateFrom);
            tv_to.setText(dateTo);
            initToolBar(getString(R.string.invoice_detail));
            setEditText(R.id.et_payment_due, invoice.getAmount() + "");
            findViewById(R.id.et_payment_due).setEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_invoice, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                startEditInvoiceActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_reminder:
                break;
            case R.id.btn_payment_received:
                startPaymentReceiveActivity();
                break;
        }
    }

    private void startPaymentReceiveActivity() {
        startNextActivityForResult(getIntent().getExtras(), ReceivePaymentActivity.class, AppConstants.TASKCODES.RECEIVE_PAYMENT);
    }

    private void startEditInvoiceActivity() {
        startNextActivity(getIntent().getExtras(), CreateNewInvoiceActivity.class);
        finish();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        studentId = bundle.getInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, -1);
        isEdit = bundle.getBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE, false);
        if (isEdit)
            invoice = (Invoice) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.TASKCODES.RECEIVE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
        this.resultCode = resultCode;
        if (resultCode == RESULT_OK) {

        }
    }
}
