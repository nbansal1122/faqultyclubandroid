package faqulty.club.activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import faqulty.club.R;

import simplifii.framework.activity.BaseActivity;

public class EditStudentDetailsActivity extends BaseActivity {

    TextView tv_billing_cycle,tv_tuition_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student_details);
        initToolBar("Edit Student Details");
        tv_billing_cycle = (TextView) findViewById(R.id.spin_billing_cycle_edit);
        tv_tuition_type = (TextView) findViewById(R.id.spin_tuition_type_edit_details);
        tv_tuition_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(EditStudentDetailsActivity.this);
                Window window = dialog.getWindow();
                window.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setTitle(null);
                dialog.setContentView(R.layout.dialog_change_tuition_type);
                dialog.setCancelable(true);

                final TextView tv_confirm = (TextView) dialog.findViewById(R.id.tv_confirm_change_type);
                final TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel_change_type);
                tv_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tv_confirm.setTextColor(getResourceColor(R.color.orange));
                        setText(tv_confirm.getText().toString(),R.id.spin_tuition_type_edit_details);
                        dialog.dismiss();
                    }
                });
                tv_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tv_cancel.setTextColor(getResourceColor(R.color.orange));
                        setText(tv_cancel.getText().toString(),R.id.spin_tuition_type_edit_details);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }

        });

        tv_billing_cycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    final Dialog dialog = new Dialog(EditStudentDetailsActivity.this);
                Window window = dialog.getWindow();
                window.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setTitle(null);
                    dialog.setContentView(R.layout.dialog_billing_cycle);
                    dialog.setCancelable(true);

                final RadioButton rb_hourly = (RadioButton) dialog.findViewById(R.id.rb_hourly);
                final RadioButton rb_monthly = (RadioButton) dialog.findViewById(R.id.rb_monthly);
                final RadioButton rb_custom = (RadioButton) dialog.findViewById(R.id.rb_custom);
                rb_hourly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked==true){
                            setText(rb_hourly.getText().toString(),R.id.spin_billing_cycle_edit);
                            dialog.dismiss();
                        }
                    }
                });
                    rb_monthly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked==true){
                                setText(rb_monthly.getText().toString(),R.id.spin_billing_cycle_edit);
                                dialog.dismiss();
                            }
                        }
                    });

                        rb_custom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked==true){
                                    setText(rb_custom.getText().toString(),R.id.spin_billing_cycle_edit);
                                    dialog.dismiss();
                                }
                            }
                        });
                    dialog.show();
                }
        });
    }
}
