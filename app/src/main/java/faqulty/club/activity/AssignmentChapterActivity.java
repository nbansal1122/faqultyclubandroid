package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.assignment.ClassModel;
import faqulty.club.models.assignment.chapters.ChapterData;
import faqulty.club.models.assignment.chapters.ChaptersResponse;
import faqulty.club.models.assignment.subjects.SubjectData;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Dhillon on 02-12-2016.
 */

public class AssignmentChapterActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private SubjectData subjectData;
    private ListView lvChapters;
    private List<ChapterData> chapterList;
    private CustomListAdapter chapterAdapter;
    private ClassModel assignmentModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapters);
        initToolBar("Assignments");
        if (subjectData != null && !TextUtils.isEmpty(subjectData.getDegreeSubject()))
            getSupportActionBar().setSubtitle("Class " + assignmentModel.getClass_() + " / " + subjectData.getDegreeSubject());
        lvChapters = (ListView) findViewById(R.id.lv_chapters);
        chapterList = new ArrayList<>();
        chapterAdapter = new CustomListAdapter(this, R.layout.row_assignment, chapterList, this);
        lvChapters.setAdapter(chapterAdapter);
        lvChapters.setOnItemClickListener(this);
    }


    private void getChaptersData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_SUBJECT_CHAPTERS);
        httpParamObject.addParameter("class", assignmentModel.getClass_());
        httpParamObject.addParameter("subject", String.valueOf(subjectData.getId()));
        httpParamObject.setClassType(ChaptersResponse.class);
        executeTask(AppConstants.TASKCODES.GET_SUBJECT_CHAPTERS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_SUBJECT_CHAPTERS:
                ChaptersResponse acr = (ChaptersResponse) response;
                if (acr != null && acr.getStatus() && acr.getData().size() > 0) {
                    chapterList.clear();
                    chapterList.addAll(acr.getData());
                    chapterAdapter.notifyDataSetChanged();
                } else {
                    findViewById(R.id.lv_chapters).setVisibility(View.GONE);
                    findViewById(R.id.tv_empty).setVisibility(View.VISIBLE);
                }
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        if (bundle != null) {
            subjectData = (SubjectData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.SUBJECT);
            assignmentModel = (ClassModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.CLASS);
            getChaptersData();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        if (null != chapterList && chapterList.size() > 0) {
            if (!TextUtils.isEmpty(chapterList.get(position).getChapterName()))
                holder.tvSubject.setText(chapterList.get(position).getChapterName());
            if (chapterList.get(position).getAssignmentCount() != null)
                holder.tvChapter.setText(chapterList.get(position).getAssignmentCount() + " Assignments");
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.CLASS, assignmentModel);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.SUBJECT, subjectData);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.CHAPTERS, chapterList.get(position));
        startNextActivity(bundle, AssignmentQuestionActivity.class);
    }

    private class Holder {
        TextView tvSubject, tvChapter;

        public Holder(View v) {
            tvSubject = (TextView) v.findViewById(R.id.tv_subject);
            tvChapter = (TextView) v.findViewById(R.id.tv_chapters);
        }
    }
}
