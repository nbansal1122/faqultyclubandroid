package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.comment.CommentData;
import faqulty.club.models.comment.GetCommentApi;
import faqulty.club.models.comment.PostCommentApi;
import faqulty.club.models.comment.PostCommentData;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class CommentActivity extends BaseActivity implements CustomListAdapterInterface {
    private CustomListAdapter commentAdapter;
    private ListView lvComment;
    private List<CommentData> commentDataList = new ArrayList<>();
    private List<PostCommentData> postCommentDataList = new ArrayList<>();
    private EditText etComment;
    private
    int entityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        initToolBar("Comments");
        findViews();
        entityId = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.ENTITY_ID);
        commentAdapter = new CustomListAdapter(this, R.layout.row_comment, commentDataList, this);
        lvComment.setAdapter(commentAdapter);
        setOnClickListener(R.id.rl_send);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getComments();
    }

    private void findViews() {
        lvComment = (ListView) findViewById(R.id.list_comment);
        etComment = (EditText) findViewById(R.id.et_comment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_send:
                postComment();
        }
    }

    private void postComment() {
        if (!TextUtils.isEmpty(etComment.getText().toString())) {
            HttpParamObject post = new HttpParamObject();
            post.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.POST_COMMENT));
            post.setPostMethod();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("comment", etComment.getText().toString());
                jsonObject.put("entityId", entityId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            post.setJson(jsonObject.toString());
            post.setJSONContentType();
            post.setClassType(PostCommentApi.class);
            executeTask(AppConstants.TASKCODES.POST_COMMENT, post);
        } else {
            showToast(getString(R.string.wriye_something));
        }
    }

    private void getComments() {
        showProgressDialog();
        HttpParamObject httpParamObject = new HttpParamObject();
//        http://ec2-54-201-39-154.us-west-2.compute.amazonaws.com:4000/api/tutors/529/feed/30/comments
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.GET_COMMENT, entityId));
        httpParamObject.setClassType(GetCommentApi.class);
        executeTask(AppConstants.TASKCODES.GET_COMMENTS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_response));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_COMMENTS:
                GetCommentApi getCommentApi = (GetCommentApi) response;
                if (getCommentApi != null) {
                    Boolean status = getCommentApi.getStatus();
                    if (status) {
                        if (getCommentApi.getData() != null) {
                            commentDataList.clear();
                            commentDataList.addAll(getCommentApi.getData());
                            initToolBar("Comments (" + commentDataList.size() + ")");
                            commentAdapter.notifyDataSetChanged();
                            etComment.setText("");
                            Util.hideKeyboard(this, etComment);
                        } else {
                            showToast(getString(R.string.no_data));
                        }
                    }
                }
                break;
            case AppConstants.TASKCODES.POST_COMMENT:
                PostCommentApi postCommentApi = (PostCommentApi) response;
                if (postCommentApi != null && postCommentApi.getStatus()) {
                    showToast(postCommentApi.getMsg());
                    getComments();
                    setResult(RESULT_OK);
                }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        CommentData commentData = commentDataList.get(position);
        holder.tvComment.setText(commentData.getComment());
        holder.tvTime.setText(Util.getRelativeTimeString(commentData.getCreatedOn(), 161000));
        if (commentData.getUser() != null) {
            holder.tvName.setText(commentData.getUser().getName());
            if (!TextUtils.isEmpty(commentData.getUser().getImage())) {
                Picasso.with(this).load(Util.getCompleteUrl(commentData.getUser().getImage())).into(holder.userPicIv);
            } else {
                holder.userPicIv.setImageResource(R.mipmap.accountselected2x);
            }
        } else {
            holder.userPicIv.setImageResource(R.mipmap.accountselected2x);
        }
        return convertView;
    }

    private class Holder {
        TextView tvName, tvComment, tvTime;
        ImageView userPicIv;

        public Holder(View v) {
            tvName = (TextView) v.findViewById(R.id.tv_name);
            tvComment = (TextView) v.findViewById(R.id.tv_comment);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
            userPicIv = (ImageView) v.findViewById(R.id.iv_gallery_clip);
        }
    }

}
