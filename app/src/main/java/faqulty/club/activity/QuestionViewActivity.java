package faqulty.club.activity;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.Switch;

import faqulty.club.R;
import faqulty.club.Util.ApiGenerator;
import faqulty.club.Util.FileUtil;
import faqulty.club.fragments.bottomsheets.AssignmentShareBottomSheet;
import faqulty.club.fragments.bottomsheets.QuestionShareFragment;
import faqulty.club.models.BasicResponse;
import faqulty.club.models.assignment.AnswerData;
import faqulty.club.models.assignment.QuestionData;
import faqulty.club.models.assignment.QuestionInfoResponse;
import faqulty.club.models.assignment.sharing.PDFLinkResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class QuestionViewActivity extends BaseActivity {
    int assignmentId;
    private List<QuestionData> list = new ArrayList<>();
    private String title, subtitle;
    private boolean showAnswers;
    private int actType;
    private WebView mathView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_view);
        Bundle bundle = getIntent().getExtras();
        title = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_TITLE);
        subtitle = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_SUBTITLE);
        initToolBar(title);
        getSupportActionBar().setSubtitle(subtitle);
        assignmentId = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.Q_ID);
        Preferences.saveData(AppConstants.PREF_KEYS.IS_REFRESH_ASSIGNMENT_VIEWED, true);
        mathView = (WebView) findViewById(R.id.math_view);
        Switch switchCompat = (Switch) findViewById(R.id.switch_answer);
        if (showAnswers) {
            switchCompat.setChecked(true);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showAnswers = isChecked;
                refreshData();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.ic_share) {
            showQuestionSharing();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        title = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_TITLE);
        subtitle = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_SUBTITLE);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchQuestionsList();
    }


    private void fetchQuestionsList() {
        HttpParamObject obj = new HttpParamObject();
        obj.setClassType(QuestionInfoResponse.class);
        obj.setUrl(AppConstants.PAGE_URL.ASSIGNMENTS + "/" + assignmentId);
        obj.addParameter("view", "true");
        obj.addParameter("tutor_id", Preferences.getUserId() + "");
        executeTask(AppConstants.TASKCODES.FETCH_ASSIGNMENT, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.FETCH_ASSIGNMENT:
                QuestionInfoResponse response1 = (QuestionInfoResponse) response;
                if (response1 != null && response1.getData() != null && response1.getData().getQuestionSet() != null) {
                    list.clear();
                    list.addAll(response1.getData().getQuestionSet());
                    refreshData();
                }
                break;
            case AppConstants.TASKCODES.GET_ASSIGNMENT_LINK: {

                PDFLinkResponse pdfLinkResponse = (PDFLinkResponse) response;
                if (pdfLinkResponse != null && pdfLinkResponse.getData() != null) {
                    if (!TextUtils.isEmpty(pdfLinkResponse.getData().getUri()))
                        takeActionOnUrl(pdfLinkResponse.getData().getUri());
                }
            }
            break;
            case AppConstants.TASKCODES.SHARE_EMAIL:
                BasicResponse basicResponse = (BasicResponse) response;
                if (basicResponse != null && basicResponse.getStatus()) {
                    showToast(basicResponse.getMsg());
                }
                break;
        }
    }

    private void refreshData() {
        mathView.removeAllViewsInLayout();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("<style>" +
                        "table, th, td {" +
                        "    border-collapse: collapse;" +
                        "}" +
                        "th, td {" +
                        "    padding: 5px;" +
                        "    text-align:left;" +
                        "vertical-align:top;" +
                        "}" +
                        ".Line{" +
                        "border: 0.2px solid #ababab;" +
                        " }" +
                        "</style><body>" +
                        "<table style=\"width:100%\">");
        int i = 1;
        for (QuestionData questionData : list) {
            stringBuilder
                    .append("<tr><td>")
                    .append("Q" + i + ": ")
                    .append("</td>")
                    .append("<td>")
                    .append(questionData.getQuestion())
                    .append("</td></tr>");
            if (showAnswers) {
                AnswerData answer = questionData.getAnswer();
                if (answer != null) {
                    stringBuilder
                            .append("<tr><td>")
                            .append("A" + i + ": ")
                            .append("</td>")
                            .append("<td>")
                            .append(answer.getAnswer())
                            .append("</td></tr>");
                }
            }
            i++;
        }

        stringBuilder.append("</table></body>");
//        String data = stringBuilder.toString().replaceAll(AppConstants.PAGE_URL.BASE_HTTPS_URL, AppConstants.PAGE_URL.PHOTO_URL);
        mathView.getSettings().setJavaScriptEnabled(true);
        mathView.loadDataWithBaseURL("", "<script type='text/javascript' "
                        + "src='http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'>"
                        + "</script>" + stringBuilder.toString(),
                "text/html", "utf-8", "");
    }

    private void delayProgressBar() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgressBar();
            }
        }, 30000);
    }


    private void showQuestionSharing() {
        QuestionShareFragment saveShareFragment = QuestionShareFragment.newInstance(new QuestionShareFragment.QuestionShareInterface() {
            @Override
            public void onActionClick(int actType) {
                showAssignmentSharingSheet(actType);
            }
        });
        saveShareFragment.show(getSupportFragmentManager(), saveShareFragment.getTag());
    }

    private void showAssignmentSharingSheet(final int questionOrAnswerKey) {
        AssignmentShareBottomSheet saveShareFragment = AssignmentShareBottomSheet.newInstance(new AssignmentShareBottomSheet.BottomSheetInterface() {
            @Override
            public void onActionClick(int actType) {
                QuestionViewActivity.this.actType = actType;
                onAssignmentBottomSheetClicked(actType, questionOrAnswerKey);
            }
        });
        saveShareFragment.show(getSupportFragmentManager(), saveShareFragment.getTag());
    }

    private void onAssignmentBottomSheetClicked(int actType, final int questionOrAnswerKey) {
        switch (actType) {
            case AppConstants.ACTION_TYPE.BOOKMARK:
                Bundle b = new Bundle();
                b.putInt(AppConstants.BUNDLE_KEYS.MODULE_ID, assignmentId);
                b.putString(AppConstants.BUNDLE_KEYS.MODULE_TYPE, "assignment");
                startNextActivity(b, SaveBookMarkActivity.class);
                break;
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED:
            case AppConstants.ACTION_TYPE.SHARE_FEED:
                if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_QUESTION_SET) {
                    downloadAssignmentToShare("questions");
                } else if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_ANSWER_KEY) {
                    downloadAssignmentToShare("fullset");
                }
                break;
            case AppConstants.ACTION_TYPE.EMAIL:
                if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_QUESTION_SET) {
                    HttpParamObject httpParamObject = ApiGenerator.sareToEmail(assignmentId, "assignment", "questions");
                    executeTask(AppConstants.TASKCODES.SHARE_EMAIL, httpParamObject);
                } else if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_ANSWER_KEY) {
                    HttpParamObject httpParamObject = ApiGenerator.sareToEmail(assignmentId, "assignment", "fullset");
                    executeTask(AppConstants.TASKCODES.SHARE_EMAIL, httpParamObject);
                }
                break;
        }
    }

    private void downloadAssignmentToShare(String type) {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(String.format(AppConstants.PAGE_URL.DOWNLOAD_ASSIGNMENT_LINK, String.valueOf(assignmentId)));
        obj.addParameter("type", type);
        obj.setClassType(PDFLinkResponse.class);
        executeTask(AppConstants.TASKCODES.GET_ASSIGNMENT_LINK, obj);
    }

    private void takeActionOnUrl(String url) {
        url = new StringBuilder().append(url).append("&user_id=").append(Preferences.getUserId()).append("&"+AppConstants.X_ACCESS_TOKEN+"=").append(Preferences.getData(AppConstants.PREF_KEYS.USER_TOKEN, "")).toString();
        switch (actType) {
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED: {
                DownloadManager manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                FileUtil.downloadFile(manager, url);
            }
            break;
            case AppConstants.ACTION_TYPE.SHARE_FEED: {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(i, "Choose an app to share"));
            }
            break;
//            case AppConstants.ACTION_TYPE.EMAIL: {
//                if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_QUESTION_SET) {
//                    downloadAssignmentToShare("questions");
//                } else if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_ANSWER_KEY) {
//                    downloadAssignmentToShare("fullset");
//                }
//
////                UserDetailsApi api = UserDetailsApi.getInstance();
////                Util.shareViaEmail(this, api.getEmail(), "The Faculty Club", url);
//            }
//            break;
        }
    }


}
