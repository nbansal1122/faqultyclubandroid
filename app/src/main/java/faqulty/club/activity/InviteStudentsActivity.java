package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.AppUtil;
import faqulty.club.Util.SelectorDialog;
import faqulty.club.models.BaseApi;
import faqulty.club.models.creategroup.Contacts;
import faqulty.club.models.creategroup.PhoneContact;
import faqulty.club.models.creategroup.PupilInfo;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.tutorstudent.StudentProfileResponse;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class InviteStudentsActivity extends BaseActivity {
    private static final int RESULT_PICK_CONTACT = 1;
    private ArrayList<PhoneContact> contacts = new ArrayList<>();
    private ChipView chipView;
    private LinearLayout linearLayout;
    private TextView tv;
    private View v;
    private String type;
    private int studentId;
    private boolean isGroupStudent;
    private Contacts selectedContacts = new Contacts();

    private boolean isEdit;
    private StudentBaseClass invitedStudentData;
    private static final int PICK_CONTACT = 2;
    private List<PhoneContact> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_student);
        initToolBar(getString(R.string.title_invite_students));
        linearLayout = (LinearLayout) findViewById(R.id.ll);
        v = findViewById(R.id.included);
        tv = (TextView) findViewById(R.id.tv_duration_heading);

        chipView = (ChipView) findViewById(R.id.student_chip_view);
        type = "1:1";
        if (!isEdit) {
            setText("1:1", R.id.spin_tuition_type_invite);
            setOnClickListener(R.id.btn_invite_student, R.id.iv_add_students, R.id.tv_hint_phone, R.id.spin_billing_cycle_invite,
                    R.id.spin_tuition_type_invite, R.id.iv_min, R.id.et_min);
        } else {
            initToolBar(getString(R.string.title_edit_invite));
            fillData(invitedStudentData);

            setOnClickListener(R.id.btn_invite_student,
                    R.id.et_min);

            if (isGroupStudent) {
                type = "group";
                setVisibilityGone();
            } else {
                type = "1:1";
                getStudentInfo();
            }
            setText(type, R.id.spin_tuition_type_invite);
            if (invitedStudentData != null) {
                Button button = (Button) findViewById(R.id.btn_invite_student);
                if (invitedStudentData.isInvitePending()) {
                    button.setText(getString(R.string.resend_invite));
                } else {
                    button.setText(getString(R.string.save));
                }
            }
        }


    }

    private void showMinutesDialog() {
        List<String> minutesList = SelectorDialog.getMinutes();
        SelectorDialog.getInstance(this, getString(R.string.title_select_minutes), minutesList, new SelectorDialog.ItemSelector() {
            @Override
            public void onItemSelected(String item) {
                setText(item, R.id.et_min);
            }
        }, getTextFromTV(R.id.et_min)).showDialog();
    }

    private void showTuitionTypeDialog() {
//        SelectorDialog.getInstance(this, getString(R.string.title_select_tuition_type), SelectorDialog.getTuitionTypes(), new SelectorDialog.ItemSelector() {
//            @Override
//            public void onItemSelected(String item) {
//                setText(item, R.id.spin_tuition_type_invite);
//                type = item;
//                setLayout(type);
//            }
//        }, getTvText(R.id.spin_tuition_type_invite)).showDialog();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        studentId = bundle.getInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID);
        isEdit = true;
        studentId = bundle.getInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID);
        isGroupStudent = bundle.getBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT);
        invitedStudentData = (StudentBaseClass) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
    }

    private void getStudentInfo() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GET_STUDENT_DATA, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
        httpParamObject.setClassType(StudentProfileResponse.class);
        executeTask(AppConstants.TASKCODES.GET_STUDENT_DATA, httpParamObject);
    }

    private void setLayout(String type) {
        if (type.toLowerCase().compareTo("1:1") == 0)
            setVisibilityVisible();
        else if (type.toLowerCase().compareTo("group") == 0)
            setVisibilityGone();
    }

    private void setVisibilityGone() {
        linearLayout.setVisibility(View.GONE);
        v.setVisibility(View.GONE);
        tv.setVisibility(View.GONE);
    }

    private void setVisibilityVisible() {
        linearLayout.setVisibility(View.VISIBLE);
        v.setVisibility(View.VISIBLE);
        tv.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_invite_student:
                if (isValid()) {
                    if (isGroupType()) {
                        inviteStudent(2);
                    } else {
                        inviteStudent(1);
                    }
                }
                break;
            case R.id.iv_add_students:
            case R.id.tv_hint_phone:
                selectContact();
                break;
            case R.id.spin_billing_cycle_invite:
//                openBillingDialog();
                break;
            case R.id.spin_tuition_type_invite:
                showTuitionTypeDialog();
                break;
            case R.id.iv_min:
            case R.id.et_min:
                showMinutesDialog();
                break;
        }
    }

    private void selectContact() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, selectedContacts.getPhoneContact());
        Intent intent = new Intent(InviteStudentsActivity.this, AddFromContact.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, PICK_CONTACT);
    }

    private boolean isGroupType() {
        return (type.toLowerCase().compareTo("group") == 0);
    }


    public void pickContact() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case RESULT_PICK_CONTACT:
                contactPicked(data);
                break;
            case PICK_CONTACT:
                selectedContacts = (Contacts) data.getExtras().getSerializable(AppConstants.BUNDLE_KEYS.SELECTEDCONTACTS);
                contacts.clear();
                contacts = selectedContacts.getPhoneContact();
                if (contacts.size() == 0)
                    showToast(getString(R.string.no_contact_selected));
                setStudentsChipView();
                break;

        }
    }


    private void contactPicked(Intent data) {
        PhoneContact c = AppUtil.getContactDataFromIntent(data, this);
        if (c != null) {
            contacts.add(c);
            // Set the value to the textviews
            setStudentsChipView();
        } else {
            showToast(R.string.error_contact_information_not_found);
        }
    }

    private void setStudentsChipView() {
        List<Chip> chips = new ArrayList<>();
        for (PhoneContact phone : contacts) {
            chips.add(phone);
        }
        chipView.setChipLayoutRes(R.layout.row_chip);
        chipView.setChipList(chips);
        if (!isEdit) {
            chipView.setOnChipClickListener(studentsChipViewListener);
        } else {
            hideVisibility(R.id.iv_add_students);
        }
    }

    private OnChipClickListener studentsChipViewListener = new OnChipClickListener() {
        @Override
        public void onChipClick(Chip chip) {

            PhoneContact clickedAPi = null;
            for (PhoneContact phone : contacts) {
                if (phone.getText().equalsIgnoreCase(chip.getText())) {
                    clickedAPi = phone;
                }
            }
            if (clickedAPi != null) {
                contacts.remove(clickedAPi);
                setStudentsChipView();
            }
        }
    };

    private void openBillingDialog() {
        SelectorDialog.getInstance(this, getString(R.string.select_billing_cycle), SelectorDialog.getBillingCycles(), new SelectorDialog.ItemSelector() {
            @Override
            public void onItemSelected(String item) {
                setText(item, R.id.spin_billing_cycle_invite);
            }
        }, getTvText(R.id.spin_billing_cycle_invite)).showDialog();
    }


    private void inviteStudent(int i) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.INVITESTUDENTS);
        if (isEdit) {
            httpParamObject.setPutMethod();
            httpParamObject.setUrl(Util.getStudentUrl(AppConstants.PAGE_URL.GET_STUDENT_DATA, invitedStudentData.getId()));
        } else {
            httpParamObject.setPostMethod();
        }
        httpParamObject.setClassType(BaseApi.class);

        httpParamObject.setJSONContentType();
        switch (i) {
            case 1:
                httpParamObject.setJson(getInviteData().toString());
                break;
            case 2:
                httpParamObject.setUrl(AppConstants.PAGE_URL.INVITESTUDENTS);
                httpParamObject.setPostMethod();
                httpParamObject.setJson(getGroupInviteData().toString());
                break;
        }
        executeTask(AppConstants.TASKCODES.INVITESTUDENTS, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.INVITESTUDENTS:
                BaseApi basicResponse = (BaseApi) response;
                if (null != basicResponse) {
                    showToast(basicResponse.getMsg());
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case AppConstants.TASKCODES.GET_STUDENT_DATA:
                StudentProfileResponse res = (StudentProfileResponse) response;
                if (res != null && res.getStudent() != null) {
                    fillInviteInfoForIndividual(res);
                }
                break;
        }
    }

    private void fillData(StudentBaseClass studentInfo) {
        PhoneContact contact = new PhoneContact();
        contact.setName(studentInfo.getName());
        contact.setNumber(studentInfo.getPhone());
        contact.setEmail(studentInfo.getEmail());
        contacts.add(contact);
        setStudentsChipView();
    }

    private void fillInviteInfoForIndividual(StudentProfileResponse res) {
        PupilInfo info = res.getPupilInfo();
        if (info != null) {
            setText(info.getBillingCycle(), R.id.spin_billing_cycle_invite);
            if (info.getClassDurationMins() != null)
                setText(info.getClassDurationMins() + "", R.id.et_min);
            if (info.getFee() != null)
                setEditText(R.id.et_fee_invite, info.getFee() + "");
            if (info.getClassDurationHours() != null)
                setEditText(R.id.et_hours, info.getClassDurationHours() + "");
        }
    }

    private JSONObject getInviteData() {
        JSONObject jsonObject = new JSONObject();
        JSONObject obj = new JSONObject();
        try {
            JSONArray arr = getStudentsJsonArray();
            if (!isEdit) {
                jsonObject.put("tutorId", Preferences.getUserId());
                jsonObject.put("students", arr);
            }
            jsonObject.put("tuitionType", "individual");
            obj.put("billingCycle", getTvText(R.id.spin_billing_cycle_invite));
            obj.put("fee", parsetoInt(getEditText(R.id.et_fee_invite)));
            int anInt = parsetoInt(getEditText(R.id.et_hours));
            obj.put("classDurationHours", anInt);
            obj.put("classDurationMins", parsetoInt(getTextFromTV(R.id.et_min)));
            jsonObject.put("pupilInfo", obj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private JSONObject getGroupInviteData() {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray arr = getStudentsJsonArray();
            jsonObject.put("tutorId", Preferences.getUserId());
            jsonObject.put("students", arr);
            jsonObject.put("tuitionType", "group");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean isValid() {
        String hours = getEditText(R.id.et_hours);
        String min = getTextFromTV(R.id.et_min);
        String fee = getEditText(R.id.et_fee_invite);
        String bill = getTvText(R.id.spin_billing_cycle_invite);

//        if (isEdit) {
//            String email = getTextFromTil(R.id.til_email);
//            if (!TextUtils.isEmpty(email)) {
//                if (!Util.isValidEmail(email)) {
//                    setTilError(R.id.til_email, R.string.invalid_email);
//                    return false;
//                }
//            }
//        }

        if (contacts.size() == 0) {
            showToast(getString(R.string.error_select_contact));
            return false;
        }


        if (type == null) {
            showToast(R.string.error_select_tuition_type);
            return false;
        }
        if (true) {
            return true;
        }


        if (!isGroupType()) {
            return true;
        }

        if (TextUtils.isEmpty(hours)) {
            if (TextUtils.isEmpty(min)) {
                showToast(R.string.error_session_duration);
                return false;
            }
        }
        if (TextUtils.isEmpty(fee)) {
            showToast(R.string.error_empty_fee);
            return false;
        }
        if (TextUtils.isEmpty(bill)) {
            showToast(R.string.error_empty_billing_cycle);
            return false;
        }
        return true;
    }

    private JSONArray getStudentsJsonArray() throws JSONException {
        JSONArray array = new JSONArray();
        for (PhoneContact c : contacts) {
            JSONObject obj = new JSONObject();
            obj.put("phone", c.getNumber());
            if (!TextUtils.isEmpty(c.getName())) {
                obj.put("name", c.getName());
            }
            array.put(obj);
        }
        return array;
    }

    private int parsetoInt(String str) {
        return AppUtil.parseStringToInt(str);
    }

    private String getTvText(int id) {
        TextView tv = (TextView) findViewById(id);
        if (tv != null)
            return tv.getText().toString();
        return "";
    }

}
