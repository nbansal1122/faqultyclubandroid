package faqulty.club.activity;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.BaseApi;
import faqulty.club.models.creategroup.PhoneContact;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class GetReviewActivity extends BaseActivity implements PermissionListener, LoaderManager.LoaderCallbacks<Cursor>, CustomListAdapterInterface, SearchView.OnQueryTextListener {

    private List<PhoneContact> allContacts = new ArrayList<>();
    private List<PhoneContact> selectedContacts = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private SearchView svContacts;
    private List<PhoneContact> copyAllContacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_review);
        initToolBar(getString(R.string.get_reviews));
        new TedPermission(this)
                .setPermissions(Manifest.permission.READ_CONTACTS)
                .setPermissionListener(this).check();
        svContacts = (SearchView) findViewById(R.id.sv_search);
        svContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                svContacts.setIconified(false);
            }
        });
        svContacts.setOnQueryTextListener(this);
        svContacts.setQueryHint(getString(R.string.search_here));

        ListView listView = (ListView) findViewById(R.id.list_reviews);
        customListAdapter = new CustomListAdapter(this, R.layout.row_review, allContacts, this);
        listView.setAdapter(customListAdapter);
        setOnClickListener(R.id.btn_save_changes);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_changes:
                if(selectedContacts.size()==0){
                    showToast(R.string.erro_nothing_selected);
                    return;
                }
                sendReview();
                break;
        }
    }

    @Override
    public void onPermissionGranted() {
        getSupportLoaderManager().initLoader(1, null, this);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> arrayList) {

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(this, CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        cursor.moveToFirst();
        allContacts.clear();
        while (!cursor.isAfterLast()) {
            final String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            final String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            final String imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
            PhoneContact c = new PhoneContact(name, number, imageUri);
            allContacts.add(c);
            cursor.moveToNext();
        }
        Collections.sort(allContacts, new Comparator<PhoneContact>() {
            public int compare(PhoneContact s1, PhoneContact s2) {
                if (s1.getName() != null && s2.getName() != null) {
                    return s1.getName().compareTo(s2.getName());
                } else {
                    return s1.getNumber().compareTo(s2.getNumber());
                }
            }
        });
        copyAllContacts.addAll(allContacts);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void sendReview() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SEND_REVIEW);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tutorId", Preferences.getUserId());
            JSONArray jsonArray = new JSONArray();
            for (PhoneContact numberContact : selectedContacts) {
                try {
                    String number = Util.getModifiedNumber(numberContact.getNumber());
                    jsonArray.put(Double.parseDouble(number));
                } catch (Exception e) {
                }
            }
            jsonObject.put("contacts", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(BaseApi.class);
        executeTask(AppConstants.TASKCODES.SEND_REVIEW, httpParamObject);
        allContacts.removeAll(selectedContacts);
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.SEND_REVIEW:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi != null) {
                    String msg = baseApi.getMsg();
                    if (!TextUtils.isEmpty(msg)) {
                        showToast(msg);
                        setResult(RESULT_OK);
                        finish();
                    }
                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final PhoneContact phoneContact = allContacts.get(position);
        if (phoneContact != null) {
            if (!TextUtils.isEmpty(phoneContact.getName()))
                holder.tvName.setText(phoneContact.getName());
            else
                holder.tvName.setText("Unnamed contact");

            if (!TextUtils.isEmpty(phoneContact.getNumber()))
                holder.tvNumber.setText(phoneContact.getNumber());

            if (!TextUtils.isEmpty(phoneContact.getPhoto())) {
                Picasso.with(this).load(phoneContact.getPhoto()).placeholder(R.mipmap.accountselected).into(holder.ivContact);
            } else {
                holder.ivContact.setImageResource(R.mipmap.accountselected);
            }
        }
        holder.cbSelected.setChecked(isSelected(phoneContact));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact(phoneContact);
            }
        });
        return convertView;
    }

    private boolean isSelected(PhoneContact phoneContact) {
        return selectedContacts.contains(phoneContact);
    }

    private void selectContact(PhoneContact phoneContact) {
        if (isSelected(phoneContact)) {
            selectedContacts.remove(phoneContact);
        } else {
            selectedContacts.add(phoneContact);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 0 && copyAllContacts.size() > 0) {
            filterList(newText.toString());
        } else {
            allContacts.clear();
            allContacts.addAll(copyAllContacts);
            customListAdapter.notifyDataSetChanged();
        }
        return false;
    }

    private void filterList(String s) {
        allContacts.clear();
        for (PhoneContact c : copyAllContacts) {
            if (c.getName().toLowerCase().contains(s.toLowerCase()) || c.getNumber().contains(s)) {
                allContacts.add(c);
            }
        }
        customListAdapter.notifyDataSetChanged();
    }


    class Holder {
        TextView tvName, tvNumber;
        CircleImageView ivContact;
        CheckBox cbSelected;
        FrameLayout layOver;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name_invitation);
            tvNumber = (TextView) view.findViewById(R.id.tv_number_invitation);
            ivContact = (CircleImageView) view.findViewById(R.id.iv_invitation_pic);
            cbSelected = (CheckBox) view.findViewById(R.id.cb_send_invitation);
            layOver = (FrameLayout) view.findViewById(R.id.lay_over);
        }
    }

}
