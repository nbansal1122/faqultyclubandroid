package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import faqulty.club.HomeProfileFragment;
import faqulty.club.R;
import faqulty.club.enums.NotificationType;
import faqulty.club.fragments.ChatDemoListFragment;
import faqulty.club.fragments.ContentFragment;
import faqulty.club.fragments.StudentsFragment;
import faqulty.club.models.push_notification.NotificationData;
import faqulty.club.service.FacultiClubeServices;
import faqulty.club.service.SocketService;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class HomeActivity extends BaseActivity {

    private int prevSelected = -1;
    private ContentFragment contentFragment;
    private StudentsFragment studentsFragment;
    private HomeProfileFragment homeProfileFragment;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setOnClickListener(R.id.ll_chat, R.id.ll_feed, R.id.ll_students, R.id.ll_profile);
        initFragment();
        unSelectAllFooterIcons();
        Intent intent = getIntent();
        if (intent != null) {
            int intExtra = intent.getIntExtra(AppConstants.BUNDLE_KEYS.TYPE, 0);
            if (intExtra == 1) {
                addFragment(homeProfileFragment);
                setFooterIcon(R.id.ll_profile, R.mipmap.accountselected, true);
            } else {
//                Bundle bundle = intent.getExtras();
//                if (bundle != null) {
//                    loadNotification(bundle);
//                }else {
//                    addFragment(contentFragment);
//                    setFooterIcon(R.id.ll_feed, R.mipmap.feedselected, true);
//                }
                addFragment(contentFragment);
                setFooterIcon(R.id.ll_feed, R.mipmap.feedselected, true);
            }
        }

        FacultiClubeServices.getReviews(this);
    }


    protected void loadNotification(Bundle bundle) {
        int i = bundle.getInt(AppConstants.BUNDLE_KEYS.TYPE);
        NotificationData notificationData = (NotificationData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        switch (NotificationType.findType(notificationData.getType())) {
            case REVIEW:
                addFragment(homeProfileFragment);
                unSelectAllFooterIcons();
                setFooterIcon(R.id.ll_profile, R.mipmap.accountselected, true);
                break;
            case LIKE_PROFILE:
                break;
            case BIRTHDAY:
                setFooterIcon(R.id.ll_students, R.mipmap.studentsselected, true);
                studentsFragment = new StudentsFragment();
                addFragment(studentsFragment);
                break;
            case STUDENT_REGISTERED:
                break;
            case FEED:
                addFragment(new ContentFragment());
                setFooterIcon(R.id.ll_feed, R.mipmap.feedselected, true);
                break;
            default:
                addFragment(contentFragment);
                unSelectAllFooterIcons();
                setFooterIcon(R.id.ll_feed, R.mipmap.feedselected, true);
                break;
        }
    }


    private void initFragment() {
        homeProfileFragment = new HomeProfileFragment();
        studentsFragment = new StudentsFragment();
        contentFragment = new ContentFragment();
    }

    protected int getHomeIcon() {
        return 0;
    }

    private void addFragment(Fragment f) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, f).commitAllowingStateLoss();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == prevSelected) {
            return;
        }
        unSelectAllFooterIcons();
        switch (v.getId()) {
            case R.id.ll_profile:
                addFragment(new HomeProfileFragment());
                setFooterIcon(R.id.ll_profile, R.mipmap.accountselected, true);
                break;
            case R.id.ll_feed:
                addFragment(new ContentFragment());
                setFooterIcon(R.id.ll_feed, R.mipmap.feedselected, true);
                break;
            case R.id.ll_chat:
                setFooterIcon(R.id.ll_chat, R.mipmap.chatselected, true);
                addFragment(new ChatDemoListFragment());
                break;
            case R.id.ll_students:
                setFooterIcon(R.id.ll_students, R.mipmap.studentsselected, true);
                studentsFragment = new StudentsFragment();
                addFragment(studentsFragment);
                break;

        }

    }


    private void unSelectAllFooterIcons() {
        setFooterIcon(R.id.ll_profile, R.mipmap.account, false);
        setFooterIcon(R.id.ll_feed, R.mipmap.feed, false);
        setFooterIcon(R.id.ll_chat, R.mipmap.chat, false);
        setFooterIcon(R.id.ll_students, R.mipmap.students, false);
    }

    private void setFooterIcon(int linearLayoutId, int iconId, boolean isSelected) {
        LinearLayout layout = (LinearLayout) findViewById(linearLayoutId);
        ImageView iv = (ImageView) layout.getChildAt(0);
        TextView tv = (TextView) layout.getChildAt(1);
        iv.setImageResource(iconId);
        if (isSelected) {
            tv.setTextColor(ContextCompat.getColor(this, R.color.black));
        } else {
            tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
        if (isSelected) {
            prevSelected = linearLayoutId;
        }

    }

    private void setProfileFooter(boolean isSelected) {

    }

    private void setFeedFooter(boolean isSelected) {

    }

    private void toggleFooter(int id) {
        if (prevSelected != -1) {

        } else if (prevSelected != id) {

        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SocketService.startService(this);
    }

    @Override
    protected void onHomePressed() {
    }
}
