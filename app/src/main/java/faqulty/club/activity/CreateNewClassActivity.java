package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.AppUtil;
import faqulty.club.Util.SelectorDialog;
import faqulty.club.models.creategroup.Contacts;
import faqulty.club.models.creategroup.CreateGroupResponse;
import faqulty.club.models.creategroup.PhoneContact;
import faqulty.club.models.creategroup.PupilInfo;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class CreateNewClassActivity extends BaseActivity {
    private static final int RESULT_PICK_CONTACT = 1;
    private static final int PICK_CONTACT = 2;
    private TextView tv_billing_cycle;
    private Button btn_create_class;
    private ArrayList<PhoneContact> contacts = new ArrayList<>();
    private ChipView chipView;
    private ListPopupWindow timeLpw;
    private List<Integer> timeList = new ArrayList<>();
    private CustomListAdapter timeAdapter;
    private TextView etMin;
    private boolean isEdit;
    private int groupId = -1;
    private Contacts selectedContacts = new Contacts();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_class);
        initToolBar(getString(R.string.title_create_new_group));
        etMin = (TextView) findViewById(R.id.et_min);
        tv_billing_cycle = (TextView) findViewById(R.id.spin_billing_cycle_create);
        btn_create_class = (Button) findViewById(R.id.btn_create_class);
        chipView = (ChipView) findViewById(R.id.students_chip_view);
        setOnClickListener(R.id.iv_min, R.id.tv_hint_phone, R.id.et_min, R.id.spin_billing_cycle_create, R.id.lbl_add_students, R.id.iv_add_students, R.id.btn_create_class);

        if (isEdit) {
            initToolBar(getString(R.string.title_edit_group));
            Button btn = (Button) findViewById(R.id.btn_create_class);
            btn.setText(R.string.update_group);
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        groupId = bundle.getInt(AppConstants.BUNDLE_KEYS.GROUP_ID);
        isEdit = true;
        fetchGroupData();
    }

    private void fillGroupData(CreateGroupResponse groupResponse) {
        contacts.clear();
        contacts.addAll(groupResponse.getCreateGroupData().getStudents());
        setStudentsChipView();

        PupilInfo info = groupResponse.getCreateGroupData().getPupilInfo();
        if (info != null) {
            setText(info.getBillingCycle(), R.id.spin_billing_cycle_create);
            if (info.getClassDurationMins() != null)
                setText(info.getClassDurationMins() + "", R.id.et_min);
            if (info.getFee() != null)
                setEditText(R.id.et_fee_invite, info.getFee() + "");
            if (info.getClassDurationHours() != null)
                setEditText(R.id.et_hours, info.getClassDurationHours() + "");
        }
        setEditText(R.id.et_class_name, groupResponse.getCreateGroupData().getName());
    }

    private void fetchGroupData() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GROUP_URL + groupId);
        obj.setClassType(CreateGroupResponse.class);
        executeTask(AppConstants.TASKCODES.FETCH_GROUP, obj);
    }


    private void showMinutesDialog() {
        List<String> minutesList = SelectorDialog.getMinutes();
        SelectorDialog.getInstance(this, getString(R.string.title_select_minutes), minutesList, new SelectorDialog.ItemSelector() {
            @Override
            public void onItemSelected(String item) {
                setText(item, R.id.et_min);
            }
        }, getTextFromTV(R.id.et_min)).showDialog();

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.spin_billing_cycle_create:
//                openBillingDialog();
                break;
            case R.id.lbl_add_students:
            case R.id.iv_add_students:
            case R.id.tv_hint_phone:
//                pickContact();
                selectContact(contacts);
                break;
            case R.id.btn_create_class:
                createGroup();
                break;
            case R.id.iv_min:
            case R.id.et_min:
                showMinutesDialog();
                break;
        }
    }


    public void pickContact() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }

    private void selectContact(ArrayList<PhoneContact> contacts) {
        Intent intent = new Intent(CreateNewClassActivity.this, AddFromContact.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, contacts);
        intent.putExtras(bundle);
        startActivityForResult(intent, PICK_CONTACT);
    }


    /**
     * Query the Uri and read contact details. Handle the picked contact data.
     *
     * @param data
     */
    private void contactPicked(Intent data) {
        PhoneContact c = AppUtil.getContactDataFromIntent(data, this);
        if (c != null) {
            contacts.add(c);
            // Set the value to the textviews
            setStudentsChipView();
        } else {
            showToast(R.string.error_contact_information_not_found);
        }
    }

    private void setStudentsChipView() {
        List<Chip> chips = new ArrayList<>();
        for (PhoneContact phone : contacts) {
            chips.add(phone);
        }
        chipView.setChipLayoutRes(R.layout.row_chip);
        chipView.setChipList(chips);
        chipView.setOnChipClickListener(studentsChipViewListener);
    }

    private OnChipClickListener studentsChipViewListener = new OnChipClickListener() {
        @Override
        public void onChipClick(Chip chip) {
            PhoneContact clickedAPi = null;
            for (PhoneContact phone : contacts) {
                if (phone.getText().equalsIgnoreCase(chip.getText())) {
                    clickedAPi = phone;
                }
            }
            if (clickedAPi != null) {
                contacts.remove(clickedAPi);
                setStudentsChipView();
            }
        }
    };

    private void createGroup() {
        if (isValid()) {
            HttpParamObject httpParamObject = new HttpParamObject();
            if (isEdit) {
                httpParamObject.setUrl(AppConstants.PAGE_URL.GROUP_URL + groupId);
                httpParamObject.setPutMethod();
            } else {
                httpParamObject.setUrl(AppConstants.PAGE_URL.CREATEGROUP);
                httpParamObject.setPostMethod();
            }
            httpParamObject.setJSONContentType();
            httpParamObject.setJson(getGroupData().toString());
            httpParamObject.setClassType(CreateGroupResponse.class);
            executeTask(AppConstants.TASKCODES.CREATEGROUP, httpParamObject);
        }
    }

//    {
//        "tutorId": 4,
//            "name": "First group 1",
//            "pupilInfo": {
//        "billingCycle": "month",
//                "fee": 230,
//                "classDurationHours": 1,
//                "classDurationMins": 30
//    },
//        "students": [{
//        "id": 492
//    }]
//    }

    private JSONObject getGroupData() {
        JSONObject jsonObject = new JSONObject();
        JSONObject obj = new JSONObject();
        try {
            JSONArray arr = getStudentsJsonArray();
            jsonObject.put("tutorId", Preferences.getUserId());
            jsonObject.put("name", getEditText(R.id.et_class_name));
            obj.put("billingCycle", tv_billing_cycle.getText().toString());
            obj.put("fee", parsetoInt(getEditText(R.id.et_fee_invite)));
            obj.put("classDurationHours", parsetoInt(getEditText(R.id.et_hours)));
            obj.put("classDurationMins", parsetoInt(getTextFromTV(R.id.et_min)));
            jsonObject.put("pupilInfo", obj);
            jsonObject.put("students", arr);
            Log.d("JSON EditCreate", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private boolean isValid() {
        String hours = getEditText(R.id.et_hours);
        String min = getTextFromTV(R.id.et_min);
        String name = getEditText(R.id.et_class_name);
        String fee = getEditText(R.id.et_fee_invite);
        String bill = tv_billing_cycle.getText().toString();
        if (TextUtils.isEmpty(name)) {
            showToast(getString(R.string.error_empty_grp_name));
            return false;
        }
        if (TextUtils.isEmpty(bill)) {
            showToast(getString(R.string.error_empty_billing_cycle));
            return false;
        }


        return true;
    }

    private JSONArray getStudentsJsonArray() throws JSONException {
        JSONArray array = new JSONArray();
        for (PhoneContact c : contacts) {
            JSONObject obj = new JSONObject();
            obj.put("phone", c.getNumber());
            array.put(obj);
        }
        return array;
    }

    private int parsetoInt(String str) {
        if (TextUtils.isEmpty(str))
            return 0;
        int i;
        i = Integer.parseInt(str);
        return i;
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.CREATEGROUP:
                CreateGroupResponse createGroupResponse = (CreateGroupResponse) response;
                if (createGroupResponse != null && createGroupResponse.getStatus()) {
//                    startNextActivity(ClassSubjectDetailsActivity.class);
                    if(isEdit){
                        showToast("Group updated");
                    }else {
                        showToast("New group created");
                    }
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case AppConstants.TASKCODES.FETCH_GROUP:
                CreateGroupResponse groupResponse = (CreateGroupResponse) response;
                if (groupResponse != null && groupResponse.getStatus()) {
                    fillGroupData(groupResponse);
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            case RESULT_PICK_CONTACT:
                contactPicked(data);
                break;
            case PICK_CONTACT:
                selectedContacts = (Contacts) data.getExtras().getSerializable(AppConstants.BUNDLE_KEYS.SELECTEDCONTACTS);
                contacts = selectedContacts.getPhoneContact();
                if (contacts.size() > 0) {
                    setStudentsChipView();
                } else
                    showToast("You have not selected any contact.");
                break;

        }
    }

    private void openBillingDialog() {
        SelectorDialog.getInstance(this, getString(R.string.select_billing_cycle), SelectorDialog.getBillingCycles(), new SelectorDialog.ItemSelector() {
            @Override
            public void onItemSelected(String item) {
                setText(item, R.id.spin_billing_cycle_create);
            }
        }, getTextFromTV(R.id.spin_billing_cycle_create)).showDialog();
    }

}
