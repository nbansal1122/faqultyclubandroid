package faqulty.club.activity;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;
import faqulty.club.Util.ApiGenerator;
import faqulty.club.Util.FileUtil;
import faqulty.club.Util.ImageDownloaderTask;
import faqulty.club.fragments.bottomsheets.TemplateOptionsFragment;
import faqulty.club.models.BasicResponse;
import faqulty.club.models.pemphlets.UserPamphletsData;
import com.squareup.picasso.Picasso;

import java.io.File;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class PamphletPreviewActivity extends BaseActivity implements TemplateOptionsFragment.BottomSheetInterface {

    private UserPamphletsData userPamphletsData;
    private boolean isEditable;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_preview);
        if (!isEditable) {
            hideVisibility(R.id.btn_edit);
        }
        initToolBar("Pamphlet Edu");
        if (userPamphletsData != null) {
            setData(userPamphletsData);
        }
        setOnClickListener(R.id.btn_share, R.id.btn_edit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share:
                TemplateOptionsFragment.newInstance(this).show(getSupportFragmentManager(), "Options");
                break;
            case R.id.btn_edit:
                startNextActivity(bundle, CreateTemplateActivity.class);
                break;
        }
    }

    private void setData(UserPamphletsData userPamphletsData) {
        ImageView imageView = (ImageView) findViewById(R.id.iv_template);
        String url = userPamphletsData.getUrl();
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(this).load(Util.getCompleteUrl(url)).into(imageView);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pamphlets_preview, menu);
        if (!isEditable) {
            menu.findItem(R.id.action_delete).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                delete();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void delete() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.DELETE_PAMPHLET, Preferences.getUserId(), userPamphletsData.getId()));
        httpParamObject.setDeleteMethod();
        executeTask(AppConstants.TASKCODES.DELETE_TASK, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.DELETE_TASK:
                showToast("successfully deleted !");
                setResult(RESULT_OK);
                finish();
                break;
            case AppConstants.TASKCODES.SHARE_EMAIL:
                BasicResponse basicResponse= (BasicResponse) response;
                if(basicResponse!=null&&basicResponse.getStatus()){
                    showToast(basicResponse.getMsg());
                }
                break;
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        this.bundle = bundle;
        userPamphletsData = (UserPamphletsData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.PAMPHLET_DATA);
        isEditable = bundle.getBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE);
    }

    @Override
    public void onActionClick(int actType) {
        switch (actType) {
            case AppConstants.ACTION_TYPE.EMAIL:
                HttpParamObject httpParamObject = ApiGenerator.sareToEmail(userPamphletsData.getId(), "pamphlet", null);
                executeTask(AppConstants.TASKCODES.SHARE_EMAIL,httpParamObject);
//                new TedPermission(this).setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE).setPermissionListener(new PermissionListener() {
//                    @Override
//                    public void onPermissionGranted() {
//                        sendEmail();
//                    }
//
//                    @Override
//                    public void onPermissionDenied(ArrayList<String> arrayList) {
//
//                    }
//                }).check();
                break;
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED:
                DownloadManager manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                FileUtil.downloadFile(manager, Util.getCompleteUrl(userPamphletsData.getUrl()));
                break;
            case AppConstants.ACTION_TYPE.SHARE_FEED:
                shareImage();
                break;
        }
    }

    private void shareImage() {
        ImageDownloaderTask imageDownloaderTask = new ImageDownloaderTask(this, new ImageDownloaderTask.OnImageDownLoadedListener() {
            @Override
            public void onLoaded(String path) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                Uri screenshotUri = Uri.fromFile(new File(path));
                sharingIntent.setType("image/jpeg");
                sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                startActivity(Intent.createChooser(sharingIntent, "Share template"));
            }

            @Override
            public void onFailed() {
                showToast("Failed to send !");
            }
        });
        imageDownloaderTask.execute(Util.getCompleteUrl(userPamphletsData.getUrl()));
    }

    private void sendEmail() {
        ImageDownloaderTask imageDownloaderTask = new ImageDownloaderTask(this, new ImageDownloaderTask.OnImageDownLoadedListener() {
            @Override
            public void onLoaded(String path) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Template");
                emailIntent.setType("application/image");
                Uri uri = Uri.parse("file://" + path);
                emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(emailIntent);
            }

            @Override
            public void onFailed() {
                showToast("Failed to send !");
            }
        });
        imageDownloaderTask.execute(Util.getCompleteUrl(userPamphletsData.getUrl()));
    }
}
