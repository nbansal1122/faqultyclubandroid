package faqulty.club.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.attendance.group.GroupAttendance;
import faqulty.club.models.attendance.group.StudentSession;
import faqulty.club.models.creategroup.CreateGroupResponse;
import faqulty.club.models.creategroup.PhoneContact;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class MarkAttendenceActivity extends BaseActivity implements CustomListAdapterInterface {

    private static final int REQ_ADD_CUSTOM = 2;
    private CustomListAdapter<PhoneContact> listAdapter;

    List<PhoneContact> studentsList = new ArrayList<>();
    List<PhoneContact> allStudentsList = new ArrayList<>();

    private ListView lv_search_result;
    private SearchView searchView;
    private int counter = 0;
    private CheckBox selecAllCheckBox;
    private int groupId;
    private GroupAttendance attendance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_attendence);
        initToolBar(getString(R.string.title_mark_attendance));
        selecAllCheckBox = (CheckBox) findViewById(R.id.cb_select_all);
        searchView = (SearchView) findViewById(R.id.et_search);
        lv_search_result = (ListView) findViewById(R.id.lv_search_students);

        listAdapter = new CustomListAdapter<>(this, R.layout.row_mark_attendence, studentsList, this);
        lv_search_result.addFooterView(getFooterView());
        lv_search_result.setAdapter(listAdapter);

        lv_search_result.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition = position;
                showToast(itemPosition);
                PhoneContact studentBaseClass = studentsList.get(position);
                if (studentBaseClass.isSelected()) {
                    studentBaseClass.setSelected(false);
                } else {
                    studentBaseClass.setSelected(true);
                }
                listAdapter.notifyDataSetChanged();
                updateToolbar();
            }
        });

        selecAllCheckBox.setOnCheckedChangeListener(null);
        searchData();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        groupId = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.GROUP_ID);
        attendance = (GroupAttendance) getIntent().getExtras().getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
    }


    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            for (PhoneContact sbc : studentsList) {
                sbc.setSelected(isChecked);
            }
            listAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getGroupData();
    }

    private View getFooterView() {
        View footerView = LayoutInflater.from(this).inflate(R.layout.footer_mark_present, null);
        Button btn = (Button) footerView.findViewById(R.id.btn_add_student);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markPresentClicked();
            }
        });
        return footerView;
    }

    private void markPresentClicked() {
        try {
            if (!isValid()) {
                return;
            }
            String jsonString = getSelectedStudents();
            Intent i = new Intent();
            Bundle b = new Bundle();
            b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, getStudentsSessions());
            b.putString(AppConstants.BUNDLE_KEYS.ARRAY_STUDENT_ATTENDANCE, jsonString);
            i.putExtras(b);
            setResult(RESULT_OK, i);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void getGroupData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.FETCH_GROUP, groupId));
        httpParamObject.setClassType(CreateGroupResponse.class);
        executeTask(AppConstants.TASKCODES.GET_GROUP_DATA, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_GROUP_DATA:
                CreateGroupResponse groupResponse = (CreateGroupResponse) response;
                if (groupResponse != null && groupResponse.getCreateGroupData() != null) {
                    groupResponse = updateAttendance(groupResponse);
                    studentsList.clear();
                    studentsList.addAll(groupResponse.getCreateGroupData().getStudents());
                    allStudentsList.clear();
                    allStudentsList.addAll(groupResponse.getCreateGroupData().getStudents());
                    listAdapter.notifyDataSetChanged();
                    checkAllSelect();
                }
                break;
        }
    }

    private CreateGroupResponse updateAttendance(CreateGroupResponse response) {
        if (attendance != null) {

            HashMap<Integer, StudentSession> map = new HashMap<>();
            for (StudentSession s : attendance.getStudentSessions()) {
                map.put(s.getStudentId(), s);
            }

            for (PhoneContact c : response.getCreateGroupData().getStudents()) {
                if (map.containsKey(c.getId())) {
                    StudentSession session = map.get(c.getId());
                    c.setPerformance(session.getPerformance());
                    c.setSelected(true);
                }
            }
        }
        return response;
    }

    private void searchData() {
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.et_search);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//
//        } else {
//            searchView.setBackgroundColor(getResourceColor(R.color.white));
//        }
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setTextColor(getResourceColor(R.color.black));
        searchAutoComplete.setHintTextColor(getResourceColor(R.color.et_gray));
        searchAutoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String str = "";
                if (charSequence != null) {
                    str = charSequence.toString();
                }
                filterListView(str);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

    private void filterListView(String str) {
        studentsList.clear();
        if (str == null || str.isEmpty()) {
            studentsList.addAll(allStudentsList);
        } else {
            for (PhoneContact sbc : allStudentsList) {
                if (sbc.getDisplayString().toLowerCase().contains(str.toLowerCase()))
                    studentsList.add(sbc);
            }
        }
        listAdapter.notifyDataSetChanged();
    }

    private void updateToolbar() {
        counter = 0;
        for (PhoneContact student : studentsList) {
            if (student.isSelected()) {
                ++counter;
            }
        }
        getSupportActionBar().setSubtitle(counter + " Selected");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final PhoneContact s = studentsList.get(position);
        if (s.isSelected()) {
            holder.checkBox.setChecked(true);
            holder.checkBox.setSelected(true);
        } else {
            holder.checkBox.setChecked(false);
            holder.checkBox.setSelected(false);
        }

        if (!TextUtils.isEmpty(s.getDisplayName()))
            holder.nameTv.setText(s.getDisplayName());

        if (!TextUtils.isEmpty(s.getPhoto()))
            Picasso.with(this).load(Util.getCompleteUrl(s.getPhoto())).placeholder(R.mipmap.accountselected2x).
                    into(holder.userImage);

        unselectAllPerformance(holder.sadIv, holder.neutralIv, holder.happyIv);
        int performance = s.getPerformance();
        if (performance == 0) {
            setPerformanceIcon(holder.sadIv, R.mipmap.sad_icon);
        } else if (performance == 1) {
            setPerformanceIcon(holder.neutralIv, R.mipmap.ic_performance_neutral_selected);
        } else if (performance == 2) {
            setPerformanceIcon(holder.happyIv, R.mipmap.smile_icon);
        }

        holder.sadIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPerformance(s, 0);
            }
        });

        holder.neutralIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPerformance(s, 1);
            }
        });

        holder.happyIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPerformance(s, 2);
            }
        });

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setSelected(!s.isSelected());
                listAdapter.notifyDataSetChanged();
                updateToolbar();
                checkAllSelect();
            }
        });

        return convertView;
    }

    private void checkAllSelect() {
        boolean isAllSelected = true;
        for (PhoneContact c : allStudentsList) {
            if (!c.isSelected()) {
                isAllSelected = false;
                break;
            }
        }
        selecAllCheckBox.setOnCheckedChangeListener(null);
        selecAllCheckBox.setSelected(isAllSelected);
        selecAllCheckBox.setChecked(isAllSelected);
        selecAllCheckBox.setOnCheckedChangeListener(checkedChangeListener);
    }

    private void setPerformance(PhoneContact student, int performance) {
        if (student.getPerformance() != performance) {
            student.setPerformance(performance);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_mark_present:
                break;
        }
    }

    private boolean isValid() {
        boolean isSelected = false;
        for (PhoneContact s : allStudentsList) {
            if (s.isSelected()) {
                isSelected = true;
//                if (s.getPerformance() == -1) {
//                    showToast(getString(R.string.error_mark_performance_student, s.getDisplayName()));
//                    return false;
//                }
            }
        }
        if (!isSelected) {
            showToast(R.string.error_no_student_selected);
        }
        return isSelected;
    }

    private String getSelectedStudents() throws JSONException {
        JSONArray array = new JSONArray();
        for (PhoneContact s : allStudentsList) {
            if (!s.isSelected()) continue;
            JSONObject student = new JSONObject();
            student.put("studentId", s.getId());
            student.put("performance", s.getPerformance());
            student.put("attendance", 1);
            array.put(student);
        }
        return array.toString();
    }

    private ArrayList<StudentSession> getStudentsSessions() throws JSONException {
        ArrayList<StudentSession> sessions = new ArrayList<>();
        for (PhoneContact s : allStudentsList) {
            if (!s.isSelected()) continue;
            StudentSession ss = new StudentSession();
            ss.setStudentId(s.getId());
            ss.setPerformance(s.getPerformance() == -1 ? 1 : s.getPerformance());
            ss.setAttendance(1);
            sessions.add(ss);
        }
        return sessions;
    }

    private void unselectAllPerformance(ImageView sadIv, ImageView neutralIv, ImageView happyIv) {
        setPerformanceIcon(sadIv, R.mipmap.ic_performance_sad_unselected);
        setPerformanceIcon(happyIv, R.mipmap.ic_performance_happy_unselected);
        setPerformanceIcon(neutralIv, R.mipmap.neutral_face_icon);
    }

    private void setPerformanceIcon(ImageView imageView, int iconId) {
        imageView.setImageResource(iconId);
    }


    private class ViewHolder {
        CheckBox checkBox;
        ImageView sadIv, neutralIv, happyIv, userImage;
        TextView nameTv;

        ViewHolder(View v) {
            checkBox = (CheckBox) v.findViewById(R.id.cb_new_class);
            sadIv = (ImageView) v.findViewById(R.id.icon_sad);
            neutralIv = (ImageView) v.findViewById(R.id.icon_neutral);
            happyIv = (ImageView) v.findViewById(R.id.icon_smile);
            userImage = (ImageView) v.findViewById(R.id.iv_user_image);
            nameTv = (TextView) v.findViewById(R.id.tv_name);
        }
    }
}
