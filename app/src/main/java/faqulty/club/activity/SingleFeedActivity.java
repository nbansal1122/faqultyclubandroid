package faqulty.club.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import faqulty.club.R;
import faqulty.club.Util.FileUtil;
import faqulty.club.Util.SpacesItemDecoration;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.fragments.EditFeedPostFragment;
import faqulty.club.fragments.FeedOptionsFragment;
import faqulty.club.models.feed.BookMarkFeedApi;
import faqulty.club.models.feed.Datum;
import faqulty.club.models.feed.DeleteFeed;
import faqulty.club.models.feed.LikeApi;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 23/1/17.
 */

public class SingleFeedActivity extends BaseActivity implements BaseRecycleAdapter.RecyclerClickInterface, FeedOptionsFragment.BottomSheetInterface, PermissionListener {
    private RecyclerView rvSingleFeed;
    private BaseRecycleAdapter baseRecyclerAdapter;
    private List<BaseAdapterModel> feedList;
    private int anInt;
    private long userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_feed);
        initToolBar("Bookmark Feed");
        feedList = new ArrayList<>();
        rvSingleFeed = (RecyclerView) findViewById(R.id.rv_single_feed);
        baseRecyclerAdapter = new BaseRecycleAdapter(this, feedList);
        rvSingleFeed.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvSingleFeed.addItemDecoration(new SpacesItemDecoration(4));
        rvSingleFeed.setAdapter(baseRecyclerAdapter);
        baseRecyclerAdapter.setClickInterface(this);

        Bundle getId = getIntent().getExtras();
        if (getId != null) {
            anInt = getId.getInt(AppConstants.BUNDLE_KEYS.ID);
        }
        userId = Preferences.getData(AppConstants.PREF_KEYS.USER_ID, 111L);
        getFeedBookmarkData();
    }

    private void getFeedBookmarkData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.FETCH_BOOKMARK_FEED) + anInt);
        object.setClassType(BookMarkFeedApi.class);
        executeTask(AppConstants.TASKCODES.GETBOOKMARKFEED, object);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_respponse));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GETBOOKMARKFEED:
                BookMarkFeedApi bookMarkFeedApi = (BookMarkFeedApi) response;
                if (bookMarkFeedApi != null && bookMarkFeedApi.getStatus()) {
                    if (bookMarkFeedApi != null) {
                        Datum data = bookMarkFeedApi.getData();
                        if (data != null) {
                            feedList.clear();
                            feedList.add(data);
                            baseRecyclerAdapter.notifyDataSetChanged();
                        }
                    }
                }
                break;
            case AppConstants.TASKCODES.LIKE_FEED:
                LikeApi likeApi = (LikeApi) response;
                if (likeApi != null && likeApi.getStatus().equals(true)) {
                    showToast("Like Successful");
                    likeApi.setStatus(false);
                    getFeedBookmarkData();
                }
                break;

            case AppConstants.TASKCODES.DELETE_FEED:
                DeleteFeed deleteFeed = (DeleteFeed) response;
                if (deleteFeed != null && deleteFeed.getStatus()) {
                    showToast(deleteFeed.getMsg());
                    GenericLocalReceiver.sendBroadcast(this, GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH_BOOKMARKS);
                    finish();
                }
                break;
        }
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        Datum feedData = (Datum) obj;
        switch (actionType) {
            case AppConstants.ACTION_TYPE.EDIT_BOTTOMSHEET:
                Bundle content = new Bundle();
                content.putSerializable(AppConstants.BUNDLE_KEYS.EDIT_POSTFEED, (Datum) obj);
                EditFeedPostFragment editFeedPostFragment = EditFeedPostFragment.newInstance(this);
                editFeedPostFragment.setArguments(content);
                editFeedPostFragment.show(getSupportFragmentManager(), editFeedPostFragment.getTag());
                break;
            case AppConstants.ACTION_TYPE.SHARE_BOTTOMSHEET:
                FeedOptionsFragment feedOptionsFragment = FeedOptionsFragment.newInstance(this);
                feedOptionsFragment.setFeedData(feedData);
                feedOptionsFragment.show(getSupportFragmentManager(), feedOptionsFragment.getTag());
                break;
            case AppConstants.ACTION_TYPE.LIKE_EVENT:
                if(!Util.isConnectingToInternet(this)){
                    return;
                }
                likeApi(feedData);
                if(feedData.isHasLiked()){
                    feedData.setHasLiked(false);
                }else {
                    feedData.setHasLiked(true);
                }
                baseRecyclerAdapter.notifyItemChanged(position);
                break;
            case AppConstants.ACTION_TYPE.COMMENT_BOOKMARK:
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.BUNDLE_KEYS.ENTITY_ID, feedData.getId());
                Intent commentIntent = new Intent(this, CommentActivity.class);
                commentIntent.putExtras(bundle);
                startActivityForResult(commentIntent, AppConstants.REQUEST_CODES.UPDATE_DATA);
                break;

        }
    }

    private void likeApi(Datum feedData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.LIKE_FEED_POST);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", Preferences.getUserId());
            jsonObject.put("entityId", feedData.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(LikeApi.class);
        if (feedData.isHasLiked()) {
            httpParamObject.setDeleteMethod();
            executeTask(AppConstants.TASKCODES.UNLIKE_FEED, httpParamObject);
        } else {
            httpParamObject.setPostMethod();
            executeTask(AppConstants.TASKCODES.LIKE_FEED, httpParamObject);
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        if((taskCode==AppConstants.TASKCODES.UNLIKE_FEED)||(taskCode==AppConstants.TASKCODES.LIKE_FEED)){
            return;
        }
        super.onPreExecute(taskCode);
    }

    @Override
    public void onActionClick(int actType, Datum feedData) {
        long tutorId = feedData.getTutorId();

        switch (actType) {
            case AppConstants.ACTION_TYPE.EDIT_FEED:
                if (tutorId == userId) {
                    Bundle b = new Bundle();
                    b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, feedData);
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE, true);
                    Intent postFeedActivity = new Intent(this, PostFeedActivity.class);
                    postFeedActivity.putExtras(b);
                    startActivityForResult(postFeedActivity, AppConstants.REQUEST_CODES.UPDATE_DATA);
                } else {
                    showToast("you are not authenticated");
                }

                break;
            case AppConstants.ACTION_TYPE.BOOKMARK:
                setBookmark(feedData);
                break;
            case AppConstants.ACTION_TYPE.DELETE_FEED:
                if (tutorId == userId) {
                    deleteFeed(feedData);
                } else {
                    showToast("you are not authenticated");
                }
                break;
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED:
                downloadFeedData(feedData);
                break;
            case AppConstants.ACTION_TYPE.SHARE_FEED:
                checkPermission();
                break;


        }
    }

    private void checkPermission() {
        new TedPermission(this)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setPermissionListener(this)
                .setDeniedMessage("Allow to mail/print prescription.").check();
    }


    public void shareFeedData() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
            String sAux = "\nThis is dummy text sending by Faqulty Club\n";
            sAux = sAux + "--send Url here--";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void setBookmark(Datum feedData) {
        Bundle b = new Bundle();
        b.putInt(AppConstants.BUNDLE_KEYS.MODULE_ID, feedData.getId());
        b.putString(AppConstants.BUNDLE_KEYS.MODULE_TYPE, "feed");
        startNextActivity(b, SaveBookMarkActivity.class);
    }

    private void downloadFeedData(Datum feedData) {
        List<String> list = new ArrayList<>();
        for (SelectContent content : feedData.getContentList()) {
            list.add(content.getFullUrl());
        }
        FileUtil.downloadFile(this, list);
    }

    private void deleteFeed(Datum feedData) {
        Integer id = feedData.getId();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.DELETE_FEED) + id);
        httpParamObject.setDeleteMethod();
        httpParamObject.setClassType(DeleteFeed.class);
        executeTask(AppConstants.TASKCODES.DELETE_FEED, httpParamObject);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        } else {
            switch (requestCode) {
                case AppConstants.REQUEST_CODES.UPDATE_BOOKMARK_FEED:
                case AppConstants.REQUEST_CODES.UPDATE_DATA:
                    getFeedBookmarkData();
                    baseRecyclerAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }

    @Override
    public void onPermissionGranted() {
        shareFeedData();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> arrayList) {

    }
}
