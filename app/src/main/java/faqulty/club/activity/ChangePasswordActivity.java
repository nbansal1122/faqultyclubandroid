package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import faqulty.club.R;
import faqulty.club.models.UserProfile.UpdatePassword;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class ChangePasswordActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initToolBar("Change Password");

        setOnClickListener(R.id.btn_change_pass);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_change_pass:
                if (isValid()) {
                    changePassword();
                }
        }
    }

    private boolean isValid() {
        String mobile = UserDetailsApi.getInstance().getPhone();
        String password = getEditText(R.id.et_new_password);
        String confirmPass = getEditText(R.id.et_confirm_password);
        String currPass = getEditText(R.id.et_current_password);
        if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
            showToast("Please correct mobile number");
            return false;
        }
        if (TextUtils.isEmpty(currPass)) {
            showToast("Please enter current password");
            return false;
        }
        if (TextUtils.isEmpty(password) || password.length() < 4) {
            showToast("Password can not be less than 4 characters");
            return false;
        }
        if (!password.equals(confirmPass)) {
            showToast("Password mismatch");
            return false;
        }

        return true;
    }

    private void changePassword() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.RESET_PASSWORD);
        httpParamObject.setClassType(UpdatePassword.class);
        httpParamObject.setJson(getData().toString());
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        executeTask(AppConstants.TASKCODES.RESET_PASSWORD, httpParamObject);

    }

    private JSONObject getData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", UserDetailsApi.getInstance().getPhone());
            jsonObject.put("password", getEditText(R.id.et_new_password));
            jsonObject.put("confirmPassword", getEditText(R.id.et_confirm_password));
            jsonObject.put("currentPassword", getEditText(R.id.et_current_password));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.RESET_PASSWORD:
                UpdatePassword updatePassword = (UpdatePassword) response;
                if (updatePassword != null) {
                    showToast(updatePassword.getMsg());
                    finish();
                }
                break;
        }
    }
}
