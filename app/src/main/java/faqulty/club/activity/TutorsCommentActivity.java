package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.ProfileComments;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class TutorsCommentActivity extends BaseActivity implements CustomListAdapterInterface {
    List<ProfileComments> list = new ArrayList<>();
    private CustomListAdapter<ProfileComments> customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutors_comment);
        initToolBar("Profile Reviews");
        ListView listView = (ListView) findViewById(R.id.list_comment);
        customListAdapter = new CustomListAdapter<>(this, R.layout.row_profile_comment, list, this);
        listView.setAdapter(customListAdapter);
        listView.setEmptyView(findViewById(R.id.tv_empty));
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        List<ProfileComments> comments = (List<ProfileComments>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if(comments!=null){
            list.clear();
            list.addAll(comments);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ProfileComments profileComments = list.get(position);
        holder.onBindData(profileComments);
        return convertView;
    }

    class Holder {
        private ImageView imageView;
        private TextView tvName, tvComment, tvTime;

        public Holder(View view) {
            imageView = (ImageView) view.findViewById(R.id.iv_profile);
            tvName = (TextView) view.findViewById(R.id.tv_user_name);
            tvComment = (TextView) view.findViewById(R.id.tv_comment);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }

        public void onBindData(ProfileComments profileComments) {
            String image = profileComments.getImage();
            if (!TextUtils.isEmpty(image)) {
                Picasso.with(TutorsCommentActivity.this).load(image).placeholder(R.drawable.accountselected).into(imageView);
            } else {
                imageView.setImageResource(R.drawable.accountselected);
            }
            if (!TextUtils.isEmpty(profileComments.getName())) {
                tvName.setVisibility(View.VISIBLE);
                tvName.setText(profileComments.getName());
            } else {
                tvName.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(profileComments.getReviews())) {
                tvComment.setVisibility(View.VISIBLE);
                tvComment.setText(profileComments.getReviews());
            } else {
                tvComment.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(profileComments.getCreatedOn())) {
                tvTime.setVisibility(View.VISIBLE);
                tvTime.setText(Util.getRelativeTimeString(profileComments.getCreatedOn()));
            } else {
                tvTime.setVisibility(View.GONE);
            }
        }
    }
}
