package faqulty.club.activity;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import faqulty.club.R;
import faqulty.club.Util.ClassChip;
import faqulty.club.Util.SubjectChip;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.autoadapters.ClassMultiSelectDialog;
import faqulty.club.autoadapters.HashTagsMultiSelectDialog;
import faqulty.club.autoadapters.SubjectMultiSelectDialogNew;
import faqulty.club.fragments.AddFromCameraFragment;
import faqulty.club.fragments.AddFromGalleryFragment;

import simplifii.framework.requestmodels.SelectContent;

import faqulty.club.models.HashTag;
import faqulty.club.models.HashTagsResponse;
import faqulty.club.models.UserProfile.ClassObject;
import faqulty.club.models.UserProfile.SubjectObject;
import faqulty.club.models.feed.Datum;
import faqulty.club.models.feed.PostData;
import faqulty.club.models.feed.PostFeedModel;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by Neeraj Yadav on 12/3/2016.
 */

public class PostFeedActivity extends BaseActivity implements AddFromGalleryFragment.GetMediaListener, BaseRecycleAdapter.RecyclerClickInterface {
    private List<SelectContent> selectContent = new ArrayList<>();
    private EditText writeSomething;
    private BaseRecycleAdapter<SelectContent> adapter;
    private Datum feedData;
    private List<ClassObject> allClasses = new ArrayList<>();
    private List<ClassObject> selectedClasses = new ArrayList<>();
    ChipView chipSubject, chipClass, chipHashTags;
    private List<SubjectObject> allSubjects = new ArrayList<>();
    private List<SubjectObject> selectedSubjects = new ArrayList<>();
    private ArrayList<HashTag> hashTags = new ArrayList<>();
    private ArrayList<HashTag> selectedHashTags = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_feed);
        initToolBar("Post to feed");
        findviews();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            feedData = (Datum) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (feedData != null) {
                initToolBar("Edit Feed");
                setUpFeedData();
            }
        }
        setUpRecyclerView();
        setOnClickListener(R.id.rl_add_from_gallery, R.id.rl_add_from_camera, R.id.lay_subject, R.id.lay_class, R.id.lay_hash_tags);
        hideVisibility(R.id.btn_post);
        getHashTags();
        getClasses();
        getSubjects();
    }

    private void getHashTags() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HASH_TAGS);
        httpParamObject.setClassType(HashTagsResponse.class);
        executeTask(AppConstants.TASKCODES.GET_HASH_TAGS, httpParamObject, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_add_from_gallery:
                Bundle bundle = new Bundle();
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.POST_FEED, false);
                BottomSheetDialogFragment bottomSheetDialogFragment = AddFromGalleryFragment.getInstance(this);
                bottomSheetDialogFragment.setArguments(bundle);
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                return;
            case R.id.rl_add_from_camera:
                Bundle b = new Bundle();
                b.putBoolean(AppConstants.BUNDLE_KEYS.POST_FEED, true);
                BottomSheetDialogFragment bottomSheetDialogFragment1 = new AddFromCameraFragment();
                bottomSheetDialogFragment1.setArguments(b);
                bottomSheetDialogFragment1.show(getSupportFragmentManager(), bottomSheetDialogFragment1.getTag());
                return;
            case R.id.lay_class:
                showClassesDialog();
                break;
            case R.id.lay_subject:
                showSubjectsDialog();
                break;
            case R.id.lay_hash_tags:
                showHashTagsDialog();
                break;
        }
    }

    private void showHashTagsDialog() {
        HashTagsMultiSelectDialog f = HashTagsMultiSelectDialog.getInstance("Select Hash Tags", hashTags, new HashTagsMultiSelectDialog.ItemSelector() {
            @Override
            public void onItemsSelected(List<HashTag> selectedItems) {
                selectedHashTags.clear();
                selectedHashTags.addAll(selectedItems);
                setHashTagsChip();
            }
        }, selectedHashTags);
        f.show(getSupportFragmentManager(), "HashTag");
    }


    private void showClassesDialog() {
        ClassMultiSelectDialog f = ClassMultiSelectDialog.getInstance("Select Classes",
                allClasses, new ClassMultiSelectDialog.ItemSelector() {
                    @Override
                    public void onItemsSelected(List<ClassObject> selectedItems) {
                        selectedClasses.clear();
                        selectedClasses.addAll(selectedItems);
                        setClassesChipView();
                    }
                }, selectedClasses);

        f.show(getSupportFragmentManager(), "Classes");
    }

    private void showSubjectsDialog() {
        SubjectMultiSelectDialogNew f = SubjectMultiSelectDialogNew.getInstance("Select Subjects",
                allSubjects, new SubjectMultiSelectDialogNew.ItemSelector() {
                    @Override
                    public void onItemsSelected(List<SubjectObject> selectedItems) {
                        selectedSubjects.clear();
                        selectedSubjects.addAll(selectedItems);
                        setSubjectsChipView();
                    }
                }, selectedSubjects);

        f.show(getSupportFragmentManager(), "Classes");
    }

    private void getClasses() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_CLASSES);
        obj.setClassType(ClassObject.class);
        executeTask(AppConstants.TASKCODES.GET_CLASSES, obj, true);
    }

    private void getSubjects() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_SUBJECTS);
        obj.setClassType(SubjectObject.class);
        executeTask(AppConstants.TASKCODES.GET_SUBJECTS, obj, true);
    }

    private void setUpFeedData() {
        writeSomething.setText(feedData.getDescription());
        selectContent.clear();
        selectContent.addAll(feedData.getContentList());
        for (SelectContent content : selectContent) {
            content.setDeleteRequired(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.feed_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.post:
                Util.hideKeyboard(this, writeSomething);
                postFiles();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void findviews() {
        writeSomething = (EditText) findViewById(R.id.et_write_something);
        chipSubject = (ChipView) findViewById(R.id.chipview_subjects);
        chipClass = (ChipView) findViewById(R.id.chipview_class);
        chipHashTags = (ChipView) findViewById(R.id.chipview_hash_tags);
    }

    private void addContent(Bundle bundle) {
        Object content = bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (content instanceof SelectContent) {
            SelectContent c = (SelectContent) content;
            c.setFeedContent(true);
            c.setDeleteRequired(true);
            selectContent.add(c);
            setUpRecyclerView();
//            adapter.notifyDataSetChanged();
        } else if (content instanceof ArrayList) {
            ArrayList<SelectContent> selectContents = (ArrayList<SelectContent>) content;
            if (selectContents != null && selectContents.size() > 0) {
                for (SelectContent c : selectContents) {
                    c.setFeedContent(true);
                    c.setDeleteRequired(true);
                }
                selectContent.addAll(selectContents);
                setUpRecyclerView();
//                adapter.notifyDataSetChanged();
            }
        }

        if (selectContent != null && selectContent.size() > 0) {
            initToolBar("Post Feed");
        }
    }


    private void setUpRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(manager);
        adapter = new BaseRecycleAdapter<>(this, selectContent);
        adapter.setClickInterface(this);
        recyclerView.setAdapter(adapter);
    }

    private void createFeed(JSONArray contentArray) {
        HttpParamObject obj = new HttpParamObject();
        if (feedData != null) {
            obj.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.POST_FEED_URL) + "/" + feedData.getId());
            obj.setPutMethod();
        } else {
            obj.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.POST_FEED_URL));
            obj.setPostMethod();
        }

        obj.setClassType(PostFeedModel.class);
        obj.setJson(getJsonToCreateFeed(contentArray));
        obj.setJSONContentType();
        executeTask(AppConstants.TASKCODES.UPLOAD_FEED, obj);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (taskCode == AppConstants.TASKCODES.UPLOAD_FEED) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private String getJsonToCreateFeed(JSONArray contentArray) {
        JSONObject root = new JSONObject();
        String msg = writeSomething.getText().toString().trim();
        try {
            root.put("description", msg);
            if (contentArray != null && contentArray.length() > 0) {
                root.put("content", contentArray);
            }
            root.put("subjects", SubjectObject.getSelectedSubjects(selectedSubjects));
            root.put("classes", ClassObject.getSelectedClasses(selectedClasses));
            root.put("tags", getSelectedTags());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("http", "RequestJson: " + root.toString());
        return root.toString();
    }

    private void postFiles() {
        if (selectContent != null && selectContent.size() > 0) {
            executeTask(AppConstants.TASKCODES.MULTI_FEED_UPLOAD, selectContent);
        } else {
            String msg = writeSomething.getText().toString().trim();
            if (TextUtils.isEmpty(msg)) {
                showToast(getString(R.string.please_write));
                return;
            }
            createFeed(null);
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        super.onPreExecute(taskCode);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_respponse));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.UPLOAD_FEED:
                PostFeedModel postFeedModel = (PostFeedModel) response;
                if (postFeedModel != null && postFeedModel.getStatus()) {
                    PostData dataPost = postFeedModel.getData();
                    if (dataPost != null) {
                        showToast(postFeedModel.getMsg());
                        setResult(RESULT_OK);
                        finish();
                    }
                }
                break;
            case AppConstants.TASKCODES.MULTI_FEED_UPLOAD:
                JSONArray contentArray = (JSONArray) response;
                createFeed(contentArray);
                break;
            case AppConstants.TASKCODES.GET_SUBJECTS:
                if (response != null) {
                    List<SubjectObject> subjectsList = (List<SubjectObject>) response;
                    if (subjectsList.size() > 0) {
                        Collections.sort(subjectsList);
                        allSubjects.clear();
                        allSubjects.addAll(subjectsList);
                        setSelectedSubjects();
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_CLASSES:
                if (response != null) {
                    List<ClassObject> cityApiList = (List<ClassObject>) response;
                    if (cityApiList.size() > 0) {
                        allClasses.clear();
                        allClasses.addAll(cityApiList);
                        setSelectedClasses();
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_HASH_TAGS:
                if (response != null) {
                    HashTagsResponse hashTagsResponse= (HashTagsResponse) response;
                    if(hashTagsResponse!=null){
                        List<HashTag> hashTagList = hashTagsResponse.getData();
                        if (hashTagList.size() > 0) {
                            hashTags.clear();
                            hashTags.addAll(hashTagList);
                            setSelectedHashTags();
                        }
                    }
                }
                break;
        }
    }

    private void setSelectedClasses() {
        if (feedData != null) {
            List<ClassObject> classObjectList = feedData.getClasses();
            if (classObjectList != null) {
                for (ClassObject listObject : classObjectList) {
                    for (ClassObject classObject : allClasses) {
                        if (classObject.getId() == listObject.getId()) {
                            if (!selectedClasses.contains(classObject)) {
                                selectedClasses.add(classObject);
                            }
                        }
                    }
                }
            }
            setClassesChipView();
        }
    }

    private void setSelectedSubjects() {
        if (feedData != null) {
            List<SubjectObject> classObjectList = feedData.getSubjects();
            if (classObjectList != null) {
                for (SubjectObject listObject : classObjectList) {
                    for (SubjectObject classObject : allSubjects) {
                        if (classObject.getId() == listObject.getId()) {
                            if (!selectedSubjects.contains(classObject)) {
                                selectedSubjects.add(classObject);
                            }
                        }
                    }
                }
            }
            setSubjectsChipView();
        }
    }

    private void setSelectedHashTags() {
        if (feedData != null) {
            List<String> hashTags = feedData.getHashTags();
            if (hashTags != null) {
                for (String tag : hashTags) {
                    for (HashTag hashTag : this.hashTags) {
                        if (hashTag.isMatchingConstraint(tag)) {
                            if (!selectedHashTags.contains(hashTag)) {
                                selectedHashTags.add(hashTag);
                            }
                        }
                    }
                }
                setHashTagsChip();
            }
        }
    }

    private void setClassesChipView() {
        List<Chip> chips = new ArrayList<>();
        List<Chip> groupedClasses = getGroupedClasses(selectedClasses);
        if (groupedClasses != null && groupedClasses.size() > 0)
            chips.addAll(groupedClasses);
        if (chips.size() > 0) {
            showVisibility(R.id.class_container);
            chipClass.setChipLayoutRes(R.layout.row_chip);
            chipClass.setChipList(chips);
            chipClass.setOnChipClickListener(new OnChipClickListener() {
                @Override
                public void onChipClick(Chip chip) {
                    ClassChip classChip = (ClassChip) chip;
                    List<ClassObject> classObjects = new ArrayList<>();
                    List<ClassObject> copySelectedClasses = new ArrayList<>();
                    if (selectedClasses != null && selectedClasses.size() > 0)
                        copySelectedClasses.addAll(selectedClasses);

                    if (classChip != null)
                        classObjects.addAll(classChip.getClassObjectList());

                    if (classObjects != null && classObjects.size() > 0) {
                        for (ClassObject c2 : copySelectedClasses) {
                            for (ClassObject c1 : classObjects) {
                                if (c1.getDisplayString().compareTo(c2.getDisplayString()) == 0)
                                    selectedClasses.remove(c1);
                            }
                        }
                    }
                    setClassesChipView();
                }
            });
        } else {
            chipClass.removeAllViews();
            hideVisibility(R.id.class_container);
        }
    }

    private List<Chip> getGroupedClasses(List<ClassObject> selectedClasses) {
        List<List<ClassObject>> mainList = new ArrayList<>();
        List<ClassObject> tempObjects = new ArrayList<>();
        for (ClassObject classObject : selectedClasses) {
            try {
                int classValue = Integer.parseInt(classObject.getClassVal());
                if (tempObjects.size() > 0) {
                    ClassObject tempObject = tempObjects.get(tempObjects.size() - 1);
                    int tempValue = Integer.parseInt(tempObject.getClassVal());
                    if ((tempValue + 1) == classValue) {
                        tempObjects.add(classObject);
                    } else {
                        mainList.add(tempObjects);
                        tempObjects = new ArrayList<>();
                        tempObjects.add(classObject);
                    }
                } else {
                    tempObjects.add(classObject);
                }

            } catch (Exception e) {
                if (tempObjects.size() > 0) {
                    mainList.add(tempObjects);
                    tempObjects = new ArrayList<>();
                }
                List<ClassObject> classObjects = new ArrayList<>();
                classObjects.add(classObject);
                mainList.add(classObjects);
            }
        }
        if (tempObjects.size() > 0) {
            mainList.add(tempObjects);
        }
        List<Chip> groupedChips = new ArrayList<>();

        for (List<ClassObject> classObject : mainList) {
            ClassChip classChip = new ClassChip();
            classChip.setClassObjectList(classObject);
            groupedChips.add(classChip);
        }

        return groupedChips;
    }

    private void setHashTagsChip() {
        List<Chip> chips = new ArrayList<>();
        chips.addAll(selectedHashTags);
        if (chips.size() > 0) {
            showVisibility(R.id.hash_tags_container);
            chipHashTags.setChipLayoutRes(R.layout.row_chip_small_tag);
            chipHashTags.setChipList(chips);
            chipHashTags.setOnChipClickListener(new OnChipClickListener() {
                @Override
                public void onChipClick(Chip chip) {
                    HashTag hashTag = (HashTag) chip;
                    if (selectedHashTags.contains(hashTag))
                        selectedHashTags.remove(hashTag);
                    setHashTagsChip();
                }
            });
        } else {
            chipHashTags.removeAllViews();
            hideVisibility(R.id.hash_tags_container);
        }
    }

    private void setSubjectsChipView() {
        List<Chip> chips = new ArrayList<>();
        List<Chip> groupedClasses = getGroupedSubjects(selectedSubjects);
        if (groupedClasses != null && groupedClasses.size() > 0)
            chips.addAll(groupedClasses);
        if (chips.size() > 0) {
            showVisibility(R.id.subject_container);
            chipSubject.setChipLayoutRes(R.layout.row_chip);
            chipSubject.setChipList(chips);
            chipSubject.setOnChipClickListener(new OnChipClickListener() {
                @Override
                public void onChipClick(Chip chip) {
                    SubjectChip classChip = (SubjectChip) chip;
                    SubjectObject subjectObject = classChip.getSubjectObject();
                    if (selectedSubjects.contains(subjectObject))
                        selectedSubjects.remove(subjectObject);
                    setSubjectsChipView();
                }
            });
        } else {
            chipSubject.removeAllViews();
            hideVisibility(R.id.subject_container);
        }
    }

    private List<Chip> getGroupedSubjects(List<SubjectObject> selectedSubjects) {
        List<Chip> chips = new ArrayList<>();
        for (SubjectObject subjectObject : selectedSubjects) {
            SubjectChip chip = new SubjectChip();
            chip.setSubjectObject(subjectObject);
            chips.add(chip);
        }
        return chips;
    }

    @Override
    public void imageData(Bundle bundle) {
        addContent(bundle);
    }

    @Override
    public void videoData(Bundle bundle) {
        addContent(bundle);
    }

    @Override
    public void audioData(Bundle bundle) {
        addContent(bundle);
    }

    @Override
    public void pdfData(Bundle bundle) {
        addContent(bundle);
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM:
                SelectContent content = (SelectContent) obj;
                selectContent.remove(position);
                adapter.notifyDataSetChanged();
                break;
        }
    }


    public JSONArray getSelectedTags() throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (HashTag hashTag : selectedHashTags) {
            jsonArray.put(hashTag.getTag());
        }
        return jsonArray;
    }
}
