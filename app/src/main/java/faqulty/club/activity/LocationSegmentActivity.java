package faqulty.club.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.autocomplete.CityLocationSegment;
import faqulty.club.models.UserProfile.TutorLocation;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 19/10/16.
 */

public class LocationSegmentActivity extends BaseActivity implements CityLocationSegment.LocationDeleteInterface {
    private static final int MAX_LOCATIONS = 6;
    private LinearLayout primaryCityLocationLayout;
    private LinearLayout otherCityLocationLayout;
    private UserDetailsApi user;
    private List<CityLocationSegment> cityLocationSegmentList = new ArrayList<>();
    private int otherLocationIndex;
    private int totalLocations = 0;
    private JSONObject json;
    private static final int disableColorId = R.color.color_EditTextGray;
    private static final int enabledColorId = R.color.orange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);
        initToolBar(getString(R.string.edit_location));
        primaryCityLocationLayout = (LinearLayout) findViewById(R.id.ll_primary_location);
        otherCityLocationLayout = (LinearLayout) findViewById(R.id.ll_other_location);
        setOnClickListener(R.id.add_more_location, R.id.btn_save_changes);
        user = UserDetailsApi.getInstance();
        addTutorLocations();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.add_more_location:
                addOtherCityLocation(null);
                break;
            case R.id.btn_save_changes:
                if (isCityLocationValid()) {
                    updateProfile();
                }
                break;
        }
    }

    private void setMaxLocationRestriction(boolean isMax) {
        setMaxRestriction(R.id.add_more_location, isMax);
    }

    private void setMaxRestriction(int tvId, boolean isMax) {
        TextView tv = (TextView) findViewById(tvId);
        if (isMax) {
            tv.setTextColor(ContextCompat.getColor(this, disableColorId));
            tv.setOnClickListener(null);
        } else {
            tv.setTextColor(ContextCompat.getColor(this, enabledColorId));
            tv.setOnClickListener(this);
        }
    }

    private void addTutorLocations() {
        List<TutorLocation> tutorLocations = user.getTutorLocation();
        if (tutorLocations != null && tutorLocations.size() > 0) {
            for (TutorLocation l : tutorLocations) {
                addCityLocation(l, l.isPrimary());
            }
            if (otherCityLocationLayout.getChildCount() == 0 && cityLocationSegmentList.size() < 2) {
                addOtherCityLocation(null);
            }
        } else {
            addPrimaryCityLocation(null);
            addOtherCityLocation(null);
        }
    }

    private void addPrimaryCityLocation(TutorLocation location) {
        addCityLocation(location, true);
    }

    private void addOtherCityLocation(TutorLocation location) {
        addCityLocation(location, false);
    }

    private void addCityLocation(TutorLocation location, Boolean isPrimary) {
        if (cityLocationSegmentList.size() < MAX_LOCATIONS) {
            if (cityLocationSegmentList.size() == MAX_LOCATIONS - 1) {
                setMaxLocationRestriction(true);
            }

        } else {
            return;
        }
        CityLocationSegment f = CityLocationSegment.getInstance(location, isPrimary, cityLocationSegmentList.size());
        f.setLocationDeleteInterface(this);
        cityLocationSegmentList.add(f);
        if (isPrimary) {
            getSupportFragmentManager().beginTransaction().add(R.id.ll_primary_location, f).commitAllowingStateLoss();
        } else {
            ++totalLocations;
            getSupportFragmentManager().beginTransaction().add(R.id.ll_other_location, f).commitAllowingStateLoss();
        }
    }

    @Override
    public void onLocationDelete(CityLocationSegment segment, TutorLocation location) {

        otherCityLocationLayout.removeViewAt(segment.index - 1);
        for (int i = segment.index + 1; i < cityLocationSegmentList.size(); i++) {
            cityLocationSegmentList.get(i).index -= 1;
        }
        cityLocationSegmentList.remove(segment.index);
        if (otherCityLocationLayout.getChildCount() == 0) {
            addOtherCityLocation(null);
        }
        setMaxLocationRestriction(cityLocationSegmentList.size() >= MAX_LOCATIONS);
    }

    private void updateProfile() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUserTypeUrl("basic");
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setContentType("application/json");
//        boolean isValid = isValid();
        json = getJsonObjectToUpdate();
        if (json != null && !TextUtils.isEmpty(json.toString())) {
            httpParamObject.setJson(json.toString());
            executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
        } else {
            showToast("Duplicate entries are not allowed in locations.");
        }
    }

    private boolean isValid() {
        if (getJsonObjectToUpdate() != null) {
            return true;
        } else {
            showToast("Duplicate entries are not allowed in locations.");
            return false;
        }
    }

    private boolean isCityLocationValid() {
        HashSet<String> set = new HashSet<>();
        for (CityLocationSegment s : cityLocationSegmentList) {
            if (s.isValid()) {
                if (set.contains(s.getCityLocationString())) {
                    showToast("Duplicate entries are not allowed in locations.");
                    return false;
                } else {
                    String cityLocationString = s.getCityLocationString();
                    if (!TextUtils.isEmpty(cityLocationString)) {
                        set.add(cityLocationString);
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private JSONObject getJsonObjectToUpdate() {
        JSONObject jsonObject = null;
        try {
            jsonObject = UserDetailsApi.getBasicInfoJsonObject(user);
            JSONArray jsonArray = new JSONArray();
            for (CityLocationSegment s : cityLocationSegmentList) {
                JSONObject json = s.getJsonObject();
                if (json != null) {
                    jsonArray.put(json);
                } else {
                }
            }
            jsonObject.put("tutorLocation", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_USER:
                showToast(R.string.basic_profile_updated_successfully);
                setResult(RESULT_OK);
                finish();
                break;
        }
    }
}
