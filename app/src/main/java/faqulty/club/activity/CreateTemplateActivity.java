package faqulty.club.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import faqulty.club.R;
import faqulty.club.Util.PamphletFieldView;
import faqulty.club.fragments.AddImageFragment;
import faqulty.club.fragments.Image;
import faqulty.club.fragments.MediaFragment;
import faqulty.club.models.GenerateTemplateResponse;
import faqulty.club.models.UserProfile.UserDetailsApi;
import faqulty.club.models.pemphlets.AllPamphletsData;
import faqulty.club.models.pemphlets.AllPamphletsResponse;
import faqulty.club.models.pemphlets.CreatePamphletsResponse;
import faqulty.club.models.pemphlets.PamphletsData;
import faqulty.club.models.pemphlets.UserPamphletsData;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.requestmodels.UploadImageData;
import simplifii.framework.requestmodels.UploadImageResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class CreateTemplateActivity extends BaseActivity {
    private AllPamphletsData pamphletsData;
    private MediaFragment mediaFragment;
    private Bitmap logoBitmap;
    private List<PamphletFieldView> pamphletFieldViewList = new ArrayList<>();
    private ImageView imageView;
    private UserPamphletsData userPamphletsData;
    private int createdPamphletId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pamphlet);
        initToolBar(getString(R.string.create_pamphlet));
        imageView = (ImageView) findViewById(R.id.iv_logo);
        mediaFragment = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "Media Fragment").commitAllowingStateLoss();
        setOnClickListener(R.id.btn_genrete_pemphlet, R.id.iv_get_image);
        setPamphletsList(pamphletsData);
    }

    private void setPamphletsList(AllPamphletsData pamphletsData) {
        if (userPamphletsData != null) {
            initToolBar(getString(R.string.edit_pamphlet));
            AllPamphletsResponse savedInstance = AllPamphletsResponse.getSavedInstance();
            AllPamphletsData selectedPamphletsData = null;
            if (savedInstance != null) {
                List<AllPamphletsData> pamphletsDataList = savedInstance.getData();
                for (AllPamphletsData allPamphletsData : pamphletsDataList) {
                    if (allPamphletsData.getType().equalsIgnoreCase(userPamphletsData.getType())) {
                        selectedPamphletsData = allPamphletsData;
                    }
                }
            }
            if (selectedPamphletsData != null) {
                if (selectedPamphletsData.getIsLogoRequired() == 0) {
                    hideVisibility(R.id.lay_logo);
                }
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_pamphlet_data_container);
                addField("Pamphlet Name", "title", 50, userPamphletsData.getTitle(), linearLayout, 0, selectedPamphletsData.getIsTitleRequired());
                addField("Description", "description", 100, userPamphletsData.getDescription(), linearLayout, 0, selectedPamphletsData.getIsDescriptionRequired());
                addField("Classes", "classes", 100, userPamphletsData.getClasses(), linearLayout, 0, selectedPamphletsData.getIsClassesRequired());
                addField("Subjects", "subjects", 100, userPamphletsData.getSubjects(), linearLayout, 0, selectedPamphletsData.getIsSubjectsRequired());
                addField("Boards", "boards", 100, userPamphletsData.getBoards(), linearLayout, 0, selectedPamphletsData.getIsBoardsRequired());
//                addField("Degrees", "degrees", 100, userPamphletsData.get(), linearLayout, 0, selectedPamphletsData.getIsDegreesRequired());
                int maxCountFreeText = 100;
                if (selectedPamphletsData.getId() == 1 || selectedPamphletsData.getId() == 2) {
                    maxCountFreeText = 50;
                }
                addField("Free Text", "brandingText", maxCountFreeText, userPamphletsData.getBrandingText(), linearLayout, 0, selectedPamphletsData.getIsBrandingTextRequired());
                linearLayout = (LinearLayout) findViewById(R.id.lay_contact_detail_container);

                addField("Name", "name", 50, userPamphletsData.getName(), linearLayout, 0, selectedPamphletsData.getIsNameRequired());
                addField("Address", "address", 100, userPamphletsData.getAddress(), linearLayout, 0, selectedPamphletsData.getIsAddressRequired());
//                addField("Email", "email", 50, userPamphletsData.getEmail(), linearLayout, 1, 1);
                addField("Mobile Number", "mobile", 40, userPamphletsData.getMobile(), linearLayout, 2, 1);

                String url = userPamphletsData.getLogo();
                if (!TextUtils.isEmpty(url)) {
                    Picasso.with(this).load(Util.getCompleteUrl(url)).into(imageView);
                }
            }
        } else {
            if (pamphletsData != null) {
                UserDetailsApi detailsApi = UserDetailsApi.getInstance();
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_pamphlet_data_container);
                if (detailsApi != null) {
                    addField("Pamphlet Name", "title", 50, "", linearLayout, 0, pamphletsData.getIsTitleRequired());
                    addField("Description", "description", 100, "", linearLayout, 0, pamphletsData.getIsDescriptionRequired());
                    addField("Classes", "classes", 100, detailsApi.getClassesString(), linearLayout, 0, pamphletsData.getIsClassesRequired());
                    addField("Subjects", "subjects", 100, detailsApi.getSubjectsString(), linearLayout, 0, pamphletsData.getIsSubjectsRequired());
                    addField("Boards", "boards", 100, detailsApi.getBoardString(), linearLayout, 0, pamphletsData.getIsBoardsRequired());
//                    addField("Degrees", "degrees", 100, detailsApi.getDegreesString(), linearLayout, 0, pamphletsData.getIsDegreesRequired());
                    int maxCountFreeText = 100;
                    if (pamphletsData.getId() == 1 || pamphletsData.getId() == 2) {
                        maxCountFreeText = 50;
                    }

                    addField("Free Text", "brandingText", maxCountFreeText, "", linearLayout, 0, pamphletsData.getIsBrandingTextRequired());
                    linearLayout = (LinearLayout) findViewById(R.id.lay_contact_detail_container);
                    addField("Name", "name", 50, detailsApi.getName(), linearLayout, 0, pamphletsData.getIsNameRequired());
                    addField("Address", "address", 100, detailsApi.getLocation(), linearLayout, 0, pamphletsData.getIsAddressRequired());
//                    addField("Email", "email", 50, detailsApi.getEmail(), linearLayout, 1, 1);
                    addField("Mobile Number", "mobile", 40, detailsApi.getPhone(), linearLayout, 2, pamphletsData.getIsContactRequired());
                    if (pamphletsData.getIsLogoRequired() == 0) {
                        hideVisibility(R.id.lay_logo);
                    }
                }
            }
        }
    }

    private void addField(String label, String key, int maxCount, String text, LinearLayout linearLayout, int inputType, int isRequired) {
        if (isRequired == 0) {
            return;
        }
        PamphletFieldView pamphletName = new PamphletFieldView(label, key, maxCount, text, inputType, this);
        linearLayout.addView(pamphletName.getView());
        pamphletFieldViewList.add(pamphletName);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        pamphletsData = (AllPamphletsData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        userPamphletsData = (UserPamphletsData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.PAMPHLET_DATA);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_genrete_pemphlet:
                validateData();
                break;
            case R.id.iv_get_image:
                getLogoImage();
                break;

        }
    }

    private void getLogoImage() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.upload_logo));
        mediaFragment.setImageListener(new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                logoBitmap = bitmap;
                imageView.setImageBitmap(Util.getResizeBitmap(bitmap, 1024));
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                if (images.size() > 0) {
                    String path = images.get(0).getFilePath();
                    logoBitmap = Util.getBitmapFromFilePath(path, CreateTemplateActivity.this);
                    imageView.setImageBitmap(logoBitmap);
                }
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(mediaFragment);
        bottomSheetDialogFragment.setArguments(bundle);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    private void validateData() {
        for (PamphletFieldView pamphletFieldView : pamphletFieldViewList) {
            if (!pamphletFieldView.isValidate()) {
                return;
            }
        }
        if (logoBitmap != null) {
            UploadLogo();
        } else {
            if (userPamphletsData != null) {
                submitData(userPamphletsData.getLogo());
            } else {
                submitData("");
            }
        }
    }

    private void UploadLogo() {
        showProgressDialog();
        try {
            File file = Util.getFile(logoBitmap, "pamphlet logo");
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submitData(String imageUrl) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (pamphletsData != null) {
                jsonObject.put("type", pamphletsData.getType());
            }
            if (userPamphletsData != null) {
                jsonObject.put("type", userPamphletsData.getType());
            }
            for (PamphletFieldView pamphletFieldView : pamphletFieldViewList) {
                jsonObject.put(pamphletFieldView.getKey(), pamphletFieldView.getText());
            }
            jsonObject.put("logo", imageUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        if (userPamphletsData != null) {
            httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.CREATE_PAMPHLETS, Preferences.getUserId()) + "/" + userPamphletsData.getId());
            httpParamObject.setPutMethod();
        } else {
            if (createdPamphletId == 0) {
                httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.CREATE_PAMPHLETS, Preferences.getUserId()));
                httpParamObject.setPostMethod();
            } else {
                httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.CREATE_PAMPHLETS, Preferences.getUserId()) + "/" + createdPamphletId);
                httpParamObject.setPutMethod();
            }
        }

        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(CreatePamphletsResponse.class);
        httpParamObject.setJSONContentType();
        executeTask(AppConstants.TASKCODES.CREATE_PAMPHLETS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_respponse));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.CREATE_PAMPHLETS:
                CreatePamphletsResponse createPamphletsResponse = (CreatePamphletsResponse) response;
                if (createPamphletsResponse.isStatus()) {
                    PamphletsData data = createPamphletsResponse.getData();
                    if (data != null) {
                        GentareTemplate(data);
                        createdPamphletId = data.getId();
                    }
                }
                break;
            case AppConstants.TASKCODES.GENERATE_TEMPLE:
                GenerateTemplateResponse generateTemplateResponse = (GenerateTemplateResponse) response;
                if (generateTemplateResponse != null) {
                    UserPamphletsData userPamphletsData = generateTemplateResponse.getData();
                    if (userPamphletsData != null) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.PAMPHLET_DATA, userPamphletsData);
                        startNextActivity(bundle, PamphletPreviewActivity.class);
                    }
                }
                break;
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    UploadImageData data = uploadImageResponse.getData();
                    if (data != null) {
                        submitData(data.getUri());
                    }
                }
                break;
        }
    }

    private void GentareTemplate(PamphletsData data) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GENERATE_TEMPLATE, data.getUserId(), data.getId()));
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(GenerateTemplateResponse.class);
        executeTask(AppConstants.TASKCODES.GENERATE_TEMPLE, httpParamObject);
    }
}
