package faqulty.club.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.ApiGenerator;
import faqulty.club.fragments.dialogs.CreatePaymentDialogGragment;
import faqulty.club.models.BaseApi;
import faqulty.club.models.transaction.CreateTransactionResponse;
import faqulty.club.models.transaction.ReminderData;
import faqulty.club.models.transaction.Transaction;
import faqulty.club.models.invoice.InvoiceResponse;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.tutorstudent.StudentProfileResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class CreateNewInvoiceActivity extends BaseActivity {
    TextView tv_from, tv_to;
    private int mYear, mMonth, mDay;
    private int studentId;
    private long fromDate, toDate = 0;
    private InvoiceResponse invoiceResponse;
    private boolean isEdit;
    private StudentProfileResponse studentProfile;
    private Transaction invoice;
    private long pendingAmount;
    private LinearLayout llInvoiceData;
    private StudentBaseClass studentBase;
    private String fromDatePick;
    private String toDatePick;
    private boolean isGroupStudent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_invoice);
        initToolBar(getString(R.string.create_new_invoice));
        setText(getString(R.string.amount_rs, pendingAmount + ""), R.id.tv_out_standing_balence);

        llInvoiceData = (LinearLayout) findViewById(R.id.ll_invoice_data);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            studentBase = (StudentBaseClass) bundle.getSerializable(AppConstants.BUNDLE_KEYS.STUDENT_DATA);
            isGroupStudent = bundle.getBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, false);
        }

        tv_from = (TextView) findViewById(R.id.tv_from_date);
        tv_to = (TextView) findViewById(R.id.tv_to_date);
        tv_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateNewInvoiceActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                fromDatePick = simpleDateFormat.format(c.getTime());
                                tv_from.setText(fromDatePick);
                                fromDate = getTimeFromCalendar(dayOfMonth, monthOfYear, year);
                                if (toDate != 0) {
                                    getInvoiceData();
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
            }
        });
        tv_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateNewInvoiceActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                toDatePick = simpleDateFormat.format(c.getTime());
                                tv_to.setText(toDatePick);
                                toDate = getTimeFromCalendar(dayOfMonth, monthOfYear, year);
                                getInvoiceData();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
            }
        });
        setOnClickListener(R.id.btn_create_new_invoice);
        if (isEdit) {
            setViewsIfEditingInvoice();
            setText(getString(R.string.update), R.id.btn_create_new_invoice);
        } else {
//            setEditText(R.id.et_payment_due, String.valueOf(pendingAmount));
        }
    }

    private void setViewsIfEditingInvoice() {
        if (isEdit) {
            String dateFromString = invoice.getDateFrom();
            if (!TextUtils.isEmpty(dateFromString)) {
                String dateFrom = Util.convertDateFormatWithUTC(dateFromString, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.INVOICE_EDIT_FORMAT);
                Date fromDate = Util.convertStringToDate(dateFromString, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT);
                this.fromDate = fromDate.getTime();
                tv_from.setText(dateFrom);
            } else {

            }

            String dateToString = invoice.getDateTo();
            if (!TextUtils.isEmpty(dateFromString)) {
                String dateTo = Util.convertDateFormatWithUTC(dateToString, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.INVOICE_EDIT_FORMAT);
                Date toDate = Util.convertStringToDate(invoice.getDateTo(), Util.ATTENDANCE_SERVER_CREATEDON_FORMAT);
                this.toDate = toDate.getTime();
                tv_to.setText(dateTo);
            }
            initToolBar(getString(R.string.edit_invoice));
            Button btn = (Button) findViewById(R.id.btn_create_new_invoice);
            btn.setText(R.string.done);
            setEditText(R.id.et_payment_due, invoice.getAmount() + "");
            getInvoiceData();
        } else {
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        studentId = bundle.getInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, -1);
        isEdit = bundle.getBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE, false);
        pendingAmount = getIntent().getLongExtra(AppConstants.BUNDLE_KEYS.PENDING_AMOUNT, 0);
        if (isEdit)
            invoice = (Transaction) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
    }

    private long getTimeFromCalendar(int day, int month, int year) {
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        return c.getTimeInMillis();
    }

    private void getInvoiceData() {
        if (isValidDates()) {

            Bundle bundle = getIntent().getExtras();
            int groupId = -1;
            if (bundle != null) {
                groupId = bundle.getInt(AppConstants.BUNDLE_KEYS.GROUP_ID);
            }


            String fromDate = getDateServerFormat(R.id.tv_from_date);
            String toDate = getDateServerFormat(R.id.tv_to_date);
            HttpParamObject obj = new HttpParamObject();
            if (isGroupStudent) {
                obj.setUrl(String.format(AppConstants.PAGE_URL.INVOICE_URL_GROUP, String.valueOf(Preferences.getUserId()), String.valueOf(groupId)));
            } else {
                obj.setUrl(String.format(AppConstants.PAGE_URL.INVOICE_URL, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
            }

            obj.addParameter("dateFrom", fromDate);
            obj.addParameter("dateTo", toDate);
            obj.setClassType(InvoiceResponse.class);
            executeTask(AppConstants.TASKCODES.GET_INVOICE_DATA, obj);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_INVOICE_DATA:
                invoiceResponse = (InvoiceResponse) response;
                if (invoiceResponse != null) {
                    setInvoiceResponse(invoiceResponse);
                }
                break;
            case AppConstants.TASKCODES.CREATE_INVOICE:
                CreateTransactionResponse createTransactionResponse = (CreateTransactionResponse) response;
                if (createTransactionResponse != null) {
                    Transaction data = createTransactionResponse.getData();
                    if (data != null) {
                        setResult(RESULT_OK);
                        showToast(R.string.success_transaction_creaate);
                        showReminderDialog(data.getId());
                    } else {
                        showToast(createTransactionResponse.getMsg());
                    }
                }
                break;
            case AppConstants.TASKCODES.DELETE_INVOICE:
                setResult(RESULT_OK);
                showToast(R.string.success_transaction_delete);
                finish();
                break;
            case AppConstants.TASKCODES.GET_STUDENT_TRANSACTION_REMINDER:
                if (response != null) {
                    BaseApi baseApi = (BaseApi) response;
                    if (baseApi != null) {
                        String msg = baseApi.getMsg();
                        if (!TextUtils.isEmpty(msg)) {
                            showToast(msg);
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                }
                break;
        }
    }

    private void showReminderDialog(final long id) {
        CreatePaymentDialogGragment.showDialog(new CreatePaymentDialogGragment.SendReminderListener() {
            @Override
            public void onSubmit(ReminderData reminderData) {
                reminderData.setTransactionId(id);
                sendReminder(reminderData);
            }

            @Override
            public void onCancel() {
                setResult(RESULT_OK);
                finish();
            }
        }, getSupportFragmentManager());
    }

    private void sendReminder(ReminderData reminderData) {
        HttpParamObject httpParamObject = ApiGenerator.sendReminder(reminderData.getType(), reminderData.getMode(), studentId, reminderData.getNotificationType(), reminderData.getTransactionId());
        executeTask(AppConstants.TASKCODES.GET_STUDENT_TRANSACTION_REMINDER, httpParamObject);
    }


    private void createInvoice() {
        if (isValidDates() && isValidAmount()) {
            HttpParamObject obj = new HttpParamObject();
            obj.setJson(getTransactionsJSON());
            obj.setClassType(CreateTransactionResponse.class);
            obj.setJSONContentType();
            if (isEdit) {
                obj.setUrl(String.format(AppConstants.PAGE_URL.CREATE_NEW_TRANSACTION, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)) + "/" + invoice.getId());
                obj.setPutMethod();
            } else {
                obj.setUrl(String.format(AppConstants.PAGE_URL.CREATE_NEW_TRANSACTION, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
                obj.setPostMethod();
            }
            executeTask(AppConstants.TASKCODES.CREATE_INVOICE, obj);
        }
    }

    private boolean isValidAmount() {
        String amountString = getEditText(R.id.et_payment_due);
        if (TextUtils.isEmpty(amountString)) {
            showToast(R.string.error_empty_invoice_amount);
            return false;
        }
        double amount = Double.parseDouble(getEditText(R.id.et_payment_due));
        if (amount <= 0.0) {
            showToast(R.string.error_empty_invoice_amount_0);
            return false;
        }
        return true;
    }


    private String getTransactionsJSON() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("amount", Double.parseDouble(getEditText(R.id.et_payment_due)));
            obj.put("type", getString(R.string.request));
            obj.put("dateFrom", getDateServerFormat(R.id.tv_from_date));
            obj.put("dateTo", getDateServerFormat(R.id.tv_to_date));
            if (invoiceResponse != null) {
                obj.put("suggestedSessions", invoiceResponse.getData().getNoOfSessions());
                obj.put("suggestedHours", invoiceResponse.getData().getNoOfHours());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    private boolean isValidDates() {
        if (toDate == 0) {
            showToast(R.string.error_empty_invoice_date);
            return false;
        }
        if (toDate > fromDate) {
            return true;
        } else {
            showToast(R.string.error_invalid_invoice_date);
            return false;
        }
    }

    private void setInvoiceResponse(InvoiceResponse invoiceResponse) {
        showVisibility(R.id.ll_invoice_data);

        String duration = "";
        if (0 != invoiceResponse.getData().getNoOfHours()) {
            duration = invoiceResponse.getData().getNoOfHours() + " h ";
        }
        if (0 != invoiceResponse.getData().getNoOfMins()) {
            duration += invoiceResponse.getData().getNoOfMins() + " m";
        }
        if (TextUtils.isEmpty(duration)) {
            duration = "0";
        }

        if (!isEdit) {
            setEditText(R.id.et_payment_due, invoiceResponse.getData().getAmount() + "");
        }

        setText(duration, R.id.tv_invoice_hours);
        setText(String.valueOf(invoiceResponse.getData().getNoOfSessions()), R.id.tv_invoice_sessions);

        llInvoiceData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT, studentBase);
                b.putString(AppConstants.BUNDLE_KEYS.FROM_DATE, fromDatePick);
                b.putString(AppConstants.BUNDLE_KEYS.TO_DATE, toDatePick);
                b.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, isGroupStudent);
                startNextActivity(b, ViewAttendanceActivity.class);
            }
        });
    }

    private String getDateServerFormat(int tvId) {
        String text = getTextFromTV(tvId);
        return Util.convertDateFormat(text, Util.INVOICE_DATE_PATTERN, Util.ATTENDANCE_POST_SERVER_FORMAT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isEdit) {
            getMenuInflater().inflate(R.menu.menu_invoice, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteTransaction();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteTransaction() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.DELETE_TRANSACTION + invoice.getId());
        obj.setDeleteMethod();
        executeTask(AppConstants.TASKCODES.DELETE_INVOICE, obj);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_create_new_invoice:
                createInvoice();
                break;
        }
    }
}
