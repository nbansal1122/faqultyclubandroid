package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.assignment.ClassModel;
import faqulty.club.models.assignment.subjects.SubjectData;
import faqulty.club.models.assignment.subjects.SubjectsResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Dhillon on 02-12-2016.
 */

public class SubjectsActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView lvSubjects;
    private CustomListAdapter subjectAdapter;
    private List<SubjectData> subjectList = new ArrayList<>();
    private ClassModel assignmentModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);
        initToolBar("Assignments");
        if (assignmentModel != null && !TextUtils.isEmpty(assignmentModel.getClass_()))
            getSupportActionBar().setSubtitle("Class " + assignmentModel.getClass_());
        subjectList = new ArrayList();
        lvSubjects = (ListView) findViewById(R.id.lv_subjects);
        subjectAdapter = new CustomListAdapter(this, R.layout.row_assignment, subjectList, this);
        lvSubjects.setAdapter(subjectAdapter);
        lvSubjects.setOnItemClickListener(this);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        if (bundle != null) {
            assignmentModel = (ClassModel) bundle.get(AppConstants.BUNDLE_KEYS.CLASS);
            getSubjectData();
        }
    }

    private void getSubjectData() {
        showProgressDialog();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ASSIGNMENT_SUBJECTS);
        httpParamObject.setClassType(SubjectsResponse.class);
        httpParamObject.addParameter("class", assignmentModel.getClass_());
        executeTask(AppConstants.TASKCODES.GET_ASSIGNMENT_SUBJECTS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_ASSIGNMENT_SUBJECTS:
                SubjectsResponse asr = (SubjectsResponse) response;
                if (asr != null && asr.getStatus() && asr.getData().size() > 0) {
                    subjectList.clear();
                    subjectList.addAll(asr.getData());
                    subjectAdapter.notifyDataSetChanged();
                } else {
                    hideVisibility(R.id.tv_select);
                    findViewById(R.id.lv_subjects).setVisibility(View.GONE);
                    findViewById(R.id.tv_empty).setVisibility(View.VISIBLE);

                }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        if (null != subjectList && subjectList.size() > 0) {
            if (!TextUtils.isEmpty(subjectList.get(position).getDegreeSubject()))
                holder.tvSubject.setText(subjectList.get(position).getDegreeSubject());
            if (subjectList.get(position).getChapterCount() != null)
                holder.tvChapter.setText(subjectList.get(position).getChapterCount() + " Chapters");
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.SUBJECT, subjectList.get(position));
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.CLASS, assignmentModel);
        startNextActivity(bundle, AssignmentChapterActivity.class);
    }


    private class Holder {
        TextView tvSubject, tvChapter;

        public Holder(View v) {
            tvSubject = (TextView) v.findViewById(R.id.tv_subject);
            tvChapter = (TextView) v.findViewById(R.id.tv_chapters);
        }
    }
}
