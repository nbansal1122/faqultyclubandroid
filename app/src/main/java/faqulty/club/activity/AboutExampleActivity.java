package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 24/10/16.
 */

public class AboutExampleActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView listView;
    private CustomListAdapter<String> adapter;
    private List<String> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_about_me);
        initToolBar(getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.KEY_ABOUT_TITLE));
        initList();
        listView = (ListView) findViewById(R.id.lv_examples);
        adapter = new CustomListAdapter<>(this, R.layout.row_autocomplete, list, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    private void initList() {
        String fileName = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.KEY_FILE_NAME);
        String json = Util.getStringFromAsset(this, fileName);
        if (!TextUtils.isEmpty(json)) {
            try {
                JSONArray array = new JSONArray(json);
                for (int i = 0; i < array.length(); i++) {
                    list.add(array.getString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.from(this).inflate(resourceID, null);
        }
        TextView view = (TextView) convertView;
        view.setText(list.get(position));
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Intent i = new Intent();
//        i.putExtra(AppConstants.BUNDLE_KEYS.KEY_EXAMPLE_TEXT, list.get(position));
//        setResult(RESULT_OK, i);
//        finish();
    }
}
