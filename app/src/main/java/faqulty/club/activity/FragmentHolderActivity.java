package faqulty.club.activity;

import android.os.Bundle;

import faqulty.club.R;
import faqulty.club.fragments.ProfilePreviewFragment;

import simplifii.framework.activity.BaseActivity;

public class FragmentHolderActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        initToolBar(getString(R.string.title_profile));
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, new ProfilePreviewFragment()).commitAllowingStateLoss();
    }
}
