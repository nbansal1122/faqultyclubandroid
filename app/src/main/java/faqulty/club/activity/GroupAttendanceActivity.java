package faqulty.club.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.fragments.attendance.GroupSessionFragment;
import faqulty.club.models.attendance.group.GroupAttendance;
import faqulty.club.models.attendance.group.GroupSessionResponse;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class GroupAttendanceActivity extends BaseActivity {

    private TextView tv_date;
    private SimpleDateFormat sdf;
    private Calendar c;
    private LinearLayout ll_session;
    private Button btn_save;
    private HashMap<String, List<GroupAttendance>> mapAttendanceData = new HashMap<>();
    private HashMap<String, List<GroupSessionFragment>> mapAttendanceFragments = new HashMap<>();
    private List<GroupAttendance> attendanceResponseList = new ArrayList<>();
    private int groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);
        initToolBar(getString(R.string.title_mark_attendance));
        Bundle bundle = getIntent().getExtras();
        c = Calendar.getInstance();
        ll_session = (LinearLayout) findViewById(R.id.ll_session);
        tv_date = (TextView) findViewById(R.id.tv_date);
        sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        hideVisibility(R.id.btn_save_session);
        setOnClickListener(R.id.tv_previous_day, R.id.tv_next_day, R.id.tv_new_session);
        if (bundle != null) {
            groupId = bundle.getInt(AppConstants.BUNDLE_KEYS.GROUP_ID);
        }
        getAttendanceData();
    }

    private void getAttendanceData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        String attendanceUrl = String.format(AppConstants.PAGE_URL.GET_GROUP_ATTENDANCE, String.valueOf(groupId));
        httpParamObject.setUrl(attendanceUrl);
        httpParamObject.addParameter("offset", "0");
        httpParamObject.addParameter("count", "100");
        httpParamObject.setClassType(GroupSessionResponse.class);
        executeTask(AppConstants.TASKCODES.GET_GROUP_ATTENDANCE, httpParamObject);
    }

    private void hideNextLabelIfRequired(String selectedDateString) {
        Date currentDate = new Date();
        String dateString = sdf.format(currentDate);
        if (dateString.equalsIgnoreCase(selectedDateString)) {
            findViewById(R.id.tv_next_day).setVisibility(View.INVISIBLE);
            findViewById(R.id.tv_next_day).setOnClickListener(null);
        } else {
            findViewById(R.id.tv_next_day).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_next_day).setOnClickListener(this);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_GROUP_ATTENDANCE:
                if (response != null) {
                    GroupSessionResponse sessionResponse = (GroupSessionResponse) response;
                    if (sessionResponse != null) {
                        attendanceResponseList.addAll(sessionResponse.getData());
                        createAttendanceMap();
                    }
                    setDataForCurrentDate();

                }
                break;
            case AppConstants.TASKCODES.MARK_ATTENDANCE:
                showToast(R.string.sessions_updated_successfully);
                finish();
                break;

        }
    }

    private void setDataForCurrentDate() {
        String date = getDate(0);
        setDataBasedOnDate(date);
    }



    private String getDate(int dayAdditionValue) {
        c.add(Calendar.DAY_OF_MONTH, dayAdditionValue);
        String dateString = sdf.format(c.getTime());
        tv_date.setText(dateString);
        hideNextLabelIfRequired(dateString);
        return dateString;
    }

    private void createAttendanceMap() {
        for (int i = 0; i < attendanceResponseList.size(); i++) {
            GroupAttendance response = attendanceResponseList.get(i);
            List<GroupAttendance> mapList;
            if (mapAttendanceData.containsKey(response.getCreatedOn())) {
                mapList = mapAttendanceData.get(response.getCreatedOn());
            } else {
                mapList = new ArrayList<>();
            }
            mapList.add(response);
            mapAttendanceData.put(response.getCreatedOn(), mapList);
        }
    }

    private void setDataBasedOnDate(String date) {
        ll_session.removeAllViewsInLayout();
        if (mapAttendanceFragments.containsKey(date)) {
            List<GroupSessionFragment> fragments = mapAttendanceFragments.get(date);
            List<GroupSessionFragment> copyList = new ArrayList<>();
            for (GroupSessionFragment f : fragments) {
                copyList.add(f.copyFragment());
            }
            mapAttendanceFragments.put(date, copyList);
            for (GroupSessionFragment f : copyList) {
                addGroupSessionFragment(f);
            }
        } else if (mapAttendanceData.containsKey(date)) {
            // Check if date exists in attendance coming from server
            List<GroupAttendance> list = mapAttendanceData.get(date);
            for (int i = 0; i < list.size(); i++) {
                GroupAttendance response = list.get(i);
                addSessionToLayout(response, date, (i == 0), i);
            }
        } else {
            addSessionToLayout(null, date, true, 0);
        }
    }

    private void addGroupSessionFragment(GroupSessionFragment fragment) {
//        FrameLayout layout = getFrame();
//        ll_session.addView(layout, ll_session.getChildCount());
//        if (fragment.isAdded()) {
//            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//        }
        getSupportFragmentManager().beginTransaction().add(R.id.ll_session, fragment).commitAllowingStateLoss();
//        ll_session.postInvalidate();
//        ll_session.requestLayout();
    }

    private FrameLayout getFrame() {
        return (FrameLayout) LayoutInflater.from(this).inflate(R.layout.row_session_frame, null);
    }

    private void addSessionToLayout(GroupAttendance attendanceResponse, String date, boolean isNewSession, int counter) {
        GroupSessionFragment f = GroupSessionFragment.getInstance(attendanceResponse, date, isNewSession, counter, groupId);
        List<GroupSessionFragment> fragments = null;
        if (mapAttendanceFragments.containsKey(date)) {
            fragments = mapAttendanceFragments.get(date);
        } else {
            fragments = new ArrayList<>();
        }
        fragments.add(f);
        mapAttendanceFragments.put(date, fragments);
        addGroupSessionFragment(f);
    }

    private void saveSessionForSelectedDate() {
        String date = getTextFromTV(R.id.tv_date);
        if (mapAttendanceFragments.containsKey(date)) {
            List<GroupSessionFragment> fragments = mapAttendanceFragments.get(date);
            for (GroupSessionFragment s : fragments) {
                if (!s.isValid()) {
                    return;
                }
            }
            saveSessions(fragments);
        } else {
            showToast("No New Session");
        }
    }

    private void saveSessions(List<GroupSessionFragment> fragments) {
        if (fragments.size() == 0) return;
        List<HttpParamObject> objects = new ArrayList<>();
        for (GroupSessionFragment f : fragments) {
            HttpParamObject obj = new HttpParamObject();
            obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE);
            obj.setPostMethod();
            try {
                obj.setJson(f.getSessionJson().toString());
                obj.setJSONContentType();
                objects.add(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        executeTask(AppConstants.TASKCODES.MARK_ATTENDANCE, objects);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_previous_day:
                setDataBasedOnDate(getDate(-1));
                break;
            case R.id.tv_next_day:
                setDataBasedOnDate(getDate(1));
                break;
            case R.id.tv_new_session:
                String date = getDate(0);
                addSessionToLayout(null, date, true, mapAttendanceFragments.get(date).size());
                break;
        }
    }
}
