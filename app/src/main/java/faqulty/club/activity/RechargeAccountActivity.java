package faqulty.club.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import faqulty.club.R;

import simplifii.framework.activity.BaseActivity;

public class RechargeAccountActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_account);

        initToolBar("FacultyClub");

        TextView tvRecharge = (TextView) findViewById(R.id.tv_recharge_account);
        tvRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(RechargeAccountActivity.this);
                dialog.setContentView(R.layout.dialog_recharge);
                dialog.show();
            }
        });
    }
}
