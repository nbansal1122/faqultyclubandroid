package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import faqulty.club.AttendenceActivity;
import faqulty.club.R;
import faqulty.club.Util.ApiGenerator;
import faqulty.club.fragments.dialogs.SendReminderDialogGragment;
import faqulty.club.models.BaseApi;
import faqulty.club.models.transaction.NewInvoiceData;
import faqulty.club.models.transaction.NewInvoiceResponse;
import faqulty.club.models.transaction.ReminderData;
import faqulty.club.models.transaction.Transaction;
import faqulty.club.models.creategroup.PupilInfo;
import faqulty.club.models.tutorgroup.TutorGroupData;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.tutorstudent.StudentProfileResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class StudentInvoiceActivity extends BaseActivity {

    private int studentId;
    private StudentProfileResponse studentProfile;
    private LinearLayout invoiceList;
    private boolean isGroupStudent;

    private static final String NOT_MENTIONED = "Not Mentioned";
    private NewInvoiceData newInvoiceData;
    private long outstanding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_invoice);
        invoiceList = (LinearLayout) findViewById(R.id.ll_list_invoices);
        studentId = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getStudentProfile();
        getInvoices();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lay_request_invoice:
            case R.id.frame_create_invoice:
                createTransaction();
                break;
            case R.id.rl_view_edit_attendance:
//                startAttendanceActivity(studentProfile.getStudent());
                startViewAttendanceActivity(studentProfile.getStudent());
                break;
            case R.id.iv_edit_tution_details:
                startInviteActivity(studentProfile.getStudent());
                break;
            case R.id.lay_received_invoice:
                startReciveInvoiceActivity(newInvoiceData, null);
                break;
            case R.id.lay_reminder_invoice:
                if (outstanding <= 0) {
                    showToast("Reminder not sent as there is no outstanding balance");
                    return;
                }
                showReminderDialog();
                break;
        }
    }

    private void showReminderDialog() {
        SendReminderDialogGragment.showDialog(new SendReminderDialogGragment.SendReminderListener() {
            @Override
            public void onSubmit(ReminderData reminderData) {
                sendReminder(reminderData);
            }

            @Override
            public void onCancel() {

            }
        }, getSupportFragmentManager());
    }

    private void sendReminder(ReminderData reminderData) {
        HttpParamObject httpParamObject = ApiGenerator.sendReminder(reminderData.getType(), reminderData.getMode(), studentId, reminderData.getNotificationType(), 0);
        executeTask(AppConstants.TASKCODES.GET_STUDENT_TRANSACTION_REMINDER, httpParamObject);
    }

    private void getInvoices() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(String.format(AppConstants.PAGE_URL.GET_STUDENT_TRANSACTION, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
        obj.setClassType(NewInvoiceResponse.class);
        executeTask(AppConstants.TASKCODES.GET_STUDENT_TRANSACTION, obj);
    }

    private void getStudentProfile() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(String.format(AppConstants.PAGE_URL.GET_STUDENT_DATA, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
        obj.setClassType(StudentProfileResponse.class);
        executeTask(AppConstants.TASKCODES.GET_STUDENT_DATA, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_STUDENT_DATA:
                if (response != null) {
                    studentProfile = (StudentProfileResponse) response;
                    setViews();
                }
                break;
            case AppConstants.TASKCODES.GET_STUDENT_TRANSACTION:
                NewInvoiceResponse newInvoiceResponse = (NewInvoiceResponse) response;
                if (newInvoiceResponse != null) {
                    NewInvoiceData newInvoiceData = newInvoiceResponse.getData();
                    if (newInvoiceData != null) {
                        setInvoicesData(newInvoiceData);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_STUDENT_TRANSACTION_REMINDER:
                if (response != null) {
                    BaseApi baseApi = (BaseApi) response;
                    if (baseApi != null) {
                        String msg = baseApi.getMsg();
                        if (!TextUtils.isEmpty(msg)) {
                            showToast(msg);
                        }
                    }
                    getInvoices();
                }
                break;
        }
    }
//    private void initInvoiceSection(List<Invoice> invoiceList) {
//        if (invoiceList != null && invoiceList.size() > 0) {
//            this.invoiceList.removeAllViewsInLayout();
//            showVisibility(R.id.ll_list_invoices);
//            showVisibility(R.id.rl_unpaid_invoice);
//            hideVisibility(R.id.frame_create_invoice);
//            for (Invoice invoice : invoiceList) {
//                addInvoiceRow(invoice);
//            }
//        } else {
//            hideVisibility(R.id.ll_list_invoices);
//            hideVisibility(R.id.rl_unpaid_invoice);
//            showVisibility(R.id.frame_create_invoice);
//        }
//    }

    private void setInvoicesData(NewInvoiceData newInvoiceData) {
        this.newInvoiceData = newInvoiceData;
        outstanding = newInvoiceData.getOutstanding();
        setText(getString(R.string.amount_rs, outstanding + ""), R.id.tv_out_standing_balence);
        List<Transaction> transactions = newInvoiceData.getTransactions();
        if (transactions != null && transactions.size() > 0) {
            this.invoiceList.removeAllViewsInLayout();
            showVisibility(R.id.ll_list_invoices);
            showVisibility(R.id.rl_unpaid_invoice);
            showVisibility(R.id.lay_table);
            hideVisibility(R.id.frame_create_invoice);
            for (Transaction transaction : transactions) {
                addInvoiceRow(transaction);
            }
        } else {
            hideVisibility(R.id.ll_list_invoices);
            hideVisibility(R.id.rl_unpaid_invoice);
            hideVisibility(R.id.lay_table);
            showVisibility(R.id.frame_create_invoice);
        }
    }

    private void setViews() {
        setOnClickListener(R.id.frame_create_invoice, R.id.lay_request_invoice, R.id.iv_edit_tution_details, R.id.iv_edit_tution_details_group, R.id.rl_view_edit_attendance, R.id.lay_received_invoice, R.id.lay_reminder_invoice);
        StudentBaseClass student = studentProfile.getStudent();
        if (student != null) {
            initHeaderSection(student);
            initTypeSection(student);
            initToolBar(student.getDisplayName());
        }
//        initInvoiceSection(studentProfile.getInvoices());
    }

    private void initHeaderSection(StudentBaseClass student) {
        ImageView imageView = (ImageView) findViewById(R.id.iv_profile_pic);
        if (!TextUtils.isEmpty(student.getImage())) {
            Picasso.with(this).load(Util.getCompleteUrl(student.getImage())).placeholder(R.mipmap.accountselected2x).into(imageView);
        }

        if (!TextUtils.isEmpty(student.getDateOfBirth())) {
            String dob = Util.convertUTCDateFormat(student.getDateOfBirth(), Util.DOB_SERVER_FORMAT, Util.DOB_UI_FORMAT);
            setText(dob, R.id.tv_dob_info);
        } else {
            hideVisibility(R.id.tv_dob_info);
        }
        setText(student.getEmail(), R.id.tv_email_info);
        setText(student.getPhone(), R.id.tv_phone_info);
    }

    @Override
    public void setText(String text, int textViewId) {
        if (TextUtils.isEmpty(text)) {
            super.setText(getString(R.string.not_mentioned), textViewId);
        } else {
            super.setText(text, textViewId);
        }

    }

    private void initTypeSection(StudentBaseClass student) {
        if (studentProfile.isGroupStudent()) {
            isGroupStudent = true;
            hideVisibility(R.id.layout_type_individual);
            showVisibility(R.id.layout_type_group);
            initGroupSection();
        } else {
            showVisibility(R.id.layout_type_individual);
            hideVisibility(R.id.layout_type_group);
            initIndividualSection(studentProfile.getPupilInfo());
        }
    }

    private void initGroupSection() {
        setText(getString(R.string.group), R.id.tv_type_group);
        hideVisibility(R.id.iv_edit_tution_details_group);
        StringBuilder builder = new StringBuilder();
        for (TutorGroupData data : studentProfile.getGroups()) {
            builder.append(data.getName() + ", ");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
            builder.deleteCharAt(builder.length() - 1);
            setText(builder.toString(), R.id.tv_groups_info);
        } else {
            hideVisibility(R.id.tv_groups_info, R.id.tv_groups);
        }

//        setOnClickListener(R.id.iv_edit_tution_details_group);
    }

    private void initIndividualSection(PupilInfo info) {
        if (info != null) {
            setText((info.getFee() == null ? NOT_MENTIONED : info.getFee() + ""), R.id.tv_fee_info);
            setText(info.getBillingCycle() + "", R.id.tv_billing_info);
            if (null != info.getClassDurationMins() && null != info.getClassDurationHours())
                setText(info.getClassDurationHours() + " Hours " + info.getClassDurationMins() + " Minutes", R.id.tv_class_duration);
            else if (null == info.getClassDurationMins() && null == info.getClassDurationHours()) {
                setText(NOT_MENTIONED, R.id.tv_class_duration);
            } else {
                setText(get0IfNull(info.getClassDurationHours()) + " Hours " + get0IfNull(info.getClassDurationMins()) + " Minutes", R.id.tv_class_duration);
            }
            setOnClickListener(R.id.iv_edit_tution_details);
        }
    }

    private int get0IfNull(Integer value) {
        if (value == null) {
            return 0;
        }
        return value;
    }

    private void startInviteActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, sbc.getId());
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, sbc);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, sbc.isGroupStudent());
        startNextActivityForResult(bundle, InviteStudentsActivity.class, AppConstants.REQUEST_CODES.INVITESTUDENT);
    }

    private void addInvoiceRow(final Transaction transaction) {
        View row = LayoutInflater.from(this).inflate(R.layout.row_previous_invoices_new, null);
        String createdOn = transaction.getCreatedOn();
        if (!TextUtils.isEmpty(createdOn)) {
            String createdDate = Util.convertDateFormat(createdOn, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.INVOICE_NEW_DATE_PATTERN);
            setText(createdDate, R.id.tv_date, row);
        }

        setText(getString(R.string.amount_rs, String.valueOf(transaction.getAmount())), R.id.tv_amount, row);
        boolean flag = true;
        if (getString(R.string.request).equalsIgnoreCase(transaction.getType())) {
            setText(getString(R.string.requested), R.id.tv_activity, row);
        } else if (getString(R.string.reminder).equalsIgnoreCase(transaction.getType())) {
            flag = false;
            setText(getString(R.string.reminded), R.id.tv_activity, row);
        } else {
            setText(getString(R.string.received_payment), R.id.tv_activity, row);
        }
        if (flag) {
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startEditInvoice(transaction);
                }
            });
        }
        invoiceList.addView(row);
    }


    private void startViewAttendanceActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT, sbc);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, isGroupStudent);
        if (sbc.isGroupStudent()) {
            startNextActivityForResult(bundle, ViewAttendanceActivity.class, AppConstants.REQUEST_CODES.VIEW_ATTENDANCE);
        } else {
            startNextActivityForResult(bundle, ViewAttendanceActivity.class, AppConstants.REQUEST_CODES.VIEW_ATTENDANCE);
        }
    }

    private void startAttendanceActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT, sbc);
        if (sbc.isGroupStudent()) {
            startNextActivity(bundle, AttendenceActivity.class);
        } else {
            startNextActivity(bundle, AttendenceActivity.class);
        }
    }

    private void startReciveInvoiceActivity(NewInvoiceData newInvoiceData, Transaction transaction) {
        if (newInvoiceData == null) {
            return;
        }
        Bundle bundle = new Bundle();
        if (transaction != null) {
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, transaction);
        }
        bundle.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, studentId);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE, true);
        bundle.putLong(AppConstants.BUNDLE_KEYS.PENDING_AMOUNT, newInvoiceData.getOutstanding());
        Intent i = new Intent(this, ReceivePaymentActivity.class);
        i.putExtras(bundle);
        startActivityForResult(i, AppConstants.REQUEST_CODES.CREATE_INVOICE);
    }

    private void createTransaction() {
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT_DATA, studentProfile.getStudent());
        b.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, studentId);
        if (newInvoiceData != null) {
            b.putLong(AppConstants.BUNDLE_KEYS.PENDING_AMOUNT, newInvoiceData.getOutstanding());
        }
        if (studentProfile.isGroupStudent() && studentProfile.getGroups() != null && studentProfile.getGroups().size() > 0) {
            TutorGroupData data = studentProfile.getGroups().get(0);
            b.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, true);
            b.putInt(AppConstants.BUNDLE_KEYS.GROUP_ID, data.getId());
        }
        Intent i = new Intent(this, CreateNewInvoiceActivity.class);
        i.putExtras(b);
        startActivityForResult(i, AppConstants.REQUEST_CODES.CREATE_INVOICE);
    }

    private void startEditInvoice(Transaction transaction) {
        String type = transaction.getType();
        if (type.equals(getString(R.string.request))) {
            Bundle b = new Bundle();
            b.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, studentId);
            if (newInvoiceData != null) {
                b.putLong(AppConstants.BUNDLE_KEYS.PENDING_AMOUNT, newInvoiceData.getOutstanding());
                b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, transaction);
                b.putBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE, true);
                b.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT_DATA, studentProfile.getStudent());
            }
            if (studentProfile.isGroupStudent() && studentProfile.getGroups() != null && studentProfile.getGroups().size() > 0) {
                TutorGroupData data = studentProfile.getGroups().get(0);
                b.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, true);
                b.putInt(AppConstants.BUNDLE_KEYS.GROUP_ID, data.getId());
            }
            Intent i = new Intent(this, CreateNewInvoiceActivity.class);
            i.putExtras(b);
            startActivityForResult(i, AppConstants.REQUEST_CODES.CREATE_INVOICE);
        } else {
            startReciveInvoiceActivity(newInvoiceData, transaction);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.CREATE_INVOICE:
                GenericLocalReceiver.sendBroadcast(this, GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH);
                getInvoices();
                break;
            case AppConstants.REQUEST_CODES.INVITESTUDENT:
            case AppConstants.REQUEST_CODES.VIEW_ATTENDANCE:
                if (resultCode == RESULT_OK) {
                    getStudentProfile();
                }
                break;
        }
    }
}
