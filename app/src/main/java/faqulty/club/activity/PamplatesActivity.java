package faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.pemphlets.AllPamphletsData;
import faqulty.club.models.pemphlets.AllPamphletsResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class PamplatesActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<AllPamphletsData> pamphletList = new ArrayList<>();
    CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pamphlets);
        initToolBar(getString(R.string.pemphlets));
        ListView lvPamphlet = (ListView) findViewById(R.id.lv_pamplete);
        customListAdapter = new CustomListAdapter(this, R.layout.row_website_template, pamphletList, this);
        lvPamphlet.setAdapter(customListAdapter);
        lvPamphlet.setOnItemClickListener(this);
        setHeader(lvPamphlet);
    }

    private void setHeader(ListView listView) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.row_website_template, null);
        listView.addHeaderView(view);
        Holder holder = new Holder(view);
        holder.bindData("Saved Pamphlets", "Find all previously created pamphlets", R.mipmap.folder);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNextActivity(SavedPamplatesActivity.class);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllPamphlets();
    }

    private void getAllPamphlets() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.FEATCH_ALL_PAMPHLETS);
        httpParamObject.setClassType(AllPamphletsResponse.class);
        executeTask(AppConstants.TASKCODES.FEATCH_ALL_PAMPHLETS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.FEATCH_ALL_PAMPHLETS:
                AllPamphletsResponse allPamphletsResponse = (AllPamphletsResponse) response;
                if (allPamphletsResponse != null) {
                    List<AllPamphletsData> allPamphletsDataList = allPamphletsResponse.getData();
                    pamphletList.addAll(allPamphletsDataList);
                    customListAdapter.notifyDataSetChanged();
                    allPamphletsResponse.save();
                }
                break;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final AllPamphletsData allPamphletsData = pamphletList.get(position);
        String sample = allPamphletsData.getSample();
        holder.bindData(allPamphletsData.getTitle(), allPamphletsData.getDescription(), sample);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, allPamphletsData);
                startNextActivity(bundle, CreateTemplateActivity.class);
            }
        });
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        startNextActivity(CreateTemplateActivity.class);
    }

    class Holder {
        TextView tvTitle, tvSubtitle;
        ImageView ivTImage;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.template_title);
            tvSubtitle = (TextView) view.findViewById(R.id.subtitle_template);
            ivTImage = (ImageView) view.findViewById(R.id.template_image);
        }

        public void bindData(String title, String description, String imageUrl) {
            tvTitle.setText(title);
            tvSubtitle.setText(description);
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(PamplatesActivity.this).load(Util.getCompleteUrl(imageUrl)).into(ivTImage);
            }
        }
        public void bindData(String title, String description, int imageId) {
            tvTitle.setText(title);
            tvSubtitle.setText(description);
            ivTImage.setImageResource(imageId);
        }
    }
}
