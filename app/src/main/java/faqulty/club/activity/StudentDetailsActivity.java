package faqulty.club.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.transaction.Invoice;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;

public class StudentDetailsActivity extends BaseActivity {

    LinearLayout ll_invoices;
    LayoutInflater inflater;
    List<Invoice> invoiceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);
        initToolBar("Abish Mathew");
        initData();
        ll_invoices = (LinearLayout) findViewById(R.id.ll_list_invoices);
        inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setData();
        ImageView iv_edit = (ImageView) findViewById(R.id.iv_edit_tution_details);
        TextView tv_generate_invoice = (TextView) findViewById(R.id.tv_generate_invoice);
        tv_generate_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(CreateNewInvoiceActivity.class);
            }
        });
        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(EditStudentDetailsActivity.class);
            }
        });
    }

    private void setData() {
        for (int y = 0; y < invoiceList.size(); y++) {
            Invoice details = invoiceList.get(y);
            View view = inflater.inflate(R.layout.row_previous_invoices, null);
            setText(details.getInvoiceNum(), R.id.tv_invoice, view);
            ll_invoices.addView(view);
        }
    }

    private void initData() {
        invoiceList.clear();
        for (int x = 0; x < 3; x++) {
            Invoice invoiceDetails = new Invoice();
            invoiceDetails.setInvoiceNum("Invoice" + x);
            invoiceDetails.setImageNext(R.mipmap.rightarrowgrey);
            invoiceList.add(invoiceDetails);
        }
    }
}