package faqulty.club.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import faqulty.club.R;
import faqulty.club.models.BaseApi;
import faqulty.club.models.transaction.Transaction;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class ReceivePaymentActivity extends BaseActivity {
    private boolean isNotEditable;
    private int studentId;
    private long paymentDue;
    private double discountAmount, paidAmount, carryForwardAmount;
    private Transaction transaction;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_payment);
        initToolBar(getString(R.string.title_receive_payment));
        initBundleAndViews();
        if (isNotEditable) {
            hideVisibility(R.id.btn_receive_payment);
        } else {
            setOnClickListener(R.id.btn_receive_payment);
        }
        initViews();
    }

    private void initViews() {
        setEditText(R.id.et_balance_carried, "0");
        setUneditable(R.id.et_balance_carried);
        setTextWatcherOnAmountPaid();
    }

    private void setTextWatcherOnAmountPaid() {
        EditText amountPaid = (EditText) findViewById(R.id.et_amount_paid);
        amountPaid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null) {
                    if (s.length() > 0) {
                        double amountPaid = Double.parseDouble(s.toString());
                        fillCarryForward(amountPaid, 0);
                    } else {
                        fillCarryForward(0, 0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

//    private void setTextWatcherOnDiscount() {
//        EditText discount = (EditText) findViewById(R.id.et_discount);
//        discount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s != null && s.length() > 0) {
//                    double discount = Double.parseDouble(s.toString());
//                    fillCarryForward(paidAmount, discount);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//    }

    private void fillCarryForward(double amountPaid, double discount) {
        this.paidAmount = amountPaid;
//        this.discountAmount = getDoubleFromEt(R.id.et_discount);
        double carryForward = paymentDue - (amountPaid + discount);
        setEditText(R.id.et_balance_carried, carryForward + "");
    }

    private void setUneditable(int... editTextIds) {
        for (int i : editTextIds) {
            EditText et = (EditText) findViewById(i);
            et.setEnabled(false);
            et.setFocusable(false);
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_receive_payment:
                receivePayment();
                break;
        }
    }

    private void receivePayment() {
        if (isValid()) {
            HttpParamObject obj = new HttpParamObject();
            if (isEdit) {
                obj.setUrl(String.format(AppConstants.PAGE_URL.CREATE_NEW_TRANSACTION, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)) + "/" + transaction.getId());
                obj.setPutMethod();
            } else {
                obj.setUrl(String.format(AppConstants.PAGE_URL.CREATE_NEW_TRANSACTION, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
                obj.setPostMethod();
            }
            obj.setClassType(BaseApi.class);
            obj.setJSONContentType();
            obj.setJson(getJSONForPayment());
            executeTask(AppConstants.TASKCODES.PAYMENT, obj);
        }
    }

    private String getJSONForPayment() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("type", getString(R.string.received));
            obj.put("amount", (paidAmount));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.PAYMENT:
                BaseApi api = (BaseApi) response;
                if (api != null) {
                    showToast(R.string.success_invoice_payment);
                    GenericLocalReceiver.sendBroadcast(ReceivePaymentActivity.this, GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case AppConstants.TASKCODES.DELETE_INVOICE:
                setResult(RESULT_OK);
                finish();
                break;
        }

    }

    private boolean isValid() {
        paidAmount = getDoubleFromEt(R.id.et_amount_paid);
        carryForwardAmount = getDoubleFromEt(R.id.et_balance_carried);

        if (paidAmount == 0) {
            showToast(R.string.error_empty_invoice_amount_0);
            return false;
        }
//        if (discountAmount > paymentDue) {
//            showToast(R.string.error_invalid_discount_entered);
//            return false;
//        }
//        double carryFrwd = paymentDue - discountAmount - paidAmount;
//        if (carryForwardAmount != carryFrwd) {
//            showToast(getString(R.string.invalid_balance_amount));
//            return false;
//        }
        return true;
    }

    private double getDoubleFromEt(int etID) {
        String text = getEditText(etID);
        if (TextUtils.isEmpty(text)) {
            return 0;
        } else {
            return Double.valueOf(text);
        }
    }

    private void initBundleAndViews() {
        Bundle bundle = getIntent().getExtras();
        transaction = (Transaction) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if (transaction != null) {
            isEdit = true;
            paymentDue = bundle.getLong(AppConstants.BUNDLE_KEYS.PENDING_AMOUNT);
            setText(transaction.getAmount() + "", R.id.et_amount_paid);
            setText(getString(R.string.update), R.id.btn_receive_payment);
        } else {
            paymentDue = bundle.getLong(AppConstants.BUNDLE_KEYS.PENDING_AMOUNT);
        }
        studentId = bundle.getInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID);
        isNotEditable = bundle.getBoolean(AppConstants.BUNDLE_KEYS.IS_NOT_EDITABLE);
        setText(getString(R.string.amount_rs, paymentDue + ""), R.id.et_payment_due);
        if (paidAmount > 0) {
            setEditText(R.id.et_balance_carried, paymentDue + "");
        } else {
            setEditText(R.id.et_balance_carried, "0");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isEdit) {
            getMenuInflater().inflate(R.menu.menu_invoice, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteTransaction();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteTransaction() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.DELETE_TRANSACTION + transaction.getId());
        obj.setDeleteMethod();
        executeTask(AppConstants.TASKCODES.DELETE_INVOICE, obj);
    }
}
