package faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.pemphlets.UserPamphletsData;
import faqulty.club.models.pemphlets.UserPamphletsResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class SavedPamplatesActivity extends BaseActivity implements CustomListAdapterInterface {
    private static final int RESULT_CODE_EDIT = 10;
    List<UserPamphletsData> pamphletList = new ArrayList<>();
    CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pamphlets);
        initToolBar(getString(R.string.saved_pemphlets));
        ListView lv_pamphlet = (ListView) findViewById(R.id.lv_pamplete);
        lv_pamphlet.setEmptyView(findViewById(R.id.tv_empty));
        customListAdapter = new CustomListAdapter(this, R.layout.row_website_template, pamphletList, this);
        lv_pamphlet.setAdapter(customListAdapter);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getUserPamphlets();
    }

    private void getUserPamphlets() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.FEATCH_USER_PAMPGLETS, Preferences.getUserId()));
        httpParamObject.setClassType(UserPamphletsResponse.class);
        executeTask(AppConstants.TASKCODES.FEATCH_USER_PAMPHLETS, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.FEATCH_USER_PAMPHLETS:
                UserPamphletsResponse userPamphletsResponse = (UserPamphletsResponse) response;
                if (userPamphletsResponse != null) {
                    List<UserPamphletsData> data = userPamphletsResponse.getData();
                    if (data != null) {
                        pamphletList.clear();
                        pamphletList.addAll(data);
                        customListAdapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final UserPamphletsData userPamphletsData = pamphletList.get(position);
        holder.bindData(userPamphletsData.getTitle(), userPamphletsData.getDescription(), userPamphletsData.getUrl());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.PAMPHLET_DATA,userPamphletsData);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.IS_EDITABLE,true);
                startNextActivityForResult(bundle,PamphletPreviewActivity.class,RESULT_CODE_EDIT);
            }
        });
        return convertView;
    }

    class Holder {
        TextView tvTitle, tvSubtitle;
        ImageView ivTImage;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.template_title);
            tvSubtitle = (TextView) view.findViewById(R.id.subtitle_template);
            ivTImage = (ImageView) view.findViewById(R.id.template_image);
        }

        public void bindData(String title, String description, String imageUrl) {
            tvTitle.setText(title);
            tvSubtitle.setText(description);
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(SavedPamplatesActivity.this).load(Util.getCompleteUrl(imageUrl)).into(ivTImage);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            getUserPamphlets();
        }
    }
}
