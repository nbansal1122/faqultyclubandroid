package faqulty.club.activity;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.ApiGenerator;
import faqulty.club.Util.FileUtil;
import faqulty.club.fragments.bottomsheets.AssignmentShareBottomSheet;
import faqulty.club.fragments.bottomsheets.QuestionShareFragment;
import faqulty.club.models.BasicResponse;
import faqulty.club.models.assignment.ClassModel;
import faqulty.club.models.assignment.QuestionInfo;
import faqulty.club.models.assignment.QuestionListResponse;
import faqulty.club.models.assignment.chapters.ChapterData;
import faqulty.club.models.assignment.sharing.PDFLinkResponse;
import faqulty.club.models.assignment.subjects.SubjectData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Dhillon on 02-12-2016.
 */

public class AssignmentQuestionActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private SubjectData subjectData;
    private ChapterData chapterData;
    private ClassModel assignmentModel;
    private ListView listView;
    private CustomListAdapter<QuestionInfo> adapter;
    private List<QuestionInfo> list = new ArrayList<>();
    private List<QuestionInfo> copyList = new ArrayList<>();
    private String subtitle;
    private int actType;
    private int assignmentId;
    private LinearLayout llQuestions;
    private Set<String> selectedLabels = new HashSet<>();

    private String selectedLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        initToolBar("Assignments");
        if (chapterData != null && !TextUtils.isEmpty(chapterData.getChapterName())) {
            subtitle = "Class " + assignmentModel.getClass_() + " / " + subjectData.getDegreeSubject() + " / " +
                    chapterData.getChapterName();
            getSupportActionBar().setSubtitle(subtitle);
        }
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        if (bundle != null) {
            assignmentModel = (ClassModel) bundle.get(AppConstants.BUNDLE_KEYS.CLASS);
            subjectData = (SubjectData) bundle.get(AppConstants.BUNDLE_KEYS.SUBJECT);
            chapterData = (ChapterData) bundle.get(AppConstants.BUNDLE_KEYS.CHAPTERS);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setListView();
        fetchAssignments();
    }

    private void addAllChips(HashSet<String> chips) {
        List<String> sortedList = new ArrayList();
        if(chips.contains("#MCQ")){
            sortedList.add("#MCQ");
        }
        if(chips.contains("#v-short".toUpperCase())){
            sortedList.add("#v-short".toUpperCase());
        }
        if(chips.contains("#short".toUpperCase())){
            sortedList.add("#short".toUpperCase());
        }
        if(chips.contains("#long".toUpperCase())){
            sortedList.add("#long".toUpperCase());
        }
        if(chips.contains("#hots".toUpperCase())){
            sortedList.add("#hots".toUpperCase());
        }
        if(chips.contains("#easy".toUpperCase())){
            sortedList.add("#easy".toUpperCase());
        }
        if(chips.contains("#med".toUpperCase())){
            sortedList.add("#med".toUpperCase());
        }
        if(chips.contains("#hard".toUpperCase())){
            sortedList.add("#hard".toUpperCase());
        }
        chips.removeAll(sortedList);
        sortedList.addAll(chips);
        llQuestions = (LinearLayout) findViewById(R.id.ll_questions);
        llQuestions.removeAllViewsInLayout();
        if (sortedList != null && sortedList.size() > 0) {
            llQuestions.setVisibility(View.VISIBLE);
            for (String chip : sortedList) {
                TextView tv = getChipButtonTile(chip);
                llQuestions.addView(tv);
            }
        } else {
            llQuestions.setVisibility(View.GONE);
        }
    }

    private TextView getChipButtonTile(final String label) {
        View v = LayoutInflater.from(this).inflate(R.layout.row_chip_header_label, null);
        TextView tv = (TextView) v.findViewById(android.R.id.text1);
        if (selectedLabels.contains(label)) {
            tv.setBackgroundResource(R.drawable.chip_label_selected);
            tv.setTextColor(getResourceColor(R.color.white));
        } else {
            tv.setTextColor(getResourceColor(R.color.black));
            tv.setBackgroundResource(R.drawable.chip_label_unselected);
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int margin = getResources().getDimensionPixelOffset(R.dimen.margin_4dp);
        params.setMargins(margin, 0, margin, 0);
        tv.setLayoutParams(params);
        tv.setTag(label);
        tv.setText(label);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lbl = (String) v.getTag();
                if (selectedLabels.contains(lbl)) {
                    selectedLabels.remove(lbl);
                } else {
                    selectedLabels.add(lbl);
                }
                setLabels();
            }
        });
        return tv;
    }


    private void setLabels() {
        HashSet<String> set = new HashSet<>();
        list.clear();
        for (QuestionInfo info : copyList) {
            HashSet<String> hashTagSet = info.getHashTagSet();
            set.addAll(info.getHashTagSet());
            boolean flag = true;
            for (String s : selectedLabels) {
                if (!hashTagSet.contains(s)) {
                    flag = false;
                }
            }
            if (flag) {
                list.add(info);
            }
        }
        if (list.size() > 0) {
            hideVisibility(R.id.tv_empty);
        } else {
            showVisibility(R.id.tv_empty);
        }
        addAllChips(set);
        adapter.notifyDataSetChanged();
    }

    private void setListView() {
        listView = (ListView) findViewById(R.id.lv_questions);
        adapter = new CustomListAdapter<>(this, R.layout.row_question_with_label, list, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    private void fetchAssignments() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.ASSIGNMENTS);
//        class=5&subject=8&chapter=3
        obj.addParameter("class", assignmentModel.getId() + "");
        obj.addParameter("subject", subjectData.getId() + "");
        obj.addParameter("chapter", chapterData.getChapterId() + "");
        obj.setClassType(QuestionListResponse.class);
        executeTask(AppConstants.TASKCODES.ASSIGNMENTS, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.ASSIGNMENTS:
                if (response != null) {
                    QuestionListResponse r = (QuestionListResponse) response;
                    if (r.getData() != null) {
                        list.clear();
                        copyList.addAll(r.getData());
                        for (QuestionInfo info : copyList) {
                            info.initHashTagList();
                        }
                        setLabels();
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_ASSIGNMENT_LINK: {
                PDFLinkResponse pdfLinkResponse = (PDFLinkResponse) response;
                if (pdfLinkResponse != null && pdfLinkResponse.getData() != null) {
                    if (!TextUtils.isEmpty(pdfLinkResponse.getData().getUri()))
                        takeActionOnUrl(pdfLinkResponse.getData().getUri());
                }
            }
            break;
            case AppConstants.TASKCODES.SHARE_EMAIL:
                BasicResponse basicResponse= (BasicResponse) response;
                if(basicResponse!=null&&basicResponse.getStatus()){
                    showToast(basicResponse.getMsg());
                }
                break;

        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final QuestionInfo info = list.get(position);
        holder.name.setText(info.getName());
        holder.label.setText(info.getDisplayHashTagString());
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuestionSharing(info);
            }
        });
        return convertView;
    }


    private class ViewHolder {
        TextView name, label;
        ImageView icon;

        public ViewHolder(View v) {
            name = (TextView) v.findViewById(R.id.tv_question_set_name);
            label = (TextView) v.findViewById(R.id.tv_question_set_label);
            icon = (ImageView) v.findViewById(R.id.iv_share);
        }
    }

    private void showQuestionSharing(final QuestionInfo info) {
        QuestionShareFragment saveShareFragment = QuestionShareFragment.newInstance(new QuestionShareFragment.QuestionShareInterface() {
            @Override
            public void onActionClick(int actType) {
                showAssignmentSharingSheet(info, actType);
            }
        });
        saveShareFragment.show(getSupportFragmentManager(), saveShareFragment.getTag());
    }

    private void showAssignmentSharingSheet(final QuestionInfo info, final int questionOrAnswerKey) {
        AssignmentShareBottomSheet saveShareFragment = AssignmentShareBottomSheet.newInstance(new AssignmentShareBottomSheet.BottomSheetInterface() {
            @Override
            public void onActionClick(int actType) {
                AssignmentQuestionActivity.this.assignmentId = info.getId();
                AssignmentQuestionActivity.this.actType = actType;
                onAssignmentBottomSheetClicked(actType, questionOrAnswerKey);
            }
        });
        saveShareFragment.show(getSupportFragmentManager(), saveShareFragment.getTag());
    }

    private void onAssignmentBottomSheetClicked(int actType, final int questionOrAnswerKey) {
        switch (actType) {
            case AppConstants.ACTION_TYPE.BOOKMARK:
                Bundle b = new Bundle();
                b.putInt(AppConstants.BUNDLE_KEYS.MODULE_ID, assignmentId);
                b.putString(AppConstants.BUNDLE_KEYS.MODULE_TYPE, "assignment");
                startNextActivity(b, SaveBookMarkActivity.class);
                break;
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED:
            case AppConstants.ACTION_TYPE.SHARE_FEED:

                if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_QUESTION_SET) {
                    downloadAssignmentToShare("questions");
                } else if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_ANSWER_KEY) {
                    downloadAssignmentToShare("fullset");
                }
                break;
            case AppConstants.ACTION_TYPE.EMAIL:
                if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_QUESTION_SET) {
                    HttpParamObject httpParamObject = ApiGenerator.sareToEmail(assignmentId, "assignment", "questions");
                    executeTask(AppConstants.TASKCODES.SHARE_EMAIL, httpParamObject);
                } else if (questionOrAnswerKey == AppConstants.ACTION_TYPE.SHARE_ANSWER_KEY) {
                    HttpParamObject httpParamObject = ApiGenerator.sareToEmail(assignmentId, "assignment", "fullset");
                    executeTask(AppConstants.TASKCODES.SHARE_EMAIL, httpParamObject);
                }
                break;
        }
    }

    private void downloadAssignmentToShare(String type) {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(String.format(AppConstants.PAGE_URL.DOWNLOAD_ASSIGNMENT_LINK, String.valueOf(assignmentId)));
        obj.addParameter("type", type);
        obj.setClassType(PDFLinkResponse.class);
        executeTask(AppConstants.TASKCODES.GET_ASSIGNMENT_LINK, obj);
    }

    private void takeActionOnUrl(String url) {
        url = new StringBuilder().append(url).append("&user_id=").append(Preferences.getUserId()).append("&"+AppConstants.X_ACCESS_TOKEN+"=").append(Preferences.getData(AppConstants.PREF_KEYS.USER_TOKEN, "")).toString();
        switch (actType) {
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED: {
                DownloadManager manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                FileUtil.downloadFile(manager, url);
            }
            break;
            case AppConstants.ACTION_TYPE.SHARE_FEED: {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(i, "Choose an app to share"));
            }
            break;
//            case AppConstants.ACTION_TYPE.EMAIL:
//                HttpParamObject httpParamObject = ApiGenerator.sareToEmail(assignmentId, "assignment", "fullset");
//                executeTask(AppConstants.TASKCODES.SHARE_EMAIL, httpParamObject);
//            {
//                UserDetailsApi api = UserDetailsApi.getInstance();
//                Util.shareViaEmail(this, api.getEmail(), "The Faculty Club", url);
//            }
//            break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        QuestionInfo info = list.get(position);
        Bundle b = new Bundle();
        b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, info.getName());
        b.putString(AppConstants.BUNDLE_KEYS.KEY_SUBTITLE, subtitle);
        b.putInt(AppConstants.BUNDLE_KEYS.Q_ID, info.getId());
        startNextActivity(b, QuestionViewActivity.class);
    }
}
