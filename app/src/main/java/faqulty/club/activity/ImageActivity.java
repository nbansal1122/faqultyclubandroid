package faqulty.club.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import faqulty.club.R;
import faqulty.club.fragments.ImageFragment;

import java.util.ArrayList;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;

public class ImageActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface<String> {

    public static final String KEY_IMAGE = "imageKey";
    private static final String KEY_IMAGES = "imagesKey";
    private static final String KEY_POSITION = "position";
    String imageUrl;
    ArrayList<String> images = new ArrayList<>();
    private ViewPager pager;
    private CustomPagerAdapter<String> adapter;

    public static void startActivity(Context ctx, String image) {
        ArrayList<String> images = new ArrayList<>();
        images.add(image);
        startActivity(ctx, images, 0);
    }

    public static void startActivity(Context ctx, ArrayList<String> images, int selectedPosition) {
        Bundle b = new Bundle();
        b.putStringArrayList(KEY_IMAGES, images);
        b.putInt(KEY_POSITION, selectedPosition);
        Intent i = new Intent(ctx, ImageActivity.class);
        i.putExtras(b);
        ctx.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        initToolBar(getString(R.string.app_faq_club));
        int position = getIntent().getIntExtra(KEY_POSITION, 0);
        pager = (ViewPager) findViewById(R.id.viewPager);
        images = getIntent().getStringArrayListExtra(KEY_IMAGES);
        adapter = new CustomPagerAdapter<>(getSupportFragmentManager(), images, this);
        pager.setAdapter(adapter);
        pager.setCurrentItem(position);
    }


    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        return ImageFragment.getInstance(listItem);
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return "";
    }
}
