package faqulty.club.activity;

import android.os.Bundle;
import android.view.View;

import faqulty.club.R;

import io.github.kexanie.library.MathView;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.Util;

public class TestActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        String text = Util.getStringFromAsset(this, "test3.txt");
        String text2 = Util.getStringFromAsset(this, "test3.txt");
        MathView mathView1 = (MathView) findViewById(R.id.formula_one);
        MathView mathView2 = (MathView) findViewById(R.id.formula_two);
        mathView1.setText(text);
        mathView2.setText(text2);
    }

    private void getUserProfile() {

    }

    @Override
    public void onClick(View v) {
    }


}
