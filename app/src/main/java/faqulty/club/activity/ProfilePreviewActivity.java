package faqulty.club.activity;

import android.os.Bundle;

import faqulty.club.R;

import simplifii.framework.activity.BaseActivity;

/**
 * Created by Neeraj Yadav on 9/27/2016.
 */

public class ProfilePreviewActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_preview);
    }
}
