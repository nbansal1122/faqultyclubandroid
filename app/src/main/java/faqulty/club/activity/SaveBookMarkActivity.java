package faqulty.club.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.bookmark.BaseBookmark;
import faqulty.club.models.bookmark.Bookmark;
import faqulty.club.models.bookmark.BookmarkApi;
import faqulty.club.models.bookmark.Data;
import faqulty.club.models.bookmark.FeedBookmark;
import faqulty.club.models.bookmark.FeedBookmarkData;
import faqulty.club.models.bookmark.Folder;
import faqulty.club.models.bookmark.NewBookmarkFolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class SaveBookMarkActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private ListView lvBookmark;
    private CustomListAdapter bookmarkAdapter;
    private List<BookmarkApi> bookMarkList = new ArrayList<>();
    private ArrayList<BaseBookmark> baseBookmarksArrayList = new ArrayList<>();
    private List<Folder> folders;
    private Dialog dialogBookmark;
    private int moduleId;
    private String moduleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_book_mark);
        initToolBar(getString(R.string.save_to_bookmarks));
        setOnClickListener(R.id.btn_bookmark);
        lvBookmark = (ListView) findViewById(R.id.lv_bookmark);
        bookmarkAdapter = new CustomListAdapter(this, R.layout.row_bookmark, baseBookmarksArrayList, this);
        lvBookmark.setAdapter(bookmarkAdapter);
        lvBookmark.setOnItemClickListener(this);
        getBookmarkData();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        moduleId = bundle.getInt(AppConstants.BUNDLE_KEYS.MODULE_ID);
        moduleType = bundle.getString(AppConstants.BUNDLE_KEYS.MODULE_TYPE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bookmark, menu);
        return true;
    }

    private void getBookmarkData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.BOOKMARK_GET));
        httpParamObject.setClassType(BookmarkApi.class);
        executeTask(AppConstants.TASKCODES.GET_BOOKMARK, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_response));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_BOOKMARK:
                BookmarkApi bookmarkApi = (BookmarkApi) response;
                if (bookmarkApi != null && bookmarkApi.getStatus()) {
                    Data data = bookmarkApi.getData();
                    if (data != null) {
                        folders = data.getFolders();
                        if (folders != null) {
                            baseBookmarksArrayList.clear();
                            baseBookmarksArrayList.addAll(folders);
                            bookmarkAdapter.notifyDataSetChanged();
                        }
                    }

                }
                break;
            case AppConstants.TASKCODES.BOOKMARK_FOLDER:
                NewBookmarkFolder newBookmarkFolder = (NewBookmarkFolder) response;
                if (newBookmarkFolder != null) {
                    if (newBookmarkFolder.getStatus().equals(true)) {
                        GenericLocalReceiver.sendBroadcast(this, GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH_BOOKMARKS);
                        showToast(getString(R.string.folder_create_bookmark));
                        getBookmarkData();
                        hideDialog();
                    }
                }
                break;
            case AppConstants.TASKCODES.BOOKMARK_FEED:
                FeedBookmark res = (FeedBookmark) response;
                if (res != null && res.getStatus()) {
                    FeedBookmarkData data = res.getData();
                    if (data != null) {

                    }
                    GenericLocalReceiver.sendBroadcast(this, GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH_BOOKMARKS);
                    showToast(res.getMsg());
                    setResult(RESULT_OK);
                    finish();
                } else {
                    showToast(res.getMsg());
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_bookmark:
                setBookmark(moduleId, moduleType, -1);
                break;
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final BaseBookmark baseBookmark = baseBookmarksArrayList.get(position);
        if (baseBookmark.getView() == AppConstants.VIEW_TYPE.FOLDER) {
            Folder folder = (Folder) baseBookmark;
            if (folder != null) {
                String name = folder.getName();
                holder.tvBookmark.setText(name);
            }
        } else {
            Bookmark bookmark = (Bookmark) baseBookmark;
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(baseBookmark);
            }
        });
        return convertView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_folder:
                showDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog() {
        dialogBookmark = new Dialog(this);
        dialogBookmark.setContentView(R.layout.dialog_bookmark);

        final EditText et_folderName = (EditText) dialogBookmark.findViewById(R.id.et_bookmark_name);
        TextView tv_create = (TextView) dialogBookmark.findViewById(R.id.tv_create);
        TextView tv_cancel = (TextView) dialogBookmark.findViewById(R.id.tv_cancel);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
            }
        });

        tv_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folderName = et_folderName.getText().toString().trim();
                if (TextUtils.isEmpty(folderName)) {
                    showToast(getString(R.string.enter_folder_name));
                    return;
                } else {
                    createBookmark(folderName);
                }
            }
        });

        dialogBookmark.show();
    }

    private void createBookmark(String folderName) {

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.CREATE_BOOKMARK_FOLDER));
        httpParamObject.setPostMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", folderName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(NewBookmarkFolder.class);
        executeTask(AppConstants.TASKCODES.BOOKMARK_FOLDER, httpParamObject);

    }

    private void setBookmark(int moduleId, String moduleType, int folderId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.CREATE_BOOKMARK));
        httpParamObject.setPostMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("moduleId", moduleId);
            jsonObject.put("moduleType", moduleType);
            if (folderId != -1) {
                jsonObject.put("folderId", folderId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(FeedBookmark.class);
        executeTask(AppConstants.TASKCODES.BOOKMARK_FEED, httpParamObject);
    }


    private void hideDialog() {
        if (dialogBookmark != null) {
            dialogBookmark.dismiss();
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null) {
            String message = re.getMessage();
            showToast(message);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        BaseBookmark baseBookmark = baseBookmarksArrayList.get(position);
        onItemClick(baseBookmark);
    }

    private void onItemClick(BaseBookmark baseBookmark) {
        if (baseBookmark.getView() == AppConstants.VIEW_TYPE.FOLDER) {
            Folder folder = (Folder) baseBookmark;
            setBookmark(moduleId, moduleType, folder.getId());
        } else {
            showToast(getString(R.string.select_folder_to_save));
        }
    }


    class Holder {
        TextView tvBookmark;

        public Holder(View view) {
            tvBookmark = (TextView) view.findViewById(R.id.tv_bookmark);
        }
    }


}
