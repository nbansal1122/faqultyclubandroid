package faqulty.club.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.transaction.PaymentModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;

public class SelectPaymentOptionActivity extends AppCompatActivity implements CustomListAdapterInterface {

    private ListView lvPayment;
    private CustomListAdapter customListAdapter;
    private List<PaymentModel> paymentModels = new ArrayList<>();
    private String[] name;
    private Integer[] img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment_option);

        lvPayment = (ListView) findViewById(R.id.lv_payment);
        customListAdapter = new CustomListAdapter(this,R.layout.row_payment_option,paymentModels,this);
        lvPayment.setAdapter(customListAdapter);

        name = new String[]{"Credit/Debit card","Netbanking","MobiKwik Wallet","PayUMoney","Paytm"};

        img = new Integer[]{
                R.mipmap.credit_card,
                R.mipmap.internet,
                R.mipmap.internet,
                R.mipmap.internet,
                R.mipmap.internet,
        };

        setData();
    }

    private void setData() {
        for (int i=0; i<name.length; i++){
            PaymentModel paymentModel = new PaymentModel();
            paymentModel.setTitle(name[i]);
            paymentModel.setLogo(img[i]);
            paymentModels.add(paymentModel);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if(convertView == null){
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        PaymentModel paymentModel = paymentModels.get(position);
        holder.tvTitle.setText(paymentModel.getTitle());
        return convertView;
    }


    class Holder {
        TextView tvTitle;
        ImageView ivIcon;

        public Holder(View view){
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            ivIcon = (ImageView) view.findViewById(R.id.iv_icon);
        }
    }

}
