package faqulty.club.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.BaseApi;
import faqulty.club.models.bookmark.BaseBookmark;
import faqulty.club.models.bookmark.Bookmark;
import faqulty.club.models.bookmark.Folder;
import faqulty.club.models.bookmark.FolderItemResponse;

import java.util.ArrayList;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class FolderItemActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private ListView lvBookmark;
    private CustomListAdapter bookmarkAdapter;
    private ArrayList<BaseBookmark> baseBookmarksArrayList = new ArrayList<>();

    private int folderId;
    private String folderName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_book_mark);
        initToolBar(folderName);
        lvBookmark = (ListView) findViewById(R.id.lv_bookmark);
        hideVisibility(R.id.btn_bookmark);
        bookmarkAdapter = new CustomListAdapter(this, R.layout.row_bookmark, baseBookmarksArrayList, this);
        lvBookmark.setAdapter(bookmarkAdapter);
        lvBookmark.setOnItemClickListener(this);
        baseBookmarksArrayList.clear();
        getFolderData();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        folderName = bundle.getString(AppConstants.BUNDLE_KEYS.FOLDER_NAME);
        folderId = bundle.getInt(AppConstants.BUNDLE_KEYS.FOLDER_ID);
    }

    private void getFolderData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.BOOKMARKS_FOLDER, folderId));
        httpParamObject.addParameter("scope", "detailed");
        httpParamObject.setClassType(FolderItemResponse.class);
        executeTask(AppConstants.TASKCODES.FOLDER_ITEMS, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_response));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.FOLDER_ITEMS:
                FolderItemResponse bookmarkApi = (FolderItemResponse) response;
                if (bookmarkApi != null) {
                    baseBookmarksArrayList.addAll(bookmarkApi.getData().getBookmarks());
                    bookmarkAdapter.notifyDataSetChanged();
                }
                break;
            case AppConstants.TASKCODES.DELETE_FOLDER:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi != null) {
                    showToast(baseApi.getMsg());
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final BaseBookmark baseBookmark = baseBookmarksArrayList.get(position);
        if (baseBookmark.getView() == AppConstants.VIEW_TYPE.FOLDER) {
            Folder folder = (Folder) baseBookmark;
            holder.bookMarkIcon.setImageResource(R.mipmap.black_folder);
            if (folder != null) {
                String name = folder.getName();
                holder.tvBookmark.setText(name);
            }
        } else if (baseBookmark.getView() == AppConstants.VIEW_TYPE.BOOKMARK) {
            Bookmark bookmark = (Bookmark) baseBookmark;
            holder.desc.setVisibility(View.VISIBLE);
            holder.tvBookmark.setText(bookmark.getName());
            holder.desc.setText(bookmark.getDescription() + "");
            holder.bookMarkIcon.setImageResource(R.mipmap.ic_bookmark_module);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(baseBookmark);
            }
        });
        return convertView;
    }


    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null) {
            String message = re.getMessage();
            showToast(message);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        BaseBookmark baseBookmark = baseBookmarksArrayList.get(position);
        onItemClick(baseBookmark);
    }

    private void onItemClick(BaseBookmark baseBookmark) {
        if (baseBookmark.getView() == AppConstants.VIEW_TYPE.FOLDER) {
            Folder folder = (Folder) baseBookmark;
            Bundle b = new Bundle();
            b.putString(AppConstants.BUNDLE_KEYS.FOLDER_NAME, folder.getName());
            b.putInt(AppConstants.BUNDLE_KEYS.FOLDER_ID, folder.getId());
            startNextActivity(b, FolderItemActivity.class);
        } else {
            Bookmark bookmark = (Bookmark) baseBookmark;
            goToBookmarkedItem(bookmark);
        }
    }

    private void goToBookmarkedItem(Bookmark bookmark) {
        switch (bookmark.getModuleType()) {
            case "assignment":
                openAssignment(bookmark);
                break;
            case "feed":
                openFeedItem(bookmark);
                break;
        }

    }

    private void openAssignment(Bookmark bookmark) {
        Bundle b = new Bundle();
        b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, bookmark.getName());
        b.putString(AppConstants.BUNDLE_KEYS.KEY_SUBTITLE, bookmark.getDescription());
        b.putInt(AppConstants.BUNDLE_KEYS.Q_ID, bookmark.getModuleId());
        startNextActivity(b, QuestionViewActivity.class);
    }

    private void openFeedItem(Bookmark bookmark) {
        int id1 = bookmark.getModuleId();
        Bundle idBundel = new Bundle();
        idBundel.putInt(AppConstants.BUNDLE_KEYS.ID, id1);
        startNextActivity(idBundel, SingleFeedActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.folder_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteFolder();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteFolder() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setDeleteMethod();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.DELETE_FOLDER, Preferences.getUserId(), folderId));
        httpParamObject.setClassType(BaseApi.class);
        executeTask(AppConstants.TASKCODES.DELETE_FOLDER, httpParamObject);
    }

    class Holder {
        TextView tvBookmark, desc;
        ImageView bookMarkIcon;

        public Holder(View view) {
            tvBookmark = (TextView) view.findViewById(R.id.tv_bookmark);
            desc = (TextView) view.findViewById(R.id.tv_subTitle);
            bookMarkIcon = (ImageView) view.findViewById(R.id.iv_bookmark);
        }
    }


}
