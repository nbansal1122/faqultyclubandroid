package faqulty.club.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import faqulty.club.R;
import faqulty.club.autocomplete.CityLocationSegment;
import faqulty.club.models.UserProfile.TutorLocation;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;

/**
 * Created by Neeraj Yadav on 9/27/2016.
 */

public class CompleteProfileTuitionActivity extends BaseActivity {
    CityLocationSegment f;
    private EditText email, name;
    Spinner subject_spinner;
    private Button signup;
    private LinearLayout cityLocationRow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complete_profile_tuition);

        initToolBar("Complete Your Profile");
        getHomeIcon(R.mipmap.arrows);

        subject_spinner = (Spinner) findViewById(R.id.subject_spinner);

        email = (EditText) findViewById(R.id.et_email_id);
        name = (EditText) findViewById(R.id.et_name);
//        city = (AutoCompleteTextView) findViewById(R.id.city);
//        location = (AutoCompleteTextView) findViewById(R.id.locality);
//        courses = (MultiAutoCompleteTextView) findViewById(R.id.courses_spinner);
        signup = (Button) findViewById(R.id.btn_sign_up);

        addTutorLocation(null,true);
        setOnClickListener(R.id.btn_sign_up);

        List<String> subject = new ArrayList<String>();
        subject.add("Math");
        subject.add("English");
        subject.add("Physics");
        subject.add("Chemistry");
        subject.add("Hindi");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CompleteProfileTuitionActivity.this, android.R.layout.simple_spinner_item, subject);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subject_spinner.setAdapter(dataAdapter);

    }

    private void addTutorLocation(TutorLocation location, Boolean isPrimary) {
        f = CityLocationSegment.getInstance(location,isPrimary);
        getSupportFragmentManager().beginTransaction().add(R.id.segament_city, f).commitAllowingStateLoss();
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_sign_up:
                if (isValidate(R.id.et_email_id, R.id.et_name)) {
                    completeProfile();
                }
        }
    }

    private void completeProfile() {
    }


}
