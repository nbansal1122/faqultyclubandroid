package faqulty.club;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import faqulty.club.R;

import faqulty.club.activity.HomeActivity;
import faqulty.club.activity.SocialLoginActivity;
import faqulty.club.models.LoginResponse;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by saurabh on 14-09-2016.
 */
public class LoginActivity extends BaseActivity {
    Button login;
    EditText et_mobileno, et_password;
    TextView forgotPass;
    private LoginResponse loginResponse;

    //9802788314
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        initToolBar("Login");
        getHomeIcon(R.mipmap.arrows);

        setOnClickListener(R.id.login, R.id.tv_forgot_password);
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.login:
                if (isValid()) {
                    HttpParamObject httpParamObject = new HttpParamObject();
                    httpParamObject.setUrl(AppConstants.PAGE_URL.LOGIN);
                    httpParamObject.setClassType(LoginResponse.class);
                    httpParamObject.setPostMethod();
                    httpParamObject.setJson(getJsonData().toString());
                    httpParamObject.setContentType("application/json");
                    executeTask(AppConstants.TASKCODES.LOGIN, httpParamObject);
                }
                break;
            case R.id.tv_forgot_password:
                startNextActivity(ForgotPasswordActivity.class);

        }
    }

    private boolean isValid() {
//        clearTilError(R.id.til_mobile_number, R.id.til_password);
        String mobile = getEditText(R.id.et_mobile_number);
        String password = getEditText(R.id.password);
        if (!TextUtils.isEmpty(mobile) && mobile.length() == 10) {
        } else {
            showToast(getString(R.string.error_invalid_mobile));
            return false;
        }

        if (!TextUtils.isEmpty(password) && password.length() >= 4) {
        } else {
            showToast(getString(R.string.error_password));
            return false;
        }
        return true;
    }


    private JSONObject getJsonData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", getEditText(R.id.et_mobile_number));
            jsonObject.put("password", getEditText(R.id.password));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    //// TODO: toast message on empty field

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    saveUserData(loginResponse);
                    getUserData(loginResponse);
                } else {
                    showToast("Please try again");
                }
                break;
            case AppConstants.TASKCODES.GET_USER:
                moveToNextActivity();
                break;
        }
    }

    public static void saveUserData(LoginResponse loginResponse) {
        Preferences.saveData(AppConstants.PREF_KEYS.USER_TOKEN, loginResponse.getToken());
        Preferences.saveData(AppConstants.PREF_KEYS.USER_ID, loginResponse.getUserId());
        Preferences.saveData(Preferences.LOGIN_KEY, true);
    }

    private void moveToNextActivity() {
        UserDetailsApi instance = UserDetailsApi.getInstance();
        if(instance != null) {
            if(instance.isActive()) {
                Intent intent = new Intent(this, HomeActivity.class);
                intent.putExtra(AppConstants.BUNDLE_KEYS.TYPE, 1);
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(this, CompleteProfileActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void getUserData(LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_USER + loginResponse.getUserId());
        obj.setClassType(UserDetailsApi.class);
        obj.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, obj);
    }

    @Override
    public void onBackPressed() {
        startNextActivity(SocialLoginActivity.class);
        finish();
    }
}