package faqulty.club.chatutil;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;


import faqulty.club.AppController;
import faqulty.club.SplashActivity;
import faqulty.club.models.chat.ChatMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Logger;
import simplifii.framework.utility.Preferences;

/**
 * @author : Nitin Bansal
 * @date : 28/05/16 : 10:59 AM
 */
public final class SocketManager {
    private static final String TAG = "##SocketManager";
    private static final Object mLock = new Object();
    public static final String MESSAGE = "message";
    private static final String START_TYPING = "typing";
    private static final String ERROR = "errored";
    private static final String STOP_TYPING = "stop typing";
    private static final String DELIVERED = "delivered";
    private static final String CONNECTION = "connection";
    private static final String ADD_USER = "addUser";
    private static final String RECEIVE = "receive";
    private static final String MESSAGE_RECEIVED = "messageReceived";
    private static final String PENDING_MESSAGE= "pendingMessages";

    private static SocketManager instance;
    private Socket mSocket;
    //    private SocketListener mSocketListener;
    private HashSet<SocketListener> listeners = new HashSet<>();


    public static void init() {
        if (instance == null) {
            instance = new SocketManager();
        }
        instance.initVars();
    }

    public void initVars() {
        if (mSocket != null) {
            return;
        }
        try {
            mSocket = IO.socket(AppConstants.PAGE_URL.CHAT_URL);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(MESSAGE, onNewMessage);
        mSocket.on(START_TYPING, onTyping);
        mSocket.on(ERROR, errored);
        mSocket.on(STOP_TYPING, onStopTyping);
        mSocket.on(DELIVERED, onDelivered);
        mSocket.on(RECEIVE, onReceive);
        mSocket.on(Socket.EVENT_CONNECT, onConnection);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(ADD_USER, onAddUser);
        mSocket.on(MESSAGE_RECEIVED, onDeliveryReceiptReceived);
        mSocket.on(PENDING_MESSAGE, onPendingMessagesReceived);

    }

    public void close() {
        Log.d(TAG, "close socket");
        if (mSocket != null) {
            mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.off(MESSAGE, onNewMessage);
            mSocket.off(START_TYPING, onTyping);
            mSocket.off(ERROR, errored);
            mSocket.off(STOP_TYPING, onStopTyping);
            mSocket.off(DELIVERED, onDelivered);
            mSocket.off(Socket.EVENT_CONNECT, onConnection);
            mSocket.off(ADD_USER, onAddUser);
            mSocket.off(RECEIVE, onReceive);
            mSocket.disconnect();
            mSocket = null;
        }
    }

    public static SocketManager getInstance() {
        return instance;
    }

    public static void setInstance(SocketManager instance) {
        SocketManager.instance = instance;
    }

    private void addUser(final String id, final String name) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", id);
            jsonObject.put("userName", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("adduser", jsonObject);
    }

    public Socket getSocket() {
        if (mSocket != null) {
            return mSocket;
        }
        try {
            mSocket = IO.socket(AppConstants.PAGE_URL.CHAT_URL);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return mSocket;
    }

//    public SocketListener getSocketListener() {
//        return mSocketListener;
//    }

//    public void setSocketListener(SocketListener socketListener) {
//        mSocketListener = socketListener;
//    }

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onConnectError");
            isConnected = false;
        }
    };

    public void emmitEvent(String message, ChatMessage chatMessage) {
        mSocket.emit(message, chatMessage.getJson());
        chatMessage.save();
    }

    private class IOAcknowledge implements Ack {

        @Override
        public void call(Object... args) {
            // ISSUE HERE - THIS NEVER FIRES! //////////////////////////////////////////////////////////////////////////////////
            JSONObject data = (JSONObject) args[0];
            Log.i(TAG, " Login confirmed: " + data.toString());
        }
    }

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            synchronized (mLock) {
                Log.d(TAG, "onDisconnect");
//                JSONObject data = (JSONObject) args[0];
//                Logger.debug(TAG, data.toString());
//                sendMessageAdded(message);
//                mSocket.emit(DELIVERED, message.getMessageId());
            }
        }
    };

    private Emitter.Listener onDeliveryReceiptReceived = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            synchronized (mLock) {
                Log.d(TAG, "on Delivery Receipt Received");
                JSONObject data = (JSONObject) args[0];
                Logger.debug(TAG, data.toString());
            }
        }
    };


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            synchronized (mLock) {
                Log.d(TAG, "onNewMessage");
                JSONObject data = (JSONObject) args[0];
                Logger.debug(TAG, data.toString());
//                sendMessageAdded(message);
//                mSocket.emit(DELIVERED, message.getMessageId());
            }
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            synchronized (mLock) {
                JSONObject data = (JSONObject) args[0];
                String username;
                try {
                    username = data.getString("username");
                } catch (JSONException e) {
                    return;
                }
                sendTypingStarted("", username);
            }
        }
    };
    private Emitter.Listener errored = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            synchronized (mLock) {
                Log.d(TAG, "error state");
                final String data = (String) args[0];
                sendError((String) args[0]);
            }
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            synchronized (mLock) {
                JSONObject data = (JSONObject) args[0];
                String username;
                try {
                    username = data.getString("username");
                } catch (JSONException e) {
                    return;
                }
                sendTypingEnd("", username);
            }
        }
    };


    public void addListener(SocketListener listener) {
        listeners.add(listener);
    }

    public void removeListener(SocketListener listener) {
        listeners.remove(listener);
    }

    private void sendTypingEnd(String channelId, String userName) {
        for (SocketListener listener : listeners) {
            listener.onTypingEnded(channelId, userName);
        }
    }

    private void sendTypingStarted(String channelId, String userName) {
        for (SocketListener listener : listeners) {
            listener.onTypingStarted(channelId, userName);
        }
    }

    private void sendError(String error) {
        for (SocketListener listener : listeners) {
            listener.onError(error);
        }
    }

    private void sendMessageAdded(ChatMessage message) {
        for (SocketListener listener : listeners) {
            listener.onMessageAdded(message);
        }
    }

    private Emitter.Listener onDelivered = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.d(TAG, "onDelivered " + data.toString());
            Ack ack = (Ack) args[args.length - 1];
            ack.call();
        }
    };
    private Emitter.Listener onReceive = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.i(TAG, "onReceive called !");
            ChatMessage chatMessage = ChatMessage.getFromJson(args[0].toString());
            if(chatMessage!=null){
                chatMessage.save();
                final JSONObject object = new JSONObject();
                try {
                    object.put("fromUserId", Preferences.getUserId());
                    object.put("toUserId", chatMessage.getFromUserID());
                    object.put("messageId", chatMessage.getMessageId());
                    object.put("messageType", DELIVERED);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSocket.emit(DELIVERED, object);
                AppController.showNotification(chatMessage);
            }
        }
    };
    private Emitter.Listener onPendingMessagesReceived=new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.i(TAG, "onPendingMessages called !");
            String json = args[0].toString();
            if(!TextUtils.isEmpty(json)){
                try {
                    JSONObject jsonObject=new JSONObject(json);
                    if(jsonObject.has("messages")){
                        JSONArray jsonArray= jsonObject.getJSONArray("messages");
                        for (int x=0;x<jsonArray.length();x++){
                            ChatMessage chatMessage=ChatMessage.getFromJson(jsonArray.getString(x));
                            if(chatMessage!=null){
                                chatMessage.save();
                                final JSONObject object = new JSONObject();
                                try {
                                    object.put("fromUserId", Preferences.getUserId());
                                    object.put("toUserId", chatMessage.getFromUserID());
                                    object.put("messageId", chatMessage.getMessageId());
                                    object.put("messageType", DELIVERED);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mSocket.emit(DELIVERED, object);
                            }
                        }
                        Intent intent = new Intent(AppController.getInstance(), SplashActivity.class);
                        AppController.sendNotification(intent,jsonArray.length()+" new messages received","",5);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private boolean isConnected;
    private Emitter.Listener onConnection = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            isConnected = true;
            Log.d(TAG, "onConnection " + args);
            final JSONObject json = new JSONObject();

            try {
                json.put("fromUserId", Preferences.getUserId());
                json.put("messageType", ADD_USER);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(ADD_USER, json);
        }
    };
    private Emitter.Listener onAddUser = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onAdd User " + args);
        }
    };

    public boolean isConnected() {
        return isConnected;
    }
}
