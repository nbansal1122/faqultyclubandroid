package faqulty.club.chatutil;

import faqulty.club.models.chat.ChatMessage;

public interface SocketListener {
    void onMessageAdded(ChatMessage message);
    void onTypingStarted(String channelId, String userName);
    void onTypingEnded(String channelId, String userName);
    void onError(String error);
    void onConnectionError();
}