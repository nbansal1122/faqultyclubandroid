package faqulty.club;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import faqulty.club.R;

import faqulty.club.activity.NewContentActivity;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.fragments.AddFromGalleryFragment;

import faqulty.club.models.UserProfile.UserDetailsApi;
import faqulty.club.models.content.ContentData;
import simplifii.framework.requestmodels.BaseAdapterModel;

import faqulty.club.models.Content;

import simplifii.framework.requestmodels.SelectContent;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class ContentActivity extends BaseActivity implements BaseRecycleAdapter.RecyclerClickInterface {
    LinearLayout ll_content;
    LayoutInflater inflater;
    List<Content> sampleContent = new ArrayList<>();
    RecyclerView recyclerView;
    private List<BaseAdapterModel> contentList = new ArrayList<>();
    private BaseRecycleAdapter contentAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        initToolBar(getString(R.string.title_content));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_plus);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        inflater = LayoutInflater.from(this);
        setOnClickListener(R.id.fab_plus);
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        UserDetailsApi api = UserDetailsApi.getInstance();
        if (api != null && api.getContentUpload() != null && api.getContentUpload().size() > 0) {
            setTutorContentData(api.getContentUpload());
        } else {
            setContentData();
        }
    }


    private void setContentData() {
        showVisibility(R.id.rl_footer);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        contentList = new ArrayList<>();
        SelectContent selectContent = new SelectContent();
        selectContent.setViewType(AppConstants.VIEW_TYPE.GET_HEADER);
        contentList.add(selectContent);
        contentAdapter = new BaseRecycleAdapter(this, contentList);
        recyclerView.setAdapter(contentAdapter);
    }

    private void setTutorContentData(List<ContentData> data) {
        hideVisibility(R.id.rl_footer);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        contentList.clear();
        contentList.addAll(data);
        contentAdapter = new BaseRecycleAdapter(this, contentList);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(contentAdapter);
        contentAdapter.setClickInterface(this);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_USER:
                setUpRecyclerView();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_plus:
                AddFromGalleryFragment addFromGalleryFragment = new AddFromGalleryFragment();
                addFromGalleryFragment.setImageSize(1);
                addFromGalleryFragment.show(getSupportFragmentManager(), addFromGalleryFragment.getTag());
                break;
        }
        super.onClick(v);
    }


    private void getUserProfile() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUserUrl();
        obj.setClassType(UserDetailsApi.class);
        obj.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, obj);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    String keyMessage = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_MESSAGE);
                    if (AppConstants.BUNDLE_KEYS.UPDATE.equalsIgnoreCase(keyMessage)) {
                        getUserProfile();
                    }
                }
            }
        }
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.ACTION_EDIT_CONTENT:
                editContentData((ContentData) obj);
                break;
        }
    }

    public void editContentData(ContentData data) {
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, data);
        Intent i = new Intent(this, NewContentActivity.class);
        i.putExtras(b);
        startActivityForResult(i, AppConstants.REQUEST_CODES.EDIT_CONTENT);
    }

}