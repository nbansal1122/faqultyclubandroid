package faqulty.club;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.astuetz.PagerSlidingTabStrip;
import faqulty.club.R;

import faqulty.club.Util.AppUtil;
import faqulty.club.fragments.ProfileListFragment;
import faqulty.club.fragments.PromoteTabFragment;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

/**
 * Created by saurabh on 09-09-2016.
 */
public class HomeProfileFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface<String> {
    private ViewPager pager;
    private TabLayout tabLayout;
    private CustomPagerAdapter<String> pagerAdapter;
    private List<String> tabs = new ArrayList<>();


    @Override
    public void initViews() {
        initToolBar("");
        initTabs();
        setViewPager();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_profile_tab;
    }

    private void initTabs() {
        tabs.clear();
        tabs.add("Profile");
        tabs.add("Promote");
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.fc_icon;
    }

    private void setViewPager() {
        pager = (ViewPager) findView(R.id.view_pager_profile);
        toolbar = (Toolbar) findView(R.id.toolbar);
        pagerAdapter = new CustomPagerAdapter<>(getChildFragmentManager(), tabs, this);
        pager.setAdapter(pagerAdapter);
        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findView(R.id.tab_profile_view);
        tabStrip.setShouldExpand(true);
        tabStrip.setTextSize((int) Util.convertDpToPixel(14, getContext()));
        tabStrip.setTextColor(getResourceColor(R.color.gray));
//        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + AppConstants.DEF_REGULAR_FONT);
//        tabStrip.setTypeface(typeface, R.style.tab_style);
        tabStrip.setViewPager(pager);
        AppUtil.setTabFont(tabStrip);
    }
    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        switch (position) {
            case 0:
                return new ProfileListFragment();
            case 1:
                return new PromoteTabFragment();
        }
        return new ProfileListFragment();
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return tabs.get(position);
    }
    @Override
    protected void onInternetException() {
        super.onInternetException();
        showVisibility(R.id.layout_connectivity);
    }


}
