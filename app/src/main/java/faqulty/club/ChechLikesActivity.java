package faqulty.club;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;


import faqulty.club.models.feed.FeedLike;
import faqulty.club.models.feed.FeedLikeResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class ChechLikesActivity extends BaseActivity implements CustomListAdapterInterface {

    public static void startActivity(int feedId, Activity context) {
        Bundle b = new Bundle();
        b.putInt(AppConstants.BUNDLE_KEYS.FEED_ID, feedId);
        Intent i = new Intent(context, ChechLikesActivity.class);
        i.putExtras(b);
        context.startActivity(i);
    }

    private ListView listView;
    private CustomListAdapter<FeedLike> adapter;
    private List<FeedLike> list = new ArrayList<>();
    private int feedId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_likes);
        initToolBar("Likes");
        listView = (ListView) findViewById(R.id.lv_likes);
        listView.setEmptyView(findViewById(android.R.id.empty));
        adapter = new CustomListAdapter<>(this, R.layout.row_likes, list, this);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchFeedLikes();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        feedId = bundle.getInt(AppConstants.BUNDLE_KEYS.FEED_ID, 0);
    }

    private void fetchFeedLikes() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.FEED_LIKES, feedId));
        obj.setClassType(FeedLikeResponse.class);
        executeTask(AppConstants.TASKCODES.FETCH_FEED_LIKES, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.FETCH_FEED_LIKES: {
                FeedLikeResponse res = (FeedLikeResponse) response;
                if (res != null && res.getData() != null) {
                    list.clear();
                    list.addAll(res.getData());
                    adapter.notifyDataSetChanged();
                }
            }
            break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        FeedLike like = list.get(position);
        holder.tv.setText(like.getName());
        if (!TextUtils.isEmpty(like.getImage())) {
            Picasso.with(this).load(Util.getCompleteUrl(like.getImage())).placeholder(R.mipmap.accountselected2x).into(holder.imageView);
        } else {
            holder.imageView.setImageResource(R.mipmap.accountselected2x);
        }
        return convertView;
    }

    private class Holder {
        private TextView tv;
        private CircleImageView imageView;

        public Holder(View v) {
            tv = (TextView) v.findViewById(R.id.name_liker);
            imageView = (CircleImageView) v.findViewById(R.id.profile_pic_liker);
        }
    }
}
