package faqulty.club.autocomplete;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.ClassChip;
import faqulty.club.autoadapters.ClassMultiSelectDialog;
import faqulty.club.autoadapters.SubjectSingleSelectDialogNew;
import faqulty.club.models.UserProfile.ClassObject;
import faqulty.club.models.UserProfile.CoursesTaught;
import faqulty.club.models.UserProfile.SubjectObject;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 17/09/16.
 */
public class CoursesSegment extends BaseFragment {
    public ChipView classActv;
    public TextView subjectActv;
    private CoursesSegmentDeleteListener listener;

//    private SingleDiaogManager<SubjectObject> subjectAutoView;

    private CoursesTaught courseDetail;
    private List<ClassObject> allClasses = new ArrayList<>();
    private List<ClassObject> selectedClasses = new ArrayList<>();
    private List<SubjectObject> subjectObjects = new ArrayList<>();
    private SubjectObject selectedSubjectClass;
    public int index;
    private int selectedSubjectId = -1;
    private List<Chip> chips;

    public static CoursesSegment getInstance(CoursesTaught courseDetail, CoursesSegmentDeleteListener listener, int index) {
        CoursesSegment c = new CoursesSegment();
        c.courseDetail = courseDetail;
        c.listener = listener;
        c.index = index;
        return c;
    }

//    public String getSubject() {
//        if (subjectAutoView.getSelectedItem() != null) {
//            return subjectAutoView.getSelectedItem().getDisplayString();
//        }
//        return "";
//    }

    public boolean isValid() {
        if (selectedClasses.size() != 0 && selectedSubjectClass != null) {
            return true;
        } else if (selectedClasses.size() == 0 && selectedSubjectClass == null) {
            return true;
        } else if (selectedSubjectClass == null) {
            showToast(R.string.error_subject);
            return false;
        } else {
            showToast("Please enter classes for '" + selectedSubjectClass.getDisplayString() + "'");
            return false;
        }
    }

    public JSONObject getJsonObject() throws JSONException {
        if (selectedClasses != null && selectedSubjectClass != null) {
            JSONObject obj = new JSONObject();
            JSONArray classArray = new JSONArray();
            if (selectedClasses != null) {
                for (ClassObject classObject : selectedClasses) {
                    JSONObject classObj = new JSONObject();
                    classObj.put("id", classObject.getId());
                    classObj.put("class", classObject.getDisplayString());
                    classArray.put(classObj);
                }
            } else {
                return null;
            }
            if (selectedSubjectClass != null) {
                obj.put("subjectId", selectedSubjectClass.getId());
                obj.put("subject", selectedSubjectClass.getDisplayString());
            } else {
                if (!TextUtils.isEmpty(subjectActv.getText().toString())) {
                    obj.put("subject", subjectActv.getText().toString());
                }
                if (selectedSubjectId != -1) {
                    obj.put("subjectId", selectedSubjectId);
                }
            }
            obj.put("classes", classArray);
            return obj;
        }
        return null;
    }

    @Override
    public void initViews() {
        subjectActv = (TextView) findView(R.id.actv_subject);
        classActv = (ChipView) findView(R.id.mctv_classes);
        subjectActv.setHint(getString(R.string.select_subjects));
        getSubjects();
        getClasses();
        initSelectedClasses();
        setOnClickListener(R.id.mctv_classes, R.id.iBtn_delete_location, R.id.iv_down_arrow, R.id.actv_subject);
        setOnClickListener(R.id.tv_selector_classes);
        if (courseDetail != null) {
            subjectActv.setText(courseDetail.getSubject());
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.mctv_classes:
                showClassesDialog();
                break;
            case R.id.iBtn_delete_location:
                if (listener != null) {
                    listener.onItemDelete(this);
                }
                break;
            case R.id.iv_down_arrow:
                showClassesDialog();
                break;
            case R.id.tv_selector_classes:
                showClassesDialog();
                break;
            case R.id.actv_subject:
                showSubjectsDialog();
                break;
        }
    }

    private void showSubjectsDialog() {
        SubjectSingleSelectDialogNew subjectSingleSelectDialogNew = SubjectSingleSelectDialogNew.getInstance(getString(R.string.select_subjects), subjectObjects, new SubjectSingleSelectDialogNew.ItemSelector() {
            @Override
            public void onItemsSelected(SubjectObject selectedItems) {
                selectedSubjectClass = selectedItems;
                subjectActv.setText(selectedSubjectClass.getDisplayString());
            }
        });
        subjectSingleSelectDialogNew.show(getChildFragmentManager().beginTransaction(), "");
    }


    private void getClasses() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_CLASSES);
        obj.setClassType(ClassObject.class);
        executeTask(AppConstants.TASKCODES.GET_CLASSES, obj);
    }

    private void getSubjects() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_SUBJECTS);
        obj.setClassType(SubjectObject.class);
        executeTask(AppConstants.TASKCODES.GET_SUBJECTS, obj);
    }


    @Override
    public int getViewID() {
        return R.layout.layout_course_segment;
    }

    public String getSubject() {
        if (selectedSubjectClass != null) {
            return selectedSubjectClass.getDisplayString();
        }
        return "";
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_SUBJECTS:
                if (response != null) {
                    List<SubjectObject> subjectsList = (List<SubjectObject>) response;
                    if (subjectsList.size() > 0) {
                        Collections.sort(subjectsList);
                        subjectObjects.clear();
                        subjectObjects.addAll(subjectsList);
                        if (courseDetail != null) {
                            for (SubjectObject subjectObject : subjectObjects) {
                                if (courseDetail.getSubject().equalsIgnoreCase(subjectObject.getDisplayString())) {
                                    selectedSubjectClass = subjectObject;
                                }
                            }
                        }
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_CLASSES:
                if (response != null) {
                    List<ClassObject> cityApiList = (List<ClassObject>) response;
                    if (cityApiList.size() > 0) {
                        allClasses.clear();
                        allClasses.addAll(cityApiList);
                        initSelectedClasses();
                    }
                }
                break;
        }
    }


    private void initSelectedClasses() {
        if (courseDetail != null && courseDetail.getClasses() != null) {
            selectedClasses.clear();
            selectedClasses.addAll(courseDetail.getClasses());

            setClassesChipView();
        }

    }

    private void showClassesDialog() {
        ClassMultiSelectDialog f = ClassMultiSelectDialog.getInstance("Select Classes",
                allClasses, new ClassMultiSelectDialog.ItemSelector() {
                    @Override
                    public void onItemsSelected(List<ClassObject> selectedItems) {
                        selectedClasses.clear();
                        selectedClasses.addAll(selectedItems);
                        setClassesChipView();
                    }
                }, selectedClasses);

        f.show(getChildFragmentManager(), "Classes");
    }

    private OnChipClickListener classChipClickListener = new OnChipClickListener() {
        @Override
        public void onChipClick(Chip chip) {
            ClassChip classChip = (ClassChip) chip;
            List<ClassObject> classObjects = new ArrayList<>();
            List<ClassObject> copySelectedClasses = new ArrayList<>();
            if (selectedClasses != null && selectedClasses.size() > 0)
                copySelectedClasses.addAll(selectedClasses);

            if (classChip != null)
                classObjects.addAll(classChip.getClassObjectList());

            if (classObjects != null && classObjects.size() > 0) {
                for (ClassObject c2 : copySelectedClasses) {
                    for (ClassObject c1 : classObjects) {

                        String s1 = c1.getDisplayString().contains("Class") ? c1.getDisplayString().replace("Class", "").trim() : c1.getDisplayString();
                        String s2 = c2.getDisplayString().contains("Class") ? c2.getDisplayString().replace("Class", "").trim() : c2.getDisplayString();

                        if (s1.compareTo(s2) == 0)
                            selectedClasses.remove(c1);
                    }
                }
            }
            setClassesChipView();


//            ClassObject clickedChip = null;
//            for (ClassObject api : selectedClasses) {
//                if (api.getDisplayString().equalsIgnoreCase(chip.getText())) {
//                    clickedChip = api;
//                }
//            }
//            if (clickedChip != null) {
//                selectedClasses.remove(clickedChip);
//                setClassesChipView();
        }


//        }
    };

    private void setClassesChipView() {
        chips = new ArrayList<>();
        List<Chip> groupedClasses = getGroupedClasses(selectedClasses);
        if (groupedClasses != null && groupedClasses.size() > 0)
            chips.addAll(groupedClasses);
        if (chips.size() > 0) {
            hideVisibility(R.id.tv_selector_classes);
            classActv.setChipLayoutRes(R.layout.row_chip);
            classActv.setChipList(chips);
            classActv.setOnChipClickListener(classChipClickListener);
        } else {
            classActv.setChipList(chips);
            showVisibility(R.id.tv_selector_classes);
            setOnClickListener(R.id.tv_selector_classes);
        }
    }

    private List<Chip> getGroupedClasses(List<ClassObject> selectedClasses) {
        List<List<ClassObject>> mainList = new ArrayList<>();
        List<ClassObject> tempObjects = new ArrayList<>();
        for (ClassObject classObject : selectedClasses) {
            try {
                int classValue = Integer.parseInt(classObject.getClassVal());
                if (tempObjects.size() > 0) {
                    ClassObject tempObject = tempObjects.get(tempObjects.size() - 1);
                    int tempValue = Integer.parseInt(tempObject.getClassVal());
                    if ((tempValue + 1) == classValue) {
                        tempObjects.add(classObject);
                    } else {
                        mainList.add(tempObjects);
                        tempObjects = new ArrayList<>();
                        tempObjects.add(classObject);
                    }
                } else {
                    tempObjects.add(classObject);
                }

            } catch (Exception e) {
                if (tempObjects.size() > 0) {
                    mainList.add(tempObjects);
                    tempObjects = new ArrayList<>();
                }
                List<ClassObject> classObjects = new ArrayList<>();
                classObjects.add(classObject);
                mainList.add(classObjects);
            }
        }
        if (tempObjects.size() > 0) {
            mainList.add(tempObjects);
        }
        List<Chip> groupedChips = new ArrayList<>();

        for (List<ClassObject> classObject : mainList) {
            ClassChip classChip = new ClassChip(false);
            classChip.setClassObjectList(classObject);
            groupedChips.add(classChip);
        }

        return groupedChips;
    }

    public static interface CoursesSegmentDeleteListener {
        public void onItemDelete(CoursesSegment segment);
    }
}
