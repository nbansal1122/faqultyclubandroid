package faqulty.club.autocomplete;

/**
 * Created by nbansal2211 on 17/09/16.
 */
public abstract class BaseSegment {
    public abstract String getDisplayString();
    public abstract boolean isMatchingConstraint(String text);
}
