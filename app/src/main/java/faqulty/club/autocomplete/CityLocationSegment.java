package faqulty.club.autocomplete;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.autoadapters.AutoCompleteView;
import faqulty.club.autoadapters.SingleDiaogManager;
import faqulty.club.models.BaseAutoFilter;
import faqulty.club.models.CityApi;
import faqulty.club.models.LocationApi;
import faqulty.club.models.UserProfile.TutorLocation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by nbansal2211 on 17/09/16.
 */
public class CityLocationSegment extends BaseFragment implements AutoCompleteView.TextChangeListener {
    private AutoCompleteTextView primarycity;
    private AutoCompleteTextView primarylocation;
    private static final int SEARCH_DELAY = 500;
    private static final int MESSAGE_TEXT_CHANGED = 1;

    public SingleDiaogManager<CityApi> cityAutoview;
    public SingleDiaogManager<LocationApi> locationAutoview;
    public boolean isPrimary;
    public int index;
    private ImageButton deleteLocation;
    private int MIN_CHARACTER_LENGTH = 1;

    private int selectedCityId = -1;

    public void setLocationDeleteInterface(LocationDeleteInterface locationDeleteInterface) {
        this.locationDeleteInterface = locationDeleteInterface;
    }

    private LocationDeleteInterface locationDeleteInterface;

    private TutorLocation tutorLocation;

    public static CityLocationSegment getInstance(TutorLocation location, boolean isPrimary) {
        return getInstance(location, isPrimary, 0);
    }

    public String getCityLocationString() {
        if (cityAutoview.getSelectedItem() == null && locationAutoview.getSelectedItem() == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        builder.append(cityAutoview.getSelectedItem()).append(locationAutoview.getSelectedItem());
        return builder.toString();
    }

    public static CityLocationSegment getInstance(TutorLocation location, boolean isPrimary, int index) {
        CityLocationSegment c = new CityLocationSegment();
        c.isPrimary = isPrimary;
        c.tutorLocation = location;
        c.index = index;
        return c;
    }


    public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
//        JSONObject locationObj = new JSONObject();
//        JSONObject cityObj = new JSONObject();

        if (cityAutoview.getSelectedItem() != null) {
            jsonObject.put("city", cityAutoview.getSelectedItem().getCity());
        } else {
            return null;
        }
        if (locationAutoview.getSelectedItem() != null) {
//            jsonObject.put("city", cityObj);
//            locationObj.put("id", locationAutoview.getSelectedItem().getId());
            jsonObject.put("locality", locationAutoview.getSelectedItem().getLocality());
        }
        jsonObject.put("isPrimary", isPrimary);
//        if (isPrimary) {
//            jsonObject.put("primary", "1");
//        }
        //// TODO: 24-09-2016 change the value of primary 
        return jsonObject;
    }

    public boolean isValid() {
        if (cityAutoview.getSelectedItem() == null && locationAutoview.getSelectedItem() == null) {
            return true;
        } else if (cityAutoview.getSelectedItem() != null && locationAutoview.getSelectedItem() != null) {
            return true;
        } else if (locationAutoview.getSelectedItem() == null) {
            showToast(R.string.please_enter_location);
            return false;
        }
        return true;
    }

    @Override
    public void initViews() {
        primarycity = (AutoCompleteTextView) findView(R.id.actv_primary_city);
        primarylocation = (AutoCompleteTextView) findView(R.id.actv_primary_location);
        cityAutoview = new SingleDiaogManager<CityApi>((AppCompatActivity) getActivity(), primarycity, new AutoCompleteView.ItemSelector() {
            @Override
            public void onItemSelected(BaseAutoFilter item) {
                primarylocation.setText("");
                getLocations(null);
                locationAutoview.setSelectedItem(null);
            }
        });
        locationAutoview = new SingleDiaogManager<LocationApi>((AppCompatActivity) getActivity(), primarylocation, null, this);
        primarylocation.setHint("Location");
        if (isPrimary) {
            hideVisibility(R.id.iBtn_delete_location);
            primarycity.setHint("Primary City");
            primarylocation.setHint("Primary Location");
        } else {
            showVisibility(R.id.iBtn_delete_location);
            setOnClickListener(R.id.iBtn_delete_location);
            primarycity.setHint("City");
            primarylocation.setHint("Location");
        }
        initCityLocation();
//        setCityAdapter();
//        setLocalityAdapter();
        getCities();
//        getLocations("");
        registerClickListeners(R.id.iBtn_delete_location);
        setOnClickListener(R.id.actv_primary_city, R.id.actv_primary_location);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iBtn_delete_location:
                if (locationDeleteInterface != null) {
                    locationDeleteInterface.onLocationDelete(this, tutorLocation);
                }
                break;
            case R.id.actv_primary_location:
                locationAutoview.showDropDown();
                break;
            case R.id.actv_primary_city:
                cityAutoview.showDropDown();
                break;
        }
    }

    private void initCityLocation() {
        if (tutorLocation != null) {
            cityAutoview.setSelectedItem(tutorLocation.getCity());
            locationAutoview.setSelectedItem(tutorLocation.getLocality());
            getLocations("");
        }
    }

    private void getCities() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_CITIES);
        obj.setClassType(CityApi.class);
        // 5 days time for cache city data
        long l = 5 * 24 * 60 * 60 * 1000;
        executeTask(AppConstants.TASKCODES.GET_CITIES, obj, true, Preferences.KEY_CITIES, l);
    }

    private void getLocations(String searchTerm) {
        if (cityAutoview == null || cityAutoview.getSelectedItem() == null) {
            return;
        }
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_LOCALITIES);
        obj.setClassType(LocationApi.class);

        String id = "";
        if (cityAutoview.getSelectedItem().getId() == null && selectedCityId != -1) {
            id = selectedCityId + "";
        } else {
            id = cityAutoview.getSelectedItem().getId() + "";
        }
        obj.addParameter("cityId", id + "");
        String keyCityIdForCache = "CityId:" + id;
        obj.addParameter("count", "1000");
        obj.addParameter("offset", "0");
        executeTask(AppConstants.TASKCODES.GET_LOCALITIES, obj, keyCityIdForCache);
    }


    @Override
    public int getViewID() {
        return R.layout.segment_city_location;
    }

    @Override
    public void onTextChanged(String searchTerm) {
        if (searchTerm.length() >= MIN_CHARACTER_LENGTH) {
            sendSearchMessage(searchTerm);
        }
    }

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            getLocations((String) msg.obj);
        }
    };


    private void sendSearchMessage(String searchTerm) {
        handler.removeMessages(MESSAGE_TEXT_CHANGED);
        handler.sendMessageDelayed(handler.obtainMessage(MESSAGE_TEXT_CHANGED, searchTerm), SEARCH_DELAY);
    }

    private class Holder {
        TextView tv;

        Holder(View v) {
            tv = (TextView) v.findViewById(R.id.tv_item);
        }
    }

    @Override
    public void onPreExecute(int taskCode) {

        switch (taskCode) {
            case AppConstants.TASKCODES.GET_LOCALITIES:
// to hide progress bar
                break;
            default:
                super.onPreExecute(taskCode);
                break;
        }
        super.onPreExecute(taskCode);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_LOCALITIES:
                if (response != null) {
                    List<LocationApi> locationApiList = (List<LocationApi>) response;
                    if (locationApiList.size() > 0) {
                        locationAutoview.setItems(locationApiList);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_CITIES:
                if (response != null) {
                    List<CityApi> cityApiList = (List<CityApi>) response;
                    if (cityApiList.size() > 0) {
                        setSelectedCityId(cityApiList);
                        cityAutoview.setItems(cityApiList);
                    }
                }
                break;
        }
    }

    private void setSelectedCityId(List<CityApi> cityApiList) {
        if (tutorLocation != null) {
            for (CityApi cityApi : cityApiList) {
                if (cityApi.getCity().equalsIgnoreCase(tutorLocation.getCity().getCity())) {
                    selectedCityId = cityApi.getId();
                }
            }
        }
        if (selectedCityId != -1) {
            getLocations("");
        }

    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    public static interface LocationDeleteInterface {
        public void onLocationDelete(CityLocationSegment segment, TutorLocation location);
    }
}
