package faqulty.club.holders;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.tutorgroup.TutorGroupData;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 26/12/16.
 */

public class GroupHolder extends BaseHolder {
    ImageView ivInvite, ivBday, ivFee;
    TextView tvName;

    public GroupHolder(View itemView) {
        super(itemView);
        View view = itemView;
        ivInvite = (ImageView) view.findViewById(R.id.iv_invite);
        ivBday = (ImageView) view.findViewById(R.id.icon1);
        ivFee = (ImageView) view.findViewById(R.id.icon2);
        tvName = (TextView) view.findViewById(R.id.tv_name);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final TutorGroupData tgd = (TutorGroupData) obj;
        if(tgd.isSelectedForDelete()){
            itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.light_gray));
        }else {
            itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.white));
        }
        if (null != tgd) {
            if (!TextUtils.isEmpty(tgd.getName()))
                tvName.setText(tgd.getName());
            if (!tgd.getIsBdayToday()) {
                ivBday.setVisibility(View.GONE);
            } else {
                ivBday.setVisibility(View.VISIBLE);
            }
        }

        ivInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.GROUP_ATTENDANCE, tgd);
            }
        });
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.GROUP_CLICKED, tgd);
            }
        });
        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onEventAction(AppConstants.ACTION_TYPE.GROUP_LONG_CLICK, tgd);
                return true;
            }
        });
    }
}
