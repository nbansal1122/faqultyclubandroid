package faqulty.club.holders;

import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;

import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;

/**
 * Created by my on 28-10-2016.
 */

public class PDFHolder extends BaseHolder implements View.OnClickListener {

    private ImageView image, deleteEditIcon;
    private SelectContent selectContent;
    private Uri uri;


    public PDFHolder(View itemView) {
        super(itemView);
        image = (ImageView) findView(R.id.iv_pdf_view);
        deleteEditIcon = (ImageView) findView(R.id.iv_delete_content);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        selectContent = (SelectContent) obj;
        deleteEditIcon.setImageResource(R.mipmap.ic_delete_degree);
        deleteEditIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, selectContent);
            }
        });

    }
}
