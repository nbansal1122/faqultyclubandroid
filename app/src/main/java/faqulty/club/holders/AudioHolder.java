package faqulty.club.holders;

import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.R;

import simplifii.framework.requestmodels.SelectContent;

import java.io.File;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import simplifii.framework.utility.AppConstants;

/**
 * Created by my on 28-10-2016.
 */

public class AudioHolder extends BaseHolder implements View.OnClickListener {

    private TextView tv_views, tv_post_time;
    private ExposureVideoPlayer exposureVideoPlayer;
    private SelectContent selectContent;
    private Uri uri;
    ImageView deleteContent;

    public AudioHolder(View itemView) {
        super(itemView);
        tv_views = (TextView) findView(R.id.tv_view);
        tv_post_time = (TextView) findView(R.id.tv_days_ago);
        exposureVideoPlayer = (ExposureVideoPlayer) findView(R.id.videoView);
        deleteContent = (ImageView) findView(R.id.iv_delete_content);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        selectContent = (SelectContent) obj;
        tv_views.setText(selectContent.getViews());
        File file = new File(selectContent.getFilePath());
        uri = Uri.fromFile(file);
        exposureVideoPlayer.setVideoSource(uri);
        if (selectContent.isFeedContent()) {
            tv_views.setVisibility(View.GONE);
            tv_post_time.setVisibility(View.GONE);
        }
        deleteContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, selectContent);
            }
        });
    }
}
