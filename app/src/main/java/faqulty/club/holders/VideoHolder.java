package faqulty.club.holders;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;
import faqulty.club.activity.AudioVideoViewer;

import simplifii.framework.requestmodels.SelectContent;

import simplifii.framework.utility.AppConstants;

/**
 * Created by my on 28-10-2016.
 */

public class VideoHolder extends BaseHolder implements View.OnClickListener {
    private final ImageView ivThumbnile;
    private ImageView deleteContent;
    public VideoHolder(View itemView) {
        super(itemView);
        deleteContent = (ImageView) findView(R.id.iv_delete_content);
        ivThumbnile= (ImageView) itemView.findViewById(R.id.iv_thumbnile);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final SelectContent selectContent = (SelectContent) obj;
        final String filePath = selectContent.getFilePath();
        if(!TextUtils.isEmpty(filePath)){
            new AsyncTask<Object,Object,Bitmap>(){
                @Override
                protected Bitmap doInBackground(Object[] params) {
                    Bitmap imageBitmap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                    return imageBitmap;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    ivThumbnile.setImageBitmap(bitmap);
                }
            }.execute();
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioVideoViewer.startActivity(context,filePath,"");
            }
        });
        deleteContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, selectContent);
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
