package faqulty.club.holders;

import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;
import faqulty.club.Util.VideoThumbnailTask;
import faqulty.club.activity.AudioVideoViewer;
import faqulty.club.activity.ImageActivity;
import faqulty.club.activity.PDFViewer;

import simplifii.framework.requestmodels.SelectContent;

import com.squareup.picasso.Picasso;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 19/12/16.
 */

public class FeedContentHolder extends BaseHolder {
    private ImageView ivActionContent, contentThumbnail, deleteIcon;

    public FeedContentHolder(View itemView) {
        super(itemView);
        ivActionContent = (ImageView) findView(R.id.iv_action_content);
        contentThumbnail = (ImageView) findView(R.id.iv_select_content);
        deleteIcon = (ImageView) findView(R.id.iv_delete_content);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final SelectContent contentData = (SelectContent) obj;
        int minWidth = context.getResources().getDimensionPixelOffset(R.dimen.feed_imgae_width);
        String url = contentData.getUri();
        if (!TextUtils.isEmpty(url)) {
            String mimetype = contentData.getMimetype();
            if(!TextUtils.isEmpty(mimetype)){

            if (!contentData.isSingleItem()) {
                contentThumbnail.setMinimumWidth(minWidth);
                contentThumbnail.setMaxWidth(minWidth);
                Picasso.with(context).load(Util.getImageUrlBasedOnMimeType(url, mimetype)).into(contentThumbnail);
            } else {
                minWidth = context.getResources().getDisplayMetrics().widthPixels - (2 * context.getResources().getDimensionPixelOffset(R.dimen.margin_8dp));
                contentThumbnail.setMinimumWidth(minWidth);
                contentThumbnail.setMaxWidth(minWidth);
                itemView.setMinimumWidth(minWidth);
                Picasso.with(context).load(Util.getImageUrlBasedOnMimeType(url, mimetype)).into(contentThumbnail);
            }
            }
        }else {
            contentThumbnail.setMinimumWidth(minWidth);
            contentThumbnail.setMaxWidth(minWidth);
        }
        itemView.setTag(contentData);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectContent data = (SelectContent) v.getTag();
                String mimeType = data.getMimetype();
                if (!TextUtils.isEmpty(mimeType)) {
                    if (mimeType.contains("pdf")) {
                        if (!TextUtils.isEmpty(data.getUri()))
                            PDFViewer.startActivity(context,AppConstants.PAGE_URL.BASEURL+ data.getUri());
                    } else if (mimeType.contains("image")) {
                        if (!TextUtils.isEmpty(data.getUri()))
                            ImageActivity.startActivity(context, Util.getCompleteUrl(data.getUri()));
                    } else if (mimeType.contains("video")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(data.getUri()), mimeType);
                    } else if (mimeType.contains("audio")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(data.getUri()), mimeType);
                    }
                }
            }
        });
        if (contentData.isDeleteRequired()) {
            deleteIcon.setVisibility(View.VISIBLE);
            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, contentData);
                }
            });
        } else {
            deleteIcon.setVisibility(View.GONE);
            deleteIcon.setOnClickListener(null);
        }

        placeActionIcon(contentData);
    }

    private void placeActionIcon(SelectContent content) {
        String mimeType = content.getMimetype();
        ivActionContent.setImageResource(0);
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.contains("pdf")) {
                ivActionContent.setImageResource(R.mipmap.pdf);
            } else if (mimeType.contains("image")) {
                ivActionContent.setImageResource(0);
            } else if (mimeType.contains("video")) {
                ivActionContent.setImageResource(R.mipmap.video_icon);
                setThumbnile(content);
            } else if (mimeType.contains("audio")) {
                ivActionContent.setImageResource(R.mipmap.audio_icon);
            }
        }
    }

    private void setThumbnile(SelectContent content) {
        VideoThumbnailTask.getThumbnile(content.getFullUrl(), new VideoThumbnailTask.OnLoadThumbnile() {
            @Override
            public void onLoadThumbnile(Uri pathUri) {
                contentThumbnail.setImageURI(pathUri);
            }
        });
    }
}
