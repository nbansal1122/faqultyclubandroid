package faqulty.club.holders;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import faqulty.club.R;
import faqulty.club.Util.VideoThumbnailTask;
import faqulty.club.activity.AudioVideoViewer;
import faqulty.club.models.Content;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by my on 28-10-2016.
 */

public class HeaderHolder extends BaseHolder {
    LinearLayout ll_content;
    LayoutInflater inflater;
    List<Content> sampleContent = new ArrayList<>();


    public HeaderHolder(View itemView) {
        super(itemView);
        initData();
        ll_content = (LinearLayout) findView(R.id.ll_content);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setData();
    }

    private void setData() {
        for (Content content : sampleContent) {
            View view = inflater.inflate(R.layout.view_sample_content, null);
            setContent(content, view);
            ll_content.addView(view);
        }
    }

    private void setContent(final Content content, View view) {
        ImageView imageView = (ImageView) view.findViewById(R.id.iv_video_type);
        ImageView ivPlayButton = (ImageView) view.findViewById(R.id.iv_play_button);
        switch (content.getMimeType()) {
            case "video":
                setThumbnile(content.getUrl(),imageView);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AudioVideoViewer.startActivity(context, content.getUrl(), "video");
                    }
                });
                break;
            default:
                ivPlayButton.setVisibility(View.GONE);
                break;
        }
    }
    private void setThumbnile(String url, final ImageView imageView) {
        VideoThumbnailTask.getThumbnile(url, new VideoThumbnailTask.OnLoadThumbnile() {
            @Override
            public void onLoadThumbnile(Uri pathUri) {
                imageView.setImageURI(pathUri);
            }
        });
    }

    private void initData() {
        sampleContent.clear();
        Content content = new Content();
        content.setUrl(context.getString(R.string.sample_video_1));
        content.setMimeType("video");
        sampleContent.add(content);

        Content content1 = new Content();
        content1.setUrl(context.getString(R.string.sample_video_2));
        content1.setMimeType("video");
        sampleContent.add(content1);
    }
}
