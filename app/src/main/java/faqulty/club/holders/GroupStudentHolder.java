package faqulty.club.holders;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import com.squareup.picasso.Picasso;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 26/12/16.
 */

public class GroupStudentHolder extends BaseHolder {
    ImageView ivProfilePic, ivBday, ivFee;
    TextView tvName, tvStatus, tvTitle;
    LinearLayout ll;

    public GroupStudentHolder(View itemView) {
        super(itemView);
        View view = itemView;
        ll = (LinearLayout) view.findViewById(R.id.ll_attendance);
        ivProfilePic = (ImageView) view.findViewById(R.id.iv_user_image);
        ivBday = (ImageView) view.findViewById(R.id.icon1);
        ivFee = (ImageView) view.findViewById(R.id.icon2);
        tvName = (TextView) view.findViewById(R.id.tv_name);
        tvStatus = (TextView) view.findViewById(R.id.tv_invite_status);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        GroupStudentHolder holder = this;
        final StudentBaseClass sbc = (StudentBaseClass) obj;
        if (sbc.isSelectedForDelete()) {
            itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.light_gray));
        } else {
            itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
        holder.tvTitle.setVisibility(View.GONE);
        if (sbc.getFirst()) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            if (!sbc.isGroupStudent()) {
                holder.tvTitle.setText("1:1 Students");
            } else {
                holder.tvTitle.setText("Group Students");
            }

        }
        if (!TextUtils.isEmpty(sbc.getDisplayName()))
            holder.tvName.setText(sbc.getDisplayName());
        if (sbc.getIsBdayToday()) {
            holder.ivBday.setVisibility(View.VISIBLE);
        } else {
            holder.ivBday.setVisibility(View.GONE);
        }
        if (sbc.getPendingAmount() > 0) {
            holder.ivFee.setVisibility(View.VISIBLE);
        } else {
            holder.ivFee.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(sbc.getImage())) {
            Picasso.with(context).load(Util.getCompleteUrl(sbc.getImage())).
                    into(holder.ivProfilePic);
        } else {
            holder.ivProfilePic.setImageResource(R.mipmap.accountselected2x);
        }
        if (sbc.isInvitePending()) {
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.ll.setVisibility(View.GONE);
        } else {
            holder.tvStatus.setVisibility(View.GONE);
            holder.ll.setVisibility(View.GONE);
            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.GROUP_STUDENT_CLICKED, sbc);
            }
        });
        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onEventAction(AppConstants.ACTION_TYPE.GROUP_STUDENT_LONG_CLICKED, sbc);
                return true;
            }
        });
    }
}
