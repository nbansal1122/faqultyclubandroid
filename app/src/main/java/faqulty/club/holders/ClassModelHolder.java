package faqulty.club.holders;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.assignment.ClassModel;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 28/01/17.
 */

public class ClassModelHolder extends BaseHolder {
    TextView tvName, tvTitle, tvSubtitle;

    public ClassModelHolder(View itemView) {
        super(itemView);
        tvName = (TextView) findView(R.id.tv_name);
        tvTitle = (TextView) findView(R.id.tv_title);
        tvSubtitle = (TextView) findView(R.id.tv_subTitle);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        ClassModel info = (ClassModel) obj;

        if (info.isFirst()) {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(R.string.select_class);
        } else {
            tvTitle.setVisibility(View.GONE);
        }

        try {
            String class_ = info.getClass_();
            int i = Integer.parseInt(class_);
            tvName.setText("Class " +i);

        } catch (Exception e){
            tvName.setText(info.getClass_());
        }

//        tvName.setText(info.getClass_());
        tvSubtitle.setVisibility(View.GONE);
        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.CLASS_CLICKED, obj);
            }
        });
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.CLASS_CLICKED, obj);
            }
        });
    }
}
