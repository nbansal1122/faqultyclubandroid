package faqulty.club.holders;

import android.view.View;

import simplifii.framework.utility.AppConstants;

/**
 * Created by admin on 4/4/17.
 */

public class ShowMoreHolder extends BaseHolder {
    public ShowMoreHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.VIEW_MORE,obj);
            }
        });
    }
}
