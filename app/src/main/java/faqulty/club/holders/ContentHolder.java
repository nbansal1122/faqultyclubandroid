package faqulty.club.holders;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.activity.AudioVideoViewer;
import faqulty.club.activity.ImageActivity;
import faqulty.club.activity.PDFViewer;
import faqulty.club.models.content.ContentData;
import faqulty.club.models.feed.Datum;
import com.squareup.picasso.Picasso;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 21/11/16.
 */

public class ContentHolder extends BaseHolder {
    private TextView tv_views, tv_post_time, desctv;
    private ImageView ivActionContent, contentThumbnail, editIcon;
    private Datum feedData;
    private int width, height;
    private String completeUrl;

    public ContentHolder(View itemView) {
        super(itemView);
        tv_views = (TextView) findView(R.id.tv_view_image);
        tv_post_time = (TextView) findView(R.id.tv_days_ago_image);
        desctv = (TextView) findView(R.id.tv_description);
        ivActionContent = (ImageView) findView(R.id.iv_action_content);
        contentThumbnail = (ImageView) findView(R.id.iv_select_content);
        editIcon = (ImageView) findView(R.id.iv_edit_content);
//        width = context.getResources().getDisplayMetrics().widthPixels - (2 * context.getResources().getDimensionPixelOffset(R.dimen.margin_8dp));
//        height = context.getResources().getDimensionPixelOffset(R.dimen.ht_content);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final ContentData contentData = (ContentData) obj;
        String url = contentData.getContentUrl();
        if (!TextUtils.isEmpty(url)) {
            completeUrl = Util.getImageUrlBasedOnMimeType(url, contentData.getMimeType());
//            Picasso.with(context).load(Util.getCompleteUrl(url) + "&thumb=true").resize(width, height).centerInside().into(contentThumbnail);
            Picasso.with(context).load(completeUrl).into(contentThumbnail);
        }
        tv_views.setText(contentData.getNumViews() + " views");
        String time = Util.convertDateFormat(contentData.getCreatedOn(), Util.CONTENT_SERVER_DATE_PATTERN, Util.UI_ATTENDANCE_DATE_PATTERN);
        tv_post_time.setText(time);
        if (contentData.isShowPencil()) {
            editIcon.setVisibility(View.VISIBLE);
            editIcon.setTag(contentData);
            editIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventAction(AppConstants.ACTION_TYPE.ACTION_EDIT_CONTENT, v.getTag());
                }
            });
        } else {
            editIcon.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(contentData.getDescription())) {
            desctv.setVisibility(View.VISIBLE);
            desctv.setText(contentData.getDescription());
        } else {
            desctv.setVisibility(View.GONE);
        }

        itemView.setTag(contentData);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentData data = (ContentData) v.getTag();
                String mimeType = data.getMimeType();
                if (!TextUtils.isEmpty(mimeType)) {
                    if (mimeType.contains("pdf")) {
                        if (!TextUtils.isEmpty(completeUrl))
                            PDFViewer.startActivity(context, data.getContentUrl());
                    } else if (mimeType.contains("image")) {
                        if (!TextUtils.isEmpty(completeUrl))
                            ImageActivity.startActivity(context, completeUrl);
                    } else if (mimeType.contains("video")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(data.getContentUrl()), mimeType);
                    } else if (mimeType.contains("audio")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(data.getContentUrl()), mimeType);
                    }
                }
            }
        });
        placeActionIcon(contentData);
    }

    private void placeActionIcon(ContentData content) {
        String mimeType = content.getMimeType();
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.contains("pdf")) {
                ivActionContent.setImageResource(R.mipmap.pdf);
            } else if (mimeType.contains("image")) {
                ivActionContent.setImageResource(0);
            } else if (mimeType.contains("video")) {
                ivActionContent.setImageResource(R.mipmap.ic_play);
            } else if (mimeType.contains("audio")) {
                ivActionContent.setImageResource(R.mipmap.ic_play);
            }
        }
    }

}
