package faqulty.club.holders;

import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;
import faqulty.club.activity.ImageActivity;

import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;

import java.io.File;

/**
 * Created by my on 28-10-2016.
 */

public class ContentViewHolder extends BaseHolder {
    private final ImageView ivThumbnile;
    private ImageView deleteContent;
    private Uri uri;
    private SelectContent selectContent;

    public ContentViewHolder(View itemView) {
        super(itemView);
        deleteContent = (ImageView) findView(R.id.iv_delete_content);
        ivThumbnile= (ImageView) itemView.findViewById(R.id.iv_thumbnile);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        selectContent = (SelectContent) obj;
        final String filePath = selectContent.getFilePath();
        if (!TextUtils.isEmpty(filePath)) {
            File file = new File(filePath);
            uri = Uri.fromFile(file);
            ivThumbnile.setImageURI(uri);
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageActivity.startActivity(context,filePath);
            }
        });
        deleteContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, selectContent);
            }
        });

    }

}

