package faqulty.club.holders;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.assignment.RecentAssignments;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 28/01/17.
 */

public class RecentAssignmentHolder extends BaseHolder {
    TextView tvName, tvTitle, tvSubtitle;
    View sepItem;

    public RecentAssignmentHolder(View itemView) {
        super(itemView);
        tvName = (TextView) findView(R.id.tv_name);
        tvTitle = (TextView) findView(R.id.tv_title);
        tvSubtitle = (TextView) findView(R.id.tv_subTitle);
        sepItem = findView(R.id.sep_item);
    }

    @Override
    public void onBind(final int position, final Object obj) {
        super.onBind(position, obj);
        RecentAssignments assignment = (RecentAssignments) obj;
        if (assignment.isFirst()) {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(R.string.recently_viewed);
        } else {
            tvTitle.setVisibility(View.GONE);
        }
        if (assignment.isLast()) {
            sepItem.setVisibility(View.GONE);
        } else {
            sepItem.setVisibility(View.VISIBLE);
        }
        tvName.setText(assignment.getAssignment().getName());
        tvSubtitle.setVisibility(View.VISIBLE);
        String subtitle = assignment.getAssignment().getClassInfo().getClass_() + " / " + assignment.getAssignment().getSubjectInfo().getName() + " / "
                + assignment.getAssignment().getChapterInfo().getChapterName();
        tvSubtitle.setText(subtitle);
        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.RECENT_ASSIGNMENT_CLICKED, obj);
            }
        });
        tvSubtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.RECENT_ASSIGNMENT_CLICKED, obj);
            }
        });
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.RECENT_ASSIGNMENT_CLICKED, obj);
            }
        });
    }
}
