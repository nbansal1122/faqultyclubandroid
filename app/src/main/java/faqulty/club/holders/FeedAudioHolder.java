package faqulty.club.holders;

import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;
import faqulty.club.activity.AudioVideoViewer;

import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 30/01/17.
 */

public class FeedAudioHolder extends BaseHolder {
    ImageView deleteContent;

    public FeedAudioHolder(View itemView) {
        super(itemView);
        deleteContent = (ImageView) findView(R.id.iv_delete_content);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final SelectContent selectContent = (SelectContent) obj;
        final String filePath = selectContent.getFilePath();
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    AudioVideoViewer.startActivity(context,filePath,"");
            }
        });
        deleteContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, selectContent);
            }
        });
    }
}
