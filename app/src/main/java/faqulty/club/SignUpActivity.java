package faqulty.club;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import faqulty.club.R;

import faqulty.club.activity.SocialLoginActivity;
import faqulty.club.models.BaseApi;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontButton;
import simplifii.framework.widgets.CustomFontRadio;

import static java.lang.Long.valueOf;

/**
 * Created by saurabh on 13-09-2016.
 */
public class SignUpActivity extends BaseActivity {
    String userType;
    CustomFontRadio tutor, tution_center;
    CleverTapAPI cleverTap;

    //    TextInputLayout mobile_no, signup_confirm_password;
    EditText signup_password, mobile_no;
    CustomFontButton signup;
    String mobileno, password, confirmpassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        initToolBar("Sign Up");
        getHomeIcon(R.mipmap.arrows);


        tutor = (CustomFontRadio) findViewById(R.id.radio_tutor);
        tution_center = (CustomFontRadio) findViewById(R.id.radio_tution_center);
        mobile_no = (EditText) findViewById(R.id.et_mobile_number);
        signup_password = (EditText) findViewById(R.id.password);
//        signup_confirm_password = (TextInputLayout) findViewById(R.id.confirm_pasword);
        signup = (CustomFontButton) findViewById(R.id.sign_up_btn);
        setOnClickListener(R.id.sign_up_btn);

    }

    private void updateCleverTap() {
        long lg = valueOf(mobileno);
        try {
            cleverTap = CleverTapAPI.getInstance(getApplicationContext());
            HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
            profileUpdate.put("home", "Home Screen");
            profileUpdate.put("user", "Test User");
            profileUpdate.put("Phone", lg);
            profileUpdate.put("Gender", "M");
            profileUpdate.put("Age", 24);                               // Not required if DOB is set
            profileUpdate.put("Photo", "http://www.w3schools.com/css/trolltunga.jpg");    // URL to the Image

            profileUpdate.put("MSG-email", true);                      // Disable email notifications
            profileUpdate.put("MSG-push", true);                        // Enable push notifications
            profileUpdate.put("MSG-sms", true);

            cleverTap.profile.push(profileUpdate);
        } catch (CleverTapMetaDataNotFoundException e) {
            // thrown if you haven't specified your CleverTap Account ID or Token in your AndroidManifest.xml
        } catch (CleverTapPermissionsNotSatisfied e) {
            // thrown if you haven’t requested the required permissions in your AndroidManifest.xml
        }

    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    private void initData() {
        mobileno = mobile_no.getText().toString();
        password = signup_password.getText().toString();
//        confirmpassword = signup_confirm_password.getEditText().getText().toString();
        if (tutor.isChecked())
            userType = AppConstants.USER_TUTOR;
        else if (tution_center.isChecked())
            userType = AppConstants.USER_TUTION_CENTER;
    }

    private boolean isValid() {
//        clearTilError(R.id.til_mobile_number, R.id.password);
        String mobile = getEditText(R.id.et_mobile_number);
        String password = getEditText(R.id.password);
//        String confirmPassword = getTilText(R.id.confirm_pasword);
        if (!TextUtils.isEmpty(mobile) && mobile.length() == 10) {
        } else {
            showToast(getString(R.string.error_invalid_mobile));
            return false;
        }

        if (!TextUtils.isEmpty(password) && password.length() >= 4) {
        } else {
            showToast(getString(R.string.error_password));
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sign_up_btn:
                initData();
                if (isValid()) {
                    signUpUser();
                    updateCleverTap();

                }
        }
    }

    private JSONObject getSignupData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", mobileno);
            jsonObject.put("password", password);
            jsonObject.put("confirmPassword", password);
            jsonObject.put("source", "local");
            jsonObject.put("userType", userType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("signup", jsonObject.toString());
        return jsonObject;
    }

    private void signUpUser() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SIGNUP);
        httpParamObject.setClassType(BaseApi.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(getSignupData().toString());
        httpParamObject.setContentType("application/json");
//        httpParamObject.setJSONContentType();
        executeTask(AppConstants.TASKCODES.SIGNUP, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.SIGNUP:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi == null) {
                    showToast(R.string.server_error);
                    return;
                }
//                showToast(baseApi.getMsg());
                //TODO:remove this toast
                Preferences.saveData(AppConstants.PREF_KEYS.PHONE_NO, mobileno);
                startOTP(mobileno, password, userType);
                break;
            case AppConstants.TASKCODES.GET_USER:
                UserDetailsApi api = (UserDetailsApi) response;
                if (api != null) {
                    moveToProfileCompleteActivity();
                }
                break;
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.SIGNUP:
                if (re != null) {
                    switch (re.getHttpStatusCode()) {
                        case 412:
                            Preferences.saveData(AppConstants.PREF_KEYS.PHONE_NO, mobileno);
                            startOTP(mobileno, password, userType);
                            break;
                    }
                }
                break;
        }

    }

    private void startOTP(String mobileno, String password, String userType) {
        Intent i = new Intent(SignUpActivity.this, OtpActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.USERTYPE, userType);
        bundle.putString(AppConstants.BUNDLE_KEYS.PHONE, mobileno);
        bundle.putString(AppConstants.BUNDLE_KEYS.PASSWORD, password);
        i.putExtra(AppConstants.BUNDLE_KEYS.SOCIALSIGNUP, false);
        i.putExtras(bundle);
        startActivityForResult(i, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            getUserBasicProfile();
        } else {
            showToast(getString(R.string.verify_mobile_to_proceed));
        }

    }

    private void getUserBasicProfile() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUserUrl();
        obj.setClassType(UserDetailsApi.class);
        obj.addParameter("scope", "basic");
        executeTask(AppConstants.TASKCODES.GET_USER, obj);
    }

    private void moveToProfileCompleteActivity() {
        Intent i = new Intent(this, CompleteProfileActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onBackPressed() {
        startNextActivity(SocialLoginActivity.class);
        finish();
    }
}
