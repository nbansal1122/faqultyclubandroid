package faqulty.club;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.TimePicker;

import faqulty.club.R;

import faqulty.club.autoadapters.BoardsMultiSelectFragment;
import faqulty.club.autoadapters.TeachingLanguageMultiSelectFragment;
import faqulty.club.autocomplete.CoursesSegment;
import faqulty.club.fragments.SchoolFragment;
import faqulty.club.models.BoardsData;
import faqulty.club.models.SchoolsTutoredApi;
import faqulty.club.models.TeachingModeApi;
import faqulty.club.models.UserProfile.CoursesTaught;
import faqulty.club.models.UserProfile.TutorTeachingLanguage;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by saurabh on 06-09-2016.
 */
public class EditTutionDetailsActivity extends BaseActivity {

    //    MultiAutoCompleteTextView schoolsTutored;
    private UserDetailsApi user;
    private List<CoursesSegment> courseSegments = new ArrayList<>();
    private JSONArray coursesTaughtArray = new JSONArray();


    private List<BoardsData> boards = new ArrayList<>();
    private List<TutorTeachingLanguage> languages = new ArrayList<>();
    private List<SchoolsTutoredApi> schools = new ArrayList<>();

    private HashSet<Integer> selectedModesIds = new HashSet<>();

    private List<TutorTeachingLanguage> selectedTeachingLanguages = new ArrayList<>();
    private List<BoardsData> selectedBoards = new ArrayList<>();
    private List<SchoolsTutoredApi> selectedSchools = new ArrayList<>();
    private boolean isStudentHomeSelected, isTutorHomeSelected, isOnline;

    //    private MultiAutoView<BoardsData> boardsMultiAutoView;
    private TextView oneOnoneSP, oneOnOneFP, groupChargeSP, groupChargeFP;

    private List<SchoolFragment> schoolFragments = new ArrayList<>();

    private ChipView boardsChipView, languageChipView;
    private boolean isOneOnOneSelected, isGroupSelected;
    private LinearLayout schoolLayout, courseSegmentLayout;
    private int totalCourseCount = 0;

    private static final int MAX_COURSES = 10;
    private static final int MAX_SCHOOLS = 5;

    private static final int disableColorId = R.color.color_EditTextGray;
    private static final int enabledColorId = R.color.orange;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_tution_details);

        initToolBar(getString(R.string.title_tuition_details));
        getHomeIcon(R.mipmap.arrows);
        boardsChipView = (ChipView) findViewById(R.id.boards_chip_view);
        languageChipView = (ChipView) findViewById(R.id.language_chip_view);
        oneOnoneSP = (TextView) findViewById(R.id.et_starting_price_one);
        oneOnOneFP = (TextView) findViewById(R.id.et_final_price_one);
        groupChargeSP = (TextView) findViewById(R.id.et_starting_price_group);
        groupChargeFP = (TextView) findViewById(R.id.et_final_price_group);
        schoolLayout = (LinearLayout) findViewById(R.id.ll_schools);
        courseSegmentLayout = (LinearLayout) findViewById(R.id.ll_new_courses);

        setOnClickListener(R.id.tv_one_on_one_selector, R.id.tv_group_selector);

        user = UserDetailsApi.getInstance();

        if (user.isTutor()) {
            setViewsForTutor();
        } else {

            setViewsForTuitionCentre();
        }

        if (user.getWeekdayTutionTime() != null) {
            if (null != user.getWeekdayTutionTime().getStartTime())
                Util.setTimeTextView(user.getWeekdayTutionTime().getStartTime(), (TextView) findViewById(R.id.et_weekdays_starting_time));
            if (null != user.getWeekdayTutionTime().getEndTime())
                Util.setTimeTextView(user.getWeekdayTutionTime().getEndTime(), (TextView) findViewById(R.id.et_weekdays_end_time));
        }

        if (user.getWeekendTutionTime() != null) {
            if (null != user.getWeekendTutionTime().getStartTime())
                Util.setTimeTextView(user.getWeekendTutionTime().getStartTime(), (TextView) findViewById(R.id.et_weekends_starting_time));
            if (null != user.getWeekendTutionTime().getEndTime())
                Util.setTimeTextView(user.getWeekendTutionTime().getEndTime(), (TextView) findViewById(R.id.et_weekends_end_time));
        }

        setOnClickListener(R.id.btn_save_changes, R.id.et_weekdays_starting_time, R.id.et_weekdays_end_time,
                R.id.et_weekends_starting_time, R.id.et_weekends_end_time, R.id.tv_add_new_course,
                R.id.tv_tutor_home, R.id.tv_students_home, R.id.tv_online, R.id.lbl_boards, R.id.lbl_teaching_language,
                R.id.rl_boards, R.id.rl_languages, R.id.tv_add_new_school);

        addTutorCourses();

        initSpinners();

    }

    private void setViewsForTutor() {
        hideVisibility(R.id.til_batch, R.id.til_experience);
        if (user.getOneOnOneTutionCharge() != null) {
            isOneOnOneSelected = true;
            setDataTextInputLayout(R.id.til_starting_price_one, user.getOneOnOneTutionCharge().getStartPrice());
            setDataTextInputLayout(R.id.til_final_price_one, user.getOneOnOneTutionCharge().getFinalPrice());
        } else {
            isOneOnOneSelected = false;
        }
        initOneOneCharges(isOneOnOneSelected);

        if (user.getGroupTutionCharge() != null) {
            isGroupSelected = true;
            setDataTextInputLayout(R.id.til_starting_price_group, user.getGroupTutionCharge().getStartPrice());
            setDataTextInputLayout(R.id.til_final_price_group, user.getGroupTutionCharge().getFinalPrice());
        } else {
            isGroupSelected = false;
        }
        initGroupCharges(isGroupSelected);
    }

    private void setViewsForTuitionCentre() {
        hideVisibility(R.id.ll_tuition_type, R.id.lbl_tuition_type, R.id.lbl_class_size, R.id.ll_class_size, R.id.lbl_tuition_charges_per_hour
                , R.id.layout_one, R.id.lbl_one_on_one_charges);
        setText(getString(R.string.tuition_charges_per_hour), R.id.lbl_group_charges);
        isGroupSelected = true;
        showVisibility(R.id.layout_group, R.id.til_starting_price_group, R.id.til_final_price_group, R.id.til_batch);
        if (user.getGroupTutionCharge() != null) {
            isGroupSelected = true;
            setDataTextInputLayout(R.id.til_starting_price_group, user.getGroupTutionCharge().getStartPrice());
            setDataTextInputLayout(R.id.til_final_price_group, user.getGroupTutionCharge().getFinalPrice());
            setDataTextInputLayout(R.id.til_batch, user.getGroupTutionCharge().getBatchSize() + "");
        } else {
        }
        isGroupSelected = true;
        setDataTextInputLayout(R.id.til_experience, user.getExperience() + "");
    }

    private void setTeachingModes() {
        if (user.isTutor()) {
            if (user.getTutorTeachingMode() != null) {
                for (TeachingModeApi api : user.getTutorTeachingMode()) {
                    selectedModesIds.add(api.getId());
                }
            }
            setViewsForModes();
        }
    }

    private void setViewsForModes() {
        setSelectedViewForTV(R.id.tv_tutor_home, selectedModesIds.contains(2));
        setSelectedViewForTV(R.id.tv_students_home, selectedModesIds.contains(1));
        setSelectedViewForTV(R.id.tv_online, selectedModesIds.contains(3));
    }


    private void initOneOneCharges(boolean isSelected) {
        isOneOnOneSelected = isSelected;
        setSelectedViewForTV(R.id.tv_one_on_one_selector, isSelected);
        if (isOneOnOneSelected) {
            showVisibility(R.id.layout_one, R.id.lbl_one_on_one_charges);
        } else {
            setDataTextInputLayout(R.id.til_starting_price_one, "");
            setDataTextInputLayout(R.id.til_final_price_one, "");
            hideVisibility(R.id.layout_one, R.id.lbl_one_on_one_charges);
        }
        hideTuitionChargesTextView();
    }

    private void hideTuitionChargesTextView() {
        if (isOneOnOneSelected || isGroupSelected) {
            showVisibility(R.id.lbl_tuition_charges_per_hour);
        } else {
            hideVisibility(R.id.lbl_tuition_charges_per_hour);
        }
    }

    private void initGroupCharges(boolean isSelected) {
        isGroupSelected = isSelected;
        setSelectedViewForTV(R.id.tv_group_selector, isSelected);
        if (isGroupSelected) {
            showVisibility(R.id.layout_group, R.id.lbl_group_charges);
        } else {
            setDataTextInputLayout(R.id.til_starting_price_group, "");
            setDataTextInputLayout(R.id.til_final_price_group, "");
            hideVisibility(R.id.layout_group, R.id.lbl_group_charges);
        }
        hideTuitionChargesTextView();
    }

    private void setSelectedViewForTV(int textViewId, boolean isSelected) {
        TextView tv = (TextView) findViewById(textViewId);
        if (isSelected) {
            tv.setBackgroundResource(R.drawable.shape_class_size);
            tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        } else {
            tv.setBackgroundResource(R.drawable.shape_class_size_unselected);
            tv.setTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }


    private void setDataMultiTextView(int id, String s) {
        MultiAutoCompleteTextView multiAutoCompleteTextView = (MultiAutoCompleteTextView) findViewById(id);
        multiAutoCompleteTextView.setText(s);
    }

    private void setDataEditText(int id, String text) {
    }

    private void initSpinners() {
//        boardsMultiAutoView = new MultiAutoView<>(this, R.id.spin_boards);
//        languagesMultiAutoView = new MultiAutoView<>(this, R.id.spin_teaching_languages);
//        modesMultiAutoView = new MultiAutoView<>(this, R.id.spin_tution_type);
//        schoolsMultiAutoView = new MultiAutoView<>(this, R.id.spin_schools_of_tution_tutored);
        getBoardsList();
        getLanguagesList();
        getModesList();
        selectedTeachingLanguages.addAll(user.getTutorTeachingLanguage());
        selectedBoards.addAll(user.getTutorBoard());
        selectedSchools.addAll(user.getSchoolsTutored());
        setTeachingModes();
        setBoardsChipView();
        setLanguagesChipView();
        addSchools();
    }

    private void addSchools() {
        if (selectedSchools.size() > 0) {
            for (int i = 0; i < selectedSchools.size(); i++) {
                SchoolsTutoredApi schoolsTutoredApi = selectedSchools.get(i);
                addSchoolFragment(schoolsTutoredApi);
            }
        } else {
            addSchoolFragment(null);
        }
    }

    private void setMaxCourseRestriction(boolean isMax) {
        setMaxRestriction(R.id.tv_add_new_course, isMax);
    }

    private void setMaxSchoolsRestriction(boolean isMax) {
        setMaxRestriction(R.id.tv_add_new_school, isMax);
    }

    private void setMaxRestriction(int tvId, boolean isMax) {
        TextView tv = (TextView) findViewById(tvId);
        if (isMax) {
            tv.setTextColor(ContextCompat.getColor(this, disableColorId));
            tv.setOnClickListener(null);
        } else {
            tv.setTextColor(ContextCompat.getColor(this, enabledColorId));
            tv.setOnClickListener(this);
        }
    }

    private void addSchoolFragment(SchoolsTutoredApi schoolsTutoredApi) {
        if (schoolFragments.size() < MAX_SCHOOLS) {
            if (schoolFragments.size() == MAX_SCHOOLS - 1) {
                setMaxSchoolsRestriction(true);
            }
            SchoolFragment f = SchoolFragment.getInstance(schoolsTutoredApi, schoolFragments.size(), schoolDeleteInterface);
            schoolFragments.add(f);
            getSupportFragmentManager().beginTransaction().add(R.id.ll_schools, f).commitAllowingStateLoss();
        }
    }

    private SchoolFragment.SchoolDeleteInterface schoolDeleteInterface = new SchoolFragment.SchoolDeleteInterface() {
        @Override
        public void onSchoolDelete(SchoolFragment schoolFragment, SchoolsTutoredApi schoolsTutoredApi) {
            if (schoolLayout.getChildCount() == 1) {
                schoolFragment.onClear();
                return;
            }
            schoolLayout.removeViewAt(schoolFragment.index);
            schoolFragments.remove(schoolFragment.index);
            for (int i = 0; i < schoolFragments.size(); i++) {
                schoolFragments.get(i).index = i;
            }
            if (schoolLayout.getChildCount() == 0) {
                addSchoolFragment(null);
            }
            Util.hideKeyboard(EditTutionDetailsActivity.this);
            setMaxSchoolsRestriction(schoolFragments.size() >= MAX_SCHOOLS);
        }
    };

    private void getBoardsList() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_BOARDS);
        obj.setClassType(BoardsData.class);
        executeTask(AppConstants.TASKCODES.GET_BOARDS, obj);
    }

    private void getLanguagesList() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_LANGUAGES);
        obj.setClassType(TutorTeachingLanguage.class);
        executeTask(AppConstants.TASKCODES.GET_LANGUAGES, obj);
    }


    private void getModesList() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_MODES);
        obj.setClassType(TeachingModeApi.class);
        executeTask(AppConstants.TASKCODES.GET_MODES, obj);
    }


    private void addTutorCourses() {
        List<CoursesTaught> tutorCourses = user.getCoursesTaught();
        if (tutorCourses != null && tutorCourses.size() > 0) {
            for (CoursesTaught l : tutorCourses) {
                addCourseSegment(l);
            }
        } else {
            addCourseSegment(null);
        }
    }

    private void addCourseSegment(CoursesTaught course) {
        if (courseSegments.size() < MAX_COURSES) {
            if (courseSegments.size() == MAX_COURSES - 1) {
                setMaxCourseRestriction(true);
            }
            CoursesSegment f = CoursesSegment.getInstance(course, coursesSegmentDeleteListener, courseSegments.size());
            courseSegments.add(f);
            ++totalCourseCount;
            getSupportFragmentManager().beginTransaction().add(R.id.ll_new_courses, f).commitAllowingStateLoss();
        }
    }

    private CoursesSegment.CoursesSegmentDeleteListener coursesSegmentDeleteListener = new CoursesSegment.CoursesSegmentDeleteListener() {
        @Override
        public void onItemDelete(CoursesSegment segment) {
            courseSegmentLayout.removeViewAt(segment.index);
            courseSegments.remove(segment.index);
            for (int i = 0; i < courseSegments.size(); i++) {
                courseSegments.get(i).index = i;
            }

            if (courseSegmentLayout.getChildCount() == 0) {
                addCourseSegment(null);
            }
            setMaxCourseRestriction(courseSegments.size() >= MAX_COURSES);
        }
    };

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_save_changes:
                if(!isValidCourses()){
                    return;
                }
                if (getValidPrice() == 1 && getValidTime() == 1 && isCoursesValid()) {
                    HttpParamObject httpParamObject = new HttpParamObject();
                    httpParamObject.setUserTypeUrl("tuition");
                    httpParamObject.setClassType(UserDetailsApi.class);
                    httpParamObject.setPutMethod();
                    httpParamObject.setContentType("application/json");
                    JSONObject obj = getJsonObject();
                    if (null != obj) {
                        httpParamObject.setJson(obj.toString());
                        executeTask(AppConstants.TASKCODES.UPDATE_TUTION_DETAILS, httpParamObject);
                    }
                }
                break;
            case R.id.et_weekdays_starting_time:
                clockPickerDialog(R.id.et_weekdays_starting_time);
                break;
            case R.id.et_weekdays_end_time:
                clockPickerDialog(R.id.et_weekdays_end_time);
                break;
            case R.id.et_weekends_starting_time:
                clockPickerDialog(R.id.et_weekends_starting_time);
                break;
            case R.id.et_weekends_end_time:
                clockPickerDialog(R.id.et_weekends_end_time);
                break;
            case R.id.tv_add_new_course:
                if (totalCourseCount < 10)
                    addCourseSegment(null);
                else
                    showToast(getString(R.string.course_limit_reached));
                break;
            case R.id.iv_boards_arrow:
            case R.id.rl_boards:
                showBoardsDialog();
                break;
            case R.id.lbl_teaching_language:
            case R.id.rl_languages:
                showTeachingLanguageDialog();
                break;
            case R.id.tv_one_on_one_selector:
                initOneOneCharges(!isOneOnOneSelected);
                break;
            case R.id.tv_group_selector:
                initGroupCharges(!isGroupSelected);
                break;
            case R.id.tv_tutor_home:
                refreshSelectedModes(2);
                break;
            case R.id.tv_students_home:
                refreshSelectedModes(1);
                break;
            case R.id.tv_online:
                refreshSelectedModes(3);
                break;
            case R.id.tv_add_new_school:
                addSchoolFragment(null);
                break;
        }
    }

    private boolean isValidCourses() {
        for (CoursesSegment c : courseSegments) {
            JSONObject obj = null;
            try {
                obj = c.getJsonObject();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (obj != null) {
                coursesTaughtArray.put(obj);
            }
        }
        if(coursesTaughtArray.length()==0){
            showToast("Courses cannot be empty !");
            return false;
        }
        return true;
    }

    private void refreshSelectedModes(Integer id) {
        if (selectedModesIds.contains(id)) {
            selectedModesIds.remove(id);
        } else {
            selectedModesIds.add(id);
        }
        setViewsForModes();
    }


    private boolean isValidWeekdayTime() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        String wkstartTime = getDataTextView(R.id.et_weekdays_starting_time);
        String wkEndTime = getDataTextView(R.id.et_weekdays_end_time);

        if (TextUtils.isEmpty(wkstartTime) && TextUtils.isEmpty(wkEndTime)) {
            return true;
        }
        if (TextUtils.isEmpty(wkstartTime) && !TextUtils.isEmpty(wkEndTime)) {
            showToast(R.string.error_empty_wkday_end_time);
            return false;
        }
        if (!TextUtils.isEmpty(wkstartTime) && TextUtils.isEmpty(wkEndTime)) {
            showToast(R.string.error_empty_wkday_startTime);
            return false;
        }
        String weekdayST = Util.convertDateFormat(wkstartTime, "hh:mm a", "HH:mm:ss");
        String weekdayET = Util.convertDateFormat(wkEndTime, "hh:mm a", "HH:mm:ss");
        Date weekdayStartTime = sdf.parse(weekdayST);
        Date weekdayEndTime = sdf.parse(weekdayET);
        if (weekdayEndTime.compareTo(weekdayStartTime) <= 0) {
            showToast("Weekday end time can't be before start time");
            return false;
        }
        return true;
    }

    private boolean isValidWeekendTime() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        String wkstartTime = getDataTextView(R.id.et_weekends_starting_time);
        String wkEndTime = getDataTextView(R.id.et_weekends_end_time);

        if (TextUtils.isEmpty(wkstartTime) && TextUtils.isEmpty(wkEndTime)) {
            return true;
        }
        if (TextUtils.isEmpty(wkstartTime) && !TextUtils.isEmpty(wkEndTime)) {
            showToast(R.string.error_empty_wkend_end_time);
            return false;
        }
        if (!TextUtils.isEmpty(wkstartTime) && TextUtils.isEmpty(wkEndTime)) {
            showToast(R.string.error_empty_wkend_start_time);
            return false;
        }
        String weekdayST = Util.convertDateFormat(wkstartTime, "hh:mm a", "HH:mm:ss");
        String weekdayET = Util.convertDateFormat(wkEndTime, "hh:mm a", "HH:mm:ss");
        Date weekdayStartTime = sdf.parse(weekdayST);
        Date weekdayEndTime = sdf.parse(weekdayET);
        if (weekdayEndTime.compareTo(weekdayStartTime) <= 0) {
            showToast(getString(R.string.end_time_greater));
            return false;
        }
        return true;
    }

    private int getValidTime() {
        try {
            boolean isValid = isValidWeekdayTime() && isValidWeekendTime();
            return isValid ? 1 : 0;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 1;
    }

    private int getValidPrice() {
        if (isOneOnOneSelected) {
            if (isValidPrice(R.id.til_starting_price_one) && isValidPrice(R.id.til_final_price_one)) {
                if (Integer.parseInt(getTilText(R.id.til_final_price_one)) < Integer.parseInt(getTilText(R.id.til_starting_price_one))) {
                    showToast(getString(R.string.price_error));
                    return 0;
                }
            }
        }
        if (isGroupSelected) {
            if (isValidPrice(R.id.til_starting_price_group) && isValidPrice(R.id.til_final_price_group)) {
                if (Integer.parseInt(getTilText(R.id.til_final_price_group)) < Integer.parseInt(getTilText(R.id.til_starting_price_group))) {
                    showToast(getString(R.string.price_error));
                    return 0;
                }
            }
        }
        return 1;
    }

    private boolean isValidPrice(int tilId) {
        String tilText = getTextFromTil(tilId);
        if (TextUtils.isEmpty(tilText)) {
            return false;
        }
        return true;
    }

    private boolean isCoursesValid() {
        HashSet<String> subjects = new HashSet<>();
        for (CoursesSegment c : courseSegments) {
            boolean flag = c.isValid();
            if (flag) {
                String subject = c.getSubject();
                if (!TextUtils.isEmpty(c.getSubject())) {
                    if (subjects.contains(subject)) {
                        showToast(getString(R.string.subjects_repeated));
                        return false;
                    } else {
                        subjects.add(subject);
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        JSONObject groupCharges = new JSONObject();
        JSONObject oneOnOneCharges = new JSONObject();
        JSONObject weekdayTiming = new JSONObject();
        JSONObject weekendTiming = new JSONObject();
        List<String> selectedSubjects = new ArrayList<>();
        try {
            if (!user.isTutor()) {
                isGroupSelected = true;
            }
            if (isGroupSelected) {
                groupCharges.put("startingPrice", getTilText(R.id.til_starting_price_group));
                groupCharges.put("finalPrice", getTilText(R.id.til_final_price_group));
            } else {
//                groupCharges.put("startingPrice", null);
//                groupCharges.put("finalPrice", null);
                groupCharges = null;
            }
            if (isOneOnOneSelected) {
                oneOnOneCharges.put("startingPrice", getTilText(R.id.til_starting_price_one));
                oneOnOneCharges.put("finalPrice", getTilText(R.id.til_final_price_one));
            } else {
//                oneOnOneCharges.put("startingPrice", null);
//                oneOnOneCharges.put("finalPrice", null);
                oneOnOneCharges = null;
            }
            weekdayTiming.put("startTime", Util.getTimeTextView((TextView) findViewById(R.id.et_weekdays_starting_time), ""));
            weekdayTiming.put("endTime", Util.getTimeTextView((TextView) findViewById(R.id.et_weekdays_end_time), ""));
            weekendTiming.put("startTime", Util.getTimeTextView((TextView) findViewById(R.id.et_weekends_starting_time), ""));
            weekendTiming.put("endTime", Util.getTimeTextView((TextView) findViewById(R.id.et_weekends_end_time), ""));

            if (user.isTutor()) {
                jsonObject.put("gender", user.getGender());
                jsonObject.put("tutorTeachingMode", getTeachingModesArray());
                jsonObject.put("oneOnOneTutionCharge", oneOnOneCharges);
            } else {
                String batchSize = getTilText(R.id.til_batch);
                String experience = getTilText(R.id.til_experience);
                if (!TextUtils.isEmpty(batchSize) && groupCharges != null) {
                    groupCharges.put("batchSize", Integer.parseInt(batchSize));
                }
                if (!TextUtils.isEmpty(experience)) {
                    jsonObject.put("experience", Integer.parseInt(experience));
                } else {
                    jsonObject.put("experience", 0);
                }
            }

            jsonObject.put("name", user.getName());

            jsonObject.put("email", user.getEmail());
            jsonObject.put("userDetails", user.getUserDetails());
            jsonObject.put("groupTutionCharge", groupCharges);

            jsonObject.put("weekdayTutionTime", weekdayTiming);
            jsonObject.put("weekendTutionTime", weekendTiming);
//            jsonObject.put("experience", getEditText(R.id.et_teaching_experience));
            jsonObject.put("tutorBoard", getBoardsJsonArray());
            jsonObject.put("tutorTeachingLanguage", getLanguageJsonArray());
            jsonObject.put("tutorSchool", getSchoolsArray());
            jsonObject.put("coursesTaught", coursesTaughtArray);
//            jsonObject.put("schoolsTutored",getSchoolsArray());
//            "tutorBoard": [5,6],  // this is from boards API
//            "tutorTeachingLanguage": [4,5], // this is from languages API
//            "TeachingModeApi": [1,2] // this is from modes API

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean isNotRepeating(String obj, List<String> selectedSubjects) {
        for (String subject : selectedSubjects) {
            if (obj.compareTo(subject) == 0) {
                return false;
            }
        }
        return true;
    }

    private void clockPickerDialog(int id) {
        final TextView time = (TextView) findViewById(id);
        final StringBuilder timestr = new StringBuilder();
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                timestr.append(pad(selectedHour)).append(":").append(pad(selectedMinute)).append(":00");
                Util.setTimeTextView(timestr.toString(), time);
            }
        }, hour, minute, true);//Yes 24 hour time
        Util.setTimeTextView(timestr.toString(), time);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private String pad(int value) {

        if (value < 10) {
            return "0" + value;
        }
        return "" + value;
    }

    private void setDataTextInputLayout(int id, String text) {
        final TextInputLayout viewById = (TextInputLayout) findViewById(id);
        if (text == null) {
            viewById.getEditText().setText("");
        } else
            viewById.getEditText().setText(text);
    }

    private void setDataTextInputLayout(int id, Integer text) {
        final TextInputLayout viewById = (TextInputLayout) findViewById(id);
        if (text == null || text == 0) {
            viewById.getEditText().setText("");
        } else
            viewById.getEditText().setText(String.valueOf(text));
    }

    private void setDataTextView(int id, String t) {
        final TextView viewById = (TextView) findViewById(id);
        if (t != null) {
            viewById.setText(t);
        }
    }

    private String getDataTextView(int id) {
        final TextView viewById = (TextView) findViewById(id);
        return viewById.getText().toString();
    }


    private void setEditText(int id, int text) {
        final EditText viewById = (EditText) findViewById(id);
        viewById.setText(String.valueOf(text));
    }


    private String getDataEditText(int id) {
        final EditText viewById = (EditText) findViewById(id);
        return viewById.getText().toString();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null)
            return;
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_TUTION_DETAILS:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    showToast(getString(R.string.tution_details_updated));
                    setResult(RESULT_OK);
                    finish();
                } else {
                    showToast(getString(R.string.tution_details_cant_be_updated));
                }
                break;
            case AppConstants.TASKCODES.GET_BOARDS:
//                this.boardsMultiAutoView.setItems((List<BoardsData>) response);
                this.boards.clear();
                this.boards.addAll((List<BoardsData>) response);
                break;

            case AppConstants.TASKCODES.GET_LANGUAGES:
//                this.languagesMultiAutoView.setItems((List<TutorTeachingLanguage>) response);
                this.languages.clear();
                this.languages.addAll((List<TutorTeachingLanguage>) response);
                break;

        }
    }

    private JSONArray getBoardsJsonArray() {
        JSONArray array = new JSONArray();
//        HashSet<BoardsData> boardsApis = boardsMultiAutoView.getSelectedList();
        for (BoardsData b : selectedBoards) {
            if (b.getId() != null) {
                array.put(b.getId());
            }
        }
        return array;
    }

    private JSONArray getLanguageJsonArray() {
        JSONArray array = new JSONArray();
//        HashSet<TutorTeachingLanguage> boardsApis = languagesMultiAutoView.getSelectedList();
        for (TutorTeachingLanguage b : selectedTeachingLanguages) {
            if (b.getId() != null) {
                array.put(b.getId());
            }
        }
        return array;
    }

    private JSONArray getTeachingModesArray() {
        JSONArray array = new JSONArray();
        for (Integer id : selectedModesIds) {
            array.put(id);
        }
        return array;
    }

    private JSONArray getSchoolsArray() throws JSONException {
        JSONArray array = new JSONArray();
        for (SchoolFragment f : schoolFragments) {
            JSONObject obj = f.getJsonObject();
            if (obj != null) {
                array.put(obj);
            }
        }
        return array;
    }

    private void setBoardsChipView() {
        List<Chip> chips = new ArrayList<>();
        for (BoardsData api : selectedBoards) {
            chips.add(api);
        }
        boardsChipView.setChipLayoutRes(R.layout.row_chip);
        boardsChipView.setChipList(chips);
        boardsChipView.setOnChipClickListener(boardsChipClickLister);
    }

    private OnChipClickListener boardsChipClickLister = new OnChipClickListener() {
        @Override
        public void onChipClick(Chip chip) {
            BoardsData clickedAPi = null;
            for (BoardsData api : selectedBoards) {
                if (api.getDisplayString().equalsIgnoreCase(chip.getText())) {
                    clickedAPi = api;
                }
            }
            if (clickedAPi != null) {
                selectedBoards.remove(clickedAPi);
                setBoardsChipView();
            }
        }
    };

    private void setLanguagesChipView() {
        List<Chip> chips = new ArrayList<>();
        for (TutorTeachingLanguage lang : selectedTeachingLanguages) {
            chips.add(lang);
        }
        languageChipView.setChipLayoutRes(R.layout.row_chip);
        languageChipView.setChipList(chips);
        languageChipView.setOnChipClickListener(languageChipClickListener);
    }

    private OnChipClickListener languageChipClickListener = new OnChipClickListener() {
        @Override
        public void onChipClick(Chip chip) {
            TutorTeachingLanguage clickedChip = null;
            for (TutorTeachingLanguage api : selectedTeachingLanguages) {
                if (api.getDisplayString().equalsIgnoreCase(chip.getText())) {
                    clickedChip = api;
                }
            }
            if (clickedChip != null) {
                selectedTeachingLanguages.remove(clickedChip);
                setLanguagesChipView();
            }

        }
    };

    private void showBoardsDialog() {

        BoardsMultiSelectFragment f = BoardsMultiSelectFragment.getInstance("Select Board",
                boards, new BoardsMultiSelectFragment.ItemSelector() {
                    @Override
                    public void onItemsSelected(List<BoardsData> selectedItems) {
                        selectedBoards.clear();
                        selectedBoards.addAll(selectedItems);
                        setBoardsChipView();
                    }
                }, selectedBoards);

        f.show(getSupportFragmentManager(), "Boards");
    }

    private void showTeachingLanguageDialog() {
        TeachingLanguageMultiSelectFragment f = TeachingLanguageMultiSelectFragment.getInstance("Select Teaching Language",
                languages, new TeachingLanguageMultiSelectFragment.ItemSelector() {
                    @Override
                    public void onItemsSelected(List<TutorTeachingLanguage> selectedItems) {
                        selectedTeachingLanguages.clear();
                        selectedTeachingLanguages.addAll(selectedItems);
                        setLanguagesChipView();
                    }
                }, selectedTeachingLanguages);
        f.show(getSupportFragmentManager(), "Teaching Language");
    }


}
