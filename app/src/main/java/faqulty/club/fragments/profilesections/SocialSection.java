package faqulty.club.fragments.profilesections;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.EditSocialProfileActivity;
import faqulty.club.R;
import faqulty.club.models.UserProfile.UserDetailsApi;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 17/09/16.
 */
public class SocialSection extends BaseProfileSection {
    private static final String EMPTY_DEFAULT_TEXT = "Not Mentioned";

    public SocialSection(Activity activity, UserDetailsApi user, View view, Fragment fragment) {
        super(activity, user, view, fragment);

    }

    @Override
    protected void initViews() {
        super.initViews();
        //// TODO: remove else from here
        if (user.getFblink() != null)
            if (!TextUtils.isEmpty(user.getFblink()))
                setDataTextView(R.id.tv_facebookURL, user.getFblink());
            else {
                setDataTextView(R.id.tv_facebookURL, EMPTY_DEFAULT_TEXT);
            }
        if (user.getTwitterlink() != null)
            if (!TextUtils.isEmpty(user.getTwitterlink()))
                setDataTextView(R.id.tv_twitterURL, user.getTwitterlink());
            else {
                setDataTextView(R.id.tv_twitterURL, EMPTY_DEFAULT_TEXT);
            }
        if (user.getLnlink() != null)
            if (!TextUtils.isEmpty(user.getLnlink()))
                setDataTextView(R.id.tv_linkedInURL, user.getLnlink());
            else {
                setDataTextView(R.id.tv_linkedInURL, EMPTY_DEFAULT_TEXT);
            }

        setDataTextView(R.id.tv_faqultyURL, user.getFacultyProfileURI() + "");

        ImageView editSocialProfile = (ImageView) findView(R.id.iv_social_profile);
        editSocialProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, EditSocialProfileActivity.class);
                fragment.startActivityForResult(i, AppConstants.TASKCODES.EDIT_SOCIAL_PROFILE);
            }
        });
    }

    private void setDataTextView(int id, String link) {
        TextView viewId = (TextView) findView(id);
        viewId.setText(link);
    }

}
