package faqulty.club.fragments.profilesections;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.EditQualificatonActivity;
import faqulty.club.R;
import faqulty.club.models.UserProfile.UserDetailsApi;

import simplifii.framework.utility.AppConstants;

/**
 * Created by saurabh on 17-09-2016.
 */
public class QualificationSection extends BaseProfileSection {

    private static final String EMPTY_DEFAULT_TEXT = "Not Mentioned";

    public QualificationSection(Activity activity, UserDetailsApi user, View view, Fragment fragment) {
        super(activity, user, view, fragment);
    }


    @Override
    protected void initViews() {
        super.initViews();
        if (user.getExperience() != null)
            setDataTextView(R.id.tv_teaching_experience, user.getExperience().toString());
        if (user.getTutorEducationDetail() != null && user.getTutorEducationDetail().size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getTutorEducationDetail().size(); x++) {
                if (user.getTutorEducationDetail().get(x).getTutorDegree().getDegree() != null &&
                        user.getTutorEducationDetail().get(x).getTutorInstitute().getInstituteName() != null)
                    stringBuilder.append(user.getTutorEducationDetail().get(x).getTutorDegree().getDegree() + ", "
                            + user.getTutorEducationDetail().get(x).getTutorInstitute().getInstituteName() + "\n");
            }
            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            setDataTextView(R.id.tv_education_qualification, stringBuilder.toString());
        } else {
            setDataTextView(R.id.tv_education_qualification, EMPTY_DEFAULT_TEXT);
        }

        if (user.getTutorAwards() != null && user.getTutorAwards().size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getTutorAwards().size(); x++) {
                stringBuilder.append(user.getTutorAwards().get(x).getAwards() + "\n");
            }
            if (stringBuilder.length() > 1) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
//            TextView tv = (TextView) findView(R.id.tv_awards);
//            tv.setText(stringBuilder.toString());
            setDataTextView(R.id.tv_award, stringBuilder.toString());
        } else{
            setDataTextView(R.id.tv_award, EMPTY_DEFAULT_TEXT);
        }

        ImageView editQualificationSection = (ImageView) findView(R.id.iv_qualification_edit_icon);
        editQualificationSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, EditQualificatonActivity.class);
                fragment.startActivityForResult(i, AppConstants.TASKCODES.EDIT_QUALIFICATION);
            }
        });


    }

    private void setDataTextView(int id, String text) {
        TextView viewId = (TextView) findView(id);
        if (viewId != null)
            viewId.setText(text);

    }

    private void setTextViewColor(int tv_username) {
        TextView tv = (TextView) findView(tv_username);
        if (tv != null) {
            tv.setTextColor(activity.getResources().getColor(R.color.color_red_bg));
        }
    }
}
