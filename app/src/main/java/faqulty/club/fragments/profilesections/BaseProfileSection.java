package faqulty.club.fragments.profilesections;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import faqulty.club.models.UserProfile.UserDetailsApi;

/**
 * Created by nbansal2211 on 17/09/16.
 */
public class BaseProfileSection {
    protected Activity activity;
    protected UserDetailsApi user;
    protected  View view;
    protected Fragment fragment;

    public BaseProfileSection(Activity activity, UserDetailsApi user, View view, Fragment fragment){
        this.activity = activity;
        this.user = user;
        this.view = view;
        this.fragment = fragment;
        initViews();
    }

    protected void initViews(){

    }

    protected View findView(int id){
        return view.findViewById(id);
    }

    protected void hideVisibility(int ... ids){
        for(int i : ids){
            findView(i).setVisibility(View.GONE);
        }
    }
}
