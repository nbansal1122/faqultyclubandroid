package faqulty.club.fragments.profilesections;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.UserProfile.FacultyData;
import faqulty.club.models.UserProfile.TutorEducationDetail;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 24/10/16.
 */

public class FacultySection extends BaseProfileSection {

    private LinearLayout facultyLayout;

    public FacultySection(Activity activity, UserDetailsApi user, View view, Fragment fragment) {
        super(activity, user, view, fragment);
    }

    @Override
    protected void initViews() {
        super.initViews();
        facultyLayout = (LinearLayout) findView(R.id.ll_faculty_vertical);
        TextView facultyCount = (TextView) findView(R.id.tv_faculty_count);
        List<FacultyData> faculties = user.getFaculties();
        int facSize = 0;
        if (faculties != null) {
            facSize = faculties.size();
            for (FacultyData data : faculties) {
                addFacultyRow(data);
            }
        } else {

        }
        facultyCount.setText(activity.getString(R.string.faculty_count, String.valueOf(facSize)));
    }

    private void addFacultyRow(FacultyData data) {
        View v = LayoutInflater.from(activity).inflate(R.layout.row_faculty, null);
        TextView facultyName = (TextView) v.findViewById(R.id.tv_faculty_name);
        TextView facultyEducation = (TextView) v.findViewById(R.id.tv_faculty_education);
        TextView facultyAward = (TextView) v.findViewById(R.id.tv_faculty_award);
        facultyName.setText(data.getName());
        addEducationDetail(data.getTutorEducationDetail(), facultyEducation, facultyAward);
        if (!TextUtils.isEmpty(data.getImage())) {
            ImageView iv = (ImageView) v.findViewById(R.id.profilepic);
            Picasso.with(activity).load(Util.getCompleteUrl(data.getImage())).into(iv);
        }
        facultyLayout.addView(v);
    }

    private void addEducationDetail(List<TutorEducationDetail> details, TextView textView, TextView facCollege) {
        if (details != null && details.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < details.size(); x++) {
                if (details.get(x).getTutorDegree().getDegree() != null &&
                        details.get(x).getTutorInstitute().getInstituteName() != null)
                    stringBuilder.append(details.get(x).getTutorDegree().getDegree() + ", "
                            + details.get(x).getTutorInstitute().getInstituteName() + "\n");
            }
            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            facCollege.setText(details.get(0).getCollege() + "");
            textView.setText(stringBuilder.toString());
        } else {
            textView.setText(activity.getString(R.string.education_details));
        }
    }
}
