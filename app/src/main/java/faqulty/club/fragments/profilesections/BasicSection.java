package faqulty.club.fragments.profilesections;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.EditIntroActivity;
import faqulty.club.R;
import faqulty.club.models.CityApi;
import faqulty.club.models.LocationApi;
import faqulty.club.models.UserProfile.TutorLocation;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 17/09/16.
 */
public class BasicSection extends BaseProfileSection {
    private TextView tvAboutyou;

    public BasicSection(Activity activity, UserDetailsApi user, View view, Fragment fragment) {
        super(activity, user, view, fragment);
    }

    @Override
    protected void initViews() {
        super.initViews();

        CircleImageView profilePic = (CircleImageView) findView(R.id.profilepic);
        tvAboutyou = (TextView) findView(R.id.tv_aboutyou);
        TextView tvOtherData= (TextView) findView(R.id.tv_other_data);
        if (user != null) {
            getUserImage(profilePic);
            if (user.getName() != null)
                setDataTextView(R.id.tv_username, user.getName().trim());
            else {
                hideVisibility(R.id.tv_username);
            }
            if(user.getLikeCount()!=null){
                setDataTextView(R.id.tv_likes,user.getLikeCount()+"");
            }
            if (user.getLikeCount() != null)
                setDataTextView(R.id.tv_likes, user.getLikeCount().toString());
            else
                setDataTextView(R.id.tv_likes, "");
            if (user.getTagline() != null && !TextUtils.isEmpty(user.getTagline()))
                setDataTextView(R.id.tv_tagline, user.getTagline());
            else {
                hideVisibility(R.id.tv_tagline);
            }
            StringBuilder sbOtherData=new StringBuilder();
            if(user.getPhone()!=null){
                sbOtherData.append("Mobile: ").append(user.getPhone()).append("\n");
            }
            if(user.getEmail()!=null){
                sbOtherData.append("Email: "+user.getEmail());
            }
            tvOtherData.setText(sbOtherData.toString());

            String userDetails = user.getUserDetails();
            if (userDetails != null && !TextUtils.isEmpty(userDetails)) {
                tvAboutyou.setText(userDetails);
//                setDataTextView(R.id.tv_aboutyou, user.getUserDetails());
                String fullText = null;
                fullText = userDetails;
                Util.setDescText(activity, fullText, tvAboutyou, R.color.black, Util.MAX_LENGTH, null);
            } else {
                hideVisibility(R.id.tv_aboutyou);
            }

            if (user.getTutorLocation() != null) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < user.getTutorLocation().size(); i++) {
                    if (user.getTutorLocation().get(i).isPrimary()) {
                        setDataTextView(R.id.tv_primary_location, user.getTutorLocation().get(i).getCity().getCity() + ", " +
                                user.getTutorLocation().get(i).getLocality().getLocality());
                    } else {
                        stringBuilder.append(user.getTutorLocation().get(i).getLocality().getLocality() + ", " +
                                user.getTutorLocation().get(i).getCity().getCity() + "\n");
                    }
                }
                if (stringBuilder.length() > 0)
                    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                if (TextUtils.isEmpty(stringBuilder)) {
                    findView(R.id.tv_other_locations_heading).setVisibility(View.GONE);
                    findView(R.id.tv_other_locations).setVisibility(View.GONE);
                } else
                    setDataTextView(R.id.tv_other_locations, stringBuilder.toString());
            }
        }

        ImageView editBasicSection = (ImageView) findView(R.id.profile_edit_icon);
        editBasicSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, EditIntroActivity.class);
                fragment.startActivityForResult(i, AppConstants.TASKCODES.EDIT_INTRO);
            }
        });


    }

    private void setTextViewColor(int tv_username) {
//        TextView tv = (TextView) findView(tv_username);
//        if (tv != null) {
//            tv.setTextColor(activity.getResources().getColor(R.color.color_red_bg));
//        }
    }

    private void getUserImage(CircleImageView profilePic) {
        if (!TextUtils.isEmpty(user.getImage())) {
            Picasso.with(activity).load(user.getCompleteImageUrl()).into(profilePic);
        }
    }

    private String getLocation(TutorLocation location) {
        StringBuilder builder = new StringBuilder();
        if (location != null) {
            LocationApi loc = location.getLocality();
            if (loc != null) {
                builder.append(loc.getLocality());
            }
            CityApi cityApi = location.getCity();
            if (cityApi != null) {
                String city = cityApi.getCity();
                if (builder.length() > 0) {
                    builder.append(", ");
                }
                builder.append(city);
            }
        }
        return builder.toString();
    }


    private void setDataTextView(int id, String text) {
        TextView viewId = (TextView) findView(id);
        if (viewId != null) {
            viewId.setText(text);
        }

    }

}
