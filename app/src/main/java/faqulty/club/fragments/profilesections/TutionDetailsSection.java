package faqulty.club.fragments.profilesections;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.EditTutionDetailsActivity;
import faqulty.club.R;
import faqulty.club.models.UserProfile.ClassObject;
import faqulty.club.models.UserProfile.CoursesTaught;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by saurabh on 17-09-2016.
 */
public class TutionDetailsSection extends BaseProfileSection {
    private LinearLayout llCourseTaught;
    private boolean oneOnOneTeach, groupTeach;
    private static final String EMPTY_DEFAULT_TEXT = "Not Mentioned";

    public TutionDetailsSection(Activity activity, UserDetailsApi user, View view, Fragment fragment) {
        super(activity, user, view, fragment);
    }

    @Override
    protected void initViews() {
        super.initViews();
        llCourseTaught = (LinearLayout) findView(R.id.layout_course_taught);

        if (user.isTutor()) {
            setViewsForTutor();
        } else {
            setViewsForTuitionCenter();
        }

        if (user.getGroupTutionCharge() != null) {
            if (user.getGroupTutionCharge().getStartPrice() != null && user.getGroupTutionCharge().getFinalPrice() != null
                    && user.getGroupTutionCharge().getStartPrice() != 0 && user.getGroupTutionCharge().getFinalPrice() != 0) {
                setDataTextView(R.id.tv_group_charges, "₹" + user.getGroupTutionCharge().getStartPrice().toString() + " - ₹" +
                        user.getGroupTutionCharge().getFinalPrice().toString());
                groupTeach = true;
            }
        } else {
            setDataTextView(R.id.tv_group_charges, EMPTY_DEFAULT_TEXT);
        }


        if (user.getWeekdayTutionTime() != null && user.getWeekdayTutionTime().getStartTime().compareTo("00:00:00") != 0
                && user.getWeekdayTutionTime().getEndTime().compareTo("00:00:00") != 0) {
            if (isValidTime(user.getWeekdayTutionTime().getStartTime()) && isValidTime(user.getWeekdayTutionTime().getEndTime())) {
//                String startTime = Util.convertDateFormat(user.getWeekdayTutionTime().getStartTime(), "HH:mm:ss", "HH:mm");
//                String endTime = Util.convertDateFormat(user.getWeekdayTutionTime().getEndTime(), "HH:mm:ss", "HH:mm");
                String sTime = Util.convertDateFormat(user.getWeekdayTutionTime().getStartTime(), "HH:mm:ss", "hh:mm a");
                String eTime = Util.convertDateFormat(user.getWeekdayTutionTime().getEndTime(), "HH:mm:ss", "hh:mm a");
                setDataTextView(R.id.tv_weekday_timing, sTime + " - " + eTime);
            }
        } else {
            setDataTextView(R.id.tv_weekday_timing, EMPTY_DEFAULT_TEXT);
        }
        if (user.getWeekendTutionTime() != null && user.getWeekendTutionTime().getStartTime().compareTo("00:00:00") != 0
                && user.getWeekendTutionTime().getEndTime().compareTo("00:00:00") != 0) {
            if (isValidTime(user.getWeekendTutionTime().getStartTime()) && isValidTime(user.getWeekendTutionTime().getEndTime())) {
                String sTime = Util.convertDateFormat(user.getWeekendTutionTime().getStartTime(), "HH:mm:ss", "hh:mm a");
                String eTime = Util.convertDateFormat(user.getWeekendTutionTime().getEndTime(), "HH:mm:ss", "hh:mm a");
                setDataTextView(R.id.tv_weekend_timing, sTime + " - " +
                        eTime);
            }
        } else {
            setDataTextView(R.id.tv_weekend_timing, EMPTY_DEFAULT_TEXT);
        }
        if (user.getTutorBoard() != null && user.getTutorBoard().size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getTutorBoard().size(); x++) {
                if (x == user.getTutorBoard().size() - 1)
                    stringBuilder.append(user.getTutorBoard().get(x).getBoard());
                else
                    stringBuilder.append(user.getTutorBoard().get(x).getBoard() + ", ");
                setDataTextView(R.id.tv_boards, stringBuilder.toString());
            }
        } else {
            setDataTextView(R.id.tv_boards, EMPTY_DEFAULT_TEXT);
        }

        if (user.getTutorTeachingLanguage() != null && user.getTutorTeachingLanguage().size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getTutorTeachingLanguage().size(); x++) {
                if (x == user.getTutorTeachingLanguage().size() - 1)
                    stringBuilder.append(user.getTutorTeachingLanguage().get(x).getLanguage());
                else
                    stringBuilder.append(user.getTutorTeachingLanguage().get(x).getLanguage() + ", ");
                setDataTextView(R.id.tv_languages, stringBuilder.toString());
            }
        } else {
            setDataTextView(R.id.tv_languages, EMPTY_DEFAULT_TEXT);
        }
        if (user.getSchoolsTutored() != null) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getSchoolsTutored().size(); x++) {
                stringBuilder.append(user.getSchoolsTutored().get(x).getSchoolName() + ", ");
            }
            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            setDataTextView(R.id.schools_of_tution_tutored, stringBuilder.toString());
        }
        //// TODO: 28-09-2016 schools taught
//// TODO: 21-09-2016 courses taught tab not working 
        setCourseSegment();

        ImageView editTutionDetails = (ImageView) findView(R.id.tution_details_edit_icon);
        editTutionDetails.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View view) {
                                                     Intent i = new Intent(activity, EditTutionDetailsActivity.class);
                                                     fragment.startActivityForResult(i, AppConstants.TASKCODES.EDIT_TUTION_DETAILS);
                                                 }
                                             }

        );
    }

    private void setCourseSegment() {
        if (user.getCoursesTaught() != null && user.getCoursesTaught().size() > 0) {
            llCourseTaught.removeAllViews();
            for (CoursesTaught c : user.getCoursesTaught()) {
                addCoursesView(c);
            }
        } else {
            View row = LayoutInflater.from(activity).inflate(R.layout.empty_course, null);
            llCourseTaught.addView(row);
        }
    }

    private void addCoursesView(CoursesTaught coursesTaught) {
        if (coursesTaught == null)
            return;
        List<ClassObject> classObjects = coursesTaught.getClasses();
        View row = LayoutInflater.from(activity).inflate(R.layout.section_row_courses, null);
        TextView subject = (TextView) row.findViewById(R.id.tv_subject_name);
        ChipView chipView = (ChipView) row.findViewById(R.id.chipview);
        subject.setText(coursesTaught.getSubject());

        List<ClassObject> selectedClasses = new ArrayList<>();
        if (classObjects != null && classObjects.size() > 0) {
            selectedClasses.addAll(classObjects);
            List<Chip> chips = new ArrayList<>();
            if (getGroupedClasses(selectedClasses) != null && getGroupedClasses(selectedClasses).size() > 0)
                chips.addAll(getGroupedClasses(selectedClasses));
            chipView.setChipLayoutRes(R.layout.row_chip_classes);
            chipView.setChipList(chips);
        }
        llCourseTaught.addView(row);
    }

    private List<Chip> getGroupedClasses(List<ClassObject> selectedClasses) {
        List<List<ClassObject>> mainList = new ArrayList<>();
        List<ClassObject> tempObjects = new ArrayList<>();
        for (ClassObject classObject : selectedClasses) {
            try {
                int classValue = Integer.parseInt(classObject.getClassVal());
                if (tempObjects.size() > 0) {
                    ClassObject tempObject = tempObjects.get(tempObjects.size() - 1);
                    int tempValue = Integer.parseInt(tempObject.getClassVal());
                    if ((tempValue + 1) == classValue) {
                        tempObjects.add(classObject);
                    } else {
                        mainList.add(tempObjects);
                        tempObjects = new ArrayList<>();
                        tempObjects.add(classObject);
                    }
                } else {
                    tempObjects.add(classObject);
                }

            } catch (Exception e) {
                if (tempObjects.size() > 0) {
                    mainList.add(tempObjects);
                    tempObjects = new ArrayList<>();
                }
                List<ClassObject> classObjects = new ArrayList<>();
                classObjects.add(classObject);
                mainList.add(classObjects);
            }
        }
        if (tempObjects.size() > 0) {
            mainList.add(tempObjects);
        }
        List<Chip> groupedChips = new ArrayList<>();

        for (List<ClassObject> classObject : mainList) {
            faqulty.club.Util.ClassChip classChip = new faqulty.club.Util.ClassChip(false);
            classChip.setClassObjectList(classObject);
            groupedChips.add(classChip);
        }

        return groupedChips;
    }

    private void setViewsForTutor() {
        hideVisibility(R.id.lbl_years_of_operation, R.id.tv_years_of_operation, R.id.lbl_no_of_students, R.id.tv_no_of_students
                , R.id.awards_heading, R.id.tv_awards);
        if (user.getTutorTeachingMode() != null && user.getTutorTeachingMode().size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getTutorTeachingMode().size(); x++) {
                if (x == user.getTutorTeachingMode().size() - 1)
                    stringBuilder.append(user.getTutorTeachingMode().get(x).getMode());
                else
                    stringBuilder.append(user.getTutorTeachingMode().get(x).getMode() + ", ");
                setDataTextView(R.id.tv_tution_type, stringBuilder.toString());
            }
        } else {
            setDataTextView(R.id.tv_tution_type, EMPTY_DEFAULT_TEXT);
        }
        if (user.getOneOnOneTutionCharge() != null) {
            if (user.getGroupTutionCharge() != null && user.getGroupTutionCharge().getStartPrice() != null && user.getGroupTutionCharge().getFinalPrice() != null
                    && user.getOneOnOneTutionCharge().getStartPrice() != 0 && user.getOneOnOneTutionCharge().getFinalPrice() != 0) {
                setDataTextView(R.id.tv_one_on_one_charges, "₹" + user.getOneOnOneTutionCharge().getStartPrice().toString() + " - ₹" +
                        user.getOneOnOneTutionCharge().getFinalPrice().toString());
                oneOnOneTeach = true;
            }
        } else {
            setDataTextView(R.id.tv_one_on_one_charges, EMPTY_DEFAULT_TEXT);
        }
        setClasses();
    }

    private void setClasses() {
        if (oneOnOneTeach) {
            if (groupTeach) {
                setDataTextView(R.id.tv_class_size, "1:1, Group");
            } else setDataTextView(R.id.tv_class_size, "1:1");
        } else if (groupTeach) {
            setDataTextView(R.id.tv_class_size, "Group");
        } else {
            setDataTextView(R.id.tv_class_size, EMPTY_DEFAULT_TEXT);
        }
    }

    private void setViewsForTuitionCenter() {
        hideVisibility(R.id.one_on_one_pic, R.id.tv_one_on_one_charges, R.id.tution_type_heading, R.id.tv_tution_type, R.id.class_size_heading, R.id.tv_class_size);
        setDataTextView(R.id.tv_years_of_operation, user.getExperience() + "");
        if (user.getGroupTutionCharge() != null) {
            setDataTextView(R.id.tv_no_of_students, user.getGroupTutionCharge().getBatchSize() + "");
        } else {
            setDataTextView(R.id.tv_no_of_students, EMPTY_DEFAULT_TEXT);
        }
        setAwardsNRecognition();
    }

    private void setAwardsNRecognition() {
        if (user.getTutorAwards() != null && user.getTutorAwards().size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < user.getTutorAwards().size(); x++) {
                stringBuilder.append(user.getTutorAwards().get(x).getAwards() + "\n");
            }
            if (stringBuilder.length() > 0)
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            setDataTextView(R.id.tv_awards, stringBuilder.toString());
        } else {
            setDataTextView(R.id.tv_awards, EMPTY_DEFAULT_TEXT);
        }
    }


    private void setDataTextView(int id, String link) {
        TextView viewId = (TextView) findView(id);
        viewId.setText(link);
    }

    private boolean isValidTime(String time) {
        if (!TextUtils.isEmpty(time) && !"00:00:00".equals(time)) {
            return true;
        } else return false;
    }

    private class ClassChip implements Chip {
        String className;

        ClassChip(String className) {
            this.className = className;
        }

        @Override
        public String getText() {
            return className;
        }
    }


}
