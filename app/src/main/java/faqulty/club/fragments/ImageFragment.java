package faqulty.club.fragments;

import android.view.View;
import android.widget.ProgressBar;

import faqulty.club.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import ooo.oxo.library.widget.TouchImageView;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by nbansal2211 on 23/12/16.
 */

public class ImageFragment extends BaseFragment {
    private TouchImageView fullImage;
    private String image;

    public static ImageFragment getInstance(String image) {
        ImageFragment imageFragment = new ImageFragment();
        imageFragment.image = image;
        return imageFragment;
    }

    @Override
    public void initViews() {
        setHasOptionsMenu(true);
        fullImage = (TouchImageView) findView(R.id.iv_full_image);
        final ProgressBar progressBar= (ProgressBar) findView(R.id.progressbar);
        Picasso.with(getActivity()).load(image).into(fullImage, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_image;
    }


}
