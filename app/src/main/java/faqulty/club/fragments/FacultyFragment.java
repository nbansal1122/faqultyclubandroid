package faqulty.club.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.autoadapters.AutoCompleteView;
import faqulty.club.autoadapters.SingleDiaogManager;
import faqulty.club.models.BaseAutoFilter;

import faqulty.club.models.UserProfile.FacultyData;
import faqulty.club.models.UserProfile.TutorDegree;
import faqulty.club.models.UserProfile.TutorDegreeSubject;
import faqulty.club.models.UserProfile.TutorEducationDetail;
import faqulty.club.models.UserProfile.TutorInstitute;
import simplifii.framework.requestmodels.UploadImageResponse;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 23/10/16.
 */

public class FacultyFragment extends BaseFragment {
    TutorEducationDetail tutorEducationDetail;
    private ImageView facultyImageView;
    private String profilePicUri;
    private boolean fromPostFeed;

    private SingleDiaogManager<TutorDegree> degreeAutoView;
    private SingleDiaogManager<TutorDegreeSubject> subjectAutoView;
    private SingleDiaogManager<TutorInstitute> instituteAutoView;

    public int index;
    private FacultyDeleteListener listener;
    private FacultyData facultyData;
    private MediaFragment imagePicker;
    private Bitmap profileBitmap;
    private RadioButton maleButton, femaleButton;

    public static FacultyFragment getInstance(FacultyData facultyData, FacultyDeleteListener listener, int index) {
        FacultyFragment f = new FacultyFragment();
        if (facultyData != null && facultyData.getTutorEducationDetail() != null && facultyData.getTutorEducationDetail().size() > 0) {
            f.facultyData = facultyData;
            f.tutorEducationDetail = facultyData.getTutorEducationDetail().get(0);
        }
        f.listener = listener;
        f.index = index;
        return f;
    }


    public JSONObject getFacultyJsonObject() throws JSONException {
        String name = getTilText(R.id.til_faculty_name);
        if (TextUtils.isEmpty(name) && degreeAutoView.getSelectedItem() == null && subjectAutoView.getSelectedItem() == null &&
                instituteAutoView.getSelectedItem() == null) {
            return null;
        }
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();

        obj.put("name", name);
        if (maleButton.isChecked()) {
            obj.put("gender", "Male");
        } else {
            obj.put("gender", "Female");
        }
        if (!TextUtils.isEmpty(profilePicUri)) {
            obj.put("image", profilePicUri);
        }
        JSONObject educationObject = getJsonObject();
        if (educationObject != null) {
            array.put(educationObject);
        }
        obj.put("tutorEducationDetail", array);
        return obj;
    }


    public JSONObject getJsonObject() throws JSONException {


        JSONObject jsonObject = new JSONObject();
        JSONObject jsonDegree = new JSONObject();
        JSONObject jsonDegreeSubject = new JSONObject();
        JSONObject jsonInstitute = new JSONObject();


        TutorDegree selectedDegree = degreeAutoView.getSelectedItem();
        if (selectedDegree != null) {
            jsonDegree.put("id", selectedDegree.getId());
            jsonDegree.put("degree", selectedDegree.getDegree());
        }
        TutorDegreeSubject selectedSubject = subjectAutoView.getSelectedItem();
        if (selectedSubject != null) {
            jsonDegreeSubject.put("id", selectedSubject.getId());
            jsonDegreeSubject.put("degreeId", selectedSubject.getDegreeId());
            jsonDegreeSubject.put("degreeSubject", selectedSubject.getDegreeSubject());
        }
        TutorInstitute selectedInstitute = instituteAutoView.getSelectedItem();
        if (selectedInstitute != null) {
            jsonInstitute.put("id", selectedInstitute.getId());
        }
        if (tutorEducationDetail != null) {
            jsonObject.put("id", tutorEducationDetail.getId());
        } else {

        }
        jsonObject.put("tutorDegree", jsonDegree);
        jsonObject.put("tutorDegreeSubject", jsonDegreeSubject);
        jsonObject.put("tutorInstitute", jsonInstitute);
        jsonObject.put("college", getEditText(R.id.et_achievements));
        return jsonObject;
    }

    public boolean isValid() {
        String name = getTilText(R.id.til_faculty_name);
        if (TextUtils.isEmpty(name) && degreeAutoView.getSelectedItem() == null && subjectAutoView.getSelectedItem() == null &&
                instituteAutoView.getSelectedItem() == null) {
            return true;
        }
        if (isDataValid()) {
            if (!TextUtils.isEmpty(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean isDataValid() {
        if (degreeAutoView.getSelectedItem() != null && subjectAutoView.getSelectedItem() != null &&
                instituteAutoView.getSelectedItem() != null) {
            return true;
        } else {
            showToast(R.string.error_qualification_details);
            return false;
        }
    }


    private class Holder {
        TextView tv;

        Holder(View v) {
            tv = (TextView) v.findViewById(R.id.tv_item);
        }
    }

    private void initFacultyViews() {
        if (!TextUtils.isEmpty(facultyData.getGender())) {

            if ("female".equalsIgnoreCase(facultyData.getGender())) {
                maleButton.setChecked(false);
                femaleButton.setChecked(true);
            } else if ("male".equalsIgnoreCase(facultyData.getGender())) {
                maleButton.setChecked(true);
                femaleButton.setChecked(false);
            }
        }
        setEditText(R.id.et_faculty_name, facultyData.getName());
        profilePicUri = facultyData.getImage();
        if (!TextUtils.isEmpty(facultyData.getImage())) {
            Picasso.with(getActivity()).load(Util.getCompleteUrl(facultyData.getImage())).into(facultyImageView);
        }
    }

    private void selectImage() {
        if (imagePicker == null) {
            imagePicker = new MediaFragment();
            imagePicker.setContextOfActivity(getActivity());
            getChildFragmentManager().beginTransaction().add(imagePicker, "Profile image").commit();
        }
        Bundle b = new Bundle();
        b.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.select_profile_pic));
        b.putBoolean(AppConstants.BUNDLE_KEYS.POST_FEED, false);
        imagePicker.setImageListener( new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                if (bitmap == null) return;
                profileBitmap = bitmap;
                facultyImageView.setImageBitmap(bitmap);
                uploadImageData();
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                if (images != null && images.size() > 0) {
                    for (Image image : images) {
                        Bitmap bMap = getBitmapFromFilePath(image.getFilePath());
                        if (bMap != null) {
                            profileBitmap = bMap;
                            facultyImageView.setImageBitmap(bMap);
                            uploadImageData();
                        }
                    }
                }
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(imagePicker);
        bottomSheetDialogFragment.setArguments(b);
        bottomSheetDialogFragment.show(getChildFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    private void uploadImageData() {
        try {
            File file = Util.getFile(profileBitmap, "ProfilePicFaqultyClub");
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initViews() {
        maleButton = (RadioButton) findView(R.id.rb_male);
        femaleButton = (RadioButton) findView(R.id.rb_female);
        facultyImageView = (ImageView) findView(R.id.iv_profile_pic);
        if (facultyData != null) {
            initFacultyViews();
        }

        degreeAutoView = new SingleDiaogManager<>((AppCompatActivity) getActivity(), (AutoCompleteTextView) findView(R.id.actv_degree), new AutoCompleteView.ItemSelector() {
            @Override
            public void onItemSelected(BaseAutoFilter item) {
                TutorDegree degree = (TutorDegree) item;
                getSubjects(degree.getId().toString());
                subjectAutoView.setSelectedItem(null);
            }
        });
        subjectAutoView = new SingleDiaogManager<>((AppCompatActivity) getActivity(), (AutoCompleteTextView) findView(R.id.actv_subject));
        instituteAutoView = new SingleDiaogManager<>((AppCompatActivity) getActivity(), (AutoCompleteTextView) findView(R.id.actv_institute));
        if (null != tutorEducationDetail && !TextUtils.isEmpty(tutorEducationDetail.getCollege())) {
            setEditText(R.id.et_achievements, tutorEducationDetail.getCollege());
        }
        initQualificaitonDetails();
        getDegrees();
        getInstitutes();
        setOnClickListener(R.id.actv_subject, R.id.actv_degree, R.id.actv_institute);
        setOnClickListener(R.id.iv_delete_degree, R.id.btn_edit_profile_pic);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_delete_degree:
                if (listener != null) {
                    listener.onItemAction(this);
                }
                break;
            case R.id.btn_edit_profile_pic:
                selectImage();
                break;
            case R.id.actv_subject:
                subjectAutoView.showDropDown();
                break;
            case R.id.actv_degree:
                degreeAutoView.showDropDown();
                break;
            case R.id.actv_institute:
                instituteAutoView.showDropDown();
                break;
        }
    }

    private void initQualificaitonDetails() {
        instituteAutoView = new SingleDiaogManager<>(((AppCompatActivity) getActivity()), (AutoCompleteTextView) findView(R.id.actv_institute));
        instituteAutoView = new SingleDiaogManager<>(((AppCompatActivity) getActivity()), (AutoCompleteTextView) findView(R.id.actv_institute));
        if (tutorEducationDetail != null) {
            if (tutorEducationDetail.getTutorInstitute() != null)
                instituteAutoView.setSelectedItem(tutorEducationDetail.getTutorInstitute());
            if (tutorEducationDetail.getTutorDegreeSubject() != null)
                subjectAutoView.setSelectedItem(tutorEducationDetail.getTutorDegreeSubject());
            if (tutorEducationDetail.getTutorDegree() != null)
                degreeAutoView.setSelectedItem(tutorEducationDetail.getTutorDegree());
        }
    }


    private void getSubjects(String degreeId) {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_DEGREE_SUBJECTS);
        if (!TextUtils.isEmpty(degreeId)) {
            obj.addParameter("degreeId", degreeId);
        }
        obj.setClassType(TutorDegreeSubject.class);
        executeTask(AppConstants.TASKCODES.GET_DEGREE_SUBJECTS, obj);
    }

    private void getDegrees() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_DEGREE);
        obj.setClassType(TutorDegree.class);
        executeTask(AppConstants.TASKCODES.GET_DEGREE, obj);
    }

    private void getInstitutes() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_INSTITUTES);
        obj.setClassType(TutorInstitute.class);
        String keyUnivForCache = "UniversityList:";
        obj.addParameter("count", "1000");
        obj.addParameter("offset", "0");
        executeTask(AppConstants.TASKCODES.GET_INSTITUTES, obj, keyUnivForCache);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_faculty;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_DEGREE:
                if (response != null) {
                    List<TutorDegree> degreeList = (List<TutorDegree>) response;
                    if (degreeList != null) {
                        degreeAutoView.setItems(degreeList);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_DEGREE_SUBJECTS:
                if (response != null) {
                    List<TutorDegreeSubject> degreeSubjectList = (List<TutorDegreeSubject>) response;
                    if (degreeSubjectList != null) {
                        subjectAutoView.setItems(degreeSubjectList);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_INSTITUTES:
                if (response != null) {
                    List<TutorInstitute> instituteList = (List<TutorInstitute>) response;
                    if (instituteList != null) {
                        instituteAutoView.setItems(instituteList);
                    }
                }
                break;
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    profilePicUri = uploadImageResponse.getData().getUri();
                }
                break;
        }
    }

    private Bitmap getBitmapFromFilePath(String filePath) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            bitmap = Util.getResizeBitmap(bitmap, 1024);
            return bitmap;
        } catch (Exception e) {
            showToast(R.string.insufficient_memory);
        } catch (OutOfMemoryError e) {
            showToast(R.string.insufficient_memory);
        }
        return null;
    }

    public static interface FacultyDeleteListener {
        public void onItemAction(FacultyFragment segment);
    }
}
