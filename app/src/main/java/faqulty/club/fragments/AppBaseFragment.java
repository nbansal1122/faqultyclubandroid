package faqulty.club.fragments;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import faqulty.club.R;
import faqulty.club.activity.AskQueryActivity;
import faqulty.club.activity.NotificationActivity;
import com.google.firebase.iid.FirebaseInstanceId;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by raghu on 10/2/17.
 */

public abstract class AppBaseFragment extends BaseFragment {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                Preferences.deleteAllData();
                String tkn = FirebaseInstanceId.getInstance().getToken();
                Log.d("Not","Token ["+tkn+"]");
                if(!TextUtils.isEmpty(tkn)){
                    Preferences.saveData(AppConstants.PREF_KEYS.IS_UPDATE_TOKEN,true);
                    Preferences.saveData(AppConstants.PREF_KEYS.FCM_TOKEN,tkn);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getActivity().finishAndRemoveTask();
                }else {
                    getActivity().finish();
                }
                return true;
            case R.id.action_ask_query:
                startNextActivity(AskQueryActivity.class);
                return true;
            case R.id.action_notification:
                startNextActivity(NotificationActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
