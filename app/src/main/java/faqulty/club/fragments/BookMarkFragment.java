package faqulty.club.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.activity.FolderItemActivity;
import faqulty.club.activity.NotificationActivity;
import faqulty.club.activity.QuestionViewActivity;
import faqulty.club.activity.SingleFeedActivity;
import faqulty.club.models.bookmark.BaseBookmark;
import faqulty.club.models.bookmark.Bookmark;
import faqulty.club.models.bookmark.BookmarkApi;
import faqulty.club.models.bookmark.Data;
import faqulty.club.models.bookmark.Folder;
import faqulty.club.models.bookmark.NewBookmarkFolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Dhillon on 05-12-2016.
 */

public class BookMarkFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView lvBookmark;
    private CustomListAdapter bookmarkAdapter;
    private ArrayList<BaseBookmark> baseBookmarksArrayList = new ArrayList<>();
    private List<Folder> folders;
    private List<Bookmark> bookmarks;
    private Dialog dialogBookmark;

    @Override
    public void initViews() {
        setHasOptionsMenu(true);
        lvBookmark = (ListView) findView(R.id.lv_bookmark);
        bookmarkAdapter = new CustomListAdapter(getActivity(), R.layout.row_bookmark, baseBookmarksArrayList, this);
        lvBookmark.setAdapter(bookmarkAdapter);
        lvBookmark.setOnItemClickListener(this);
        getBookmarkData();
        registerReceiver();
    }

    @Override
    protected IntentFilter getIntentFilter() {
        return new IntentFilter(GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH_BOOKMARKS);
    }

    @Override
    public void onReceive(Intent intent) {
        super.onReceive(intent);
        if (intent != null) {
            String action = intent.getAction();
            if (GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH_BOOKMARKS.equalsIgnoreCase(action)) {
                getBookmarkData();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_bookmark, menu);
    }


    public void getBookmarkData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.BOOKMARK_GET));
        httpParamObject.setClassType(BookmarkApi.class);
        executeTask(AppConstants.TASKCODES.GET_BOOKMARK, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_response));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_BOOKMARK:
                BookmarkApi bookmarkApi = (BookmarkApi) response;
                if (bookmarkApi != null && bookmarkApi.getStatus()) {
                    Data data = bookmarkApi.getData();
                    if (data != null) {
                        baseBookmarksArrayList.clear();
                        folders = data.getFolders();
                        if (folders != null) {
                            baseBookmarksArrayList.addAll(folders);
                            bookmarkAdapter.notifyDataSetChanged();
                        }
                        this.bookmarks = data.getBookmarks();
                        if (this.bookmarks != null) {
                            baseBookmarksArrayList.addAll(this.bookmarks);
                            bookmarkAdapter.notifyDataSetChanged();
                        }
                    }

                }
                setEmptyBookmarkLayout();
                break;
            case AppConstants.TASKCODES.BOOKMARK_FOLDER:
                NewBookmarkFolder newBookmarkFolder = (NewBookmarkFolder) response;
                if (newBookmarkFolder != null) {
                    if (newBookmarkFolder.getStatus().equals(true)) {
                        showToast(getString(R.string.folder_create_bookmark));
                        getBookmarkData();
                    }
                }
                break;
        }
    }

    private void setEmptyBookmarkLayout() {
        if (baseBookmarksArrayList.size() > 0) {
            hideVisibility(R.id.tv_empty);
        } else {
            showVisibility(R.id.tv_empty);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_bookmart;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final BaseBookmark baseBookmark = baseBookmarksArrayList.get(position);
        if (baseBookmark.getView() == AppConstants.VIEW_TYPE.FOLDER) {
            holder.desc.setVisibility(View.GONE);
            holder.bookMarkIcon.setImageResource(R.mipmap.black_folder);
            Folder folder = (Folder) baseBookmark;
            if (folder != null) {
                String name = folder.getName();
                holder.tvBookmark.setText(name);
            }
        } else if (baseBookmark.getView() == AppConstants.VIEW_TYPE.BOOKMARK) {
            Bookmark bookmark = (Bookmark) baseBookmark;
            holder.tvBookmark.setText(bookmark.getName());
            holder.desc.setVisibility(View.VISIBLE);
            holder.desc.setText(bookmark.getDescription() + "");
            holder.bookMarkIcon.setImageResource(R.mipmap.ic_bookmark_module);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBookmarkRowClicked(baseBookmark);
            }
        });
        return convertView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_folder:
                showDialog();
                break;
            case R.id.action_notification:
                startNextActivity(NotificationActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog() {
        dialogBookmark = new Dialog(getActivity());
        dialogBookmark.setContentView(R.layout.dialog_bookmark);

        final EditText et_folderName = (EditText) dialogBookmark.findViewById(R.id.et_bookmark_name);
        TextView tv_create = (TextView) dialogBookmark.findViewById(R.id.tv_create);
        TextView tv_cancel = (TextView) dialogBookmark.findViewById(R.id.tv_cancel);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
            }
        });

        tv_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folderName = et_folderName.getText().toString().trim();
                if (TextUtils.isEmpty(folderName)) {
                    showToast(getString(R.string.enter_folder_name));
                    return;
                } else {
                    createBookmark(folderName);
                }
            }
        });

        dialogBookmark.show();
    }

    private void createBookmark(String folderName) {
        hideDialog();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.CREATE_BOOKMARK_FOLDER));
        httpParamObject.setPostMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", folderName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(NewBookmarkFolder.class);
        executeTask(AppConstants.TASKCODES.BOOKMARK_FOLDER, httpParamObject);

    }


    private void hideDialog() {
        if (dialogBookmark != null) {
            dialogBookmark.dismiss();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        BaseBookmark baseBookmark = baseBookmarksArrayList.get(position);
        onBookmarkRowClicked(baseBookmark);
    }

    private void onBookmarkRowClicked(BaseBookmark baseBookmark) {
        if (baseBookmark.getView() == AppConstants.VIEW_TYPE.FOLDER) {
            Folder folder = (Folder) baseBookmark;
            Bundle b = new Bundle();
            b.putString(AppConstants.BUNDLE_KEYS.FOLDER_NAME, folder.getName());
            b.putInt(AppConstants.BUNDLE_KEYS.FOLDER_ID, folder.getId());
            startNextActivityForResult(b, FolderItemActivity.class, AppConstants.REQUEST_CODES.OPEN_FOLDER);
        } else {
            Bookmark bookmark = (Bookmark) baseBookmark;
            goToBookmarkedItem(bookmark);
        }
    }

    private void goToBookmarkedItem(Bookmark bookmark) {
        switch (bookmark.getModuleType()) {
            case "assignment":
                openAssignment(bookmark);
                break;
            case "feed":
                openFeedItem(bookmark);
                break;
        }

    }

    private void openAssignment(Bookmark bookmark) {
        Bundle b = new Bundle();
        b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, bookmark.getName());
        b.putString(AppConstants.BUNDLE_KEYS.KEY_SUBTITLE, bookmark.getDescription());
        b.putInt(AppConstants.BUNDLE_KEYS.Q_ID, bookmark.getModuleId());
        startNextActivity(b, QuestionViewActivity.class);
    }

    private void openFeedItem(Bookmark bookmark) {
        int id1 = bookmark.getModuleId();
        Bundle idBundel = new Bundle();
        idBundel.putInt(AppConstants.BUNDLE_KEYS.ID, id1);
        startNextActivity(idBundel, SingleFeedActivity.class);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            getBookmarkData();
        }
    }

    class Holder {
        TextView tvBookmark, desc;
        ImageView bookMarkIcon;

        public Holder(View view) {
            tvBookmark = (TextView) view.findViewById(R.id.tv_bookmark);
            desc = (TextView) view.findViewById(R.id.tv_subTitle);
            bookMarkIcon = (ImageView) view.findViewById(R.id.iv_bookmark);
        }
    }


}
