package faqulty.club.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import faqulty.club.R;
import faqulty.club.models.feed.Datum;


import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Neeraj Yadav on 12/6/2016.
 */

public class EditFeedPostFragment extends BottomSheetDialogFragment {
    private FeedFragment feedFragment;
    private FeedOptionsFragment.BottomSheetInterface bottomSheetInterface;
    private Datum feedData;
    private long userId;
    private long tutorId;


    public static EditFeedPostFragment newInstance(FeedOptionsFragment.BottomSheetInterface bottomSheetInterface){
        EditFeedPostFragment editFeedPostFragment = new EditFeedPostFragment();
        editFeedPostFragment.bottomSheetInterface = bottomSheetInterface;
        return editFeedPostFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userId = Preferences.getData(AppConstants.PREF_KEYS.USER_ID, 111L);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState){
        View view = layoutInflater.inflate(R.layout.fragment_edit_feed_post, container, false);
        RelativeLayout editPost = (RelativeLayout) view.findViewById(R.id.rl_edit_post);
        RelativeLayout deletePost = (RelativeLayout) view.findViewById(R.id.rl_delete_post);
        RelativeLayout copyLink = (RelativeLayout) view.findViewById(R.id.rl_copy_link);

        Bundle b = getArguments();
        feedData = (Datum) b.getSerializable(AppConstants.BUNDLE_KEYS.EDIT_POSTFEED);

        if (feedData != null){
            tutorId = feedData.getTutorId();
        }

        if (userId != tutorId){
            editPost.setVisibility(View.GONE);
            deletePost.setVisibility(View.GONE);
        } else {
            editPost.setVisibility(View.VISIBLE);
            deletePost.setVisibility(View.VISIBLE);
        }


        editPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.EDIT_FEED, feedData);
                dismiss();
            }
        });

        deletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.DELETE_FEED, feedData);
                dismiss();
            }
        });
        copyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.COPY_FEED_LINK, feedData);
                dismiss();
            }
        });
        return view;
    }
}
