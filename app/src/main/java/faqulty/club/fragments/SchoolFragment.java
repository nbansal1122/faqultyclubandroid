package faqulty.club.fragments;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import faqulty.club.R;
import faqulty.club.models.SchoolsTutoredApi;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 20/10/16.
 */

public class SchoolFragment extends BaseFragment {
    private EditText schoolsEt;
    private SchoolsTutoredApi schoolsTutoredApi;
    public int index;
    private SchoolDeleteInterface schoolDeleteInterface;

    public static SchoolFragment getInstance(SchoolsTutoredApi schoolsTutoredApi, int index, SchoolDeleteInterface schoolDeleteInterface) {
        SchoolFragment f = new SchoolFragment();
        f.schoolsTutoredApi = schoolsTutoredApi;
        f.schoolDeleteInterface = schoolDeleteInterface;
        f.index = index;
        return f;
    }

    @Override
    public void initViews() {
        schoolsEt = (EditText) findView(R.id.et_schools_of_tution_tutored);
        if (schoolsTutoredApi != null) {
            schoolsEt.setText(schoolsTutoredApi.getDisplayString());
        }
        setOnClickListener(R.id.iBtn_delete_location);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iBtn_delete_location:
                if (schoolDeleteInterface != null) {
                    Util.hideKeyboard(getActivity(), schoolsEt);
                    schoolDeleteInterface.onSchoolDelete(this, schoolsTutoredApi);
                }
                break;
        }
    }

    public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        String school = getEditText(R.id.et_schools_of_tution_tutored);
        if (TextUtils.isEmpty(school)) {
            return null;
        }
        if (schoolsTutoredApi == null) {
            jsonObject.put("school", school);
        } else {
            if (school.equalsIgnoreCase(schoolsTutoredApi.getDisplayString())) {
                jsonObject.put("id", schoolsTutoredApi.getId());
                jsonObject.put("school", schoolsTutoredApi.getDisplayString());
            } else {
                jsonObject.put("school", school);
            }
        }
        return jsonObject;
    }

    @Override
    public int getViewID() {
        return R.layout.layout_schools;
    }

    public void onClear() {
        this.schoolsTutoredApi = null;
        initViews();
        schoolsEt.setText("");
    }

    public static interface SchoolDeleteInterface {
        public void onSchoolDelete(SchoolFragment schoolFragment, SchoolsTutoredApi schoolsTutoredApi);
    }
}
