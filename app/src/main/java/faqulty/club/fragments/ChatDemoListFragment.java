package faqulty.club.fragments;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import faqulty.club.R;
import faqulty.club.chat.IndividualChatActivity;
import faqulty.club.models.chat.ChatMessage;
import faqulty.club.models.chat.ChatText;
import faqulty.club.models.chat.MetaInfo;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.tutorstudent.TutorsStudentsResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class ChatDemoListFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener, GroupsFragment.OnDataSetChanged, AdapterView.OnItemLongClickListener {
    private static final int REQ_UPDATE_DATA = 1;
    private List<StudentBaseClass> arrayList = new ArrayList<>();
    private Map<Integer, ChatMessage> lastMessageMap = new HashMap<>();
    private ListView lvIndividual;
    private CustomListAdapter adapter;

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        lvIndividual = (ListView) findView(R.id.lv_students);
        adapter = new CustomListAdapter(getActivity(), R.layout.row_chat_user, arrayList, this);
        lvIndividual.setAdapter(adapter);
        lvIndividual.setOnItemClickListener(this);
        lvIndividual.setOnItemLongClickListener(this);
//        lvIndividual.setEmptyView(findView(android.R.id.empty));
//        getTutorStudentData();
    }

    protected int getHomeIcon() {
        return R.mipmap.fc_icon;
    }

    protected IntentFilter getIntentFilter() {
        return new IntentFilter(GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_no_icons, menu);
        MenuItem itemDelete = menu.findItem(R.id.action_delete);
        itemDelete.setVisible(false);
    }

    public void getTutorStudentData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GETTUTORSTUDENT, Preferences.getUserId()));
        httpParamObject.setClassType(TutorsStudentsResponse.class);
        executeTask(AppConstants.TASKCODES.GETTUTORSTUDENT, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GETTUTORSTUDENT:
                TutorsStudentsResponse tsr = (TutorsStudentsResponse) response;
                if (response != null && tsr.getStatus() && null != tsr.getData().getIndividual()) {
                    arrayList.clear();
                    arrayList.addAll(tsr.getData().getIndividual());
                    adapter.notifyDataSetChanged();
                    loadLastMessage(arrayList);
                }
                break;
        }
    }

    private void loadLastMessage(final List<StudentBaseClass> arrayList) {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                for (StudentBaseClass studentBaseClass : arrayList) {
                    List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("toUserID=? or fromUserID=? LIMIT 1", studentBaseClass.getId(), studentBaseClass.getId()).execute();
                    if (chatMessages != null && chatMessages.size() > 0) {
                        ChatMessage chatMessage = chatMessages.get(0);
                        lastMessageMap.put(studentBaseClass.getId(), chatMessage);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                adapter.notifyDataSetChanged();
            }
        }.execute();
    }


    @Override
    public int getViewID() {
        return R.layout.chat_demo_list_fragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StudentBaseClass studentBaseClass = arrayList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, studentBaseClass);
        startNextActivity(bundle, IndividualChatActivity.class);
    }

    @Override
    public void onStudentAdd() {
        getTutorStudentData();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        StudentBaseClass studentBaseClass = arrayList.get(position);
        return true;
    }

    class Holder {
        TextView tvName, tvTime, tvLastMessage;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvLastMessage = (TextView) view.findViewById(R.id.tv_last_message);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.INVITESTUDENT:
                getTutorStudentData();
                break;
            case AppConstants.REQUEST_CODES.CREATECLASS:
                getTutorStudentData();
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final StudentBaseClass studentBaseClass = arrayList.get(position);
        holder.tvName.setText(studentBaseClass.getName());
        ChatMessage chatMessage = lastMessageMap.get(studentBaseClass.getId());
        if (chatMessage != null) {
            holder.tvTime.setText(chatMessage.getTimeString());
            String messageType = chatMessage.getMessageType();
            switch (messageType) {
                case AppConstants.META_TYPES.TEXT:
                    ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
                    if (chatText != null) {
                        holder.tvLastMessage.setText(chatText.getText());
                    }
                    break;
                case AppConstants.META_TYPES.FILE:
                    holder.tvLastMessage.setText("Image");
                    break;
            }
        } else {
            holder.tvLastMessage.setText("No conversation");
            holder.tvTime.setText("");
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, studentBaseClass);
                startNextActivity(bundle, IndividualChatActivity.class);
            }
        });
        return convertView;
    }

    private boolean isSelected(StudentBaseClass sbc) {
        return false;
    }
}
