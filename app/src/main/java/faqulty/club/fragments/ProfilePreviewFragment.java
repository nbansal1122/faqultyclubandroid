package faqulty.club.fragments;

import faqulty.club.ContentActivity;
import faqulty.club.activity.TutorsCommentActivity;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.fragments.profilesections.FacultySection;

import faqulty.club.fragments.profilesections.SocialSection;
import faqulty.club.models.ProfileComments;
import faqulty.club.models.UserProfile.UserDetailsApi;
import faqulty.club.models.content.ContentData;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.requestmodels.BaseAdapterModel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import faqulty.club.R;
import faqulty.club.fragments.profilesections.BasicSection;
import faqulty.club.fragments.profilesections.TutionDetailsSection;
import faqulty.club.fragments.profilesections.QualificationSection;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by saurabh on 16-09-2016.
 */
public class ProfilePreviewFragment extends BaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {
    private SocialSection socialSection;
    private BasicSection basicSection;
    private QualificationSection qualification;
    private TutionDetailsSection tutionDetailsSections;
    private FacultySection facultySection;
    private RecyclerView recyclerViewContent;
    private List<BaseAdapterModel> contentList = new ArrayList<>();
    private BaseRecycleAdapter contentAdapter;
    private UserDetailsApi user;
    private ArrayList<ProfileComments> comments=new ArrayList<>();

    @Override
    public void initViews() {
        user = UserDetailsApi.getInstance();
        contentList = new ArrayList<>();
        recyclerViewContent = (RecyclerView) findView(R.id.rv_content);
        recyclerViewContent.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        initSections();
        setOnClickListener(R.id.tv_more_content, R.id.lay_comment);
        getProfileComments();
    }
    private void getProfileComments() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PROFILE_COMMENTS);
        httpParamObject.addParameter("reqtype", "user_review_info");
        httpParamObject.addParameter("tutid", ""+Preferences.getUserId());
        httpParamObject.setClassType(ProfileComments.class);
        executeTask(AppConstants.TASKCODES.GET_PROFILE_COMMENTS, httpParamObject);
    }
    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_PROFILE_COMMENTS:
                List<ProfileComments> profileCommentses= (List<ProfileComments>) response;
                if(profileCommentses!=null){
                    comments.clear();
                    comments.addAll(profileCommentses);
                    setText(R.id.number_of_comments,"("+profileCommentses.size()+")");
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_more_content:
                startNextActivity(ContentActivity.class);
                break;
            case R.id.lay_comment:
                Bundle bundle=new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,comments);
                startNextActivity(bundle,TutorsCommentActivity.class);
                break;
        }
    }


    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_USER:
                UserDetailsApi userDetail = UserDetailsApi.getInstance();
                if (userDetail != null) {
                    initSections();
                } else {
                    showVisibility(R.id.layout_connectivity);
                }
                break;
        }
    }


    private void initSections() {
        user = UserDetailsApi.getInstance();
        basicSection = new BasicSection(getActivity(), user, v, this);
        socialSection = new SocialSection(getActivity(), user, v, this);
        if (user.isTutor()) {
            hideVisibility(R.id.frame_section_faculty);
            qualification = new QualificationSection(getActivity(), user, v, this);
        } else {
            hideVisibility(R.id.frame_section_qualification);
            showVisibility(R.id.frame_section_faculty);
            facultySection = new FacultySection(getActivity(), user, v, this);
        }
        tutionDetailsSections = new TutionDetailsSection(getActivity(), user, v, this);
        setContentRecycler(user);
    }

    private void setContentRecycler(UserDetailsApi user) {
        if (user != null && user.getContentUpload() != null && user.getContentUpload().size() > 0) {
            LinearLayoutManager manager = new LinearLayoutManager(getActivity());
            contentList.clear();
            for (ContentData data : user.getContentUpload()) {
                data.setShowPencil(false);
            }
            if (user.getContentUpload().size() < 3) {
                findView(R.id.tv_more_content).setVisibility(View.GONE);
                contentList.addAll(user.getContentUpload());
            } else {
                findView(R.id.tv_more_content).setVisibility(View.VISIBLE);
                for (int i = 0; i < 3; i++) {
                    contentList.add(user.getContentUpload().get(i));
                }
            }
            contentAdapter = new BaseRecycleAdapter(getActivity(), contentList);
            recyclerViewContent.setLayoutManager(manager);
            recyclerViewContent.setAdapter(contentAdapter);
            contentAdapter.setClickInterface(this);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != activity.RESULT_OK)
            return;
    }


    @Override
    public int getViewID() {
        return R.layout.personal_profile;
    }


    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {

    }
}
