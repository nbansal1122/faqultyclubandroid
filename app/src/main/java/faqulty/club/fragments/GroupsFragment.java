package faqulty.club.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import faqulty.club.R;
import faqulty.club.activity.CreateNewClassActivity;
import faqulty.club.activity.GroupAttendanceActivity;
import faqulty.club.activity.InviteStudentsActivity;
import faqulty.club.activity.StudentInvoiceActivity;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.listeners.OnListSelectListener;
import faqulty.club.models.tutorgroup.TutorGroupData;
import faqulty.club.models.tutorgroup.TutorsGroupsResponse;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.tutorstudent.StudentProfileResponse;
import faqulty.club.models.tutorstudent.TutorsStudentsResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class GroupsFragment extends BaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {
    private List<BaseAdapterModel> groupsList = new ArrayList<>();
    private List<BaseAdapterModel> selectedList = new ArrayList<>();
    private BaseRecycleAdapter<BaseAdapterModel> adapter;
    private OnListSelectListener onListSelectListener;
    private ArrayList<HttpParamObject> httpParamObjects;
    private boolean isDeletding;

    public static GroupsFragment getInstance(OnListSelectListener onListSelectListener) {
        GroupsFragment studentFragment = new GroupsFragment();
        studentFragment.onListSelectListener = onListSelectListener;
        return studentFragment;
    }

    public void setDataChanged(OnDataSetChanged dataChanged) {
        this.dataChanged = dataChanged;
    }

    private OnDataSetChanged dataChanged;
    private RecyclerView recyclerView;

    @Override
    public void initViews() {
        setRecyclerView();
        getTutorGroupData();
    }

    private void setRecyclerView() {
        recyclerView = (RecyclerView) findView(R.id.lv_groups);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        adapter = new BaseRecycleAdapter<>(getActivity(), groupsList);
        recyclerView.setAdapter(adapter);
        adapter.setClickInterface(this);
    }


    private void getTutorGroupData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GETTUTORGROUP, Preferences.getUserId()));
        httpParamObject.setClassType(TutorsGroupsResponse.class);
        executeTask(AppConstants.TASKCODES.GETTUTORGROUP, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GETTUTORGROUP:
                groupsList.clear();
                TutorsGroupsResponse tgr = (TutorsGroupsResponse) response;
                List<TutorGroupData> data = tgr.getData();
                if (null != tgr && tgr.getStatus() && data.size() > 0) {
                    groupsList.addAll(data);
                }
                adapter.notifyDataSetChanged();
                getTutorStudentData();
                break;
            case AppConstants.TASKCODES.GETTUTORSTUDENT:
                TutorsStudentsResponse tsr = (TutorsStudentsResponse) response;
                if (response != null && tsr.getStatus()) {
                    List<StudentBaseClass> studentBaseClassList = tsr.getData().getGroup();
                    if (studentBaseClassList != null) {
                        groupsList.addAll(studentBaseClassList);
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
            case AppConstants.TASKCODES.DELETE:
                callDeleteApi();
                break;
            case AppConstants.TASKCODES.GET_STUDENT_DATA:
                if (response != null) {
                    StudentProfileResponse studentProfile = (StudentProfileResponse) response;
                    List<TutorGroupData> groups = studentProfile.getGroups();
                    if (groups != null && groups.size() > 0) {
                        TutorGroupData tutorGroupData = groups.get(0);
                        StudentBaseClass student = studentProfile.getStudent();
                        if (student != null) {
                            deleteGroup(tutorGroupData.getId(), student.getId());
                            return;
                        }
                    }
                }
                callDeleteApi();
                break;
        }
        if (groupsList.size() > 0) {
            hideVisibility(android.R.id.empty);
        } else {
            showVisibility(android.R.id.empty);
        }
    }

    private void deleteGroup(int tutorGroupDataId, int studentId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.DELETE_STUDENT_FROM_GROUP, tutorGroupDataId, studentId));
        httpParamObject.setDeleteMethod();
        executeTask(AppConstants.TASKCODES.DELETE, httpParamObject);
    }


    public void getTutorStudentData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GETTUTORSTUDENT, Preferences.getUserId()));
        httpParamObject.setClassType(TutorsStudentsResponse.class);
        executeTask(AppConstants.TASKCODES.GETTUTORSTUDENT, httpParamObject);
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_classes;
    }


    private void startEditGroup(TutorGroupData data) {
        Bundle b = new Bundle();
        b.putInt(AppConstants.BUNDLE_KEYS.GROUP_ID, data.getId());
        startNextActivityForResult(b, CreateNewClassActivity.class, AppConstants.REQUEST_CODES.CREATECLASS);
    }

    private void startAttendanceActivity(TutorGroupData data) {
        Bundle b = new Bundle();
        b.putInt(AppConstants.BUNDLE_KEYS.GROUP_ID, data.getId());
        startNextActivity(b, GroupAttendanceActivity.class);
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.GROUP_CLICKED:
                TutorGroupData tutorGroupData1 = (TutorGroupData) obj;
                if (selectedList.size() > 0) {
                    setSelect(tutorGroupData1);
                } else {
                    startEditGroup(tutorGroupData1);
                }
                break;
            case AppConstants.ACTION_TYPE.GROUP_LONG_CLICK:
                TutorGroupData tutorGroupData = (TutorGroupData) obj;
                setSelect(tutorGroupData);
                break;
            case AppConstants.ACTION_TYPE.GROUP_ATTENDANCE:
                TutorGroupData tutorGroupData2 = (TutorGroupData) obj;
                if (selectedList.size() > 0) {
                    setSelect(tutorGroupData2);
                } else {
                    startAttendanceActivity(tutorGroupData2);
                }
                break;
            case AppConstants.ACTION_TYPE.GROUP_STUDENT_CLICKED:
                StudentBaseClass sbc1 = (StudentBaseClass) obj;
                if (selectedList.size() > 0) {
                    setSelect(sbc1);
                } else {
                    onGroupStudentRowClicked(sbc1);
                }
                break;
            case AppConstants.ACTION_TYPE.GROUP_STUDENT_LONG_CLICKED:
                StudentBaseClass sbc = (StudentBaseClass) obj;
                setSelect(sbc);
                break;
        }
    }

    private void setSelect(BaseAdapterModel tutorGroupData) {
        if (selectedList.contains(tutorGroupData)) {
            selectedList.remove(tutorGroupData);
            tutorGroupData.setSelectedForDelete(false);
        } else {
            selectedList.add(tutorGroupData);
            tutorGroupData.setSelectedForDelete(true);
        }
        adapter.notifyDataSetChanged();
        onListSelectListener.onSelectListener(selectedList);
    }

    private void onGroupStudentRowClicked(StudentBaseClass sbc) {
        if (sbc.isInvitePending()) {
            startInviteActivity(sbc);
        } else {
            startInvoiceActivity(sbc);
        }
    }

    private void startInvoiceActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, sbc.getId());
        if (sbc.isGroupStudent()) {
            startNextActivity(bundle, StudentInvoiceActivity.class);
        } else {
            startNextActivity(bundle, StudentInvoiceActivity.class);
        }
    }

    private void startInviteActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, sbc.getId());
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, sbc);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, sbc.isGroupStudent());
        startNextActivityForResult(bundle, InviteStudentsActivity.class, AppConstants.REQUEST_CODES.INVITESTUDENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.CREATECLASS:
                getTutorGroupData();
//                if (dataChanged != null) {
//                    dataChanged.onStudentAdd();
//                }
                break;
        }
    }

    public void resetList() {
        for (BaseAdapterModel baseAdapterModel : groupsList) {
            baseAdapterModel.setSelectedForDelete(false);
        }
        for (BaseAdapterModel baseAdapterModel : groupsList) {
            int viewType = baseAdapterModel.getViewType();
            if (viewType == AppConstants.VIEW_TYPE.GROUP_STUDENT) {
                StudentBaseClass studentBaseClass = (StudentBaseClass) baseAdapterModel;
                studentBaseClass.setFirst(true);
                break;
            }
        }
        selectedList.clear();
        onListSelectListener.onSelectListener(selectedList);
        adapter.notifyDataSetChanged();
    }

    public void delete() {
        if (selectedList.size() == 0) {
            return;
        }
        for (BaseAdapterModel baseAdapterModel : selectedList) {
            groupsList.remove(baseAdapterModel);
        }
        if (!Util.isConnectingToInternet(getActivity())) {
            Toast.makeText(getActivity(), "Please Connect to Internet..!", Toast.LENGTH_SHORT).show();
            return;
        }
        httpParamObjects = new ArrayList<>();
        for (BaseAdapterModel baseAdapterModel : selectedList) {
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setJSONContentType();
            int viewType = baseAdapterModel.getViewType();
            if (viewType == AppConstants.VIEW_TYPE.GROUP) {
                TutorGroupData tutorGroupData = (TutorGroupData) baseAdapterModel;
                httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.DELETE_GROUP, Preferences.getUserId()) + "/" + tutorGroupData.getId());
                httpParamObject.setDeleteMethod();
            } else {
                StudentBaseClass studentBaseClass = (StudentBaseClass) baseAdapterModel;
                httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.DELETE_GROUP_STUDENT, studentBaseClass.getGroupId(), String.valueOf(studentBaseClass.getId())));
                httpParamObject.setClassType(StudentProfileResponse.class);
                httpParamObject.setDeleteMethod();
            }
            httpParamObjects.add(httpParamObject);
        }
        callDeleteApi();
        resetList();
    }

    private void callDeleteApi() {
        if (httpParamObjects.size() > 0) {
            HttpParamObject httpParamObject = httpParamObjects.get(0);
            if (httpParamObject.getMethod().equals("GET")) {
                executeTask(AppConstants.TASKCODES.GET_STUDENT_DATA, httpParamObject);
            } else {
                executeTask(AppConstants.TASKCODES.DELETE, httpParamObject);
            }
            httpParamObjects.remove(httpParamObject);
            isDeletding = true;
        } else {
            isDeletding = false;
            getTutorGroupData();
        }
    }

    @Override
    public void showProgressBar() {
        if (isDeletding) {
            return;
        }
        super.showProgressBar();
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        isDeletding = false;
        super.onBackgroundError(re, e, taskCode, params);
    }


    public interface OnDataSetChanged {
        void onStudentAdd();
    }
}
