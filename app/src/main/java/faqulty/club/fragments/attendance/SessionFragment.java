package faqulty.club.fragments.attendance;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.SelectorDialog;
import faqulty.club.models.attendance.AttendanceResponse;
import faqulty.club.models.attendance.PostAttendanceResponse;
import faqulty.club.models.creategroup.PupilInfo;
import faqulty.club.models.tutorstudent.StudentProfileResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 08/11/16.
 */

public class SessionFragment extends BaseFragment {

    public AttendanceResponse attendanceResponse;
    private String date;
    private boolean isNewSession;
    private int sessionCounter;
    private TextView minutesTv;
    int performance = -1;
    private NewSessionListener listener;
    private int studentId;

    public static SessionFragment getInstance(AttendanceResponse attendanceResponse, String date, boolean isNewSession, int sessionCounter, int studentId) {
        SessionFragment f = new SessionFragment();
        f.date = date;
        f.isNewSession = isNewSession;
        f.attendanceResponse = attendanceResponse;
        f.sessionCounter = sessionCounter;
        f.studentId = studentId;
        return f;
    }

    public SessionFragment copyFragment() {
        return getInstance(attendanceResponse, date, isNewSession, sessionCounter, studentId);
    }

    public void setNewSessionListener(NewSessionListener listener) {
        this.listener = listener;
    }


    public boolean isValid() {
        String hours = getEditText(R.id.et_hours);
        String mins = minutesTv.getText().toString();
        if (TextUtils.isEmpty(hours)) {
            if (TextUtils.isEmpty(mins)) {
                showToast(R.string.error_session_duration);
                return false;
            }
        }
        return true;
    }

    //{
//        "session": {
//        "name": "haha",
//        "date": "01-12-2016",
//        "hours": 1,
//        "mins":40,
//        "tutorId": 30
//        },
//        "attendances" : [{
//        "studentId": 4,
//        "performance": 0,
//        "attendance" : 1
//        }]
//        }


    public void postOrUpdateSession() {
        HttpParamObject obj = new HttpParamObject();
        if (attendanceResponse != null) {
            if (attendanceResponse.getSessionId() == null) {
                obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE + "/" + attendanceResponse.getId());
            } else {
                obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE + "/" + attendanceResponse.getSessionId());
            }

            obj.setPutMethod();
        } else {
            obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE);
            obj.setPostMethod();
        }

        try {
            obj.setJson(getSessionJson().toString());
            obj.setJSONContentType();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (attendanceResponse != null) {
            executeTask(AppConstants.TASKCODES.UPDATE_ATTENDANCE, obj);
        } else {
            obj.setClassType(PostAttendanceResponse.class);
            executeTask(AppConstants.TASKCODES.CREATE_ATTENDANCE, obj);
        }

    }

    public JSONObject getSessionJson() throws JSONException {
        JSONObject rootObj = new JSONObject();
        JSONObject sessionObj = new JSONObject();
        JSONArray attendanceArray = new JSONArray();

        sessionObj.put("name", getTextFromTV(R.id.tv_session_name));
        sessionObj.put("date", Util.convertDateFormat(date, Util.UI_ATTENDANCE_DATE_PATTERN, Util.ATTENDANCE_POST_SERVER_FORMAT));
        sessionObj.put("hours", parseInt(getEditText(R.id.et_hours)));
        sessionObj.put("mins", parseInt(getTextFromTV(R.id.et_min)));
        sessionObj.put("tutorId", Preferences.getUserId());


        JSONObject student = new JSONObject();
        student.put("studentId", studentId);
        student.put("performance", performance);
        student.put("attendance", 1);
        attendanceArray.put(student);

        rootObj.put("session", sessionObj);
        rootObj.put("attendances", attendanceArray);

        return rootObj;
    }

    private int parseInt(String text) {
        if (!TextUtils.isEmpty(text)) {
            return Integer.parseInt(text);
        }
        return 0;
    }

    @Override
    public void initViews() {
        minutesTv = (TextView) findView(R.id.et_min);
        hideVisibility(R.id.btn_mark_present);
        setText(R.id.tv_session_name, getString(R.string.session_counter, String.valueOf(sessionCounter + 1)));
        if (attendanceResponse != null) {
            fillViews(attendanceResponse);
        } else {
            getStudentProfile();
            unselectAllPerformance();
        }
        setOnClickListener(R.id.iv_sad, R.id.iv_happy, R.id.iv_neutral, R.id.iv_min, R.id.et_min);

    }

    private void getStudentProfile() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(String.format(AppConstants.PAGE_URL.GET_STUDENT_DATA, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
        obj.setClassType(StudentProfileResponse.class);
        executeTask(AppConstants.TASKCODES.GET_STUDENT_DATA, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_STUDENT_DATA:
                if (response != null) {
                    StudentProfileResponse studentProfile = (StudentProfileResponse) response;
                    if (null != studentProfile.getPupilInfo()) {
                        fillDefaultAttendanceDuration(studentProfile.getPupilInfo());
                    }
                }
                break;
            case AppConstants.TASKCODES.UPDATE_ATTENDANCE:
                showToast(R.string.attendance_registered_successfully);
                break;
            case AppConstants.TASKCODES.CREATE_ATTENDANCE:
                PostAttendanceResponse res = (PostAttendanceResponse) response;
                if (res != null && res.getData() != null) {
                    this.attendanceResponse = res.getData();
                }
                showShortToast(R.string.success_attendance);
                break;
        }
    }

    private void fillDefaultAttendanceDuration(PupilInfo info) {
        setEditText(R.id.et_hours, String.valueOf(info.getClassDurationHours() == null ? "" : info.getClassDurationHours()));
        setText(R.id.et_min, String.valueOf(info.getClassDurationMins() == null ? "" : info.getClassDurationMins()));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_sad:
                setPerformance(0);
                break;
            case R.id.iv_happy:
                setPerformance(2);
                break;
            case R.id.iv_neutral:
                setPerformance(1);
                break;
            case R.id.iv_min:
            case R.id.et_min:
                showMinutesDialog();
                break;
//            case R.id.tv_new_session:
//                if (null != null) {
//                    listener.onNewSessionClicked(date, attendanceResponse);
//                }
//                break;
        }
    }

    private void fillViews(AttendanceResponse attendanceResponse) {
//        setText(R.id.tv_session_name, getString(R.string.session_counter, session.getName()));
        setEditText(R.id.et_hours, String.valueOf(attendanceResponse.getDurationHours() == null ? "" : attendanceResponse.getDurationHours()));
        setText(R.id.et_min, String.valueOf(attendanceResponse.getDurationMins() == null ? "" : attendanceResponse.getDurationMins()));
        setPerformance(attendanceResponse.getPerformance());
    }

    private void showMinutesDialog() {
        List<String> minutesList = SelectorDialog.getMinutes();
        SelectorDialog.getInstance(getActivity(), getString(R.string.title_select_minutes), minutesList, new SelectorDialog.ItemSelector() {
            @Override
            public void onItemSelected(String item) {
                setText(R.id.et_min, item);
            }
        }, getTextFromTV(R.id.et_min)).showDialog();

    }

    public void setPerformance(int performance) {
        this.performance = performance;
        // 0 poor, average, good
        unselectAllPerformance();
        if (performance == 0) {
            setPerformanceIcon(R.id.iv_sad, R.mipmap.sad_icon);
        } else if (performance == 1) {
            setPerformanceIcon(R.id.iv_neutral, R.mipmap.ic_performance_neutral_selected);
        } else if (performance == 2) {
            setPerformanceIcon(R.id.iv_happy, R.mipmap.smile_icon);
        }
    }

    private void unselectAllPerformance() {
        setPerformanceIcon(R.id.iv_sad, R.mipmap.ic_performance_sad_unselected);
        setPerformanceIcon(R.id.iv_happy, R.mipmap.ic_performance_happy_unselected);
        setPerformanceIcon(R.id.iv_neutral, R.mipmap.neutral_face_icon);
    }

    private void setPerformanceIcon(int imageViewId, int iconId) {
        ImageView imageView = (ImageView) findView(imageViewId);
        imageView.setImageResource(iconId);
    }

    @Override
    public int getViewID() {
        return R.layout.layout_attendence;
    }


    public static interface NewSessionListener {
        public void onNewSessionClicked(String date, AttendanceResponse attendanceResponse);
    }
}
