package faqulty.club.fragments.attendance;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.Util.SelectorDialog;
import faqulty.club.activity.MarkAttendenceActivity;
import faqulty.club.models.attendance.group.GroupAttendance;
import faqulty.club.models.attendance.group.GroupSessionPostResponse;
import faqulty.club.models.attendance.group.GroupSessionResponse;
import faqulty.club.models.attendance.group.StudentSession;
import faqulty.club.models.creategroup.CreateGroupResponse;
import faqulty.club.models.creategroup.PupilInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 09/11/16.
 */

public class GroupSessionFragment extends BaseFragment {

    GroupAttendance attendanceResponse;
    private String date;
    private boolean isNewSession;
    private int sessionCounter;
    private TextView minutesTv;
    int performance = -1;
    private int groupId;
    private ArrayList<StudentSession> postSessions;

    public static GroupSessionFragment getInstance(GroupAttendance attendanceResponse, String date, boolean isNewSession, int sessionCounter, int groupId) {
        GroupSessionFragment f = new GroupSessionFragment();
        f.date = date;
        f.isNewSession = isNewSession;
        f.attendanceResponse = attendanceResponse;
        f.sessionCounter = sessionCounter;
        f.groupId = groupId;
        return f;
    }


    public boolean isValid() {
        String hours = getEditText(R.id.et_hours);
        String mins = minutesTv.getText().toString();
        if (TextUtils.isEmpty(hours)) {
            if (TextUtils.isEmpty(mins)) {
                showToast(getString(R.string.error_session_duration));
                return false;
            }
        }
        return true;
    }

    //{
//        "session": {
//        "name": "haha",
//        "date": "01-12-2016",
//        "hours": 1,
//        "mins":40,
//        "tutorId": 30
//        },
//        "attendances" : [{
//        "studentId": 4,
//        "performance": 0,
//        "attendance" : 1
//        }]
//        }
    public JSONObject getSessionJson() throws JSONException {
        JSONObject sessionObj = new JSONObject();

        sessionObj.put("name", getTextFromTV(R.id.tv_session_name));
        sessionObj.put("date", Util.convertDateFormat(date, Util.UI_ATTENDANCE_DATE_PATTERN, Util.ATTENDANCE_SERVER_FORMAT));
        sessionObj.put("hours", parseInt(getEditText(R.id.et_hours)));
        sessionObj.put("mins", parseInt(getTextFromTV(R.id.et_min)));
        sessionObj.put("tutorId", Preferences.getUserId());
        sessionObj.put("groupId", groupId);
        return sessionObj;
    }

    private int parseInt(String text) {
        if (!TextUtils.isEmpty(text)) {
            return Integer.parseInt(text);
        }
        return 0;
    }

    @Override
    public void initViews() {
        minutesTv = (TextView) findView(R.id.et_min);
        setText(R.id.tv_session_name, getString(R.string.session_counter, String.valueOf(sessionCounter + 1)));
        if (attendanceResponse != null) {
            fillViews(attendanceResponse);
        } else {
            fetchGroupData();
        }
        hideVisibility(R.id.tv_today_performance);
        Button btn = (Button) findView(R.id.btn_mark_present);
        btn.setText(getString(R.string.continue_attendance));
        setOnClickListener(R.id.btn_mark_present, R.id.iv_min, R.id.et_min);
        hideVisibility(R.id.iv_sad, R.id.iv_neutral, R.id.iv_happy);
    }

    private void fetchGroupData() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GROUP_URL + groupId);
        obj.setClassType(CreateGroupResponse.class);
        executeTask(AppConstants.TASKCODES.FETCH_GROUP, obj);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_mark_present:
                if (isValid()) {
                    Intent i = new Intent(getActivity(), MarkAttendenceActivity.class);
                    Bundle b = new Bundle();
                    b.putInt(AppConstants.BUNDLE_KEYS.GROUP_ID, groupId);
                    if (attendanceResponse != null && attendanceResponse.getStudentSessions() != null) {
                        b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, attendanceResponse);
                    }
                    i.putExtras(b);
                    startActivityForResult(i, AppConstants.REQUEST_CODES.MARK_ATTENDANCE);
                }
                break;
            case R.id.iv_min:
            case R.id.et_min:
                showMinutesDialog();
                break;
        }
    }

    private void showMinutesDialog() {
        List<String> minutesList = SelectorDialog.getMinutes();
        SelectorDialog.getInstance(getActivity(), getString(R.string.title_select_minutes), minutesList, new SelectorDialog.ItemSelector() {
            @Override
            public void onItemSelected(String item) {
                setText(R.id.et_min, item);
            }
        }, getTextFromTV(R.id.et_min)).showDialog();

    }

    private void fillViews(GroupAttendance attendanceResponse) {
        setEditText(R.id.et_hours, String.valueOf(attendanceResponse.getDurationHours() == null ? "" : attendanceResponse.getDurationHours()));
        setText(R.id.et_min, String.valueOf(attendanceResponse.getDurationMins() == null ? "" : attendanceResponse.getDurationMins()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;
        String attendance = data.getStringExtra(AppConstants.BUNDLE_KEYS.ARRAY_STUDENT_ATTENDANCE);
        postSessions = (ArrayList<StudentSession>) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        postAttendance(attendance);
    }

    private void postAttendance(String attendance) {
        int taskCode = AppConstants.TASKCODES.MARK_GROUP_ATTENDANCE;
        HttpParamObject obj = new HttpParamObject();
        if (attendanceResponse != null) {
            obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE + "/" + attendanceResponse.getId());
            obj.setPutMethod();
            taskCode = AppConstants.TASKCODES.UPDATE_GROUP_ATTENDANCE;
            obj.setClassType(GroupSessionResponse.class);
        } else {
            obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE);
            obj.setPostMethod();
            obj.setClassType(GroupSessionPostResponse.class);
        }
        try {
//            attendances

            JSONObject root = new JSONObject();
            JSONObject jsonObject = getSessionJson();
            JSONArray array = new JSONArray(attendance);
            root.put("attendances", array);
            root.put("session", jsonObject);
            obj.setJson(root.toString());
            obj.setJSONContentType();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        executeTask(taskCode, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.MARK_GROUP_ATTENDANCE:
                if (attendanceResponse != null) {
                    showToast(R.string.attendance_updated_successfully);
                } else {
                    showToast(R.string.attendance_marked_successfully);
                }
                GroupSessionPostResponse res = (GroupSessionPostResponse) response;
                if (res != null && res.getData() != null) {
                    attendanceResponse = res.getData();
                    attendanceResponse.setCreatedOn(Util.convertDateFormat(attendanceResponse.getCreatedOn(), Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.UI_ATTENDANCE_DATE_PATTERN));
                    attendanceResponse.setStudentSessions(postSessions);
                }
                break;
            case AppConstants.TASKCODES.FETCH_GROUP:
                CreateGroupResponse groupResponse = (CreateGroupResponse) response;
                if (groupResponse != null && groupResponse.getStatus() && groupResponse.getCreateGroupData() != null && groupResponse.getCreateGroupData().getPupilInfo() != null) {
                    fillDefaultSessionDuration(groupResponse.getCreateGroupData().getPupilInfo());
                }
                break;
            case AppConstants.TASKCODES.UPDATE_GROUP_ATTENDANCE:
                GroupSessionResponse gsr = (GroupSessionResponse) response;
                if (gsr != null && gsr.getData() != null && gsr.getData().size() > 0) {
                    showToast(R.string.attendance_updated_successfully);
                    attendanceResponse = gsr.getData().get(0);
                }
                break;
        }
    }

    private void fillDefaultSessionDuration(PupilInfo info) {
        setEditText(R.id.et_hours, String.valueOf(info.getClassDurationHours() == null ? "" : info.getClassDurationHours()));
        setText(R.id.et_min, String.valueOf(info.getClassDurationMins() == null ? "" : info.getClassDurationMins()));
    }

    @Override
    public int getViewID() {
        return R.layout.layout_attendence;
    }

    public GroupSessionFragment copyFragment() {
        return getInstance(attendanceResponse, date, isNewSession, sessionCounter, groupId);
    }


    public static interface NewSessionListener {
        public void onNewSessionClicked(Date date, GroupAttendance attendanceResponse);
    }
}
