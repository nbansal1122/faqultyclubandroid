package faqulty.club.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import faqulty.club.R;
import faqulty.club.Util.AppUtil;
import faqulty.club.Util.CustomViewPager;
import faqulty.club.activity.CreateNewClassActivity;
import faqulty.club.activity.InviteStudentsActivity;
import faqulty.club.listeners.OnListSelectListener;
import faqulty.club.models.UserProfile.UserDetailsApi;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class StudentsFragment extends AppBaseFragment implements CustomPagerAdapter.PagerAdapterInterface, OnListSelectListener, ViewPager.OnPageChangeListener {
    private CustomViewPager viewPager;
    private List<String> tab = new ArrayList<>();
    private FloatingActionButton floatingActionButton;
    private StudentFragment studentFragment;
    private GroupsFragment classesFragment;
    private boolean isDeleteoptionMenu;
    private UserDetailsApi user;
    boolean isTutor;
    private List<BaseAdapterModel> selectedList = new ArrayList<>();

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        user = UserDetailsApi.getInstance();
        isTutor = user.isTutor();
        studentFragment = StudentFragment.getInstance(this);
        classesFragment = GroupsFragment.getInstance(this);
        classesFragment.setDataChanged(studentFragment);

        floatingActionButton = (FloatingActionButton) findView(R.id.fab_plus);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = viewPager.getCurrentItem();
                switch (pos) {
                    case 0:
                        if (isTutor) {
                            Intent i0 = new Intent(getActivity(), InviteStudentsActivity.class);
                            startActivityForResult(i0, AppConstants.REQUEST_CODES.INVITESTUDENT);
                        } else {
                            Intent i1 = new Intent(getActivity(), CreateNewClassActivity.class);
                            startActivityForResult(i1, AppConstants.REQUEST_CODES.CREATECLASS);
                        }
                        break;
                    case 1:
                        Intent i1 = new Intent(getActivity(), CreateNewClassActivity.class);
                        startActivityForResult(i1, AppConstants.REQUEST_CODES.CREATECLASS);
                        break;
                }
            }
        });
        initTab();

        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findView(R.id.tab_layout);
        tabStrip.setTextSize((int) Util.convertDpToPixel(14, getContext()));
        tabStrip.setShouldExpand(true);
        tabStrip.setTextColor(getResourceColor(R.color.gray));
        viewPager = (CustomViewPager) findView(R.id.viewpager);
        CustomTabPagerAdapter customPagerAdapter = new CustomTabPagerAdapter(getChildFragmentManager(), tab, this);
        viewPager.setAdapter(customPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        tabStrip.setViewPager(viewPager);
        List<TextView> viewList=AppUtil.getTextViews(tabStrip);
        if(!isTutor){
            if(viewList.size()>0){
                TextView textView = viewList.get(0);
                textView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.error_icon,0);
            }
        }
        AppUtil.setTabFont(tabStrip);


        setOnClickListener(R.id.view_tab);
        if (!isTutor) {
            viewPager.setSwaepable(false);
            viewPager.setMyScroller();
            viewPager.setCurrentItem(1);
            showVisibility(R.id.view_tab);
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.fc_icon;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK)
            return;

        classesFragment.onActivityResult(requestCode, resultCode, data);
        if (isTutor)
            studentFragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_no_icons, menu);
        MenuItem itemDelete = menu.findItem(R.id.action_delete);
        if (isDeleteoptionMenu) {
            itemDelete.setVisible(true);
        } else {
            itemDelete.setVisible(false);
        }
    }

    private void initTab() {
        tab.add(getString(R.string.individual));
        tab.add(getString(R.string.groups_cap));
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_students;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position) {
            case 0:
                return studentFragment;
            case 1:
                return classesFragment;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }

    @Override
    public void onSelectListener(List<BaseAdapterModel> list) {
        this.selectedList = list;
        if (list != null && list.size() > 0) {
            initToolBar(list.size() + " Selected");
            isDeleteoptionMenu = true;
        } else {
            initToolBar("");
            isDeleteoptionMenu = false;
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (isTutor) {
            studentFragment.resetList();
        }
        classesFragment.resetList();
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                showDeleteDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm");
        builder.setMessage("Are you sure? you want to delete");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (isTutor) {
                    studentFragment.delete();
                }
                classesFragment.delete();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    class CustomTabPagerAdapter extends CustomPagerAdapter  {

        public CustomTabPagerAdapter(FragmentManager fm, List list, PagerAdapterInterface listener) {
            super(fm, list, listener);
        }
    }
}
