package faqulty.club.fragments;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import faqulty.club.AttendenceActivity;
import faqulty.club.R;
import faqulty.club.activity.InviteStudentsActivity;
import faqulty.club.activity.StudentInvoiceActivity;
import faqulty.club.listeners.OnListSelectListener;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.tutorstudent.TutorsStudentsResponse;
import faqulty.club.service.FacultiClubeServices;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.GenericLocalReceiver;
import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class StudentFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener, GroupsFragment.OnDataSetChanged, AdapterView.OnItemLongClickListener {
    private static final int REQ_UPDATE_DATA = 1;
    private List<StudentBaseClass> arrayList = new ArrayList<>();
    private List<BaseAdapterModel> selectedList = new ArrayList<>();
    private ListView lvIndividual;
    private CustomListAdapter adapter;
    private OnListSelectListener onListSelectListener;
    private boolean isItemsSelect;

    public static StudentFragment getInstance(OnListSelectListener onListSelectListener) {
        StudentFragment studentFragment = new StudentFragment();
        studentFragment.onListSelectListener = onListSelectListener;
        return studentFragment;
    }

    @Override
    public void initViews() {
        lvIndividual = (ListView) findView(R.id.lv_students);
        adapter = new CustomListAdapter(getActivity(), R.layout.row_student, arrayList, this);
        lvIndividual.setAdapter(adapter);
        lvIndividual.setOnItemClickListener(this);
        lvIndividual.setOnItemLongClickListener(this);
        lvIndividual.setEmptyView(findView(android.R.id.empty));
        getTutorStudentData();
        registerReceiver();
    }

    protected IntentFilter getIntentFilter() {
        return new IntentFilter(GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH);
    }

    @Override
    public void onReceive(Intent intent) {
        super.onReceive(intent);
        if (GenericLocalReceiver.DiscvrReceiver.ACTION_REFRESH.equalsIgnoreCase(intent.getAction())) {
            getTutorStudentData();
        }
    }

    public void getTutorStudentData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GETTUTORSTUDENT, Preferences.getUserId()));
        httpParamObject.setClassType(TutorsStudentsResponse.class);
        executeTask(AppConstants.TASKCODES.GETTUTORSTUDENT, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GETTUTORSTUDENT:
                TutorsStudentsResponse tsr = (TutorsStudentsResponse) response;
                if (response != null && tsr.getStatus() && null != tsr.getData().getIndividual()) {
                    arrayList.clear();
                    arrayList.addAll(tsr.getData().getIndividual());
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_student;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StudentBaseClass studentBaseClass = arrayList.get(position);
        if (isItemsSelect) {
            onSelectStudent(studentBaseClass);
        } else {
            if (studentBaseClass.isInvitePending()) {
                startInviteActivity(studentBaseClass);
            } else {
                startInvoiceActivity(studentBaseClass);
            }
        }
    }

    private void onStudentClicked(StudentBaseClass studentBaseClass) {
        if (isItemsSelect) {
            onSelectStudent(studentBaseClass);
        } else {
            if (studentBaseClass.isInvitePending()) {
                startInviteActivity(studentBaseClass);
            } else {
                startInvoiceActivity(studentBaseClass);
            }
        }
    }

    private void startInvoiceActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, sbc.getId());
        if (sbc.isGroupStudent()) {
            startNextActivity(bundle, StudentInvoiceActivity.class);
        } else {
            startNextActivity(bundle, StudentInvoiceActivity.class);
        }
    }

    private void startInviteActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BUNDLE_KEYS.KEY_STUDENT_ID, sbc.getId());
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, sbc);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.KEY_IS_GROUP_STUDENT, sbc.isGroupStudent());
        startNextActivityForResult(bundle, InviteStudentsActivity.class, AppConstants.REQUEST_CODES.INVITESTUDENT);
    }

    private void startAttendanceActivity(StudentBaseClass sbc) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.STUDENT, sbc);
        if (sbc.isGroupStudent()) {
            startNextActivity(bundle, AttendenceActivity.class);
        } else {
            startNextActivity(bundle, AttendenceActivity.class);
        }
    }

    @Override
    public void onStudentAdd() {
        getTutorStudentData();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        StudentBaseClass studentBaseClass = arrayList.get(position);
        onSelectStudent(studentBaseClass);
        return true;
    }

    private void onSelectStudent(StudentBaseClass studentBaseClass) {
        if (selectedList.contains(studentBaseClass)) {
            selectedList.remove(studentBaseClass);
        } else {
            selectedList.add(studentBaseClass);
        }
        if (selectedList.size() > 0) {
            isItemsSelect = true;
        } else {
            isItemsSelect = false;
        }
        onListSelectListener.onSelectListener(selectedList);
        adapter.notifyDataSetChanged();
    }

    public void resetList() {
        selectedList.clear();
        onListSelectListener.onSelectListener(selectedList);
        adapter.notifyDataSetChanged();
    }

    public void delete() {
        if (!Util.isConnectingToInternet(getActivity())) {
            Toast.makeText(getActivity(), "Please Connect to Internet..!", Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<HttpParamObject> httpParamObjects = new ArrayList<>();
        for (BaseAdapterModel baseAdapterModel : selectedList) {
            StudentBaseClass studentBaseClass = (StudentBaseClass) baseAdapterModel;
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setDeleteMethod();
            httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GETTUTORSTUDENT, Preferences.getUserId()) + "/" + studentBaseClass.getId());
            httpParamObjects.add(httpParamObject);
        }
        FacultiClubeServices.hitDeleteApiOnBackground(getActivity(), httpParamObjects);
        for (BaseAdapterModel baseAdapterModel : selectedList) {
            arrayList.remove(baseAdapterModel);
        }
        resetList();
    }


    class Holder {
        ImageView ivProfilePic, ivBday, ivFee;
        TextView tvName, tvStatus, tvTitle;
        LinearLayout ll;

        public Holder(View view) {
            ll = (LinearLayout) view.findViewById(R.id.ll_attendance);
            ivProfilePic = (ImageView) view.findViewById(R.id.iv_user_image);
            ivBday = (ImageView) view.findViewById(R.id.icon1);
            ivFee = (ImageView) view.findViewById(R.id.icon2);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvStatus = (TextView) view.findViewById(R.id.tv_invite_status);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.INVITESTUDENT:
                getTutorStudentData();
                break;
            case AppConstants.REQUEST_CODES.CREATECLASS:
                getTutorStudentData();
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final StudentBaseClass sbc = arrayList.get(position);
        if (isSelected(sbc)) {
            convertView.setBackgroundColor(getResourceColor(R.color.light_gray));
        } else {
            convertView.setBackgroundColor(getResourceColor(R.color.white));
        }
        holder.tvTitle.setVisibility(View.GONE);
        /*if (sbc.getFirst()) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            if (!sbc.isGroupStudent()) {
                holder.tvTitle.setText("1:1 Students");
            } else {
                holder.tvTitle.setText("Group Students");
            }

        }*/
        if (sbc.getPendingAmount() > 0) {
            holder.ivFee.setVisibility(View.VISIBLE);
        } else {
            holder.ivFee.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(sbc.getDisplayName()))
            holder.tvName.setText(sbc.getDisplayName());
        if (sbc.getIsBdayToday()) {
            holder.ivBday.setVisibility(View.VISIBLE);
        } else {
            holder.ivBday.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(sbc.getImage())) {
            Picasso.with(getActivity()).load(Util.getCompleteUrl(sbc.getImage())).placeholder(R.mipmap.accountselected2x).
                    into(holder.ivProfilePic);
        } else {
            holder.ivProfilePic.setImageResource(R.mipmap.accountselected2x);
        }
        if (sbc.isInvitePending()) {
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.ll.setVisibility(View.GONE);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStudentClicked(sbc);
                }
            });
        } else {
            holder.tvStatus.setVisibility(View.GONE);
            holder.ll.setVisibility(View.VISIBLE);
            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!sbc.isGroupStudent())
                        startAttendanceActivity(sbc);
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStudentClicked(sbc);
                }
            });
            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onSelectStudent(sbc);
                    return true;
                }
            });
        }
        return convertView;
    }

    private boolean isSelected(StudentBaseClass sbc) {
        return selectedList.contains(sbc);
    }
}
