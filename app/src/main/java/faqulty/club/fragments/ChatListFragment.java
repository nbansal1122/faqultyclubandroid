package faqulty.club.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import faqulty.club.R;
import faqulty.club.chat.IndividualChatActivity;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by admin on 2/21/17.
 */

public class ChatListFragment extends BaseFragment implements CustomListAdapterInterface {
    private List chatList = new ArrayList();
    @Override
    public void initViews() {
        initToolBar("Chat");
        initList();
        ListView listView = (ListView) findView(R.id.list_chat);
        CustomListAdapter customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_chat_list,chatList,this);
        listView.setAdapter(customListAdapter);
    }

    private void initList() {
        chatList.add(new Object());
        chatList.add(new Object());
        chatList.add(new Object());
        chatList.add(new Object());
        chatList.add(new Object());
        chatList.add(new Object());
        chatList.add(new Object());
        chatList.add(new Object());
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_chat_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if(convertView==null){
            convertView=inflater.inflate(resourceID,null);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNextActivity(IndividualChatActivity.class);
            }
        });
        return convertView;
    }
}
