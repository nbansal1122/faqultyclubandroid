package faqulty.club.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import faqulty.club.R;
import faqulty.club.activity.NewContentActivity;
import faqulty.club.activity.PostFeedActivity;

import java.util.ArrayList;

import simplifii.framework.requestmodels.SelectContent;

import simplifii.framework.utility.AppConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFromCameraFragment extends BottomSheetDialogFragment {

    private MediaFragment photoPicker,videoPicker;
    public final int RESULT_OK = 0;
    public final int REQUEST_CODE_IMG=1;
    public final int REQUEST_CODE_VIDEO=2;
    private Bitmap picBitmap;
    private boolean fromPostFeed;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_add_from_camera, null);
        dialog.setContentView(contentView);
        TextView btn_photo_frm_camera = (TextView) dialog.findViewById(R.id.tv_upld_photo_camera);
        final TextView btn_video_frm_camera =(TextView) dialog.findViewById(R.id.tv_upload_video_camera);
        photoPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(photoPicker,"Photo").commit();

        btn_photo_frm_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                photoPicker.setImageListener(new MediaFragment.ImageListener() {

                    @Override
                    public void onGetBitMap(Bitmap bitmap,String path) {
                        SelectContent selectContent = new SelectContent();
                        selectContent.setFileType(AppConstants.FILE_TYPES.IMAGE);
                        selectContent.setFilePath(path);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,selectContent);
                        Bundle b = getArguments();
                        if (b != null){
                            boolean aBoolean = b.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                            if (!aBoolean) {
                                Intent in2 = new Intent();
                                in2.putExtras(bundle);
                                startActivityForResult(in2, AppConstants.FILE_REQUEST_CODE.IMAGE);
                                dismiss();
                            }
                        } else {

                            Intent in1 = new Intent(getActivity(), NewContentActivity.class);
                            in1.putExtras(bundle);
                            startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.IMAGE);
                        }
                    }

                    @Override
                    public void onGetImageArray(ArrayList<Image> images) {

                    }
                });
                BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(photoPicker);
                bottomSheetDialogFragment.setArguments(b);
                bottomSheetDialogFragment.show(getChildFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });

        videoPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(videoPicker,"Video Upload").commit();
        btn_video_frm_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoPicker.getVideoFromCamera(new MediaFragment.MediaListener() {
                    @Override
                    public void onGetUri(Uri uri, int MediaType,String path) {
                        SelectContent selectContent = new SelectContent();
                        selectContent.setFileType(AppConstants.FILE_TYPES.VIDEO);
                        selectContent.setFilePath(path);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,selectContent);
                        Bundle b = getArguments();
                        if (b != null) {
                            boolean aBoolean = b.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                            if (!aBoolean) {
                                Intent in2 = new Intent(getActivity(), PostFeedActivity.class);
                                in2.putExtras(bundle);
                                startActivityForResult(in2, AppConstants.FILE_REQUEST_CODE.VIDEO);
                            }
                        } else {
                            Intent in1 = new Intent(getActivity(), NewContentActivity.class);
                            in1.putExtras(bundle);
                            startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.VIDEO);
                        }
                        Toast.makeText(getActivity(), "Video Selected", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                } );
            }
        });
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

}
