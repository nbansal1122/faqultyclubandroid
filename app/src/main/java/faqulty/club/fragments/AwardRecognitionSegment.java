package faqulty.club.fragments;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import faqulty.club.R;
import faqulty.club.models.UserProfile.TutorAwards;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 20/10/16.
 */

public class AwardRecognitionSegment extends BaseFragment {
    private EditText schoolsEt;
    private TutorAwards schoolsTutoredApi;
    public int index;
    private AwardDeleteInterface schoolDeleteInterface;

    public static AwardRecognitionSegment getInstance(TutorAwards schoolsTutoredApi, int index, AwardDeleteInterface schoolDeleteInterface) {
        AwardRecognitionSegment f = new AwardRecognitionSegment();
        f.schoolsTutoredApi = schoolsTutoredApi;
        f.schoolDeleteInterface = schoolDeleteInterface;
        f.index = index;
        return f;
    }

    public void onClear() {
        this.schoolsTutoredApi = null;
        initViews();
        schoolsEt.setText("");
    }

    public void putFocus() {
        schoolsEt.requestFocus();
    }

    @Override
    public void initViews() {
        schoolsEt = (EditText) findView(R.id.et_schools_of_tution_tutored);
        if (schoolsTutoredApi != null) {
            schoolsEt.setText(schoolsTutoredApi.getAwards() + "");
        }
        setOnClickListener(R.id.iBtn_delete_location);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iBtn_delete_location:
                if (schoolDeleteInterface != null) {

                    if(schoolsEt.hasFocus()){
                        Util.hideKeyboard(getActivity());
                    }
                    schoolDeleteInterface.onAwardDelete(this, schoolsTutoredApi);
                }
                break;
        }
    }

    public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        String school = getEditText(R.id.et_schools_of_tution_tutored);
        if (TextUtils.isEmpty(school)) {
            return null;
        }
        if (schoolsTutoredApi == null) {
            jsonObject.put("awardsRecognition", school);
        } else {
            if (school.equalsIgnoreCase(schoolsTutoredApi.getAwards())) {
//                jsonObject.put("id", schoolsTutoredApi.getId());
                jsonObject.put("awardsRecognition", schoolsTutoredApi.getAwards());
            } else {
                jsonObject.put("awardsRecognition", school);
            }
        }
        return jsonObject;
    }

    @Override
    public int getViewID() {
        return R.layout.layout_schools;
    }

    public static interface AwardDeleteInterface {
        public void onAwardDelete(AwardRecognitionSegment fragment, TutorAwards schoolsTutoredApi);
    }
}