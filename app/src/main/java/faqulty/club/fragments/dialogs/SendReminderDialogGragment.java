package faqulty.club.fragments.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.transaction.ReminderData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raghu on 24/1/17.
 */

public class SendReminderDialogGragment extends DialogFragment {
    ReminderData reminderData=new ReminderData();
    private SendReminderListener sendReminderListener;

    public static void showDialog(SendReminderListener sendReminderListener, FragmentManager fragmentManager){
        SendReminderDialogGragment sendReminderDialogGragment=new SendReminderDialogGragment();
        sendReminderDialogGragment.sendReminderListener=sendReminderListener;
        sendReminderDialogGragment.show(fragmentManager, "reminder");
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_send_reminder, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        initViews(v);
        return builder.create();
    }

    private void initViews(final View view) {
        final TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm_select_invoice);
        final TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_confirm.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_primary));
        reminderData.setType("brief");
        reminderData.setNotificationType("reminder");
        final CheckBox cbSms = (CheckBox) view.findViewById(R.id.rb_sms);
        final CheckBox cbEmail = (CheckBox) view.findViewById(R.id.rb_email);
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbEmail.isChecked() || cbSms.isChecked()) {
                } else {
                    view.findViewById(R.id.tv_error_text).setVisibility(View.VISIBLE);
                    return;
                }
                List<String> modes=new ArrayList();
                if(cbEmail.isChecked()){
                    modes.add("email");
                }
                if(cbSms.isChecked()){
                    modes.add("sms");
                }
                reminderData.setMode(modes);
                dismiss();
                sendReminderListener.onSubmit(reminderData);
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                sendReminderListener.onCancel();
            }
        });
    }
    public interface SendReminderListener{
        void onSubmit(ReminderData reminderData);
        void onCancel();
    }
}
