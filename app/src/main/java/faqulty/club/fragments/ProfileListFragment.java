package faqulty.club.fragments;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import faqulty.club.ContentActivity;
import faqulty.club.EditIntroActivity;
import faqulty.club.EditQualificatonActivity;
import faqulty.club.EditSocialProfileActivity;
import faqulty.club.EditTutionDetailsActivity;
import faqulty.club.FacultyDetailsActivity;
import faqulty.club.R;
import faqulty.club.activity.AskQueryActivity;
import faqulty.club.activity.FragmentHolderActivity;
import faqulty.club.activity.GetReviewActivity;
import faqulty.club.activity.LocationSegmentActivity;
import faqulty.club.models.FcmTokenUpdateResponse;
import faqulty.club.models.UserProfile.ProfileCompleteness;
import faqulty.club.models.UserProfile.UserDetailsApi;
import faqulty.club.pojos.ProfileStructure;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;

public class ProfileListFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<ProfileStructure> profileListData = new ArrayList<>();
    private ListView lv_pro_list;
    private CustomListAdapter customListAdapter;
    private View headerView, footerView;
    private boolean isListSetupDone;
    ProfileCompleteness completeness;

    @Override
    public void initViews() {
        setHasOptionsMenu(true);
        lv_pro_list = (ListView) findView(R.id.lv_profile_option);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_profile, profileListData, this);
        lv_pro_list.addHeaderView(getHeaderView());
//        lv_pro_list.addFooterView(getFooterView());
        lv_pro_list.setAdapter(customListAdapter);
        lv_pro_list.setOnItemClickListener(this);

        setDataList();
        getUserProfile();
        setOnClickListener(R.id.btn_retry, R.id.layout_connectivity);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_preview, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_preview:
                startNextActivity(FragmentHolderActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private View getHeaderView() {
        return headerView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_header_profile_list, null);
    }

    private View getFooterView() {
        footerView = LayoutInflater.from(getActivity()).inflate(R.layout.footer_profile_list, null);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNextActivity(AskQueryActivity.class);
            }
        });
        return footerView;
    }

    private void setDataList() {
        try {
            String fileType = AppConstants.ASSETS_RESOURCES.TUTOR_PROFILE_STRUCTURE;
            UserDetailsApi user = UserDetailsApi.getInstance();
            completeness = null;
            if (user != null) {
                if (!user.isTutor()) {
                    fileType = AppConstants.ASSETS_RESOURCES.TUITION_PROFILE_STRUCTURE;
                }
                completeness = user.getProfileCompleteness();
                setHeaderData(user.getProfilePercentage());
            } else {
                return;
            }
            InputStream inputStream = getResources().getAssets().open(fileType);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            StringBuilder sb = new StringBuilder("");
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            line = sb.toString();
            Gson gson = new Gson();
            Type type = new TypeToken<List<ProfileStructure>>() {
            }.getType();
            List<ProfileStructure> profileStructureList = gson.fromJson(line, type);
            if (CollectionUtils.isNotEmpty(profileStructureList)) {
                profileListData.clear();
                for (ProfileStructure profileStructure : profileStructureList) {
                    profileStructure.setCompleted(isProfileSectionCompleted(completeness, profileStructure));
                    profileListData.add(profileStructure);
                }
                customListAdapter.notifyDataSetChanged();
                isListSetupDone = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_retry:
                getUserProfile();
                break;
        }
    }

    @Override
    protected void onInternetException() {
        super.onInternetException();
        showVisibility(R.id.layout_connectivity);
    }

    private void getUserProfile() {
        hideVisibility(R.id.layout_connectivity);
        HttpParamObject obj = new HttpParamObject();
        obj.setUserUrl();
        obj.setClassType(UserDetailsApi.class);
        obj.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, obj);
    }

    private void uploadFCMToken() {
        if (Preferences.getData(AppConstants.PREF_KEYS.IS_UPDATE_TOKEN, true)) {
            String tkn = Preferences.getData(AppConstants.PREF_KEYS.FCM_TOKEN, "");
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.FCM_UPDATE_TOKEN);
            httpParamObject.setPostMethod();
            httpParamObject.setJSONContentType();
            JSONObject jsonObject = new JSONObject();
            try {
                UserDetailsApi user = UserDetailsApi.getInstance();
                if (user != null) {
                    jsonObject.put("userId", user.getId());
                    if (user.isTutor()) {
                        jsonObject.put("userType", AppConstants.USER_TYPES.TUTOR);
                    } else {
                        jsonObject.put("userType", AppConstants.USER_TYPES.TCENTRE);
                    }
                }
                jsonObject.put("fcmToken", tkn);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            httpParamObject.setJson(jsonObject.toString());
            httpParamObject.setClassType(FcmTokenUpdateResponse.class);
            executeTask(AppConstants.TASKCODES.UPDATE_TOKEN, httpParamObject);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_USER:
                UserDetailsApi userDetail = (UserDetailsApi) response;
                if (userDetail != null) {
                    setDataList();
                    setHeaderData(userDetail.getProfilePercentage());
                    uploadFCMToken();
                } else {
                    showVisibility(R.id.layout_connectivity);
                }
                break;
            case AppConstants.TASKCODES.UPDATE_TOKEN:
                FcmTokenUpdateResponse fcmTokenUpdateResponse = (FcmTokenUpdateResponse) response;
                if (fcmTokenUpdateResponse != null) {
                    if (fcmTokenUpdateResponse.isStatus()) {
                        Preferences.saveData(AppConstants.PREF_KEYS.IS_UPDATE_TOKEN, false);
                    }
                }
                break;
        }
    }

    private void setHeaderData(int percentage) {
        ProgressBar bar = (ProgressBar) headerView.findViewById(R.id.progressBar3);
        bar.setMax(100);
        bar.setProgress(percentage);
        TextView tv = (TextView) headerView.findViewById(R.id.tv_profile_rating);
        tv.setText(String.valueOf(percentage) + "%");
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        showVisibility(R.id.layout_connectivity);

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_profile_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
//            holder = new Holder(convertView);
//            convertView.setTag(holder);
        } else {
//            holder = (Holder) convertView.getTag();
        }
        holder = new Holder(convertView);
        final ProfileStructure profileList = profileListData.get(position);
        holder.tv_title.setText(profileList.getTitle());
        holder.tv_description.setText(profileList.getContent());
        boolean isCompleted = isProfileSectionCompleted(completeness, profileList);
        if (isCompleted) {
            holder.completionSymbol.setBackgroundResource(R.drawable.circle_complete_shape);
        } else {
            holder.completionSymbol.setBackgroundResource(R.drawable.circle_shape);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListItemClicked(profileList);
            }
        });
        return convertView;
    }

    private boolean isProfileSectionCompleted(ProfileCompleteness profileCompleteness, ProfileStructure profileStructure) {
        int percentage = 0;
        if (profileCompleteness != null) {
            switch (profileStructure.getProfileStructureType()) {
                case AppConstants.ProfileStructureType.LOCATION:
                    percentage = profileCompleteness.getLocation();
                    break;
                case AppConstants.ProfileStructureType.TUTION_DETAILS:
                    percentage = profileCompleteness.getTuition();
                    break;
                case AppConstants.ProfileStructureType.PERSONAL_DETAILS:
                case AppConstants.ProfileStructureType.BASIC_DETAILS:
                    percentage = profileCompleteness.getPersonalDetail();
                    break;
                case AppConstants.ProfileStructureType.QUALIFICATION:
                    percentage = profileCompleteness.getQualification();
                    break;
                case AppConstants.ProfileStructureType.SOCIAL_PROFILES:
                    percentage = profileCompleteness.getSocial();
                    break;
                case AppConstants.ProfileStructureType.CONTENT:
                    percentage = profileCompleteness.getContent();
                    break;
                case AppConstants.ProfileStructureType.FACULTY_DETAILS:
                    percentage = profileCompleteness.getQualification();
                    break;
                case AppConstants.ProfileStructureType.REVIEWS:
                    percentage = profileCompleteness.getReview();
                    break;
            }
        }
        return percentage != 0;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            return;
        } else if (position == profileListData.size() + 1) {
            return;
        }
        ProfileStructure profileList = profileListData.get(position - 1);
        onListItemClicked(profileList);
    }

    private void onListItemClicked(ProfileStructure profileList) {
        onProfileTypeClicked(profileList.getProfileStructureType());
    }

    private void onProfileTypeClicked(int type) {
        switch (type) {
            case AppConstants.ProfileStructureType.LOCATION:
                startNextActivity(LocationSegmentActivity.class);
                break;
            case AppConstants.ProfileStructureType.TUTION_DETAILS:
                startNextActivity(EditTutionDetailsActivity.class);
                break;
            case AppConstants.ProfileStructureType.PERSONAL_DETAILS:
            case AppConstants.ProfileStructureType.BASIC_DETAILS:
                startNextActivity(EditIntroActivity.class);
                break;
            case AppConstants.ProfileStructureType.QUALIFICATION:
                startNextActivity(EditQualificatonActivity.class);
                break;
            case AppConstants.ProfileStructureType.SOCIAL_PROFILES:
                startNextActivity(EditSocialProfileActivity.class);
                break;
            case AppConstants.ProfileStructureType.CONTENT:
                startNextActivity(ContentActivity.class);
                break;
            case AppConstants.ProfileStructureType.REVIEWS:
                startNextActivity(GetReviewActivity.class);
                break;

            case AppConstants.ProfileStructureType.FACULTY_DETAILS:
                startNextActivity(FacultyDetailsActivity.class);
                break;
        }
    }

    @Override
    public void startNextActivity(Class<? extends Activity> activityClass) {
        if (null != UserDetailsApi.getInstance()) {
            Intent i = new Intent(getActivity(), activityClass);
            startActivityForResult(i, AppConstants.REQUEST_CODES.UPDATE_PROFILE);
        }
    }

    class Holder {
        TextView tv_title, tv_description;
        RelativeLayout completionSymbol;

        public Holder(View view) {
            tv_title = (TextView) view.findViewById(R.id.tv_name);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            completionSymbol = (RelativeLayout) view.findViewById(R.id.rl_round_complete);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != activity.RESULT_OK)
            return;
        switch (requestCode) {
            case AppConstants.REQUEST_CODES.UPDATE_PROFILE:
                this.getUserProfile();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
