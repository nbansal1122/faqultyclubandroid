package faqulty.club.fragments;

/**
 * Created by admin on 2/17/17.
 */
public class Image {
    private String fileName;
    private String filePath;

    public Image(String fileName, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
