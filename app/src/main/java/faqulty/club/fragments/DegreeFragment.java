package faqulty.club.fragments;

import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.autoadapters.AutoCompleteView;
import faqulty.club.autoadapters.SingleDiaogManager;
import faqulty.club.models.BaseAutoFilter;
import faqulty.club.models.UserProfile.TutorDegree;
import faqulty.club.models.UserProfile.TutorDegreeSubject;
import faqulty.club.models.UserProfile.TutorEducationDetail;
import faqulty.club.models.UserProfile.TutorInstitute;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by saurabh on 16-09-2016.
 */
public class DegreeFragment extends BaseFragment {
    TutorEducationDetail tutorEducationDetail;
    private EditText achievementET;

    private SingleDiaogManager<TutorDegree> degreeAutoView;
    private SingleDiaogManager<TutorDegreeSubject> subjectAutoView;
    private SingleDiaogManager<TutorInstitute> instituteAutoView;

    public int index;
    private Switch searchResultSwitch;
    private DegreeFragmentListener listener;
    boolean isUserAction = true;

    public static DegreeFragment getInstance(TutorEducationDetail t, DegreeFragmentListener listener, int index) {
        DegreeFragment f = new DegreeFragment();
        f.tutorEducationDetail = t;
        f.listener = listener;
        f.index = index;
        return f;
    }

    public boolean isValid() {
        if (degreeAutoView.getSelectedItem() == null && subjectAutoView.getSelectedItem() == null &&
                instituteAutoView.getSelectedItem() == null) {
            return true;
        } else if (degreeAutoView.getSelectedItem() != null && subjectAutoView.getSelectedItem() != null &&
                instituteAutoView.getSelectedItem() != null) {
            return true;
        } else {
            showToast(R.string.error_qualification_details);
            return false;
        }

    }

    public JSONObject getJsonObject() throws JSONException {
        if (degreeAutoView.getSelectedItem() == null && subjectAutoView.getSelectedItem() == null &&
                instituteAutoView.getSelectedItem() == null) {
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonDegree = new JSONObject();
        JSONObject jsonDegreeSubject = new JSONObject();
        JSONObject jsonInstitute = new JSONObject();

        TutorDegree selectedDegree = degreeAutoView.getSelectedItem();
        if (selectedDegree != null) {
            jsonDegree.put("id", selectedDegree.getId());
            jsonDegree.put("degree", selectedDegree.getDegree());
        }
        TutorDegreeSubject selectedSubject = subjectAutoView.getSelectedItem();
        if (selectedSubject != null) {
            jsonDegreeSubject.put("id", selectedSubject.getId());
            jsonDegreeSubject.put("degreeId", selectedSubject.getDegreeId());
            jsonDegreeSubject.put("degreeSubject", selectedSubject.getDegreeSubject());
        }
        TutorInstitute selectedInstitute = instituteAutoView.getSelectedItem();
        if (selectedInstitute != null) {
            jsonInstitute.put("id", selectedInstitute.getId());
        }
        if (tutorEducationDetail != null) {
            jsonObject.put("id", tutorEducationDetail.getId());
            jsonObject.put("prefered", tutorEducationDetail.getPrefered());
            //// TODO: 19-09-2016 changre preferred

        } else {

        }
//        if (jsonDegree.length() > 0 && jsonDegreeSubject.length() > 0 && jsonInstitute.length() > 0) {
        jsonObject.put("prefered", searchResultSwitch.isChecked() ? 1 : 0);
        jsonObject.put("tutorDegree", jsonDegree);
        jsonObject.put("tutorDegreeSubject", jsonDegreeSubject);
        jsonObject.put("tutorInstitute", jsonInstitute);
        return jsonObject;
//        }
//        return null;
    }

    private class Holder {

        TextView tv;

        Holder(View v) {
            tv = (TextView) v.findViewById(R.id.tv_item);
        }
    }

    @Override
    public void initViews() {
        subjectAutoView = new SingleDiaogManager<>((AppCompatActivity) getActivity(), (AutoCompleteTextView) findView(R.id.actv_subject));
        degreeAutoView = new SingleDiaogManager<>((AppCompatActivity) getActivity(), (AutoCompleteTextView) findView(R.id.actv_degree), new AutoCompleteView.ItemSelector() {
            @Override
            public void onItemSelected(BaseAutoFilter item) {
                TutorDegree degree = (TutorDegree) item;
                getSubjects(degree.getId().toString());
                subjectAutoView.setSelectedItem(null);

            }
        });
        instituteAutoView = new SingleDiaogManager<>((AppCompatActivity) getActivity(), (AutoCompleteTextView) findView(R.id.actv_institute));
        searchResultSwitch = (Switch) findView(R.id.show_in_result_switch);
        initQualificaitonDetails();
        getDegrees();
//        getSubjects(null);
        getInstitutes();
        setOnClickListener(R.id.iv_delete_degree, R.id.actv_subject, R.id.actv_degree, R.id.actv_institute);
//        achievementET = (EditText) findView(R.id.et_achievements);
        searchResultSwitch.setOnCheckedChangeListener(changeListener);
        initSearchResultSwitch();
    }

    CompoundButton.OnCheckedChangeListener changeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (listener != null) {
                listener.onItemAction(DegreeFragment.this, true, isChecked);
            }
        }
    };

    private void initSearchResultSwitch() {
        boolean isChecked = false;
        if (tutorEducationDetail != null) {
            isChecked = tutorEducationDetail.getPrefered() == 1;
        } else {
            if (index == 0) {
                isChecked = true;
            }
        }
        searchResultSwitch.setChecked(isChecked);
        searchResultSwitch.setSelected(isChecked);
    }

    public void changeSwitchCheck(boolean isChecked) {
        searchResultSwitch.setOnCheckedChangeListener(null);
        searchResultSwitch.setSelected(isChecked);
        searchResultSwitch.setChecked(isChecked);
        searchResultSwitch.setOnCheckedChangeListener(changeListener);
        isUserAction = false;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_delete_degree:
                if (listener != null) {
                    listener.onItemAction(this, false, false);
                }
                break;
            case R.id.actv_subject:
                subjectAutoView.showDropDown();
                break;
            case R.id.actv_degree:
                degreeAutoView.showDropDown();
                break;
            case R.id.actv_institute:
                instituteAutoView.showDropDown();
                break;
        }
    }

    private void initQualificaitonDetails() {
        instituteAutoView = new SingleDiaogManager<>(((AppCompatActivity) getActivity()), (AutoCompleteTextView) findView(R.id.actv_institute));
        instituteAutoView = new SingleDiaogManager<>(((AppCompatActivity) getActivity()), (AutoCompleteTextView) findView(R.id.actv_institute));
        if (tutorEducationDetail != null) {
            if (!TextUtils.isEmpty(tutorEducationDetail.getCollege())) {
                setEditText(R.id.et_achievements, tutorEducationDetail.getCollege());
            }
            if (tutorEducationDetail.getPrefered() == 0) {
                searchResultSwitch.setChecked(false);
            } else {
                searchResultSwitch.setChecked(true);
            }
            if (tutorEducationDetail.getTutorInstitute() != null)
                instituteAutoView.setSelectedItem(tutorEducationDetail.getTutorInstitute());
            if (tutorEducationDetail.getTutorDegreeSubject() != null)
                subjectAutoView.setSelectedItem(tutorEducationDetail.getTutorDegreeSubject());
            if (tutorEducationDetail.getTutorDegree() != null) {
                degreeAutoView.setSelectedItem(tutorEducationDetail.getTutorDegree());
                getSubjects(tutorEducationDetail.getTutorDegree().getId() + "");
            }

        }
    }


    private void getSubjects(String degreeId) {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_DEGREE_SUBJECTS);
        if (!TextUtils.isEmpty(degreeId)) {
            obj.addParameter("degreeId", degreeId);
        }
        obj.setClassType(TutorDegreeSubject.class);
        executeTask(AppConstants.TASKCODES.GET_DEGREE_SUBJECTS, obj);
    }

    private void getDegrees() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_DEGREE);
        obj.setClassType(TutorDegree.class);
        executeTask(AppConstants.TASKCODES.GET_DEGREE, obj);
    }

    private void getInstitutes() {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_INSTITUTES);
        obj.setClassType(TutorInstitute.class);
        String keyUnivForCache = "UniversityList:";
        obj.addParameter("count", "1000");
        obj.addParameter("offset", "0");
        executeTask(AppConstants.TASKCODES.GET_INSTITUTES, obj, keyUnivForCache);
    }

    @Override
    public int getViewID() {
        return R.layout.details_educational_qualification;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_DEGREE:
                if (response != null) {
                    List<TutorDegree> degreeList = (List<TutorDegree>) response;
                    if (degreeList != null) {
                        degreeAutoView.setItems(degreeList);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_DEGREE_SUBJECTS:
                if (response != null) {
                    List<TutorDegreeSubject> degreeSubjectList = (List<TutorDegreeSubject>) response;
                    if (degreeSubjectList != null) {
                        subjectAutoView.setItems(degreeSubjectList);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_INSTITUTES:
                if (response != null) {
                    List<TutorInstitute> instituteList = (List<TutorInstitute>) response;
                    if (instituteList != null) {
                        instituteAutoView.setItems(instituteList);
                    }
                }
                break;
        }
    }

    public static interface DegreeFragmentListener {
        public void onItemAction(DegreeFragment segment, boolean isSearchChanged, boolean isShowInResults);
    }
}
