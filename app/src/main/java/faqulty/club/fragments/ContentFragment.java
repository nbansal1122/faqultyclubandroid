package faqulty.club.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import faqulty.club.R;
import faqulty.club.Util.AppUtil;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

/**
 * Created by Neeraj Yadav on 12/3/2016.
 */

public class ContentFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface, ViewPager.OnPageChangeListener {
    private ViewPager viewPager;
    private List<String> tab;
    private AssignmentFragment assignmentFragment;
    private FeedFragment feedFragment;
    private BookMarkFragment bookmarkFragment;


    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        initTab();
        viewPager = (ViewPager) findView(R.id.viewpager);
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), tab, this);
        viewPager.setAdapter(customPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(this);
        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findView(R.id.tab_layout);
        tabStrip.setTextSize((int) Util.convertDpToPixel(12.5f, getContext()));
        tabStrip.setTextColor(getResourceColor(R.color.gray));
        tabStrip.setViewPager(viewPager);
        tabStrip.setShouldExpand(true);
        AppUtil.setTabFont(tabStrip);
    }

    private void initTab() {
        tab = new ArrayList<>();
        tab.add("FEEDS");
        tab.add("ASSIGNMENTS");
        tab.add("BOOKMARKS");
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_content;
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.fc_icon;
    }


    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position) {
            case 0:
                feedFragment = new FeedFragment();
                return feedFragment;
            case 1:
                assignmentFragment = new AssignmentFragment();
                return assignmentFragment;
            case 2:
                bookmarkFragment = new BookMarkFragment();
                return bookmarkFragment;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 2 && bookmarkFragment != null) {
            bookmarkFragment.getBookmarkData();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}