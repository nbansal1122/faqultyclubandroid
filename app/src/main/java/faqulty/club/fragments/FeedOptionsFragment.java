package faqulty.club.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import faqulty.club.R;
import faqulty.club.models.feed.Datum;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 12/6/2016.
 */

public class FeedOptionsFragment extends BottomSheetDialogFragment {
    private BottomSheetInterface bottomSheetInterface;
    private FeedFragment feedFragment;
    private Datum feedData;
    String mString;

    public Datum getFeedData() {
        return feedData;
    }

    public void setFeedData(Datum feedData) {
        this.feedData = feedData;
    }

    public BottomSheetInterface getBottomSheetInterface() {
        return bottomSheetInterface;
    }

    public void setBottomSheetInterface(BottomSheetInterface bottomSheetInterface) {
        this.bottomSheetInterface = bottomSheetInterface;
    }


    public static FeedOptionsFragment newInstance(BottomSheetInterface bottomSheetInterface) {
        FeedOptionsFragment feedOptionsFragment = new FeedOptionsFragment();
        feedOptionsFragment.bottomSheetInterface = bottomSheetInterface;
        return feedOptionsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_save_share, container, false);
        RelativeLayout rlEmail = (RelativeLayout) v.findViewById(R.id.rl_email);
        RelativeLayout bookmark = (RelativeLayout) v.findViewById(R.id.rl_bookmark);
        RelativeLayout download = (RelativeLayout) v.findViewById(R.id.rl_download);
        RelativeLayout share = (RelativeLayout) v.findViewById(R.id.rl_share);
        rlEmail.setVisibility(View.GONE);
        if (feedData.getContentList() != null && feedData.getContentList().size() > 0) {

        } else {
            download.setVisibility(View.GONE);
        }
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.BOOKMARK, feedData);
                dismiss();
            }
        });
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.DOWNLOAD_FEED, feedData);
                dismiss();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.SHARE_FEED, feedData);
                dismiss();
            }
        });

        return v;
    }

    public interface BottomSheetInterface {
        public void onActionClick(int actType, Datum feedData);
    }

}
