package faqulty.club.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import faqulty.club.R;
import faqulty.club.Util.EndlessRecyclerViewScrollListener;
import faqulty.club.Util.FileUtil;
import faqulty.club.Util.SpacesItemDecoration;
import faqulty.club.activity.CommentActivity;
import faqulty.club.activity.PostFeedActivity;
import faqulty.club.activity.SaveBookMarkActivity;
import faqulty.club.autoadapters.BaseRecycleAdapter;

import faqulty.club.models.FeedData;
import faqulty.club.models.HashTag;
import faqulty.club.models.UserProfile.SubjectObject;
import faqulty.club.models.feed.Datum;
import faqulty.club.models.feed.DeleteFeed;
import simplifii.framework.requestmodels.BaseAdapterModel;

import faqulty.club.autoadapters.HashTagsMultiSelectDialog;
import faqulty.club.models.HashTagsResponse;
import faqulty.club.models.bookmark.FeedBookmark;
import faqulty.club.models.feed.FetchFeed;
import faqulty.club.models.feed.LikeApi;
import faqulty.club.service.AdHitService;
import com.plumillonforge.android.chipview.ChipView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by Neeraj Yadav on 12/5/2016.
 */

public class FeedFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface, FeedOptionsFragment.BottomSheetInterface, SwipeRefreshLayout.OnRefreshListener {
    private static final int LIMIT = 20;
    private RecyclerView rvFeed;
    private BaseRecycleAdapter baseRecyclerAdapter;
    private ArrayList<BaseAdapterModel> feedList = new ArrayList<>();
    private List<BaseAdapterModel> feedListAll = new ArrayList<>();
    private ArrayList<HashTag> selectedHashTags = new ArrayList<>();
    private ArrayList<HashTag> hashTags = new ArrayList<>();
    ChipView chipSubject, chipClass, chipHashTags;
    private List<SubjectObject> selectedSubjects = new ArrayList<>();
    private boolean needToFetchMoreFeed;
    private boolean isLoading;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener() {
        @Override
        public void onLoadMore(int page, int totalItemsCount) {
            if (!isLoading && needToFetchMoreFeed) {
                isLoading = true;
                OFFSET = feedListAll.size();
                fetchFeedData(OFFSET);
            }
        }
    };

    private SwipeRefreshLayout swipeRefreshLayout;
    Datum feeddata;
    private long userId;
    private int OFFSET = 0;


    @Override
    public void initViews() {
        setHasOptionsMenu(true);
        rvFeed = (RecyclerView) findView(R.id.rv_feed);
        baseRecyclerAdapter = new BaseRecycleAdapter(getActivity(), feedList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        endlessRecyclerViewScrollListener.setmLayoutManager(layoutManager);
        rvFeed.setLayoutManager(layoutManager);
        rvFeed.addItemDecoration(new SpacesItemDecoration(4));
        rvFeed.setAdapter(baseRecyclerAdapter);
        rvFeed.addOnScrollListener(endlessRecyclerViewScrollListener);
        if (feedList.size() == 0) {
            fetchFeedData(0);
        }
        baseRecyclerAdapter.setClickInterface(this);
        setOnClickListener(R.id.fab_plus);

        getHashTags();

        userId = Preferences.getData(AppConstants.PREF_KEYS.USER_ID, 111L);
        swipeRefreshLayout = (SwipeRefreshLayout) findView(R.id.swipeFeed);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getHashTags() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HASH_TAGS);
        httpParamObject.setClassType(HashTagsResponse.class);
        executeTask(AppConstants.TASKCODES.GET_HASH_TAGS, httpParamObject, true);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.fab_plus:
                Intent postFeedActivity = new Intent(getActivity(), PostFeedActivity.class);
                startActivityForResult(postFeedActivity, AppConstants.REQUEST_CODES.UPDATE_DATA);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_feed, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                showHashTagsDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchFeedData(int offset) {
        OFFSET = offset;
        HttpParamObject fetchData = new HttpParamObject();
        fetchData.setUrl(Preferences.getTutorsUrl(String.format(AppConstants.PAGE_URL.FETCH_FEED_DATA, OFFSET, LIMIT)));
        fetchData.setClassType(FetchFeed.class);
        if (OFFSET == 0) {
            executeTask(AppConstants.TASKCODES.FETCHFEED, fetchData);
        } else {
            executeTask(AppConstants.TASKCODES.FETCHFEED_MORE, fetchData);
        }

    }

    private void showHashTagsDialog() {
        HashTagsMultiSelectDialog f = HashTagsMultiSelectDialog.getInstance("Select Hash Tags", hashTags, new HashTagsMultiSelectDialog.ItemSelector() {
            @Override
            public void onItemsSelected(List<HashTag> selectedItems) {
                selectedHashTags.clear();
                selectedHashTags.addAll(selectedItems);
                setHashTagsFilter();
            }
        }, selectedHashTags);
        f.show(getChildFragmentManager(), "HashTag");
    }

    private void setHashTagsFilter() {

        feedList.clear();
        if (selectedHashTags.size() > 0) {
            for (HashTag hashTag : selectedHashTags) {
                String tag = hashTag.getTag();
                for (BaseAdapterModel baseAdapterModel : feedListAll) {
                    Datum datum = (Datum) baseAdapterModel;
                    List<String> hashTags = datum.getHashTags();
                    for (String s : hashTags) {
                        if (s.equalsIgnoreCase(tag)) {
                            feedList.add(baseAdapterModel);
                            break;
                        }
                    }
                }
            }
        } else {
            feedList.addAll(feedListAll);
        }
        baseRecyclerAdapter.notifyDataSetChanged();
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_feed;
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        Datum feedData = (Datum) obj;
        switch (actionType) {
            case AppConstants.ACTION_TYPE.EDIT_BOTTOMSHEET:
                Bundle content = new Bundle();
                content.putSerializable(AppConstants.BUNDLE_KEYS.EDIT_POSTFEED, (Datum) obj);
                EditFeedPostFragment editFeedPostFragment = EditFeedPostFragment.newInstance(this);
                editFeedPostFragment.setArguments(content);
                editFeedPostFragment.show(getChildFragmentManager(), editFeedPostFragment.getTag());
                break;
            case AppConstants.ACTION_TYPE.SHARE_BOTTOMSHEET:
                FeedOptionsFragment feedOptionsFragment = FeedOptionsFragment.newInstance(this);
                feedOptionsFragment.setFeedData(feedData);
                feedOptionsFragment.show(getChildFragmentManager(), feedOptionsFragment.getTag());
                break;
            case AppConstants.ACTION_TYPE.LIKE_EVENT:
                if (!Util.isConnectingToInternet(getActivity())) {
                    return;
                }
                likeApi(feedData);
                if(feedData.isHasLiked()){
                    feedData.setHasLiked(false);
                } else {
                    feedData.setHasLiked(true);
                }
                baseRecyclerAdapter.notifyItemChanged(position);
                break;
            case AppConstants.ACTION_TYPE.COMMENT_BOOKMARK:
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.BUNDLE_KEYS.ENTITY_ID, feedData.getId());
                Intent commentIntent = new Intent(getActivity(), CommentActivity.class);
                commentIntent.putExtras(bundle);
                startActivityForResult(commentIntent, AppConstants.REQUEST_CODES.UPDATE_DATA);
                break;
        }
    }

    private void likeApi(Datum feedData) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.LIKE_FEED_POST);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", Preferences.getUserId());
            jsonObject.put("entityId", feedData.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(LikeApi.class);
        if (feedData.isHasLiked()) {
            httpParamObject.setDeleteMethod();
            executeTask(AppConstants.TASKCODES.UNLIKE_FEED, httpParamObject);
        } else {
            httpParamObject.setPostMethod();
            executeTask(AppConstants.TASKCODES.LIKE_FEED, httpParamObject);
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        if((taskCode==AppConstants.TASKCODES.UNLIKE_FEED)||(taskCode==AppConstants.TASKCODES.LIKE_FEED
        || taskCode == AppConstants.TASKCODES.FETCHFEED_MORE)){
            return;
        }
        super.onPreExecute(taskCode);
    }

    public void update(FeedData editFeedData) {
        feeddata.setDescription(editFeedData.getDesc());
        String imageURI = editFeedData.getImageURI();
        if (!TextUtils.isEmpty(imageURI)) {
            feeddata.setContentUrl(editFeedData.getImageURI());
        }
        baseRecyclerAdapter.notifyDataSetChanged();
    }


    @Override
    public void onActionClick(int actType, Datum feedData) {
        long tutorId = feedData.getTutorId();

        switch (actType) {
            case AppConstants.ACTION_TYPE.EDIT_FEED:
                if (tutorId == userId) {
                    Bundle b = new Bundle();
                    b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, feedData);
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_EDITABLE, true);
                    Intent postFeedActivity = new Intent(getActivity(), PostFeedActivity.class);
                    postFeedActivity.putExtras(b);
                    startActivityForResult(postFeedActivity, AppConstants.REQUEST_CODES.UPDATE_DATA);
                } else {
                    showToast("you are not authenticated");
                }

                break;
            case AppConstants.ACTION_TYPE.BOOKMARK:
                setBookmark(feedData);
                break;
            case AppConstants.ACTION_TYPE.DELETE_FEED:
                if (tutorId == userId) {
                    deleteFeed(feedData);
                } else {
                    showToast("you are not authenticated");
                }
                break;
            case AppConstants.ACTION_TYPE.DOWNLOAD_FEED:
                downloadFeedData(feedData);
                break;
            case AppConstants.ACTION_TYPE.SHARE_FEED: {
                String feedLink = AppConstants.PAGE_URL.FEED_LINK + feedData.getId();
                shareFeedData(feedLink);
            }
            break;
            case AppConstants.ACTION_TYPE.COPY_FEED_LINK:
                String feedLink = AppConstants.PAGE_URL.FEED_LINK + feedData.getId();
                Util.copyToClipboard(getActivity(), feedLink);
                break;


        }
    }


    public void shareFeedData(String feedLink) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, feedLink);
            startActivity(Intent.createChooser(i, "Select an app to share"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void setBookmark(Datum feedData) {
        Bundle b = new Bundle();
        b.putInt(AppConstants.BUNDLE_KEYS.MODULE_ID, feedData.getId());
        b.putString(AppConstants.BUNDLE_KEYS.MODULE_TYPE, "feed");
        startNextActivity(b, SaveBookMarkActivity.class);
    }

    private void downloadFeedData(Datum feedData) {
        List<String> list = new ArrayList<>();
        for (SelectContent content : feedData.getContentList()) {
            list.add(content.getFullUrl());
        }
        FileUtil.downloadFile(getActivity(), list);
    }

    private void deleteFeed(Datum feedData) {
        Integer id = feedData.getId();
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Preferences.getTutorsUrl(AppConstants.PAGE_URL.DELETE_FEED) + id);
        httpParamObject.setDeleteMethod();
        httpParamObject.setClassType(DeleteFeed.class);
        executeTask(AppConstants.TASKCODES.DELETE_FEED, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_respponse));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.BOOKMARK_FEED:
                FeedBookmark feedBookmark = (FeedBookmark) response;
                if (feedBookmark != null && feedBookmark.getStatus()) {
                    String msg = feedBookmark.getMsg();
                    if (!TextUtils.isEmpty(msg)) {
                        showToast(msg);
                    }
                }
                break;

            case AppConstants.TASKCODES.FETCHFEED:
            case AppConstants.TASKCODES.FETCHFEED_MORE:
                isLoading = false;
                FetchFeed fetchFeed = (FetchFeed) response;
                if (fetchFeed != null && fetchFeed.getStatus()) {
                    List<Datum> data = fetchFeed.getData();
                    if (data != null) {
                        ArrayList<Integer> feedIds = new ArrayList<>();
                        if (data.size() == LIMIT) {
                            needToFetchMoreFeed = true;
                        }
                        if (OFFSET == 0) {
                            feedList.clear();
                            feedListAll.clear();
                        }
                        for (Datum datum : data) {
                            feedList.add(datum);
                            feedListAll.add(datum);
                            feedIds.add(datum.getId());
                            setHashTagsFilter();
                        }
                        AdHitService.startServiceToHitAd(getActivity(), feedIds);
                        baseRecyclerAdapter.notifyDataSetChanged();
                    }
                }
                setEmptyFeedView();
                break;

            case AppConstants.TASKCODES.GET_HASH_TAGS:
                HashTagsResponse hashTagsResponse = (HashTagsResponse) response;
                if (hashTagsResponse != null) {
                    List<HashTag> hashTagList = hashTagsResponse.getData();
                    if (hashTagList.size() > 0) {
                        hashTags.clear();
                        hashTags.addAll(hashTagList);
                    }
                }
                break;

            case AppConstants.TASKCODES.DELETE_FEED:
                DeleteFeed deleteFeed = (DeleteFeed) response;
                if (deleteFeed != null && deleteFeed.getStatus()) {
                    showToast(deleteFeed.getMsg());
                    fetchFeedData(0);
                    baseRecyclerAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    private void setEmptyFeedView() {
        if (feedListAll.size() > 0 || feedList.size() > 0) {
            findView(android.R.id.empty).setVisibility(View.GONE);
        } else {
            findView(android.R.id.empty).setVisibility(View.VISIBLE);
        }
    }

//    private void setSelectedHashTags() {
//        if (feeddata != null) {
//            List<String> hashTags = feeddata.getHashTags();
//            if (hashTags != null) {
//                for (String tag : hashTags) {
//                    for (HashTag hashTag : this.hashTags) {
//                        if (hashTag.isMatchingConstraint(tag)) {
//                            if (!selectedHashTags.contains(hashTag)) {
//                                selectedHashTags.add(hashTag);
//                            }
//                        }
//                    }
//                }
//                setHashTagsFilter();
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        } else {
            switch (requestCode) {
                case AppConstants.REQUEST_CODES.UPDATE_DATA:
                    OFFSET = 0;
                    fetchFeedData(0);
                    break;
            }
        }
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        fetchFeedData(0);
    }
}
