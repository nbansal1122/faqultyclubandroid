package faqulty.club.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import faqulty.club.R;
import faqulty.club.activity.NotificationActivity;
import faqulty.club.activity.QuestionViewActivity;
import faqulty.club.activity.SubjectsActivity;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.models.assignment.ClassModel;
import faqulty.club.models.assignment.RecentAssignmentResponse;
import faqulty.club.models.assignment.RecentAssignments;
import faqulty.club.models.assignment.ShowMoreAssignment;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

import static android.app.Activity.RESULT_OK;
import static simplifii.framework.utility.AppConstants.TASKCODES.FETCH_RECENT_ASSIGNMENTS;


public class AssignmentFragment extends AppBaseFragment implements AdapterView.OnItemClickListener, BaseRecycleAdapter.RecyclerClickInterface {

    private List<BaseAdapterModel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private BaseRecycleAdapter<BaseAdapterModel> adapter;

    @Override
    public void initViews() {
        setHasOptionsMenu(true);
        recyclerView = (RecyclerView) findView(R.id.rv_assignment);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BaseRecycleAdapter<>(getActivity(), list);
        adapter.setClickInterface(this);
        recyclerView.setAdapter(adapter);
        getRecentAssignments();
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_no_icons, menu);
//    }

    public void getRecentAssignments() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(Util.getFormattedTutorUrl(AppConstants.PAGE_URL.GET_RECENT_ASSIGNMENTS));
        httpParamObject.addParameter("type", "recent");
        httpParamObject.setClassType(RecentAssignmentResponse.class);
        executeTask(FETCH_RECENT_ASSIGNMENTS, httpParamObject);
    }

    private void getAssignment() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ASSIGNMENT_GET);
        httpParamObject.setClassType(ClassModel.class);
        executeTask(AppConstants.TASKCODES.GET_ASSIGNMENT, httpParamObject, true);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case FETCH_RECENT_ASSIGNMENTS:
                RecentAssignmentResponse res = (RecentAssignmentResponse) response;
                list.clear();
                if (res != null && res.getData() != null) {
                    List<RecentAssignments> data = res.getData();
                    if (data.size() > 2) {
                        list.add(data.get(0));
                        list.add(data.get(1));
                        data.remove(0);
                        data.remove(0);
                        ShowMoreAssignment showMoreAssignment = new ShowMoreAssignment();
                        showMoreAssignment.addAll(data);
                        list.add(showMoreAssignment);
                    } else {
                        list.addAll(data);
                    }
                    adapter.notifyDataSetChanged();
                }
                getAssignment();
                break;
            case AppConstants.TASKCODES.GET_ASSIGNMENT:
                List<ClassModel> assignmentModels = (List<ClassModel>) response;
                if (assignmentModels != null && assignmentModels.size() > 0) {
                    list.addAll(assignmentModels);
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.layout_list_assignment;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.CLASS, list.get(position));
        startNextActivity(bundle, SubjectsActivity.class);
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.CLASS_CLICKED: {
                ClassModel model = (ClassModel) obj;
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.CLASS, model);
                startNextActivityForResult(bundle, SubjectsActivity.class, AppConstants.REQUEST_CODES.OPEN_SUBJECTS);
            }
            break;
            case AppConstants.ACTION_TYPE.RECENT_ASSIGNMENT_CLICKED: {
                RecentAssignments assignment = (RecentAssignments) obj;
                String subtitle = "Class " + assignment.getAssignment().getClassInfo().getClass_() + " / " + assignment.getAssignment().getSubjectInfo().getName() + " / " +
                        assignment.getAssignment().getChapterInfo().getChapterName();
                String title = assignment.getAssignment().getName();
                Bundle b = new Bundle();
                b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, title);
                b.putString(AppConstants.BUNDLE_KEYS.KEY_SUBTITLE, subtitle);
                b.putInt(AppConstants.BUNDLE_KEYS.Q_ID, assignment.getAssignment().getId());
                startNextActivity(b, QuestionViewActivity.class);
            }
            break;
            case AppConstants.ACTION_TYPE.VIEW_MORE:
                ShowMoreAssignment showMoreAssignment = (ShowMoreAssignment) obj;
                list.remove(obj);
                list.addAll(position, showMoreAssignment.getRemaining());
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_assignment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notification:
                startNextActivity(NotificationActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQUEST_CODES.OPEN_SUBJECTS) {
            if (resultCode == RESULT_OK) {
                getRecentAssignments();
            }
        }
    }
}
