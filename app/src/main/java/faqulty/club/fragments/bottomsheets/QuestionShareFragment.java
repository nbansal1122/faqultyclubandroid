package faqulty.club.fragments.bottomsheets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import faqulty.club.R;
import faqulty.club.fragments.FeedFragment;
import faqulty.club.models.feed.Datum;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 28/01/17.
 */

public class QuestionShareFragment extends BottomSheetDialogFragment {
    private QuestionShareInterface bottomSheetInterface;
    private FeedFragment feedFragment;
    private Datum feedData;
    String mString;

    public Datum getFeedData() {
        return feedData;
    }

    public void setFeedData(Datum feedData) {
        this.feedData = feedData;
    }

    public QuestionShareInterface getBottomSheetInterface() {
        return bottomSheetInterface;
    }

    public void setBottomSheetInterface(QuestionShareInterface bottomSheetInterface) {
        this.bottomSheetInterface = bottomSheetInterface;
    }


    public static QuestionShareFragment newInstance(QuestionShareInterface bottomSheetInterface) {
        QuestionShareFragment saveShareFragment = new QuestionShareFragment();
        saveShareFragment.bottomSheetInterface = bottomSheetInterface;
        return saveShareFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_question_share, container, false);
        RelativeLayout rlEmail = (RelativeLayout) v.findViewById(R.id.rl_question_set);
        RelativeLayout bookmark = (RelativeLayout) v.findViewById(R.id.rl_answer_key);
        rlEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.SHARE_QUESTION_SET);
                dismiss();
            }
        });
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.SHARE_ANSWER_KEY);
                dismiss();
            }
        });
        return v;
    }

    public interface QuestionShareInterface {
        public void onActionClick(int actType);
    }

}