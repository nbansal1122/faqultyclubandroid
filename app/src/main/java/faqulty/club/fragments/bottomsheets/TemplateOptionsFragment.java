package faqulty.club.fragments.bottomsheets;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import faqulty.club.R;
import faqulty.club.fragments.FeedFragment;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 12/6/2016.
 */

public class TemplateOptionsFragment extends BottomSheetDialogFragment {
    private BottomSheetInterface bottomSheetInterface;
    private FeedFragment feedFragment;

    public static TemplateOptionsFragment newInstance(BottomSheetInterface bottomSheetInterface) {
        TemplateOptionsFragment feedOptionsFragment = new TemplateOptionsFragment();
        feedOptionsFragment.bottomSheetInterface = bottomSheetInterface;
        return feedOptionsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_save_share, container, false);
        RelativeLayout rlEmail = (RelativeLayout) v.findViewById(R.id.rl_email);
        RelativeLayout bookmark = (RelativeLayout) v.findViewById(R.id.rl_bookmark);
        RelativeLayout download = (RelativeLayout) v.findViewById(R.id.rl_download);
        RelativeLayout share = (RelativeLayout) v.findViewById(R.id.rl_share);
        rlEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.EMAIL);
                dismiss();
            }
        });
        bookmark.setVisibility(View.GONE);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.DOWNLOAD_FEED);
                dismiss();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.SHARE_FEED);
                dismiss();
            }
        });

        return v;
    }

    public interface BottomSheetInterface {
        public void onActionClick(int actType);
    }

}
