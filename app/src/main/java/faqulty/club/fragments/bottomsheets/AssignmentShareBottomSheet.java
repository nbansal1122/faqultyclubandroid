package faqulty.club.fragments.bottomsheets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import faqulty.club.R;
import faqulty.club.fragments.FeedFragment;
import faqulty.club.models.feed.Datum;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 28/01/17.
 */

public class AssignmentShareBottomSheet extends BottomSheetDialogFragment {
    private BottomSheetInterface bottomSheetInterface;
    private FeedFragment feedFragment;
    private Datum feedData;
    String mString;

    public Datum getFeedData() {
        return feedData;
    }

    public void setFeedData(Datum feedData) {
        this.feedData = feedData;
    }

    public BottomSheetInterface getBottomSheetInterface() {
        return bottomSheetInterface;
    }

    public void setBottomSheetInterface(BottomSheetInterface bottomSheetInterface) {
        this.bottomSheetInterface = bottomSheetInterface;
    }


    public static AssignmentShareBottomSheet newInstance(BottomSheetInterface bottomSheetInterface) {
        AssignmentShareBottomSheet saveShareFragment = new AssignmentShareBottomSheet();
        saveShareFragment.bottomSheetInterface = bottomSheetInterface;
        return saveShareFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_save_share, container, false);
        RelativeLayout rlEmail = (RelativeLayout) v.findViewById(R.id.rl_email);
        RelativeLayout bookmark = (RelativeLayout) v.findViewById(R.id.rl_bookmark);
        RelativeLayout download = (RelativeLayout) v.findViewById(R.id.rl_download);
        RelativeLayout share = (RelativeLayout) v.findViewById(R.id.rl_share);
        rlEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.EMAIL);
                dismiss();
            }
        });
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.BOOKMARK);
                dismiss();
            }
        });
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.DOWNLOAD_FEED);
                dismiss();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetInterface.onActionClick(AppConstants.ACTION_TYPE.SHARE_FEED);
                dismiss();
            }
        });

        return v;
    }

    public interface BottomSheetInterface {
        public void onActionClick(int actType);
    }

}