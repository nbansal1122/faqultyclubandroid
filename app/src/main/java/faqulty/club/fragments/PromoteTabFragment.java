package faqulty.club.fragments;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.CreateLogoActivity;
import faqulty.club.activity.PamplatesActivity;
import faqulty.club.activity.WebsiteTemplateActivity;
import faqulty.club.models.WebTemplate;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;

/**
 * Created by saurabh on 16-09-2016.
 */
public class PromoteTabFragment extends AppBaseFragment implements CustomListAdapterInterface{
    List<WebTemplate> promoteList = new ArrayList<>();
    CustomListAdapter customListAdapter;

    String[] title = {"Website Template","Pamphlets","Logo/Watermark/Branding"};
    String[] subTitle = {"Create a classy profile to increase your visibility on the faqulty.club website",
            "Ready-made Templates for tutors to create A4 size printable flyers and handouts. Tutors can personalise it to bring out their excellence",
            "Create your branding on the assignments and invoices to make an impression with students and parents"};
    int[] images={R.mipmap.website_templete,R.mipmap.pamphlets,R.mipmap.logo_watermark_branding};
    @Override
    public void initViews() {
        setHasOptionsMenu(true);
        ListView lv_promote = (ListView) findView(R.id.lv_promote);
        customListAdapter = new CustomListAdapter(getActivity(),R.layout.row_website_template,promoteList,this);
        lv_promote.setAdapter(customListAdapter);
        setData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_promote, menu);
    }

    private void setData() {
        for(int x=0; x<3; x++){
            WebTemplate promote = new WebTemplate();
            promote.setTemplateTitle(title[x]);
            promote.setTemplateSubtitle(subTitle[x]);
            promote.setTemplateImage(images[x]);
            promoteList.add(promote);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_tab_promote;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
      Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
//            convertView.setTag(holder);
        } else {
//            holder = (Holder) convertView.getTag();
        }
        holder = new Holder(convertView);
        WebTemplate webpemphletList = promoteList.get(position);
        holder.tvTitle.setText(webpemphletList.getTemplateTitle());
        holder.tvSubtitle.setText(webpemphletList.getTemplateSubtitle());
        holder.ivTImage.setImageResource(webpemphletList.getTemplateImage());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {
                    case 0:
                        startNextActivity(WebsiteTemplateActivity.class);
                        return;
                    case 1:
                        startNextActivity(PamplatesActivity.class);
                        return;
                    case 2:
                        startNextActivity(CreateLogoActivity.class);
                        return;

                }
            }
        });
        return convertView;
    }

    class Holder {
        TextView tvTitle,tvSubtitle;
        ImageView ivTImage;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.template_title);
            tvSubtitle= (TextView) view.findViewById(R.id.subtitle_template);
            ivTImage= (ImageView) view.findViewById(R.id.template_image);
        }
    }
}

