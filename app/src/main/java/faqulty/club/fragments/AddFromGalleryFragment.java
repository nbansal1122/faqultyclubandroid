package faqulty.club.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.esafirm.imagepicker.features.ImagePicker;
import faqulty.club.R;
import faqulty.club.activity.NewContentActivity;

import simplifii.framework.requestmodels.SelectContent;

import java.io.File;
import java.util.ArrayList;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFromGalleryFragment extends BottomSheetDialogFragment {

    private MediaFragment imagePicker, audioPicker, videoPicker, pdfPicker;
    private GetMediaListener getMediaListener;
    private ImageView profilePic;
    private int imageSize = 5;
    private ArrayList<Object> arrayList = new ArrayList<>();
    private boolean aBoolean;
    private String imgpath;

    public static AddFromGalleryFragment getInstance(GetMediaListener getMediaListener) {
        AddFromGalleryFragment addFromGalleryFragment = new AddFromGalleryFragment();
        addFromGalleryFragment.getMediaListener = getMediaListener;
        return addFromGalleryFragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_add_from_gallery, null);
        dialog.setContentView(contentView);

        profilePic = (ImageView) dialog.findViewById(R.id.iv_cloud_upload);
        TextView btn_image_upld = (TextView) dialog.findViewById(R.id.btn_image_upload);
        final TextView btn_audio_upld = (TextView) dialog.findViewById(R.id.btn_audio_upload);
        TextView btn_upld_video = (TextView) dialog.findViewById(R.id.btn_video_upload);
        TextView btn_pdf_upload = (TextView) dialog.findViewById(R.id.btn_pdf_upload);

        imagePicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(imagePicker, "Profile image").commit();

        btn_image_upld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle getBundleFromPostFeed = getArguments();
                boolean fromPostFeed = false;
                if (getBundleFromPostFeed != null) {
                    fromPostFeed = getBundleFromPostFeed.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                }
                Bundle bundle = new Bundle();
                if (!fromPostFeed) {
                    bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.select_image_for_feed));
                    bundle.putBoolean(AppConstants.BUNDLE_KEYS.POST_FEED, true);
                } else {
                    bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.uploadpromotional_content));
                    bundle.putBoolean(AppConstants.BUNDLE_KEYS.POST_FEED, false);
                }
                imagePicker.setImageListener(new MediaFragment.ImageListener() {
                    @Override
                    public void onGetBitMap(Bitmap bitmap, String path) {
                        onBitmap(bitmap, path);
                    }

                    @Override
                    public void onGetImageArray(ArrayList<Image> images) {
                        onImageArray(images);
                    }
                });
                BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(imagePicker);
                bottomSheetDialogFragment.setArguments(bundle);
                bottomSheetDialogFragment.show(getChildFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });

        audioPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(audioPicker, "Audio Player").commit();
        btn_audio_upld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPicker.getAudioFromPlayer(new MediaFragment.MediaListener() {
                    @Override
                    public void onGetUri(Uri uri, int MediaType, String path) {
                        SelectContent selectContent = new SelectContent();
                        selectContent.setFileType(AppConstants.FILE_TYPES.AUDIO);
                        selectContent.setFilePath(path);
                        selectContent.setMimetype("audio");
                        Bundle bundle = new Bundle();

                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, selectContent);

                        Bundle getBundleFromPostFeed = getArguments();
                        if (getBundleFromPostFeed != null) {
                            boolean fromPostFeed = getBundleFromPostFeed.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                            if (!fromPostFeed) {
                                if (getMediaListener != null) {
                                    getMediaListener.audioData(bundle);
                                }

                            }

                        } else {
                            Intent in1 = new Intent(getActivity(), NewContentActivity.class);
                            in1.putExtras(bundle);
                            startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.AUDIO);
                        }
                        Toast.makeText(getActivity(), "Audio selected", Toast.LENGTH_SHORT).show();
                        dismiss();
                        return;
                    }
                });
            }
        });
        videoPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(videoPicker, "Video Player").commit();
        btn_upld_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoPicker.getVideoFromGallary(new MediaFragment.MediaListener() {

                    @Override
                    public void onGetUri(Uri uri, int MediaType, String path) {
                        SelectContent selectContent = new SelectContent();
                        selectContent.setFileType(AppConstants.FILE_TYPES.VIDEO);
                        selectContent.setFilePath(path);
                        Bundle bundle = new Bundle();

                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, selectContent);

                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, selectContent);

                        Bundle getBundleFromPostFeed = getArguments();
                        if (getBundleFromPostFeed != null) {
                            boolean fromPostFeed = getBundleFromPostFeed.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                            if (!fromPostFeed) {
                                if (getMediaListener != null) {
                                    getMediaListener.videoData(bundle);
                                }
                            }
                        } else {
                            Intent in1 = new Intent(getActivity(), NewContentActivity.class);
                            in1.putExtras(bundle);
                            startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.VIDEO);
                        }
                        dismiss();
                        Toast.makeText(getActivity(), "Video Selected", Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                });
            }
        });
        pdfPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(pdfPicker, "PDF Files").commit();
        btn_pdf_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdfPicker.getPdfFromGallery(new MediaFragment.MediaListener() {
                    @Override
                    public void onGetUri(Uri uri, int MediaType, String path) {
                        SelectContent selectContent = new SelectContent();
                        selectContent.setFileType(AppConstants.FILE_TYPES.PDF);
                        selectContent.setFilePath(path);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, selectContent);

                        Bundle getBundleFromPostFeed = getArguments();
                        if (getBundleFromPostFeed != null) {
                            boolean fromPostFeed = getBundleFromPostFeed.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                            if (!fromPostFeed) {
                                if (getMediaListener != null) {
                                    getMediaListener.pdfData(bundle);
                                }

                            }
                        } else {
                            Intent in1 = new Intent(getActivity(), NewContentActivity.class);
                            in1.putExtras(bundle);
                            startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.PDF);
                        }
                        dismiss();
                        Toast.makeText(getActivity(), "pdf selected", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }


    private void onImageArray(ArrayList<Image> images) {
        if (images != null && images.size() > 0) {
            for (Image image : images) {
                Bitmap bMap = getBitmapFromFilePath(image.getFilePath());
                if (bMap != null) {
                    SelectContent selectContent = new SelectContent();
                    selectContent.setFileType(AppConstants.FILE_TYPES.IMAGE);
                    selectContent.setFilePath(image.getFilePath());
                    arrayList.add(selectContent);
                }
            }

            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, arrayList);
            Bundle getBundleFromPostFeed = getArguments();
            if (getBundleFromPostFeed != null) {
                boolean fromPostFeed = getBundleFromPostFeed.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
                if (!fromPostFeed) {
                    if (getMediaListener != null) {
                        getMediaListener.imageData(bundle);
                    }

                }
            } else {
                Intent in1 = new Intent(getActivity(), NewContentActivity.class);
                in1.putExtras(bundle);
                startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.IMAGE);
            }
            dismiss();

        }
    }

    private void onBitmap(Bitmap bitmap, String path) {
        if (bitmap != null) {
            try {
                File feed = Util.getFile(bitmap, "Feed");
                imgpath = feed.getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
            SelectContent selectContent = new SelectContent();
            selectContent.setFileType(AppConstants.FILE_TYPES.IMAGE);
            selectContent.setFilePath(imgpath);
            arrayList.add(selectContent);

            addToBundel();

        }
    }

    private void addToBundel() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, arrayList);
        Bundle getBundleFromPostFeed = getArguments();
        if (getBundleFromPostFeed != null) {
            boolean fromPostFeed = getBundleFromPostFeed.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
            if (!fromPostFeed) {
                if (getMediaListener != null) {
                    getMediaListener.imageData(bundle);
                }

            }
        } else {

            Intent in1 = new Intent(getActivity(), NewContentActivity.class);
            in1.putExtras(bundle);
            startActivityForResult(in1, AppConstants.FILE_REQUEST_CODE.IMAGE);
        }
        dismiss();
    }


    private Bitmap getBitmapFromFilePath(String filePath) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            return bitmap;
        } catch (Exception e) {
            Toast.makeText(getActivity(), R.string.insufficient_memory, Toast.LENGTH_SHORT);
        } catch (OutOfMemoryError e) {
            Toast.makeText(getActivity(), R.string.insufficient_memory, Toast.LENGTH_SHORT);
        }
        return null;
    }


    public void setImageSize(int i) {
        imageSize = i;
    }

    public interface GetMediaListener {
        public void imageData(Bundle bundle);

        public void videoData(Bundle bundle);

        public void audioData(Bundle bundle);

        public void pdfData(Bundle bundle);
    }

}
