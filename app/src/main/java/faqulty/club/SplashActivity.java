package faqulty.club;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import faqulty.club.R;

import faqulty.club.activity.HomeActivity;
import faqulty.club.activity.SocialLoginActivity;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;


public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreenWindow();
        setContentView(R.layout.activity_splash);
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = null;
                        if (Preferences.isUserLoggerIn()) {
                            UserDetailsApi user = UserDetailsApi.getInstance();
                            boolean isBasicProfileRequired = Preferences.getData(AppConstants.PREF_KEYS.BASIC_PROFILE_REQUIRED, false);
                            if (user != null && user.isActive() && !isBasicProfileRequired) {
                                intent = new Intent(SplashActivity.this, HomeActivity.class);
//                                Bundle bundle = getIntent().getExtras();
//                                if(bundle!=null){
//                                    intent.putExtras(bundle);
//                                }
                            } else if(user != null) {
                                intent = new Intent(SplashActivity.this, CompleteProfileActivity.class);
                            }
                        } else {
                            intent = new Intent(SplashActivity.this, SocialLoginActivity.class);
                        }
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }.start();
    }

    private void testParsing() {
        try {
            String json = Util.getStringFromAsset(this, AppConstants.ASSETS_RESOURCES.TUTOR_DATA);
            UserDetailsApi api = new Gson().fromJson(json, UserDetailsApi.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }
}
