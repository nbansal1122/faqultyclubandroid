package faqulty.club;

import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;

import faqulty.club.fragments.AwardRecognitionSegment;
import faqulty.club.fragments.FacultyFragment;
import faqulty.club.models.UserProfile.FacultyData;
import faqulty.club.models.UserProfile.TutorAwards;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class FacultyDetailsActivity extends BaseActivity {

    private static final int MAX_AWARDS = 10;
    private LinearLayout layout;
    private UserDetailsApi user;
    private List<FacultyFragment> facultyFragmentArrayList = new ArrayList<>();
    private LinearLayout facultyLayout;

    private LinearLayout awardsLayout;
    private List<AwardRecognitionSegment> tutorAwardsFragmentList = new ArrayList<>();
    private List<TutorAwards> awardsList = new ArrayList<>();
    private static final int disableColorId = R.color.color_EditTextGray;
    private static final int enabledColorId = R.color.orange;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_details);
        initToolBar(getString(R.string.faculty_details));
        getHomeIcon(R.mipmap.arrows);
        facultyLayout = (LinearLayout) findViewById(R.id.ll_faculty);
        awardsLayout = (LinearLayout) findViewById(R.id.ll_awards);
        user = UserDetailsApi.getInstance();
        List<FacultyData> faculties = user.getFaculties();
        layout = (LinearLayout) findViewById(R.id.layout_degree);
        if (faculties != null && faculties.size() > 0) {
            for (FacultyData t : faculties) {
                addRow(t);
            }
        } else {
            addRow(null);
        }

        setAwards();
        setOnClickListener(R.id.btn_save_changes, R.id.tv_add_faculty, R.id.tv_add_awards);
    }

    private void setAwards() {
        if (user.getTutorAwards() != null && user.getTutorAwards().size() > 0) {
            for (TutorAwards awards : user.getTutorAwards()) {
                addAwardFragment(awards);
            }
        } else {
            addAwardFragment(null);
        }
    }

    private void addAwardFragment(TutorAwards awards) {
        if (tutorAwardsFragmentList.size() < MAX_AWARDS) {
            if (tutorAwardsFragmentList.size() == MAX_AWARDS - 1) {
                setMaxAwardsRestriction(true);
            }
            AwardRecognitionSegment f = AwardRecognitionSegment.getInstance(awards, tutorAwardsFragmentList.size(), awardsDeleteListener);
            tutorAwardsFragmentList.add(f);
            getSupportFragmentManager().beginTransaction().add(R.id.ll_awards, f).commitAllowingStateLoss();
        } else {

        }

    }

    private AwardRecognitionSegment.AwardDeleteInterface awardsDeleteListener = new AwardRecognitionSegment.AwardDeleteInterface() {
        @Override
        public void onAwardDelete(AwardRecognitionSegment fragment, TutorAwards schoolsTutoredApi) {
            if (awardsLayout.getChildCount() == 1) {
                fragment.onClear();
                return;
            }
            awardsLayout.removeViewAt(fragment.index);
            tutorAwardsFragmentList.remove(fragment.index);
            for (int i = 0; i < tutorAwardsFragmentList.size(); i++) {
                tutorAwardsFragmentList.get(i).index = i;
            }

            if (awardsLayout.getChildCount() == 0) {
                addAwardFragment(null);
            }
            setMaxAwardsRestriction(tutorAwardsFragmentList.size() >= MAX_AWARDS);
        }
    };

    private void setMaxAwardsRestriction(boolean isMax) {
        setMaxRestriction(R.id.tv_add_awards, isMax);
    }

    private void setMaxRestriction(int tvId, boolean isMax) {
        TextView tv = (TextView) findViewById(tvId);
        if (isMax) {
            tv.setTextColor(ContextCompat.getColor(this, disableColorId));
            tv.setOnClickListener(null);
        } else {
            tv.setTextColor(ContextCompat.getColor(this, enabledColorId));
            tv.setOnClickListener(this);
        }
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    private void addRow(FacultyData data) {
        FacultyFragment f = FacultyFragment.getInstance(data, degreeFragmentListener, facultyFragmentArrayList.size());
        facultyFragmentArrayList.add(f);
        getSupportFragmentManager().beginTransaction().add(R.id.ll_faculty, f).commitAllowingStateLoss();
    }

    private FacultyFragment.FacultyDeleteListener degreeFragmentListener = new FacultyFragment.FacultyDeleteListener() {
        @Override
        public void onItemAction(FacultyFragment segment) {
            facultyLayout.removeViewAt(segment.index);
            facultyFragmentArrayList.remove(segment.index);
            for (int i = 0; i < facultyFragmentArrayList.size(); i++) {
                facultyFragmentArrayList.get(i).index = i;
            }

            if (facultyLayout.getChildCount() == 0) {
                addRow(null);
            }
        }
    };

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_save_changes:
                if (checkValidationForFaculties()) {
                    updateFacultyDetails();
                    setResult(RESULT_OK);
                }
                break;
            case R.id.tv_add_faculty:
                addRow(null);
                break;
            case R.id.tv_add_awards:
                addAwardFragment(null);
                break;
        }
    }

    private void updateFacultyDetails() {
        JSONObject json = null;
        HttpParamObject httpParamObject = new HttpParamObject();
        long userId = Preferences.getData(AppConstants.PREF_KEYS.USER_ID, 111L);
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.FACULTY_URL, String.valueOf(userId)));
        try {
            json = getJsonObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setContentType("application/json");
        httpParamObject.setJson(json.toString());
        executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
    }

    private boolean checkValidationForFaculties() {
        for (FacultyFragment f : facultyFragmentArrayList) {
            if (!f.isValid()) {
                return false;
            }
        }
        return true;
    }

    private JSONObject getJsonObject() throws JSONException {

        JSONObject obj = new JSONObject();
        JSONArray facultiesArray = new JSONArray();
        JSONArray awardsArray = new JSONArray();
        for (FacultyFragment f : facultyFragmentArrayList) {
            JSONObject json = f.getFacultyJsonObject();
            if (json != null) {
                facultiesArray.put(json);
            }
        }
        obj.put("faculties", facultiesArray);
        for (AwardRecognitionSegment s : tutorAwardsFragmentList) {
            JSONObject awardObj = s.getJsonObject();
            if (awardObj != null)
                awardsArray.put(awardObj);
        }
        obj.put("tutorAwards", awardsArray);
        return obj;
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    setResult(RESULT_OK);
                    showToast(getString(R.string.fac_det_success_msg));
                    finish();
                } else {
                    showToast("CreateGroupData could not be updated");
                }
        }
    }
}
