package faqulty.club.enums;

/**
 * Created by admin on 2/28/17.
 */

public enum NotificationType {
    NONE(0),REVIEW(1),LIKE_PROFILE(2),BIRTHDAY(3),STUDENT_REGISTERED(4),FEED(5);
    int type;
    NotificationType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
    public static NotificationType findType(int type){
        for(NotificationType notificationType:values()){
            if(notificationType.getType()==type){
                return notificationType;
            }
        }
        return NONE;
    }
}
