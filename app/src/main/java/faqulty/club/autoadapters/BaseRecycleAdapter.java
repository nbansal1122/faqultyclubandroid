package faqulty.club.autoadapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import faqulty.club.R;
import faqulty.club.holder.FeedHolder;
import faqulty.club.holders.AudioHolder;
import faqulty.club.holders.BaseHolder;
import faqulty.club.holders.ClassModelHolder;
import faqulty.club.holders.ContentHolder;
import faqulty.club.holders.ContentViewHolder;
import faqulty.club.holders.FeedAudioHolder;
import faqulty.club.holders.FeedContentHolder;
import faqulty.club.holders.GroupHolder;
import faqulty.club.holders.GroupStudentHolder;
import faqulty.club.holders.HeaderHolder;
import faqulty.club.holders.PDFHolder;
import faqulty.club.holders.RecentAssignmentHolder;
import faqulty.club.holders.ShowMoreHolder;
import faqulty.club.holders.VideoHolder;

import simplifii.framework.requestmodels.BaseAdapterModel;

import java.util.List;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.TaskFragment;
import simplifii.framework.utility.AppConstants;


/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseRecycleAdapter<T extends BaseAdapterModel> extends RecyclerView.Adapter<BaseHolder> implements TaskFragment.AsyncTaskListener {
    protected List<T> list;
    protected Context context;
    protected LayoutInflater inflater;
    protected AlertDialog dialog;
    private RecyclerClickInterface clickInterface;

    public RecyclerClickInterface getClickInterface() {
        return clickInterface;
    }

    public void setClickInterface(RecyclerClickInterface clickInterface) {
        this.clickInterface = clickInterface;
    }

    public BaseRecycleAdapter(Context context, List<T> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case AppConstants.VIEW_TYPE.GET_VIDEO:
                itemView = inflater.inflate(R.layout.row_video_feed_post, parent, false);
                VideoHolder videoHolder = new VideoHolder(itemView);
                videoHolder.setClickListener(clickInterface);
                return videoHolder;
            case AppConstants.VIEW_TYPE.GET_IMAGE:
                itemView = inflater.inflate(R.layout.image_recycler_row, parent,false);
                ContentViewHolder holder = new ContentViewHolder(itemView);
                holder.setClickListener(clickInterface);
                return holder;
            case AppConstants.VIEW_TYPE.GET_PDF:
                itemView = inflater.inflate(R.layout.video_recycler_row, parent,false);
                PDFHolder recipesHomeHolder = new PDFHolder(itemView);
                recipesHomeHolder.setClickListener(clickInterface);
                return recipesHomeHolder;
            case AppConstants.VIEW_TYPE.PDF_FILE:
                itemView = inflater.inflate(R.layout.card_feed_pdf, null);
                PDFHolder pdfHolder = new PDFHolder(itemView);
                return pdfHolder;
            case AppConstants.VIEW_TYPE.FEED_AUDIO: {
                itemView = inflater.inflate(R.layout.card_feed_audio, parent,false);
                FeedAudioHolder collectionHomeHolder = new FeedAudioHolder(itemView);
                collectionHomeHolder.setClickListener(clickInterface);
                return collectionHomeHolder;
            }
            case AppConstants.VIEW_TYPE.GET_AUDIO:
                itemView = inflater.inflate(R.layout.row_video_view, null);
                AudioHolder collectionHomeHolder = new AudioHolder(itemView);
                return collectionHomeHolder;
            case AppConstants.VIEW_TYPE.GET_HEADER:
                itemView = inflater.inflate(R.layout.view_recycler_header, null);
                HeaderHolder headerHolder = new HeaderHolder(itemView);
                return headerHolder;
            case AppConstants.VIEW_TYPE.CONTENT:
                itemView = inflater.inflate(R.layout.video_recycler_row, null);
                ContentHolder contentHolder = new ContentHolder(itemView);
                contentHolder.setClickListener(clickInterface);
                return contentHolder;
            case AppConstants.VIEW_TYPE.FEED_CONTENT:
                itemView = inflater.inflate(R.layout.feed_recycler_row, null);
                FeedContentHolder feedContentHolder = new FeedContentHolder(itemView);
                feedContentHolder.setClickListener(clickInterface);
                return feedContentHolder;
            case AppConstants.VIEW_TYPE.FEED_DATA:
                View inflate = inflater.inflate(R.layout.row_feed, null);
                FeedHolder feedHolder = new FeedHolder(inflate);
                feedHolder.setClickListener(clickInterface);
                return feedHolder;
            case AppConstants.VIEW_TYPE.GROUP:
                GroupHolder grpHolder = new GroupHolder(inflater.inflate(R.layout.row_classes, null));
                grpHolder.setClickListener(clickInterface);
                return grpHolder;
            case AppConstants.VIEW_TYPE.GROUP_STUDENT:
                GroupStudentHolder grpStudentHolder = new GroupStudentHolder(inflater.inflate(R.layout.row_student, null));
                grpStudentHolder.setClickListener(clickInterface);
                return grpStudentHolder;
            case AppConstants.VIEW_TYPE.CLASSES:
                ClassModelHolder classModelHolder = new ClassModelHolder(inflater.inflate(R.layout.row_list_assignment, null));
                classModelHolder.setClickListener(clickInterface);
                return classModelHolder;
            case AppConstants.VIEW_TYPE.RECENT_ASSIGNMENT:
                RecentAssignmentHolder recentAssignmentHolder = new RecentAssignmentHolder(inflater.inflate(R.layout.row_list_assignment, null));
                recentAssignmentHolder.setClickListener(clickInterface);
                return recentAssignmentHolder;
            case AppConstants.VIEW_TYPE.SHOW_MORE:
                ShowMoreHolder showMoreHolder=new ShowMoreHolder(inflater.inflate(R.layout.row_view_more,parent,false));
                showMoreHolder.setClickListener(clickInterface);
                return showMoreHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        holder.onBind(position, this.list.get(position));
    }


    @Override
    public int getItemViewType(int position) {
        return list.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public void onPreExecute(int taskCode) {
        showProgressBar();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        hideProgressBar();
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {

    }

    public void showProgressBar() {
        if (dialog != null && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading");
            dialog.setCancelable(false);
            dialog.show();

        }
    }

    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void showToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(int stringId) {
        Toast.makeText(context, stringId, Toast.LENGTH_LONG).show();
    }

    public static interface RecyclerClickInterface {
        public void onItemClick(View itemView, int position, Object obj, int actionType);
    }

}

