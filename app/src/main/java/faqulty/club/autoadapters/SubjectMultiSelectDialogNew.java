package faqulty.club.autoadapters;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.UserProfile.SubjectObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by nbansal2211 on 20/10/16.
 */
public class SubjectMultiSelectDialogNew extends DialogFragment implements CustomListAdapterInterface {
    private List<SubjectObject> list = new ArrayList<>();
    private List<SubjectObject> copyList = new ArrayList<>();
    private CustomAutoAdapter<SubjectObject> adapter;
    private HashSet<String> selectedItems = new HashSet<>();
    private String titleString;
    private ListView listView;
    private EditText searchEt;
    private TextView title;
    private ItemSelector selector;

    public static SubjectMultiSelectDialogNew getInstance(String title, List<SubjectObject> list,
                                                          ItemSelector selector, List<SubjectObject> selectedItems) {
        SubjectMultiSelectDialogNew f = new SubjectMultiSelectDialogNew();
        f.list = list;
        f.copyList.addAll(list);
        f.titleString = title;
        f.selector = selector;
        if (selectedItems != null) {
            for (SubjectObject filter : selectedItems) {
                f.selectedItems.add(filter.getDisplayString());
            }
        }
        return f;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_multiselect, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        initViews(v);
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selector != null) {
                    selector.onItemsSelected(getSelectedItems());
                }
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    private List<SubjectObject> getSelectedItems() {
        List<SubjectObject> selctedItems = new ArrayList<>();
        for (SubjectObject f : copyList) {
            if (selectedItems.contains(f.getDisplayString())&&(f.getParent()!=0)) {
                selctedItems.add(f);
            }
        }
        return selctedItems;
    }

    private void initViews(View v) {
        listView = (ListView) v.findViewById(R.id.lv_select);
        adapter = new CustomAutoAdapter(getActivity(), R.layout.row_select_item_new, list, this);
        listView.setAdapter(adapter);
        title = (TextView) v.findViewById(R.id.tv_select_title);
        title.setText(titleString);
        searchEt = (EditText) v.findViewById(R.id.et_search);
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        modifyList();

    }

    private void modifyList() {
        Collections.sort(list, new Comparator<SubjectObject>() {
            @Override
            public int compare(SubjectObject o1, SubjectObject o2) {
                return o1.getOrder().compareTo(o2.getOrder());
            }
        });

        LinkedHashMap<Integer, SubjectObject> headerMap = new LinkedHashMap<>();
        LinkedHashMap<Integer, List<SubjectObject>> contentMap = new LinkedHashMap<>();
        List<SubjectObject> subjectObjects = new ArrayList<>();
        subjectObjects.addAll(list);
        list.clear();
        for (SubjectObject subjectObject : subjectObjects) {
            if (subjectObject.getParent() == 0) {
                headerMap.put(subjectObject.getId(), subjectObject);
            }
        }
        for (SubjectObject subjectObject : subjectObjects) {
            if (subjectObject.getParent() != 0) {
                List<SubjectObject> subjectObjectList = contentMap.get(subjectObject.getParent());
                if (subjectObjectList == null) {
                    subjectObjectList = new ArrayList<>();
                    subjectObjectList.add(subjectObject);
                    contentMap.put(subjectObject.getParent(), subjectObjectList);
                } else {
                    subjectObjectList.add(subjectObject);
                }
                headerMap.put(subjectObject.getId(), subjectObject);
            }
        }

        for (Map.Entry<Integer, SubjectObject> integerSubjectObjectEntry : headerMap.entrySet()) {
            List<SubjectObject> subjectObjectList = contentMap.get(integerSubjectObjectEntry.getKey());
            if (!CollectionUtils.isEmpty(subjectObjectList)) {
                list.add(integerSubjectObjectEntry.getValue());
                Collections.sort(subjectObjectList);
                list.addAll(subjectObjectList);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.onBindData(list.get(position));
        return convertView;
    }

    public class Holder {
        LinearLayout layHeader;
        RelativeLayout layBody;
        TextView tv, tvHeader;
        ImageView imageView;

        Holder(View view) {
            tv = (TextView) view.findViewById(R.id.tv_item);
            tvHeader = (TextView) view.findViewById(R.id.tv_header);
            imageView = (ImageView) view.findViewById(R.id.iv_select);
            layHeader = (LinearLayout) view.findViewById(R.id.lay_header);
            layBody = (RelativeLayout) view.findViewById(R.id.lay_body);
        }

        void onBindData(final SubjectObject subjectObject) {
            if (subjectObject.getParent() == 0) {
                layBody.setVisibility(View.GONE);
                layHeader.setVisibility(View.VISIBLE);
                tvHeader.setText(subjectObject.getDisplayString());
            } else {
                layBody.setVisibility(View.VISIBLE);
                layHeader.setVisibility(View.GONE);
                String item = subjectObject.getDisplayString();
                tv.setText(subjectObject.getDisplayString());
                if (selectedItems.contains(item)) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.GONE);
                }
            }
            layBody.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedItems.contains(subjectObject.getDisplayString())) {
                        selectedItems.remove(subjectObject.getDisplayString());
                    } else {
                        selectedItems.add(subjectObject.getDisplayString());
                    }
                    adapter.notifyDataSetChanged();

                }
            });
        }
    }

    public void setItems(List<SubjectObject> items) {
        this.list.clear();
        this.list.addAll(items);
        this.copyList.clear();
        this.copyList.addAll(items);
        modifyList();
    }

    public static interface ItemSelector {
        public void onItemsSelected(List<SubjectObject> selectedItems);
    }
}

