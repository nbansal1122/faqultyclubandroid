package faqulty.club.autoadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.models.Students;

import java.util.ArrayList;

/**
 * Created by Neeraj Yadav on 10/17/2016.
 */

public class MyFilterAdapter extends ArrayAdapter {
    ArrayList<Students> studentsOrignal=null;
    ArrayList<Students> studentsFilter=null;
    OnListClickListener listener;
    int resource;
    Context context;
    private ItemFilter mFilter = new ItemFilter();

    public MyFilterAdapter(Context context, int resource, ArrayList<Students> students,ArrayList<Students> studentsFilter, OnListClickListener listener) {
        super(context, resource,studentsFilter);
        this.studentsOrignal = students;
        this.studentsFilter = studentsFilter;
        this.resource = resource;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return studentsFilter.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final Students students = studentsFilter.get(position);
        holder.tvName.setText(students.getuName());
        if (students.isSelect()){
            holder.cb_select.setChecked(true);
        }else {
            holder.cb_select.setChecked(false);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (students.isSelect()){
                    holder.cb_select.setChecked(false);
                    students.setSelect(false);
                }else {
                    holder.cb_select.setChecked(true);
                    students.setSelect(true);
                }
            }
        });
        return convertView;
    }



//    protected int getResourceColor(int colorId) {
//        return ContextCompat.getColor(context, colorId);
//    }

    class Holder {
        TextView tvName;
        CheckBox cb_select;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_name);
            cb_select = (CheckBox) view.findViewById(R.id.cb_new_class);
        }
    }

    public interface OnListClickListener {
        void onEdit(Students student);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();


            final ArrayList<Students> filterList = new ArrayList<>();
            for(Students student:studentsOrignal){
                String filterableString = student.getuName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    filterList.add(student);
                }
            }
            results.values = filterList;
            results.count = filterList.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
            ArrayList<Students> values = (ArrayList<Students>) results.values;
            studentsFilter.clear();
            studentsFilter.addAll(values);
            notifyDataSetChanged();
        }
    }
}


