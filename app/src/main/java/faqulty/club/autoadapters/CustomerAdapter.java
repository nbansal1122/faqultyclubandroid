package faqulty.club.autoadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import faqulty.club.models.BaseAutoFilter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomerAdapter<T extends BaseAutoFilter> extends ArrayAdapter {
    private LayoutInflater layoutInflater;
    List<T> mCustomers;

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((BaseAutoFilter) resultValue).getDisplayString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                List<T> suggestions = new ArrayList<T>();
                for (T customer : mCustomers) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.isMatchingConstraint(constraint.toString())) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<T> filteredList = (List<T>) results.values;
            List<T> customerList = new ArrayList<T>();
            if (results != null && results.count > 0) {
                clear();
                for (T c : filteredList) {
                    customerList.add(c);
                }
                Iterator<T> customerIterator = customerList.iterator();
                while (customerIterator.hasNext()) {
                    T data = customerIterator.next();
                    add(data);
                }
                notifyDataSetChanged();
            }
        }
    };

    public CustomerAdapter(Context context, int textViewResourceId, List<T> customers) {
        super(context, textViewResourceId, customers);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

}