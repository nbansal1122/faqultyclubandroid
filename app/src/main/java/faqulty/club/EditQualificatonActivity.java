package faqulty.club;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import faqulty.club.R;

import faqulty.club.fragments.AwardRecognitionSegment;
import faqulty.club.fragments.DegreeFragment;
import faqulty.club.models.UserProfile.TutorAwards;
import faqulty.club.models.UserProfile.TutorEducationDetail;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class EditQualificatonActivity extends BaseActivity {
    Button savechanges;
    private LinearLayout layout;
    private UserDetailsApi user;
    TextView addNewDegree;
    private List<DegreeFragment> listDegFragments = new ArrayList<>();
    private LinearLayout degreeLinearLayout;
    private List<Integer> experienceList;
    //    private AutoCompleteTextView teachingExp;
    private AutoCompleteTextView tvExperience;
    private ListPopupWindow lpw;
    private LinearLayout awardsLayout;
    private List<AwardRecognitionSegment> tutorAwardsFragmentList = new ArrayList<>();
    private List<TutorAwards> awardsList = new ArrayList<>();
    private JSONArray educationArray = new JSONArray();
    private static final int MAX_AWARDS = 10;
    private static final int MAX_DEGREES = 5;

    private static final int disableColorId = R.color.color_EditTextGray;
    private static final int enabledColorId = R.color.orange;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_qualification);

        initToolBar(getString(R.string.title_qualification));
        getHomeIcon(R.mipmap.arrows);

        degreeLinearLayout = (LinearLayout) findViewById(R.id.layout_degree);
        awardsLayout = (LinearLayout) findViewById(R.id.ll_awards);
//        teachingExp = (AutoCompleteTextView) findViewById(R.id.spin_teaching_experience);
        tvExperience = (AutoCompleteTextView) findViewById(R.id.spin_teaching_experience);
        user = UserDetailsApi.getInstance();
        if (user.getExperience() != null) {
            tvExperience.setText(String.valueOf(user.getExperience()));
        }

        List<TutorEducationDetail> tutorEducationDetail = (List<TutorEducationDetail>) user.getTutorEducationDetail();
        layout = (LinearLayout) findViewById(R.id.layout_degree);
        if (tutorEducationDetail != null && tutorEducationDetail.size() > 0) {
            for (TutorEducationDetail t : tutorEducationDetail) {
                addRow(t);
            }
        } else {
            addRow(null);
        }

        setAwards();
        setOnClickListener(R.id.btn_save_changes, R.id.tv_add_new_degree, R.id.tv_add_new_awards);
    }

    private void setTeachingExperienceData() {
        for (int i = 1; i < 50; i++) {
            experienceList.add(i);
        }
    }

    private void setActvText(int id, String s) {
        AutoCompleteTextView et = (AutoCompleteTextView) findViewById(id);
        if (et != null)
            et.setText(s);
    }

    private void setAwards() {
        if (user.getTutorAwards() != null && user.getTutorAwards().size() > 0) {
            for (TutorAwards awards : user.getTutorAwards()) {
                addAwardFragment(awards);
            }
        } else {
            addAwardFragment(null);
        }
    }

    private void addAwardFragment(TutorAwards awards) {
        if (tutorAwardsFragmentList.size() < MAX_AWARDS) {
            if (tutorAwardsFragmentList.size() == MAX_AWARDS - 1) {
                setMaxAwardsRestriction(true);
            }
            AwardRecognitionSegment f = AwardRecognitionSegment.getInstance(awards, tutorAwardsFragmentList.size(), awardsDeleteListener);
            tutorAwardsFragmentList.add(f);
            getSupportFragmentManager().beginTransaction().add(R.id.ll_awards, f).commitAllowingStateLoss();
        } else {

        }
    }

    private void setMaxAwardsRestriction(boolean isMax) {
        setMaxRestriction(R.id.tv_add_new_awards, isMax);
    }

    private void setMaxDegreeRestriction(boolean isMax) {
        setMaxRestriction(R.id.tv_add_new_degree, isMax);
    }

    private void setMaxRestriction(int tvId, boolean isMax) {
        TextView tv = (TextView) findViewById(tvId);
        if (isMax) {
            tv.setTextColor(ContextCompat.getColor(this, disableColorId));
            tv.setOnClickListener(null);
        } else {
            tv.setTextColor(ContextCompat.getColor(this, enabledColorId));
            tv.setOnClickListener(this);
        }
    }

    private AwardRecognitionSegment.AwardDeleteInterface awardsDeleteListener = new AwardRecognitionSegment.AwardDeleteInterface() {
        @Override
        public void onAwardDelete(AwardRecognitionSegment fragment, TutorAwards schoolsTutoredApi) {
            if (awardsLayout.getChildCount() == 1) {
                fragment.onClear();
                return;
            }
            awardsLayout.removeViewAt(fragment.index);
            tutorAwardsFragmentList.remove(fragment.index);
            for (int i = 0; i < tutorAwardsFragmentList.size(); i++) {
                tutorAwardsFragmentList.get(i).index = i;
            }

            if (awardsLayout.getChildCount() == 0) {
                addAwardFragment(null);
            }
            setMaxAwardsRestriction(tutorAwardsFragmentList.size() >= MAX_AWARDS);
        }
    };

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    private void addRow(TutorEducationDetail t) {
        if (listDegFragments.size() < MAX_DEGREES) {
            if (listDegFragments.size() == MAX_DEGREES - 1) {
                setMaxDegreeRestriction(true);
            }
            DegreeFragment f = DegreeFragment.getInstance(t, degreeFragmentListener, listDegFragments.size());
            listDegFragments.add(f);
            getSupportFragmentManager().beginTransaction().add(R.id.layout_degree, f).commitAllowingStateLoss();
        }
    }

    private DegreeFragment.DegreeFragmentListener degreeFragmentListener = new DegreeFragment.DegreeFragmentListener() {
        @Override
        public void onItemAction(DegreeFragment segment, boolean isSearchChanged, boolean isChecked) {
            if (!isSearchChanged) {
                degreeLinearLayout.removeViewAt(segment.index);
                listDegFragments.remove(segment.index);
                for (int i = 0; i < listDegFragments.size(); i++) {
                    listDegFragments.get(i).index = i;
                }

                if (degreeLinearLayout.getChildCount() == 0) {
                    addRow(null);
                }
                setMaxDegreeRestriction(listDegFragments.size() >= MAX_DEGREES);
            } else {
                for (DegreeFragment f : listDegFragments) {
                    f.changeSwitchCheck(false);
                }
                if (isChecked) {
                    segment.changeSwitchCheck(isChecked);
                } else {
                    for (DegreeFragment f : listDegFragments) {
                        f.changeSwitchCheck(true);
                        return;
                    }
                }
            }
        }
    };

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_save_changes:
                try {
                    if (isValid())
                        updateEducationDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_add_new_degree:
                addRow(null);
                break;
            case R.id.tv_add_new_awards:
                addAwardFragment(null);
                break;
            case R.id.spin_teaching_experience:
                lpw.show();
                break;
        }
    }

    private void updateEducationDetails() {
        JSONObject json = null;
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUserTypeUrl("qualification");
        //// TODO: 20-09-2016 remove static 111 id 
//        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_USER + "111?type=qualification");
        try {
            json = getJsonObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setContentType("application/json");
        httpParamObject.setJson(json.toString());
        executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
    }

    private boolean isValid() throws JSONException {
        for (DegreeFragment f : listDegFragments) {
            if (!f.isValid()) {
                return false;
            }
        }
        return true;
    }


    private JSONObject getJsonObject() throws JSONException {
        JSONObject obj = new JSONObject();
//        JSONArray educationArray = new JSONArray();
        JSONArray awardsArray = new JSONArray();
        for (DegreeFragment f : listDegFragments) {
            JSONObject json = f.getJsonObject();
            if (json != null) {
                educationArray.put(json);
            }
        }
        obj.put("tutorEducationDetail", educationArray);
        for (AwardRecognitionSegment s : tutorAwardsFragmentList) {
            JSONObject awardObj = s.getJsonObject();
            if (awardObj != null)
                awardsArray.put(awardObj);
        }
        obj.put("tutorAwards", awardsArray);
        if (!TextUtils.isEmpty(tvExperience.getText().toString()))
            obj.put("experience", Integer.parseInt(tvExperience.getText().toString()));
        else {
            obj.put("experience", 0);
        }
        return obj;
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    setResult(RESULT_OK);
                    showToast("Qualifications updated successfully");
                    finish();
                } else {
                    showToast("Qualifications could not be updated");
                }
        }
    }
}
