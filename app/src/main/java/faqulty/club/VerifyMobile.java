package faqulty.club;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;

import faqulty.club.R;

import faqulty.club.activity.SocialLoginActivity;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontButton;
import simplifii.framework.widgets.CustomFontRadio;

/**
 * Created by Dhillon on 29-09-2016.
 */

public class VerifyMobile extends BaseActivity {
    String userType, mobileno;
    CustomFontRadio tutor, tution_center;
    TextInputLayout mobile_no;
    CustomFontButton signup;
    private UserDetailsApi user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_mobile);
        setOnClickListener(R.id.btn_sign_up);
        user = UserDetailsApi.getInstance();

        initToolBar("Mobile Verification");
        getHomeIcon(R.mipmap.arrows);

        tutor = (CustomFontRadio) findViewById(R.id.radio_tutor);
        tution_center = (CustomFontRadio) findViewById(R.id.radio_tution_center);
        mobile_no = (TextInputLayout) findViewById(R.id.til_mobile_number);
        signup = (CustomFontButton) findViewById(R.id.sign_up_btn);

    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_sign_up:
                initData();
                if (isValid())
                    verifyMobile();
                break;
        }
    }

    private void verifyMobile() {
        HttpParamObject httpParamObject = new HttpParamObject();
//        httpParamObject.setUserTypeUrl("extra-info");
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER + Preferences.getData(AppConstants.PREF_KEYS.USER_ID, 0L)
                + "?type=extra-info");
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setContentType("application/json");
        httpParamObject.setJson(getJsonData().toString());
        executeTask(AppConstants.TASKCODES.VERIFY_PHONE, httpParamObject);
    }

    private JSONObject getJsonData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", mobileno);
            jsonObject.put("userType", userType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean isValid() {
        clearTilError(R.id.til_mobile_number);
        String mobile = getTilText(R.id.til_mobile_number);
        if (!TextUtils.isEmpty(mobile) && mobile.length() == 10) {
        } else {
            setTilError(R.id.til_mobile_number, R.string.error_invalid_mobile);
            return false;
        }
        return true;
    }

    private void initData() {
        mobileno = mobile_no.getEditText().getText().toString();
        if (tutor.isChecked())
            userType = "tutor";
        else if (tution_center.isChecked())
            userType = AppConstants.USER_TUTION_CENTER;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.VERIFY_PHONE:
                UserDetailsApi user = (UserDetailsApi) response;
                if (user != null) {
                    showToast("OTP has been sent to " + mobileno);
                    Preferences.saveData(AppConstants.PREF_KEYS.PHONE_NO, mobileno);
                    Intent i = new Intent(VerifyMobile.this, OtpActivity.class);
                    i.putExtra(AppConstants.BUNDLE_KEYS.SOCIALSIGNUP, true);
                    startActivityForResult(i, AppConstants.TASKCODES.VERIFY_PHONE);
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case AppConstants.TASKCODES.VERIFY_PHONE:
                Preferences.saveData(Preferences.LOGIN_KEY, true);
                startNextActivity(CompleteProfileActivity.class);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startNextActivity(SocialLoginActivity.class);
        finish();
    }
}
