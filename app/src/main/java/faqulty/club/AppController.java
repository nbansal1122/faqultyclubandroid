package faqulty.club;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.multidex.MultiDex;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import faqulty.club.R;
import com.facebook.FacebookSdk;
import faqulty.club.models.chat.ChatMessage;
import faqulty.club.models.chat.ChatText;
import faqulty.club.models.chat.MetaInfo;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by saurabh on 13-09-2016.
 */
public class AppController extends com.clevertap.android.sdk.Application {
    public static AppController instance;
    public static long chatUserId;

    public static AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        MultiDex.install(this);
        Preferences.initSharedPreferences(this);
        FacebookSdk.sdkInitialize(this);
        ActivityLifecycleCallback.register(this);
        ActiveAndroid.initialize(this);
//        initDB();
    }

    public boolean checkPermission(String permission) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public void initDB() {
        if (checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            ActiveAndroid.dispose();
            Configuration.Builder builder = new Configuration.Builder(this);
            builder.setDatabaseName("facultyClub.db");
// builder.setDatabaseName(BusinessUtil.getFolderPath() + "konverse2.db");
            builder.setDatabaseVersion(3);
            builder.addModelClass(ChatMessage.class);
            ActiveAndroid.initialize(builder.create());
        }
    }

    public static void logOUtAndOpenSplash(Activity activity) {
        Preferences.deleteAllData();
    }

    public static void showToast(String s) {
        Toast.makeText(instance, s, Toast.LENGTH_LONG).show();
    }

    public static void showNotification(ChatMessage chatMessage) {
        if (chatUserId != chatMessage.getFromUserID()) {
            String title = chatMessage.getFromUserName() + " message you";
            String content = "";
            String messageType = chatMessage.getMessageType();
            switch (messageType) {
                case AppConstants.META_TYPES.TEXT:
                    ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
                    if (chatText != null) {
                        content = chatText.getText();
                    }
                    break;
                case AppConstants.META_TYPES.FILE:
                    content = "Image";
                    break;
            }
            Intent intent = new Intent(instance, SplashActivity.class);
            sendNotification(intent, title, content, (int) chatUserId);
        }
    }

    public static void sendNotification(Intent intent, String title, String content, int id) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(instance, id,
                intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bm = BitmapFactory.decodeResource(instance.getResources(), R.mipmap.fc_icon);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new
                NotificationCompat.Builder(instance)
                .setSmallIcon(R.mipmap.fc_icon)
                .setContentTitle(title)
                .setLargeIcon(bm)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(content))
                .setContentText(content)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(content))
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) instance.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notificationBuilder.build());
    }
}
