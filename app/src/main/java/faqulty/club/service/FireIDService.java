
package faqulty.club.service;

import android.util.Log;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class FireIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String tkn = FirebaseInstanceId.getInstance().getToken();
        try {
            CleverTapAPI cleverTapAPI = CleverTapAPI.getInstance(this);
            cleverTapAPI.data.pushFcmRegistrationId(tkn,true);
        } catch (CleverTapMetaDataNotFoundException e) {
            e.printStackTrace();
        } catch (CleverTapPermissionsNotSatisfied cleverTapPermissionsNotSatisfied) {
            cleverTapPermissionsNotSatisfied.printStackTrace();
        }
        Log.d("Not","Token ["+tkn+"]");
        Preferences.saveData(AppConstants.PREF_KEYS.IS_UPDATE_TOKEN,true);
        Preferences.saveData(AppConstants.PREF_KEYS.FCM_TOKEN,tkn);
    }
}