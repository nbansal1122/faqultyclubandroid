package faqulty.club.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import faqulty.club.AppController;

/**
 * Created by nbansal2211 on 15/09/16.
 */
public class InternetConnectivityReceiver extends BroadcastReceiver {
    public static final String ACTION_CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    private InternetConnectListener listener;

    public InternetConnectivityReceiver() {
    }

    public InternetConnectivityReceiver(InternetConnectListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_CONNECTIVITY_CHANGE.equalsIgnoreCase(intent.getAction())) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            if (isConnected) {
                SocketService.startService(AppController.getInstance());
            } else {
                SocketService.stopService(AppController.getInstance());
            }


        }
    }

    public static interface InternetConnectListener {
        public void onInternetConnected();

        public void onInternetDisconnected();
    }
}
