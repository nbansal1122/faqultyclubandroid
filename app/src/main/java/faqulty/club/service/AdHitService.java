package faqulty.club.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;

import faqulty.club.models.BaseApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.OKHttpService;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class AdHitService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_AD_HIT = "com.talluk.app.service.action.adhit";

    // TODO: Rename parameters
    private static final String AD_ID = "com.talluk.app.service.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.talluk.app.service.extra.PARAM2";

    public AdHitService() {
        super("AdHitService");
    }

    private long timeGapMin = 2 * 60 * 1000;//2 Min

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startServiceToHitAd(Context context, ArrayList<Integer> feedIds) {
        Intent intent = new Intent(context, AdHitService.class);
        intent.setAction(ACTION_AD_HIT);
        Bundle b = new Bundle();
        b.putSerializable(AD_ID, feedIds);
        intent.putExtras(b);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_AD_HIT.equals(action)) {
                ArrayList<Integer> feedIds = (ArrayList<Integer>) intent.getExtras().getSerializable(AD_ID);
                JSONObject jsonObject = new JSONObject();
                JSONArray array = new JSONArray();
                for (Integer i : feedIds) {
                    if (needToHitAd(i)) {
                        array.put(i);
                    } else {
                        saveAdHit(i);
                    }
                }
                try {
                    if (array.length() > 0) {
                        jsonObject.put("feedIds", array);
                        handleActionFoo(jsonObject.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
//    POST http://localhost:4000/api/tutors/99/feed/views
//    {
//        "feedIds": [23,43]
//    }
    private void handleActionFoo(String postJsonString) {
        // TODO: Handle action Foo\\
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(Preferences.getTutorFeedUrl(AppConstants.PAGE_URL.FEED_VIEWS));
        obj.setJSONContentType();
        obj.setClassType(BaseApi.class);
        obj.setPostMethod();
        obj.setJson(postJsonString);
        OKHttpService service = new OKHttpService();
        try {
            BaseApi response = (BaseApi) service.getData(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveAdHit(int adId) {
        long currentTime = System.currentTimeMillis();
        Preferences.saveData(String.valueOf(adId), currentTime + timeGapMin);
    }

    private static boolean needToHitAd(int adId) {
        long currentTime = System.currentTimeMillis();
        long prevTime = Preferences.getData(String.valueOf(adId), currentTime);
        return currentTime >= prevTime;
    }

    private String getJsonForAdHit(int adId) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", adId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
