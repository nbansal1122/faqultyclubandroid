package faqulty.club.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;

import faqulty.club.models.ProfileComments;

import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class FacultiClubeServices extends IntentService {
    private static final String ACTION_DELETE = "com.faqultyclub.service.action.DELETE";
    private static final String ACTION_GET_REVIEWS = "com.faqultyclub.service.action.GET_REVIEWS";
    private static final String ACTION_LIKE = "like";

    public FacultiClubeServices() {
        super("FacultiClubeServices");
    }

    public static void hitDeleteApiOnBackground(Context context, ArrayList<HttpParamObject> httpParamObjects) {
        Intent intent = new Intent(context, FacultiClubeServices.class);
        intent.setAction(ACTION_DELETE);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, httpParamObjects);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    public static void getReviews(Context context) {
        Intent intent = new Intent(context, FacultiClubeServices.class);
        intent.setAction(ACTION_GET_REVIEWS);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DELETE.equals(action)) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    ArrayList<HttpParamObject> httpParamObjects = (ArrayList<HttpParamObject>) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                    if (httpParamObjects != null) {
                        handleDelete(httpParamObjects);
                    }
                }
            } else if (ACTION_GET_REVIEWS.equals(action)) {
                getReviews();
            }
        }
    }

    private void getReviews() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PROFILE_COMMENTS);
        httpParamObject.addParameter("reqtype", "user_review_info");
        httpParamObject.addParameter("tutid", "" + Preferences.getUserId());
        httpParamObject.setClassType(ProfileComments.class);
        hitService(httpParamObject, AppConstants.TASKCODES.GET_PROFILE_COMMENTS);
    }

    private void handleDelete(ArrayList<HttpParamObject> httpParamObjects) {
        for (HttpParamObject httpParamObject : httpParamObjects) {
            hitService(httpParamObject, AppConstants.TASKCODES.DELETE_GROUP);
        }
    }

    private void hitService(HttpParamObject httpParamObject, int taskCode) {
        Service service = ServiceFactory.getInstance(this, taskCode);
        try {
            Object data = service.getData(httpParamObject);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
