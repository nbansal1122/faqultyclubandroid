package faqulty.club.pojos;

/**
 * Created by robin on 10/18/16.
 */

public class ProfileStructure {

    private String title;
    private String content;
    private int profileStructureType;
    private boolean isCompleted;

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public int getProfileStructureType() {
        return profileStructureType;
    }

    public void setProfileStructureType(int profileStructureType) {
        this.profileStructureType = profileStructureType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ProfileStructure{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
