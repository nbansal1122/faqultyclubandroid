package faqulty.club.pojos;

import java.util.List;

/**
 * Created by robin on 10/18/16.
 */

public class ProfileStructureData {

    private List<ProfileStructure> profileStructureList;

    public List<ProfileStructure> getProfileStructureList() {
        return profileStructureList;
    }

    public void setProfileStructureList(List<ProfileStructure> profileStructureList) {
        this.profileStructureList = profileStructureList;
    }

    @Override
    public String toString() {
        return "ProfileStructureList{" +
                "profileStructureList=" + profileStructureList +
                '}';
    }
}
