package faqulty.club;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import faqulty.club.R;

import faqulty.club.models.ForgotPasswordModel;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by saurabh on 20-09-2016.
 */
public class ResetPasswordActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);

        initToolBar("Reset Password");
        getHomeIcon(R.mipmap.arrows);

        setOnClickListener(R.id.btn_update);
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_update:
                if (isValid()) {
                    HttpParamObject httpParamObject = new HttpParamObject();
                    httpParamObject.setUrl(AppConstants.PAGE_URL.RESET_PASSWORD);
                    JSONObject jsonObject = new JSONObject();
                    Bundle bundle = getIntent().getExtras();
                    try {
                        if (bundle != null)
                            jsonObject.put("phone", bundle.getString("mobno"));
                        jsonObject.put("password", getTilText(R.id.til_password));
                        jsonObject.put("confirmPassword", getTilText(R.id.til_confirmpassword));
                        jsonObject.put("token", getTilText(R.id.til_otp));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    httpParamObject.setClassType(ForgotPasswordModel.class);
                    httpParamObject.setPostMethod();
                    httpParamObject.setJson(jsonObject.toString());
                    httpParamObject.setContentType("application/json");
                    executeTask(AppConstants.TASKCODES.RESET_PASSWORD, httpParamObject);
                }
        }
    }

    private boolean isValid() {
        clearTilError(R.id.til_password, R.id.til_confirmpassword);
        String password = getTilText(R.id.til_password);
        String confirmpassword = getTilText(R.id.til_confirmpassword);

        if (!TextUtils.isEmpty(password) && password.length() >= 4) {
        } else {
            setTilError(R.id.til_password, R.string.error_password);
            return false;
        }

        if (!TextUtils.isEmpty(confirmpassword) && confirmpassword.length() >= 4) {
        } else {
            setTilError(R.id.til_confirmpassword, R.string.error_password);
            return false;
        }
        return true;

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.RESET_PASSWORD:
                ForgotPasswordModel forgotPasswordModel = (ForgotPasswordModel) response;
                if (forgotPasswordModel != null) {
                    showToast(forgotPasswordModel.getMsg());
                    startNextActivity(LoginActivity.class);
                    finish();
                } else {
                    showToast("Please try again");
                }
                break;
        }

    }
}
