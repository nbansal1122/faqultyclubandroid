package faqulty.club.holder.receive;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.holder.send.SendAssignmentMessageHolder;

/**
 * Created by admin on 2/22/17.
 */

public class ReceiveAssignmentMessageHolder extends SendAssignmentMessageHolder {
    private TextView tvUser;
    public ReceiveAssignmentMessageHolder(View itemView) {
        super(itemView);
        tvUser=findTv(R.id.tv_username);
    }
}
