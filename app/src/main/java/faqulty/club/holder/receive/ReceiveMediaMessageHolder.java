package faqulty.club.holder.receive;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.holder.send.SendMediaMessageHolder;

/**
 * Created by admin on 2/22/17.
 */

public class ReceiveMediaMessageHolder extends SendMediaMessageHolder {
    private TextView tvUserName;
    public ReceiveMediaMessageHolder(View itemView) {
        super(itemView);
        tvUserName=findTv(R.id.tv_username);
    }
}
