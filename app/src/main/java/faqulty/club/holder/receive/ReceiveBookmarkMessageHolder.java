package faqulty.club.holder.receive;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.holder.send.SendBookmarkMessageHolder;

/**
 * Created by admin on 2/22/17.
 */

public class ReceiveBookmarkMessageHolder extends SendBookmarkMessageHolder {
    private TextView tvUser;
    public ReceiveBookmarkMessageHolder(View itemView) {
        super(itemView);
        tvUser=findTv(R.id.tv_username);
    }
}
