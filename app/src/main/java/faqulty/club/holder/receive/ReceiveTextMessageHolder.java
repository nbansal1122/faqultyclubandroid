package faqulty.club.holder.receive;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.holder.send.SendTextMessageHolder;
import faqulty.club.models.chat.ChatMessage;

/**
 * Created by admin on 2/22/17.
 */

public class ReceiveTextMessageHolder extends SendTextMessageHolder {
    private TextView tvUserName;
    public ReceiveTextMessageHolder(View itemView) {
        super(itemView);
        tvUserName=findTv(R.id.tv_username);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ChatMessage chatMessage= (ChatMessage) obj;
        tvUserName.setText(chatMessage.getFromUserName());
    }
}
