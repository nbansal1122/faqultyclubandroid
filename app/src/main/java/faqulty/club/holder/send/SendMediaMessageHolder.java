package faqulty.club.holder.send;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.activity.AudioVideoViewer;
import faqulty.club.activity.ImageActivity;
import faqulty.club.activity.PDFViewer;
import faqulty.club.models.chat.ChatMessage;
import faqulty.club.models.chat.MetaInfo;
import com.squareup.picasso.Picasso;

import simplifii.framework.requestmodels.UploadImageData;
import simplifii.framework.utility.Util;

/**
 * Created by admin on 2/22/17.
 */

public class SendMediaMessageHolder extends SendTextMessageHolder {
    private TextView tvMessage,tvTimeStamp;
    private ImageView ivMain,ivPlayButton;
    public SendMediaMessageHolder(View itemView) {
        super(itemView);
        ivMain= (ImageView) findView(R.id.iv_media);
        ivPlayButton= (ImageView) findView(R.id.iv_play_button);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ChatMessage chatMessage= (ChatMessage) obj;
        final UploadImageData uploadImageData = MetaInfo.getFile(chatMessage.getMetaInfoString());
        setComanData(chatMessage, "");
        if(uploadImageData!=null){
            setMedia(uploadImageData);
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String completeUrl = Util.getImageUrlBasedOnMimeType(uploadImageData.getUri(), uploadImageData.getMimetype());
                String mimeType = uploadImageData.getMimetype();
                if (!TextUtils.isEmpty(mimeType)) {
                    if (mimeType.contains("pdf")) {
                        if (!TextUtils.isEmpty(completeUrl))
                            PDFViewer.startActivity(context, uploadImageData.getUri());
                    } else if (mimeType.contains("image")) {
                        if (!TextUtils.isEmpty(completeUrl))
                            ImageActivity.startActivity(context, completeUrl);
                    } else if (mimeType.contains("video")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(uploadImageData.getUri()), mimeType);
                    } else if (mimeType.contains("audio")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(uploadImageData.getUri()), mimeType);
                    }
                }
            }
        });

    }

    private void setMedia(UploadImageData uploadImageData) {
        String mimeType = uploadImageData.getMimetype();
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.contains("pdf")) {
                ivPlayButton.setImageResource(R.mipmap.pdf);
            } else if (mimeType.contains("image")) {
                ivPlayButton.setImageResource(0);
                setImage(uploadImageData.getUri());
            } else if (mimeType.contains("video")) {
                ivPlayButton.setImageResource(R.mipmap.play_icon);
            } else if (mimeType.contains("audio")) {
                ivPlayButton.setImageResource(R.mipmap.play_icon);
            }
        }
    }

    private void setImage(String uri) {
       if(!TextUtils.isEmpty(uri)) {
           Picasso.with(context).load(Util.getCompleteUrl(uri)).into(ivMain);
       }
    }
}
