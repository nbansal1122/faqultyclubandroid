package faqulty.club.holder.send;

import android.view.View;
import android.widget.TextView;

import faqulty.club.R;
import faqulty.club.holders.BaseHolder;
import faqulty.club.models.chat.ChatMessage;
import faqulty.club.models.chat.ChatText;
import faqulty.club.models.chat.MetaInfo;

/**
 * Created by admin on 2/22/17.
 */

public class SendTextMessageHolder extends BaseHolder {
    private TextView tvMessage,tvTimeStamp;
    public SendTextMessageHolder(View itemView) {
        super(itemView);
        tvMessage=findTv(R.id.tv_message);
        tvTimeStamp=findTv(R.id.tv_time);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ChatMessage chatMessage= (ChatMessage) obj;
        ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
        setComanData(chatMessage,chatText.getText());
    }

    protected void setComanData(ChatMessage chatMessage, String chatText) {
        if(chatText!=null){
            tvMessage.setText(chatText);
        }
        String timeString = chatMessage.getTimeString();
        tvTimeStamp.setText(timeString);
    }
}
