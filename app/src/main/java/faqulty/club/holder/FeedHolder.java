package faqulty.club.holder;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import faqulty.club.ChechLikesActivity;
import faqulty.club.R;
import faqulty.club.activity.AudioVideoViewer;
import faqulty.club.activity.ImageActivity;
import faqulty.club.activity.PDFViewer;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.holders.BaseHolder;
import faqulty.club.models.HashTag;
import faqulty.club.models.UserProfile.ClassObject;
import faqulty.club.models.UserProfile.SubjectObject;
import faqulty.club.models.feed.Datum;
import faqulty.club.models.feed.Tutor;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

/**
 * Created by Neeraj Yadav on 12/12/2016.
 */

public class FeedHolder extends BaseHolder {
    private final LinearLayout classContainer, subjectContainer, hashtagContainer;
    private final TextView tvLike;
    private final ImageView ivLike;
    ImageView ivProfilePic, ivCommentPiv, ivFeedOption, ivFeed;
    TextView tvName, tvTime, tvDesc, tvLikeComment, tvCmntName, tvCmntDesc;
    RelativeLayout rlComments, rl_share, comment, rlLike;
    private Datum feedData;
    private int feedHour;
    private int feedMinute;
    ChipView chipClass, chipSubject, chipHashTag;
    private RecyclerView horizontalRV;
    private ImageView ivActionContent, contentThumbnail, deleteIcon;
    private FrameLayout frameLayout;


    public FeedHolder(View itemView) {
        super(itemView);
        ivProfilePic = (ImageView) itemView.findViewById(R.id.iv_propic);
        ivFeed = (ImageView) itemView.findViewById(R.id.iv_feed);
        ivLike = (ImageView) itemView.findViewById(R.id.iv_like);
        ivCommentPiv = (ImageView) itemView.findViewById(R.id.iv_user_comment);
        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        tvTime = (TextView) itemView.findViewById(R.id.tv_time);
        tvDesc = (TextView) itemView.findViewById(R.id.tv_decsription);
        tvLikeComment = (TextView) itemView.findViewById(R.id.tv_like_comment);
        tvLike = (TextView) itemView.findViewById(R.id.tv_like);
        tvCmntName = (TextView) itemView.findViewById(R.id.tv_name_comment);
        tvCmntDesc = (TextView) itemView.findViewById(R.id.tv_comment_text);

        rlComments = (RelativeLayout) itemView.findViewById(R.id.rl_comment);
        comment = (RelativeLayout) itemView.findViewById(R.id.comment);
        rl_share = (RelativeLayout) itemView.findViewById(R.id.rl_share);
        rlLike = (RelativeLayout) itemView.findViewById(R.id.like);
        classContainer = (LinearLayout) itemView.findViewById(R.id.class_container);
        subjectContainer = (LinearLayout) itemView.findViewById(R.id.subject_container);
        hashtagContainer = (LinearLayout) itemView.findViewById(R.id.hash_tags_container);

        ivFeedOption = (ImageView) itemView.findViewById(R.id.iv_feed_option);
        horizontalRV = (RecyclerView) itemView.findViewById(R.id.hz_recycler_view);

        // For the frame
        ivActionContent = (ImageView) findView(R.id.iv_action_content);
        contentThumbnail = (ImageView) findView(R.id.iv_select_content);
        deleteIcon = (ImageView) findView(R.id.iv_delete_content);
        frameLayout = (FrameLayout) findView(R.id.frame_content);

//        chipClass = (ChipView) itemView.findViewById(R.id.chipview_class);
//        chipSubject = (ChipView) itemView.findViewById(R.id.chipview_subjects);
        chipHashTag = (ChipView) itemView.findViewById(R.id.chipview_hash_tags);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        feedData = (Datum) obj;
        ivFeedOption.setTag(feedData);
        if (feedData != null) {

//            setClassChipData(feedData);
//            setSubjectChip(feedData);
            setHashTagChip(feedData);

            String createdDate = feedData.getCreatedOn();
            tvTime.setText(Util.getRelativeTimeString(createdDate));
            tvDesc.setText(feedData.getDescription());
            Integer likesCount = feedData.getLikesCount();
            Integer commentsCount = feedData.getCommentsCount();
            Integer numViews = feedData.getNumViews();

            String likes = "", comments = "";
            if (likesCount > 0) {
                likes = likesCount + " Likes";
            }
            if (commentsCount > 0) {
                comments = commentsCount + " Comments";
            }
            StringBuilder builder = new StringBuilder();
            if (numViews == 0) {
                numViews = 1;
            }
            builder.append(numViews + " Views");
            if (likesCount > 0) {
                builder.append(" / ").append(likes);
            }
            if (commentsCount > 0) {
                builder.append(" / ").append(comments);
            }
            String fullText = builder.toString();
            Util.setClickableSpan(tvLikeComment, fullText, likes, comments, ContextCompat.getColor(context, R.color.color_primary), new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    // like clicked
                    ChechLikesActivity.startActivity(feedData.getId(), (Activity) context);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    // comment Clicked
                    onEventAction(AppConstants.ACTION_TYPE.COMMENT_BOOKMARK, feedData, feedData);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            });
        }

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.COMMENT_BOOKMARK, v.getTag(), feedData);
            }
        });

        ivFeedOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.EDIT_BOTTOMSHEET, v.getTag(), feedData);
            }
        });

        rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.SHARE_BOTTOMSHEET, v.getTag(), feedData);
            }
        });

        rlLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventAction(AppConstants.ACTION_TYPE.LIKE_EVENT, v.getTag(), feedData);
            }
        });
        setRecyclerView(feedData.getContentList());
        setTutorData(feedData.getTutor());
        if (feedData.isHasLiked()) {
            tvLike.setText("Like");
            tvLike.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
            ivLike.setImageResource(R.mipmap.liked_thumb);
        } else {
            tvLike.setText("Like");
            ivLike.setImageResource(R.mipmap.like_thumb);
            tvLike.setTextColor(ContextCompat.getColor(context, R.color.et_gray));
        }
    }

    private void setHashTagChip(Datum feedData) {
        List<HashTag> hashTags = feedData.gethashtagsList();
        if (!CollectionUtils.isEmpty(hashTags)) {
            hashtagContainer.setVisibility(View.VISIBLE);
            List<Chip> chipList = new ArrayList<>();
            chipList.addAll(hashTags);
            chipHashTag.setChipLayoutRes(R.layout.row_chip_small);
            chipHashTag.setChipList(chipList);

        } else {
            hashtagContainer.setVisibility(View.GONE);
        }
    }

    private void setSubjectChip(Datum feedData) {
        List<SubjectObject> subjects = feedData.getSubjects();
        if (!CollectionUtils.isEmpty(subjects)) {
            subjectContainer.setVisibility(View.VISIBLE);
            List<Chip> chipList = new ArrayList<>();
            chipList.addAll(subjects);
            chipSubject.setChipLayoutRes(R.layout.row_chip_small);
            chipSubject.setChipList(chipList);

        } else {
            subjectContainer.setVisibility(View.GONE);
        }
    }

    private void setClassChipData(Datum feedData) {

        List<ClassObject> classes = feedData.getClasses();
        if (!CollectionUtils.isEmpty(classes)) {
            classContainer.setVisibility(View.VISIBLE);
            List<Chip> chipList = new ArrayList<>();
            chipList.addAll(classes);
            chipClass.setChipLayoutRes(R.layout.row_chip_small);
            chipClass.setChipList(chipList);

        } else {
            classContainer.setVisibility(View.GONE);
        }
    }

    private void setTutorData(Tutor tutor) {
        if (!TextUtils.isEmpty(tutor.getImage())) {
            Picasso.with(context).load(Util.getCompleteUrl(tutor.getImage())).into(ivProfilePic);
        } else {
            ivProfilePic.setImageResource(R.drawable.accountselected);
        }
        if (!TextUtils.isEmpty(tutor.getName())) {
            tvName.setText(tutor.getName());
        }

    }

    private void setRecyclerView(List<SelectContent> list) {
        if (list != null && list.size() > 0) {
            LinearLayoutManager manager;
            BaseRecycleAdapter<SelectContent> adapter = new BaseRecycleAdapter<>(context, list);
            horizontalRV.setHasFixedSize(true);
            if (list.size() == 1) {
                frameLayout.setVisibility(View.VISIBLE);
                horizontalRV.setVisibility(View.GONE);
                setFrame(list.get(0));
//                list.get(0).setSingleItem(true);
////                horizontalRV.setMinimumWidth(context.getResources().getDisplayMetrics().widthPixels);
//                horizontalRV.setNestedScrollingEnabled(true);
//                manager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            } else {
                manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                horizontalRV.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
                horizontalRV.setLayoutManager(manager);
                horizontalRV.setAdapter(adapter);
            }

        } else {
            frameLayout.setVisibility(View.GONE);
            horizontalRV.setVisibility(View.GONE);
        }
    }

    private void setFrame(SelectContent content) {
        frameLayout.setVisibility(View.VISIBLE);
        setData(frameLayout, content);
    }


    public void setData(FrameLayout itemView, Object obj) {
        final SelectContent contentData = (SelectContent) obj;
        String url = contentData.getUri();
        if (!TextUtils.isEmpty(url)) {
            String mimetype = contentData.getMimetype();
            if (!TextUtils.isEmpty(mimetype)) {
                Picasso.with(context).load(Util.getImageUrlBasedOnMimeType(url, mimetype)).into(contentThumbnail);
            }
        }
        itemView.setTag(contentData);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectContent data = (SelectContent) v.getTag();
                String mimeType = data.getMimetype();
                if (!TextUtils.isEmpty(mimeType)) {
                    if (mimeType.contains("pdf")) {
                        PDFViewer.startActivity(context, data.getUri());
                    } else if (mimeType.contains("image")) {
                        if (!TextUtils.isEmpty(data.getUri()))
                            ImageActivity.startActivity(context, Util.getCompleteUrl(data.getUri()));
                    } else if (mimeType.contains("video")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(data.getUri()), mimeType);
                    } else if (mimeType.contains("audio")) {
                        AudioVideoViewer.startActivity(context, Util.getCompleteUrl(data.getUri()), mimeType);
                    }
                }
            }
        });
        if (contentData.isDeleteRequired()) {
            deleteIcon.setVisibility(View.VISIBLE);
            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventAction(AppConstants.ACTION_TYPE.DELETE_FEED_CONTENT_ITEM, contentData);
                }
            });
        } else {
            deleteIcon.setVisibility(View.GONE);
            deleteIcon.setOnClickListener(null);
        }

        placeActionIcon(contentData);
    }

    private void placeActionIcon(SelectContent content) {
        String mimeType = content.getMimetype();
        ivActionContent.setImageResource(0);
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.contains("pdf")) {
                ivActionContent.setImageResource(R.mipmap.pdf);
            } else if (mimeType.contains("image")) {
                ivActionContent.setImageResource(0);
            } else if (mimeType.contains("video")) {
                ivActionContent.setImageResource(R.mipmap.video_icon);
            } else if (mimeType.contains("audio")) {
                ivActionContent.setImageResource(R.mipmap.audio_icon);
            }
        }
    }

}
