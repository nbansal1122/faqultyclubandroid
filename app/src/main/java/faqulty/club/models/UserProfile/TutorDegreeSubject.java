package faqulty.club.models.UserProfile;

import android.text.TextUtils;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorDegreeSubject extends BaseAutoFilter {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("degreeId")
    @Expose
    private Integer degreeId;
    @SerializedName("degreeSubject")
    @Expose
    private String degreeSubject;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The degreeId
     */
    public Integer getDegreeId() {
        return degreeId;
    }

    /**
     * @param degreeId The degreeId
     */
    public void setDegreeId(Integer degreeId) {
        this.degreeId = degreeId;
    }

    /**
     * @return The degreeSubject
     */
    public String getDegreeSubject() {
        if(TextUtils.isEmpty(degreeSubject)){
            degreeSubject = subject;
        }
        return degreeSubject;
    }

    /**
     * @param degreeSubject The degreeSubject
     */
    public void setDegreeSubject(String degreeSubject) {
        this.degreeSubject = degreeSubject;
    }

    /**
     * @return The active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<TutorDegreeSubject> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<TutorDegreeSubject> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            TutorDegreeSubject tutorDegreeSubject = (TutorDegreeSubject) JsonUtil.parseJson(jsonObject.toString(), TutorDegreeSubject.class);
            list.add(tutorDegreeSubject);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return getDegreeSubject();
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.degreeSubject.toLowerCase().contains(text.toLowerCase());
    }
}
