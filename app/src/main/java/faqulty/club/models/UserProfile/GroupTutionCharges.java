package faqulty.club.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh on 16-09-2016.
 */
public class GroupTutionCharges {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("startingPrice")
    @Expose
    private Integer startPrice;
    @SerializedName("finalPrice")
    @Expose
    private Integer finalPrice;
    @SerializedName("batchSize")
    @Expose
    private int batchSize;

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Integer startPrice) {
        this.startPrice = startPrice;
    }

    public Integer getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Integer finalPrice) {
        this.finalPrice = finalPrice;
    }
}
