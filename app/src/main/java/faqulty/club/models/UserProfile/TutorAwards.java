package faqulty.club.models.UserProfile;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by robin on 9/23/16.
 */

public class TutorAwards implements Serializable {
    private int id;
    @SerializedName("awardsRecognition")
    private String awards;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }
}
