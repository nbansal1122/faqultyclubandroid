package faqulty.club.models.UserProfile;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by saurabh on 16-09-2016.
 */
public class SubjectObject extends BaseAutoFilter implements Comparable<SubjectObject>, Chip{
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("classId")
    @Expose
    private String tutorId;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("parent")
    @Expose
    private int parent;
    @SerializedName("active")
    @Expose
    private int active;
    private Integer order;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTutorId() {
        return tutorId;
    }

    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<SubjectObject> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<SubjectObject> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            SubjectObject cityApi = (SubjectObject) JsonUtil.parseJson(jsonObject.toString(), SubjectObject.class);
            list.add(cityApi);
        }
        return list;
    }

    public Integer getOrder() {
        if(order==null){
            return 1000;
        }
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public String getDisplayString() {
        return subject;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.subject.toLowerCase().contains(text.toLowerCase());
    }

    @Override
    public int compareTo(SubjectObject subjectObject) {
        return getDisplayString().compareTo(subjectObject.getDisplayString());
    }

    @Override
    public String getText() {
        return getDisplayString();
    }

    public static JSONArray getSelectedSubjects(List<SubjectObject> selectedSubjects) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (SubjectObject subjectObject : selectedSubjects) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", subjectObject.getId());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

}
