package faqulty.club.models.UserProfile;

import java.io.Serializable;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by nbansal2211 on 23/10/16.
 */
//"profileCompleteness":{"personalDetail":20,"location":10,"tuition":0,"qualification":0,"social":0,"content":0,"review":0}
public class ProfileCompleteness implements Serializable {
    private int personalDetail;
    private int location;
    private int tuition;
    private int qualification;
    private int social;
    private int content;
    private int review;

    public int getPersonalDetail() {
        return personalDetail;
    }

    public void setPersonalDetail(int personalDetail) {
        this.personalDetail = personalDetail;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public int getTuition() {
        return tuition;
    }

    public void setTuition(int tuition) {
        this.tuition = tuition;
    }

    public int getQualification() {
        return qualification;
    }

    public void setQualification(int qualification) {
        this.qualification = qualification;
    }

    public int getSocial() {
        return social;
    }

    public void setSocial(int social) {
        this.social = social;
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public int getCompletenessPercentage() {
        getReviewPercentage();
        location = getLocationPercentage();
        int totalCount = personalDetail + location + content + qualification + review + social + tuition;
        if (totalCount > 100) {
            totalCount = 100;
        }
        return totalCount;
    }

    public int getLocationPercentage() {
        if (UserDetailsApi.getInstance() != null) {
            if (UserDetailsApi.getInstance().getTutorLocation() != null && UserDetailsApi.getInstance().getTutorLocation().size() > 0) {
                return 10;
            }
        }
        return 0;
    }

    public int getReviewPercentage() {
        review = 0;
        int commentsCount = Preferences.getData(AppConstants.PREF_KEYS.COMMENTS_COUNT, 0);
        if (commentsCount > 0) {
            review = 5;
        }
        return review;
    }
}
