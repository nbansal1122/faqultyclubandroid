package faqulty.club.models.UserProfile;

import android.text.TextUtils;

import faqulty.club.models.BoardsData;
import faqulty.club.models.SchoolsTutoredApi;
import faqulty.club.models.TeachingModeApi;
import faqulty.club.models.content.ContentData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by saurabh on 15-09-2016.
 */
public class UserDetailsApi implements Serializable {

    @SerializedName("likeCount")
    @Expose
    private Integer likeCount;
    @SerializedName("coursesTaught")
    @Expose
    private List<CoursesTaught> coursesTaught = new ArrayList<CoursesTaught>();
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("userType")
    @Expose
    private int userType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("userDetails")
    @Expose
    private String userDetails;
    @SerializedName("tagline")
    @Expose
    private String tagline;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("experience")
    @Expose
    private int experience;
    @SerializedName("emailVerified")
    @Expose
    private Integer emailVerified;
    @SerializedName("phoneVerified")
    @Expose
    private Integer phoneVerified;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("lastLogged")
    @Expose
    private String lastLogged;
    @SerializedName("fblink")
    @Expose
    private String fblink;
    @SerializedName("lnlink")
    @Expose
    private String lnlink;
    @SerializedName("twitterlink")
    @Expose
    private String twitterlink;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("registrationPhase")
    @Expose
    private String registrationPhase;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("emailsent")
    @Expose
    private Integer emailsent;
    @SerializedName("tutorAwards")
    @Expose
    private List<TutorAwards> tutorAwards = new ArrayList<TutorAwards>();
    @SerializedName("tutorLocation")
    @Expose
    private List<TutorLocation> tutorLocation = new ArrayList<TutorLocation>();
    @SerializedName("tutorTeachingLanguage")
    @Expose
    private List<TutorTeachingLanguage> tutorTeachingLanguage = new ArrayList<TutorTeachingLanguage>();
    @SerializedName("tutorTeachingMode")
    @Expose
    private List<TeachingModeApi> tutorTeachingMode = new ArrayList<TeachingModeApi>();
    @SerializedName("tutorBoard")
    @Expose
    private List<BoardsData> tutorBoard = new ArrayList<BoardsData>();
    @SerializedName("tutorCourseDetail")
    @Expose
    private List<TutorCourseDetail> tutorCourseDetail = new ArrayList<TutorCourseDetail>();
    @SerializedName("tutorSchool")
    @Expose
    private List<SchoolsTutoredApi> schoolsTutored = new ArrayList<SchoolsTutoredApi>();
    @SerializedName("contentUpload")
    @Expose
    private List<ContentData> contentUpload = new ArrayList<>();

    public List<ContentData> getContentUpload() {
        return contentUpload;
    }

    public void setContentUpload(List<ContentData> contentUpload) {
        this.contentUpload = contentUpload;
    }

    private ProfileCompleteness profileCompleteness;

    public void setId(long id) {
        this.id = id;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public ProfileCompleteness getProfileCompleteness() {
        return profileCompleteness;
    }

    public void setProfileCompleteness(ProfileCompleteness profileCompleteness) {
        this.profileCompleteness = profileCompleteness;
    }

    public int getProfilePercentage() {
        if (profileCompleteness != null) {
            return profileCompleteness.getCompletenessPercentage();
        } else {
            return 0;
        }
    }

    public List<SchoolsTutoredApi> getSchoolsTutored() {
        return schoolsTutored;
    }


    public void setSchoolsTutored(List<SchoolsTutoredApi> schoolsTutored) {
        this.schoolsTutored = schoolsTutored;
    }

    public boolean isActive() {
        return active == 1;
    }

    public boolean isTutor() {
        return userType != 3;
    }

    private WeekendTutionTime weekendTutionTime;

    private WeekdayTutionTime weekdayTutionTime;

    private GroupTutionCharges groupTutionCharge;

    private OneOnOneTutionCharges oneOnOneTutionCharge;

    private String facultyProfileURI;

    public String getFacultyProfileURI() {
        return facultyProfileURI;
    }

    public void setFacultyProfileURI(String facultyProfileURI) {
        this.facultyProfileURI = facultyProfileURI;
    }

    @SerializedName("tutorEducationDetail")
    @Expose
    private List<TutorEducationDetail> tutorEducationDetail = new ArrayList<TutorEducationDetail>();
    @SerializedName("faculties")
    @Expose
    private List<FacultyData> faculties = new ArrayList<FacultyData>();

    public List<FacultyData> getFaculties() {
        return faculties;
    }

    public void setFaculties(List<FacultyData> faculties) {
        this.faculties = faculties;
    }

    /**
     * @return The likeCount
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * @param likeCount The likeCount
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * @return The coursesTaught
     */
    public List<CoursesTaught> getCoursesTaught() {
        return coursesTaught;
    }

    /**
     * @param coursesTaught The coursesTaught
     */
    public void setCoursesTaught(List<CoursesTaught> coursesTaught) {
        this.coursesTaught = coursesTaught;
    }

    /**
     * @return The id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The userType
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * @param userType The userType
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The userDetails
     */
    public String getUserDetails() {
        return userDetails;
    }

    /**
     * @param userDetails The userDetails
     */
    public void setUserDetails(String userDetails) {
        this.userDetails = userDetails;
    }

    /**
     * @return The tagline
     */
    public String getTagline() {
        return tagline;
    }

    /**
     * @param tagline The tagline
     */
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The experience
     */
    public Integer getExperience() {
        return experience;
    }

    /**
     * @param experience The experience
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * @return The emailVerified
     */
    public Integer getEmailVerified() {
        return emailVerified;
    }

    /**
     * @param emailVerified The emailVerified
     */
    public void setEmailVerified(Integer emailVerified) {
        this.emailVerified = emailVerified;
    }

    /**
     * @return The phoneVerified
     */
    public Integer getPhoneVerified() {
        return phoneVerified;
    }

    /**
     * @param phoneVerified The phoneVerified
     */
    public void setPhoneVerified(Integer phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return The lastLogged
     */
    public String getLastLogged() {
        return lastLogged;
    }

    /**
     * @param lastLogged The lastLogged
     */
    public void setLastLogged(String lastLogged) {
        this.lastLogged = lastLogged;
    }

    /**
     * @return The fblink
     */
    public String getFblink() {
        return fblink;
    }

    /**
     * @param fblink The fblink
     */
    public void setFblink(String fblink) {
        this.fblink = fblink;
    }

    /**
     * @return The lnlink
     */
    public String getLnlink() {
        return lnlink;
    }

    /**
     * @param lnlink The lnlink
     */
    public void setLnlink(String lnlink) {
        this.lnlink = lnlink;
    }

    /**
     * @return The twitterlink
     */
    public String getTwitterlink() {
        return twitterlink;
    }

    /**
     * @param twitterlink The twitterlink
     */
    public void setTwitterlink(String twitterlink) {
        this.twitterlink = twitterlink;
    }

    /**
     * @return The active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return The registrationPhase
     */
    public String getRegistrationPhase() {
        return registrationPhase;
    }

    /**
     * @param registrationPhase The registrationPhase
     */
    public void setRegistrationPhase(String registrationPhase) {
        this.registrationPhase = registrationPhase;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The emailsent
     */
    public Integer getEmailsent() {
        return emailsent;
    }

    /**
     * @param emailsent The emailsent
     */
    public void setEmailsent(Integer emailsent) {
        this.emailsent = emailsent;
    }

    /**
     * @return The tutorAwards
     */
    public List<TutorAwards> getTutorAwards() {
        return tutorAwards;
    }

    /**
     * @param tutorAwards The tutorAwards
     */
    public void setTutorAwards(List<TutorAwards> tutorAwards) {
        this.tutorAwards = tutorAwards;
    }

    /**
     * @return The tutorLocation
     */
    public List<TutorLocation> getTutorLocation() {
        TutorLocation location = new TutorLocation();
        location.setLocality(this.location);
        location.setCity(city);
        location.setPrimary(1);

        List<TutorLocation> locationList = new ArrayList<>();
        locationList.add(location);

        if (tutorLocation != null && tutorLocation.size() > 0) {
            locationList.addAll(tutorLocation);
        }
        return locationList;
    }

    /**
     * @param tutorLocation The tutorLocation
     */
    public void setTutorLocation(List<TutorLocation> tutorLocation) {
        this.tutorLocation = tutorLocation;
    }

    /**
     * @return The tutorTeachingLanguage
     */
    public List<TutorTeachingLanguage> getTutorTeachingLanguage() {
        return tutorTeachingLanguage;
    }

    /**
     * @param tutorTeachingLanguage The tutorTeachingLanguage
     */
    public void setTutorTeachingLanguage(List<TutorTeachingLanguage> tutorTeachingLanguage) {
        this.tutorTeachingLanguage = tutorTeachingLanguage;
    }

    /**
     * @return The tutorTeachingMode
     */
    public List<TeachingModeApi> getTutorTeachingMode() {
        return tutorTeachingMode;
    }

    /**
     * @param tutorTeachingMode The tutorTeachingMode
     */
    public void setTutorTeachingMode(List<TeachingModeApi> tutorTeachingMode) {
        this.tutorTeachingMode = tutorTeachingMode;
    }

    /**
     * @return The tutorBoard
     */
    public List<BoardsData> getTutorBoard() {
        return tutorBoard;
    }


    /**
     * @param tutorBoard The tutorBoard
     */
    public void setTutorBoard(List<BoardsData> tutorBoard) {
        this.tutorBoard = tutorBoard;
    }

    /**
     * @return The tutorCourseDetail
     */
    public List<TutorCourseDetail> getTutorCourseDetail() {
        return tutorCourseDetail;
    }

    /**
     * @param tutorCourseDetail The tutorCourseDetail
     */
    public void setTutorCourseDetail(List<TutorCourseDetail> tutorCourseDetail) {
        this.tutorCourseDetail = tutorCourseDetail;
    }


    /**
     * @return The tutorEducationDetail
     */
    public List<TutorEducationDetail> getTutorEducationDetail() {
        return tutorEducationDetail;
    }

    /**
     * @param tutorEducationDetail The tutorEducationDetail
     */
    public void setTutorEducationDetail(List<TutorEducationDetail> tutorEducationDetail) {
        this.tutorEducationDetail = tutorEducationDetail;
    }

    public WeekendTutionTime getWeekendTutionTime() {
        return weekendTutionTime;
    }

    public void setWeekendTutionTime(WeekendTutionTime weekendTutionTime) {
        this.weekendTutionTime = weekendTutionTime;
    }

    public WeekdayTutionTime getWeekdayTutionTime() {
        return weekdayTutionTime;
    }

    public void setWeekdayTutionTime(WeekdayTutionTime weekdayTutionTime) {
        this.weekdayTutionTime = weekdayTutionTime;
    }

    public String getCompleteImageUrl() {
        return Util.getCompleteUrl(image);
    }

    public GroupTutionCharges getGroupTutionCharge() {
        return groupTutionCharge;
    }

    public void setGroupTutionCharge(GroupTutionCharges groupTutionCharge) {
        this.groupTutionCharge = groupTutionCharge;
    }

    public OneOnOneTutionCharges getOneOnOneTutionCharge() {
        return oneOnOneTutionCharge;
    }

    public void setOneOnOneTutionCharge(OneOnOneTutionCharges oneOnOneTutionCharge) {
        this.oneOnOneTutionCharge = oneOnOneTutionCharge;
    }

    public static UserDetailsApi getInstance() {
        UserDetailsApi user = null;
        String userJson = Preferences.getData(Preferences.KEY_USER, "");
        if (userJson != null) {
            user = parseJson(userJson);
        }
        return user;
    }

    public static UserDetailsApi parseJson(String json) {
        UserDetailsApi userDetail = (UserDetailsApi) JsonUtil.parseJson(json, UserDetailsApi.class);
        if (userDetail != null) {
            Preferences.saveData(Preferences.KEY_USER, json);
        }
        return userDetail;
    }

    public static JSONObject getBasicInfoJsonObject(UserDetailsApi user) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", user.getName());
        jsonObject.put("gender", user.getGender());
        jsonObject.put("email", user.getEmail());
        jsonObject.put("userDetails", user.getUserDetails());
        return jsonObject;
    }

    public String getBoardString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (tutorBoard != null) {
            for (BoardsData boardsData : tutorBoard) {
                stringBuilder.append(boardsData.getBoard()).append(", ");
            }
            if (stringBuilder.length() > 1) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
        }
        return stringBuilder.toString();
    }

    public String getClassesString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (coursesTaught != null) {
            for (CoursesTaught tutorCourseDetail : this.coursesTaught) {
                String classesString = tutorCourseDetail.getClassesString();
                if (!TextUtils.isEmpty(classesString)) {
                    stringBuilder.append(classesString).append(", ");
                }
            }
            if (stringBuilder.length() > 1) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
        }
        return stringBuilder.toString();
    }

    public String getSubjectsString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (coursesTaught != null) {
            for (CoursesTaught tutorCourseDetail : this.coursesTaught) {
                String classObj = tutorCourseDetail.getSubject();
                if (!TextUtils.isEmpty(classObj)) {
                    stringBuilder.append(classObj).append(", ");
                }
            }
            if (stringBuilder.length() > 1) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
        }
        return stringBuilder.toString();
    }

    public String getDegreesString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (tutorEducationDetail != null) {
            for (TutorEducationDetail tutorCourseDetail : this.tutorEducationDetail) {
                TutorDegree tutorDegree = tutorCourseDetail.getTutorDegree();
                if (tutorDegree != null) {
                    stringBuilder.append(tutorDegree.getDisplayString()).append(", ");
                }
            }
            if (stringBuilder.length() > 1) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
        }
        return stringBuilder.toString();
    }
}
