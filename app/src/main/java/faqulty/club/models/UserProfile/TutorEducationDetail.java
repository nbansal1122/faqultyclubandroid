package faqulty.club.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorEducationDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("prefered")
    @Expose
    private Integer prefered;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("degree")
    @Expose
    private Integer degree;
    @SerializedName("subject")
    @Expose
    private Integer subject;
    @SerializedName("awards")
    @Expose
    private String awards;
    @SerializedName("tutor_id")
    @Expose
    private Integer tutor_id;
    @SerializedName("tutorDegree")
    @Expose
    private TutorDegree tutorDegree;
    @SerializedName("tutorDegreeSubject")
    @Expose
    private TutorDegreeSubject tutorDegreeSubject;
    @SerializedName("tutorInstitute")
    @Expose
    private TutorInstitute tutorInstitute;
    @SerializedName("college")
    @Expose
    private String college;

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The prefered
     */
    public int getPrefered() {
        return prefered;
    }

    /**
     * @param prefered The prefered
     */
    public void setPrefered(Integer prefered) {
        this.prefered = prefered;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The degree
     */
    public Integer getDegree() {
        return degree;
    }

    /**
     * @param degree The degree
     */
    public void setDegree(Integer degree) {
        this.degree = degree;
    }

    /**
     * @return The subject
     */
    public Integer getSubject() {
        return subject;
    }

    /**
     * @param subject The subject
     */
    public void setSubject(Integer subject) {
        this.subject = subject;
    }

    /**
     * @return The awards
     */
    public String getAwards() {
        return awards;
    }

    /**
     * @param awards The awards
     */
    public void setAwards(String awards) {
        this.awards = awards;
    }

    /**
     * @return The tutor_id
     */
    public Integer getTutor_id() {
        return tutor_id;
    }

    /**
     * @param tutor_id The tutor_id
     */
    public void setTutor_id(Integer tutor_id) {
        this.tutor_id = tutor_id;
    }

    /**
     * @return The tutorDegree
     */
    public TutorDegree getTutorDegree() {
        return tutorDegree;
    }

    /**
     * @param tutorDegree The tutorDegree
     */
    public void setTutorDegree(TutorDegree tutorDegree) {
        this.tutorDegree = tutorDegree;
    }

    /**
     * @return The tutorDegreeSubject
     */
    public TutorDegreeSubject getTutorDegreeSubject() {
        return tutorDegreeSubject;
    }

    /**
     * @param tutorDegreeSubject The tutorDegreeSubject
     */
    public void setTutorDegreeSubject(TutorDegreeSubject tutorDegreeSubject) {
        this.tutorDegreeSubject = tutorDegreeSubject;
    }

    /**
     * @return The tutorInstitute
     */
    public TutorInstitute getTutorInstitute() {
        return tutorInstitute;
    }

    /**
     * @param tutorInstitute The tutorInstitute
     */
    public void setTutorInstitute(TutorInstitute tutorInstitute) {
        this.tutorInstitute = tutorInstitute;
    }

}
