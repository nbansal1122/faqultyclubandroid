package faqulty.club.models.UserProfile;

import faqulty.club.models.CityApi;
import faqulty.club.models.LocationApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorLocation implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("localityId")
    @Expose
    private Integer localityId;
    @SerializedName("primary")
    @Expose
    private Integer primary;
    @SerializedName("createdOn")
    @Expose
    private String createdOn = "";
    @SerializedName("city_id")
    @Expose
    private Integer city_id;
    @SerializedName("locality_id")
    @Expose
    private Integer locality_id;
    @SerializedName("tutor_id")
    @Expose
    private Integer tutor_id;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("locality")
    @Expose
    private String locality;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The cityId
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * @param cityId The cityId
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    /**
     * @return The localityId
     */
    public Integer getLocalityId() {
        return localityId;
    }

    /**
     * @param localityId The localityId
     */
    public void setLocalityId(Integer localityId) {
        this.localityId = localityId;
    }

    /**
     * @return The primary
     */
    public Integer getPrimary() {
        return primary;
    }

    /**
     * @param primary The primary
     */
    public void setPrimary(Integer primary) {
        this.primary = primary;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The city_id
     */
    public Integer getCity_id() {
        return city_id;
    }

    /**
     * @param city_id The city_id
     */
    public void setCity_id(Integer city_id) {
        this.city_id = city_id;
    }

    /**
     * @return The locality_id
     */
    public Integer getLocality_id() {
        return locality_id;
    }

    /**
     * @param locality_id The locality_id
     */
    public void setLocality_id(Integer locality_id) {
        this.locality_id = locality_id;
    }

    /**
     * @return The tutor_id
     */
    public Integer getTutor_id() {
        return tutor_id;
    }

    /**
     * @param tutor_id The tutor_id
     */
    public void setTutor_id(Integer tutor_id) {
        this.tutor_id = tutor_id;
    }

    /**
     * @return The city
     */
    public CityApi getCity() {
        CityApi api = new CityApi();
        api.setCity(city);
        return api;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The locality
     */
    public LocationApi getLocality() {
        LocationApi l = new LocationApi();
        l.setLocality(locality);
        return l;
    }

    /**
     * @param locality The locality
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }

    public boolean isPrimary() {
        return primary != null && primary == 1 ? true : false;
    }
}
