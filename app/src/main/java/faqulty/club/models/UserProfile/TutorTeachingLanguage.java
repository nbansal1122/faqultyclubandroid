package faqulty.club.models.UserProfile;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorTeachingLanguage extends BaseAutoFilter implements Chip,Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("language")
    @Expose
    private String language;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    public static List<TutorTeachingLanguage> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<TutorTeachingLanguage> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            TutorTeachingLanguage tutorTeachingLanguage = (TutorTeachingLanguage) JsonUtil.parseJson(jsonObject.toString(), TutorTeachingLanguage.class);
            list.add(tutorTeachingLanguage);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return language;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.language.toLowerCase().contains(text.toLowerCase());
    }

    @Override
    public String getText() {
        return language;
    }
}
