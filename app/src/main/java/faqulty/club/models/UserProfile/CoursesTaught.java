package faqulty.club.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CoursesTaught implements Serializable {

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("classes")
    @Expose
    private List<ClassObject> classes = new ArrayList<ClassObject>();

    /**
     * @return The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<ClassObject> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassObject> classes) {
        this.classes = classes;
    }

    /**
     * @return The classes
     */

    public String getClassesString() {
        if (classes != null && classes.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (ClassObject s : classes) {
                stringBuilder.append(s.getClassVal()).append(", ");
            }
            if (stringBuilder.length() > 2) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            return stringBuilder.toString();
        } else {
            return "";
        }
    }

}
