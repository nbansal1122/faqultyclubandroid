package faqulty.club.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 23/10/16.
 */

public class FacultyData {

    private String name;
    private String gender;
    private String image;

    @SerializedName("tutorEducationDetail")
    @Expose
    private List<TutorEducationDetail> tutorEducationDetail = new ArrayList<TutorEducationDetail>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<TutorEducationDetail> getTutorEducationDetail() {
        return tutorEducationDetail;
    }

    public void setTutorEducationDetail(List<TutorEducationDetail> tutorEducationDetail) {
        this.tutorEducationDetail = tutorEducationDetail;
    }
}
