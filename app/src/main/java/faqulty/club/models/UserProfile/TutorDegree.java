package faqulty.club.models.UserProfile;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorDegree extends BaseAutoFilter {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("degree")
    @Expose
    private String degree;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The degree
     */
    public String getDegree() {
        return degree;
    }

    /**
     * @param degree The degree
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     * @return The level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level The level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return The active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<TutorDegree> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<TutorDegree> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            TutorDegree tutorDegree = (TutorDegree) JsonUtil.parseJson(jsonObject.toString(), TutorDegree.class);
            list.add(tutorDegree);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return degree;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        if (this.degree != null) {
            return this.degree.toLowerCase().contains(text.toLowerCase());
        }
        return false;

    }
}
