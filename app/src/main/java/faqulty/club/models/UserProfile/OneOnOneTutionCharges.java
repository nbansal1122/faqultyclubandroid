package faqulty.club.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh on 16-09-2016.
 */
public class OneOnOneTutionCharges {

    @SerializedName("startingPrice")
    @Expose
    private Integer startPrice;
    @SerializedName("finalPrice")
    @Expose
    private Integer finalPrice;

    public Integer getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Integer startPrice) {
        this.startPrice = startPrice;
    }

    public Integer getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Integer finalPrice) {
        this.finalPrice = finalPrice;
    }
}
