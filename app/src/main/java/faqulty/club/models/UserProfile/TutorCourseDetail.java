package faqulty.club.models.UserProfile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by saurabh on 16-09-2016.
 */
public class TutorCourseDetail implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tutorId")
    @Expose
    private String tutorId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("class")
    @Expose
    private Integer classId;
    @SerializedName("subject")
    @Expose
    private Integer subjectId;

    private ClassObject classObj;

    private SubjectObject subjectObj;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTutorId() {
        return tutorId;
    }

    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public ClassObject getClassObj() {
        return classObj;
    }

    public void setClassObj(ClassObject classObj) {
        this.classObj = classObj;
    }

    public SubjectObject getSubjectObj() {
        return subjectObj;
    }

    public void setSubjectObj(SubjectObject subjectObj) {
        this.subjectObj = subjectObj;
    }
}
