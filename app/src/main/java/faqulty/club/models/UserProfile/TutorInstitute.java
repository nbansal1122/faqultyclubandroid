package faqulty.club.models.UserProfile;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorInstitute extends BaseAutoFilter {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("instituteName")
    @Expose
    private String instituteName;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The instituteName
     */
    public String getInstituteName() {
        return instituteName;
    }

    /**
     * @param instituteName The instituteName
     */
    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    /**
     * @return The level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level The level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<TutorInstitute> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<TutorInstitute> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            TutorInstitute tutorInstitute = (TutorInstitute) JsonUtil.parseJson(jsonObject.toString(), TutorInstitute.class);
            list.add(tutorInstitute);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return instituteName;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.instituteName.toLowerCase().contains(text.toLowerCase());
    }
}
