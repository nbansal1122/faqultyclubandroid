package faqulty.club.models.UserProfile;

import faqulty.club.models.TeachingModeApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TutorTeachingMode extends TeachingModeApi{

    @SerializedName("tutor_id")
    @Expose
    private Integer tutor_id;
    @SerializedName("teaching_mode")
    @Expose
    private String teaching_mode;

    /**
     * @return The tutor_id
     */
    public Integer getTutor_id() {
        return tutor_id;
    }

    /**
     * @param tutor_id The tutor_id
     */
    public void setTutor_id(Integer tutor_id) {
        this.tutor_id = tutor_id;
    }

    /**
     * @return The teaching_mode
     */
    public String getTeaching_mode() {
        return teaching_mode;
    }

    /**
     * @param teaching_mode The teaching_mode
     */
    public void setTeaching_mode(String teaching_mode) {
        this.teaching_mode = teaching_mode;
    }

}
