package faqulty.club.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh on 15-09-2016.
 */
public class TeachingLanguage {

    @SerializedName("tutor_id")
    @Expose
    private Integer tutor_id;
    @SerializedName("language")
    @Expose
    private Integer language;

    /**
     * @return The tutor_id
     */
    public Integer getTutor_id() {
        return tutor_id;
    }

    /**
     * @param tutor_id The tutor_id
     */
    public void setTutor_id(Integer tutor_id) {
        this.tutor_id = tutor_id;
    }

    /**
     * @return The language
     */
    public Integer getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    public void setLanguage(Integer language) {
        this.language = language;
    }

}
