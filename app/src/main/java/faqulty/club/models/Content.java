package faqulty.club.models;

import android.text.TextUtils;

/**
 * Created by my on 22-10-2016.
 */
public class Content {
    private int baseImage;
    private int playImage;
    private String url;
    String mimeType;

    public int getPlayImage() {
        return playImage;
    }

    public void setPlayImage(int playImage) {
        this.playImage = playImage;
    }

    public int getBaseImage() {
        return baseImage;
    }

    public void setBaseImage(int baseImage) {
        this.baseImage = baseImage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMimeType() {
        if(TextUtils.isEmpty(mimeType)){
            return "";
        }
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
