package faqulty.club.models;

/**
 * Created by saurabh on 24-09-2016.
 */

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

public class SchoolsTutoredApi extends BaseAutoFilter implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("schoolName")
    @Expose
    private String schoolName;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The schoolName
     */
    public String getSchoolName() {
        if (!TextUtils.isEmpty(schoolName)) {
            return schoolName;
        } else if (!TextUtils.isEmpty(school)) {
            return school;
        }
        return "";
    }

    /**
     * @param schoolName The schoolName
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<SchoolsTutoredApi> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<SchoolsTutoredApi> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            SchoolsTutoredApi schoolsTutoredApi = (SchoolsTutoredApi) JsonUtil.parseJson(jsonObject.toString(), SchoolsTutoredApi.class);
            list.add(schoolsTutoredApi);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return getSchoolName();
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.getSchoolName().toLowerCase().contains(text.toLowerCase());
    }
}