package faqulty.club.models;

import java.io.Serializable;

/**
 * Created by Neeraj Yadav on 10/15/2016.
 */

public class Students implements Serializable{
    int userImage;
    String uName;
    boolean isSelect;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getUserImage() {
        return userImage;
    }

    public void setUserImage(int userImage) {
        this.userImage = userImage;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }
}
