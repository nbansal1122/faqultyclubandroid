package faqulty.club.models.pemphlets;

import faqulty.club.models.assignment.subjects.SubjectData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/17.
 */

public class PamphletsSubject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pamphletId")
    @Expose
    private Integer pamphletId;
    @SerializedName("subjectId")
    @Expose
    private Integer subjectId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("subject")
    @Expose
    private SubjectData subject;
}
