package faqulty.club.models.pemphlets;

import faqulty.club.models.BaseApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AllPamphletsData extends BaseApi implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sample")
    @Expose
    private String sample;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isLogoRequired")
    @Expose
    private int isLogoRequired;
    @SerializedName("isClassesRequired")
    @Expose
    private int isClassesRequired;
    @SerializedName("isSubjectsRequired")
    @Expose
    private int isSubjectsRequired;
    @SerializedName("isBoardsRequired")
    @Expose
    private int isBoardsRequired;
    @SerializedName("isDegreesRequired")
    @Expose
    private int isDegreesRequired;
    @SerializedName("isAddressRequired")
    @Expose
    private int isAddressRequired;
    @SerializedName("isNameRequired")
    @Expose
    private int isNameRequired;
    @SerializedName("isBrandingTextRequired")
    @Expose
    private int isBrandingTextRequired;
    @SerializedName("isDescriptionRequired")
    @Expose
    private int isDescriptionRequired;
    @SerializedName("isContactRequired")
    @Expose
    private int isContactRequired;
    @SerializedName("isTitleRequired")
    @Expose
    private int isTitleRequired;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getIsLogoRequired() {
        return isLogoRequired;
    }

    public int getIsClassesRequired() {
        return isClassesRequired;
    }

    public int getIsSubjectsRequired() {
        return isSubjectsRequired;
    }

    public int getIsBoardsRequired() {
        return isBoardsRequired;
    }

    public int getIsDegreesRequired() {
        return isDegreesRequired;
    }

    public int getIsAddressRequired() {
        return isAddressRequired;
    }

    public int getIsNameRequired() {
        return isNameRequired;
    }

    public int getIsBrandingTextRequired() {
        return isBrandingTextRequired;
    }

    public int getIsDescriptionRequired() {
        return isDescriptionRequired;
    }

    public int getIsContactRequired() {
        return isContactRequired;
    }

    public int getIsTitleRequired() {
        return isTitleRequired;
    }
}
