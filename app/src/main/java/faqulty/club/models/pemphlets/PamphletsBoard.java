package faqulty.club.models.pemphlets;

import faqulty.club.models.BoardsData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/17.
 */

public class PamphletsBoard {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pamphletId")
    @Expose
    private Integer pamphletId;
    @SerializedName("boardId")
    @Expose
    private Integer boardId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("subject")
    @Expose
    private BoardsData board;

    public Integer getId() {
        return id;
    }

    public Integer getPamphletId() {
        return pamphletId;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public BoardsData getBoard() {
        return board;
    }
}
