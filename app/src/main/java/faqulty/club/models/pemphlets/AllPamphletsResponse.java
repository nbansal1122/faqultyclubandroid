package faqulty.club.models.pemphlets;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class AllPamphletsResponse implements Serializable{

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("data")
@Expose
private List<AllPamphletsData> data = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<AllPamphletsData> getData() {
return data;
}

public void setData(List<AllPamphletsData> data) {
this.data = data;
}

public void save(){
    String json = new Gson().toJson(this);
    Preferences.saveData(AppConstants.PREF_KEYS.KEY_PAMPHLETS,json);
}
public static AllPamphletsResponse getSavedInstance(){
    String data = Preferences.getData(AppConstants.PREF_KEYS.KEY_PAMPHLETS, "");
    if(!TextUtils.isEmpty(data)){
       return new Gson().fromJson(data,AllPamphletsResponse.class);
    }
    return null;
}
}