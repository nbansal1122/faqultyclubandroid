package faqulty.club.models.pemphlets;

import faqulty.club.models.BaseApi;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/17.
 */
public class UserPamphletsData extends BaseApi {
    private UserDetailsApi user;
    private String subjects;
    private String classes;
    private String boards;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("email")
    @Expose
    private  String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("watermark")
    @Expose
    private String watermark;
    @SerializedName("brandingText")
    @Expose
    private String brandingText;

    private String url;
    private String thumbUrl;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;

    public UserDetailsApi getUser() {
        return user;
    }

    public String getSubjects() {
        return subjects;
    }

    public String getClasses() {
        return classes;
    }

    public String getBoards() {
        return boards;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLogo() {
        return logo;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }

    public String getWatermark() {
        return watermark;
    }

    public String getBrandingText() {
        return brandingText;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }
}
