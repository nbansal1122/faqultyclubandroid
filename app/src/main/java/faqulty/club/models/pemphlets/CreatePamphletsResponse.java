package faqulty.club.models.pemphlets;

import faqulty.club.models.BaseApi;

/**
 * Created by admin on 2/17/17.
 */

public class CreatePamphletsResponse extends BaseApi {
    private boolean status;
    private PamphletsData data;

    public boolean isStatus() {
        return status;
    }

    public PamphletsData getData() {
        return data;
    }
}
