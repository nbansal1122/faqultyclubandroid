package faqulty.club.models.pemphlets;

import faqulty.club.models.BaseApi;

import java.util.List;

/**
 * Created by admin on 2/17/17.
 */
public class UserPamphletsResponse extends BaseApi {
    private boolean status;
    private  List<UserPamphletsData> data;

    public boolean isStatus() {
        return status;
    }

    public List<UserPamphletsData> getData() {
        return data;
    }
}
