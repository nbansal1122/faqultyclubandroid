package faqulty.club.models.pemphlets;

import faqulty.club.models.assignment.ClassModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/17.
 */

    public class PamphletClass {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pamphletId")
    @Expose
    private Integer pamphletId;
    @SerializedName("classId")
    @Expose
    private Integer classId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("class")
    @Expose
    private ClassModel classModel;

    public Integer getId() {
        return id;
    }

    public Integer getPamphletId() {
        return pamphletId;
    }

    public Integer getClassId() {
        return classId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public ClassModel getClassModel() {
        return classModel;
    }
}
