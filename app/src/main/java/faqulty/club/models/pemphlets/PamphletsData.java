package faqulty.club.models.pemphlets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/17.
 */
public class PamphletsData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("brandingText")
    @Expose
    private String brandingText;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    private String subjects;
    private String classes;
    private String boards;
    private String degrees;

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }

    public String getMobile() {
        return mobile;
    }

    public String getBrandingText() {
        return brandingText;
    }

    public String getUserId() {
        return userId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public String getSubjects() {
        return subjects;
    }

    public String getClasses() {
        return classes;
    }

    public String getBoards() {
        return boards;
    }

    public String getDegrees() {
        return degrees;
    }
}
