package faqulty.club.models.tutorstudent;

import faqulty.club.models.BaseApi;
import faqulty.club.models.creategroup.PupilInfo;
import faqulty.club.models.tutorgroup.TutorGroupData;

import java.util.List;

/**
 * Created by nbansal2211 on 11/11/16.
 */

public class StudentProfileResponse extends BaseApi {

    private boolean status;
    private Data data;


    public PupilInfo getPupilInfo() {
        if (data != null) {
            return data.pupilInfo;
        }
        return null;
    }

    public List<TutorGroupData> getGroups() {
        if (data != null) {
            return data.groups;
        }
        return null;
    }

    public boolean isGroupStudent() {
        if (null != getGroups() && getGroups().size() > 0) {
            return true;
        }
        return false;
    }


    public StudentBaseClass getStudent() {
        if (data != null) {
            return data.student;
        }
        return null;
    }

    public List<Invoice> getInvoices() {
        if (data != null) {
            return data.invoices;
        }
        return null;
    }

    public double getPendingAmount() {
        if (data != null) {
            return data.pendingAmount;
        }
        return 0.0;
    }


    public static class Data {
        public StudentBaseClass student;
        private PupilInfo pupilInfo;
        private List<Invoice> invoices;
        private List<TutorGroupData> groups;
        private double pendingAmount;


        public void setPendingAmount(double pendingAmount) {
            this.pendingAmount = pendingAmount;
        }
    }
}
