package faqulty.club.models.tutorstudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhillon on 07-11-2016.
 */
public class Group {

    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("invitationStatus")
    @Expose
    private int invitationStatus;
    @SerializedName("isBdayToday")
    @Expose
    private Boolean isBdayToday;
    @SerializedName("facultyProfileURI")
    @Expose
    private String facultyProfileURI;
    @SerializedName("userTypeString")
    @Expose
    private String userTypeString;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;

    private Boolean isFirst = false;

    public Boolean getFirst() {
        return isFirst;
    }

    public void setFirst(Boolean first) {
        isFirst = first;
    }


    /**
     * @return The dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth The dateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return The invitationStatus
     */
    public int getInvitationStatus() {
        return invitationStatus;
    }

    /**
     * @param invitationStatus The invitationStatus
     */
    public void setInvitationStatus(int invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    /**
     * @return The isBdayToday
     */
    public Boolean getIsBdayToday() {
        return isBdayToday;
    }

    /**
     * @param isBdayToday The isBdayToday
     */
    public void setIsBdayToday(Boolean isBdayToday) {
        this.isBdayToday = isBdayToday;
    }

    /**
     * @return The facultyProfileURI
     */
    public String getFacultyProfileURI() {
        return facultyProfileURI;
    }

    /**
     * @param facultyProfileURI The facultyProfileURI
     */
    public void setFacultyProfileURI(String facultyProfileURI) {
        this.facultyProfileURI = facultyProfileURI;
    }

    /**
     * @return The userTypeString
     */
    public String getUserTypeString() {
        return userTypeString;
    }

    /**
     * @param userTypeString The userTypeString
     */
    public void setUserTypeString(String userTypeString) {
        this.userTypeString = userTypeString;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

}
