package faqulty.club.models.tutorstudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("individual")
    @Expose
    private List<StudentBaseClass> individual = new ArrayList<StudentBaseClass>();
    @SerializedName("group")
    @Expose
    private List<StudentBaseClass> group = new ArrayList<StudentBaseClass>();

    /**
     * @return The individual
     */
    public List<StudentBaseClass> getIndividual() {
        return individual;
    }

    /**
     * @param individual The individual
     */
    public void setIndividual(List<StudentBaseClass> individual) {
        this.individual = individual;
    }

    /**
     * @return The group
     */
    public List<StudentBaseClass> getGroup() {
        return group;
    }

    /**
     * @param group The group
     */
    public void setGroup(List<StudentBaseClass> group) {
        this.group = group;
    }

}


