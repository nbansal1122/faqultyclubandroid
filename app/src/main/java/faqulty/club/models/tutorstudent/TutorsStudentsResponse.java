package faqulty.club.models.tutorstudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by Dhillon on 07-11-2016.
 */
public class TutorsStudentsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;

    private List<StudentBaseClass> allStudentsList = new ArrayList<>();

    public List<StudentBaseClass> getAllStudentsList() {
        return allStudentsList;
    }

    public void setAllStudentsList(List<StudentBaseClass> allStudentsList) {
        this.allStudentsList = allStudentsList;
    }

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public Data getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    public static TutorsStudentsResponse parseJson(String json) {
        TutorsStudentsResponse response = (TutorsStudentsResponse) JsonUtil.parseJson(json, TutorsStudentsResponse.class);
        if (response != null && response.data != null) {
            response.allStudentsList.clear();
            int i = 0;
            for (StudentBaseClass student : response.data.getIndividual()) {
                if (i == 0) {
                    student.setFirst(true);
                    i++;
                }
            }
            response.allStudentsList.addAll(response.data.getIndividual());
            i = 0;
            for (StudentBaseClass student : response.data.getGroup()) {
                if (i == 0) {
                    student.setFirst(true);
                    i++;
                }
                student.setGroupStudent(true);
            }
            response.allStudentsList.addAll(response.data.getGroup());
        }
        return response;
    }

}
