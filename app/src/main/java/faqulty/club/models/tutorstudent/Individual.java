package faqulty.club.models.tutorstudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhillon on 07-11-2016.
 */
public class Individual {

    @SerializedName("tutorLocation")
    @Expose
    private List<Object> tutorLocation = new ArrayList<Object>();
    @SerializedName("tutorAwards")
    @Expose
    private List<Object> tutorAwards = new ArrayList<Object>();
    @SerializedName("dateOfBirth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("invitationStatus")
    @Expose
    private Integer invitationStatus;
    @SerializedName("isBdayToday")
    @Expose
    private Boolean isBdayToday;
    @SerializedName("facultyProfileURI")
    @Expose
    private String facultyProfileURI;
    @SerializedName("userTypeString")
    @Expose
    private String userTypeString;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tagline")
    @Expose
    private String tagline;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("userType")
    @Expose
    private Integer userType;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    private Boolean isFirst=false;

    public Boolean getFirst() {
        return isFirst;
    }

    public void setFirst(Boolean first) {
        isFirst = first;
    }

    /**
     * @return The tutorLocation
     */
    public List<Object> getTutorLocation() {
        return tutorLocation;
    }

    /**
     * @param tutorLocation The tutorLocation
     */
    public void setTutorLocation(List<Object> tutorLocation) {
        this.tutorLocation = tutorLocation;
    }

    /**
     * @return The tutorAwards
     */
    public List<Object> getTutorAwards() {
        return tutorAwards;
    }

    /**
     * @param tutorAwards The tutorAwards
     */
    public void setTutorAwards(List<Object> tutorAwards) {
        this.tutorAwards = tutorAwards;
    }

    /**
     * @return The dateOfBirth
     */
    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth The dateOfBirth
     */
    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return The invitationStatus
     */
    public Integer getInvitationStatus() {
        return invitationStatus;
    }

    /**
     * @param invitationStatus The invitationStatus
     */
    public void setInvitationStatus(Integer invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    /**
     * @return The isBdayToday
     */
    public Boolean getIsBdayToday() {
        return isBdayToday;
    }

    /**
     * @param isBdayToday The isBdayToday
     */
    public void setIsBdayToday(Boolean isBdayToday) {
        this.isBdayToday = isBdayToday;
    }

    /**
     * @return The facultyProfileURI
     */
    public String getFacultyProfileURI() {
        return facultyProfileURI;
    }

    /**
     * @param facultyProfileURI The facultyProfileURI
     */
    public void setFacultyProfileURI(String facultyProfileURI) {
        this.facultyProfileURI = facultyProfileURI;
    }

    /**
     * @return The userTypeString
     */
    public String getUserTypeString() {
        return userTypeString;
    }

    /**
     * @param userTypeString The userTypeString
     */
    public void setUserTypeString(String userTypeString) {
        this.userTypeString = userTypeString;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The tagline
     */
    public String getTagline() {
        return tagline;
    }

    /**
     * @param tagline The tagline
     */
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The userType
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * @param userType The userType
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

}
