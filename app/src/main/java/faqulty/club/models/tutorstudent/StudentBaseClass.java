package faqulty.club.models.tutorstudent;

import android.text.TextUtils;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by Dhillon on 08-11-2016.
 */

public class StudentBaseClass extends BaseAutoFilter implements Serializable {

    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("invitationStatus")
    @Expose
    private int invitationStatus;
    @SerializedName("isBdayToday")
    @Expose
    private Boolean isBdayToday;
    @SerializedName("facultyProfileURI")
    @Expose
    private String facultyProfileURI;
    @SerializedName("userTypeString")
    @Expose
    private String userTypeString;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("tagline")
    @Expose
    private String tagline;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("groupId")
    @Expose
    private int groupId;
    @SerializedName("userType")
    @Expose
    private Integer userType;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("moneyOwed")
    @Expose
    private double pendingAmount;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public double getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(double pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    private boolean isGroupStudent = false;

    private boolean isFirst = false;

    private boolean isSelected = false;

    private int performance = -1;

    public int getPerformance() {
        return performance;
    }

    public void setPerformance(int performance) {
        this.performance = performance;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public boolean isInvitePending() {
        return invitationStatus == 0;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getBdayToday() {
        return isBdayToday;
    }

    public void setBdayToday(Boolean bdayToday) {
        isBdayToday = bdayToday;
    }

    public boolean isGroupStudent() {
        return isGroupStudent;
    }

    public void setGroupStudent(boolean groupStudent) {
        isGroupStudent = groupStudent;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public Boolean getFirst() {
        return isFirst;
    }

    public void setFirst(Boolean first) {
        isFirst = first;
    }


    /**
     * @return The invitationStatus
     */
    public Integer getInvitationStatus() {
        return invitationStatus;
    }

    /**
     * @param invitationStatus The invitationStatus
     */
    public void setInvitationStatus(Integer invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    /**
     * @return The isBdayToday
     */
    public Boolean getIsBdayToday() {
        Date date= Util.convertUTCDate(dateOfBirth, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT);
        if(date!=null){
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM");
            Calendar calendar=Calendar.getInstance();
            if(simpleDateFormat.format(date).equalsIgnoreCase(simpleDateFormat.format(calendar.getTime()))){
                return true;
            }
        }
        return isBdayToday;
    }

    /**
     * @param isBdayToday The isBdayToday
     */
    public void setIsBdayToday(Boolean isBdayToday) {
        this.isBdayToday = isBdayToday;
    }

    /**
     * @return The facultyProfileURI
     */
    public String getFacultyProfileURI() {
        return facultyProfileURI;
    }

    /**
     * @param facultyProfileURI The facultyProfileURI
     */
    public void setFacultyProfileURI(String facultyProfileURI) {
        this.facultyProfileURI = facultyProfileURI;
    }

    /**
     * @return The userTypeString
     */
    public String getUserTypeString() {
        return userTypeString;
    }

    /**
     * @param userTypeString The userTypeString
     */
    public void setUserTypeString(String userTypeString) {
        this.userTypeString = userTypeString;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The tagline
     */
    public String getTagline() {
        return tagline;
    }

    /**
     * @param tagline The tagline
     */
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The userType
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * @param userType The userType
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public void setInvitationStatus(int invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDisplayName() {
        if (TextUtils.isEmpty(name)) {
            return phone;
        }
        return name;
    }

    @Override
    public String getDisplayString() {
        return getDisplayName();
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        if (TextUtils.isEmpty(text)) return true;
        return getDisplayName().contains(text);
    }

    @Override
    public int getViewType() {
        if (isGroupStudent()) {
            return AppConstants.VIEW_TYPE.GROUP_STUDENT;
        } else {
            return AppConstants.VIEW_TYPE.STUDENT;
        }

    }
}
