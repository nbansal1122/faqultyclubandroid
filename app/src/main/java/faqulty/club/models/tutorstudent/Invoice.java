package faqulty.club.models.tutorstudent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Invoice implements Serializable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("dateFrom")
    @Expose
    private String dateFrom;
    @SerializedName("dateTo")
    @Expose
    private String dateTo;
    @SerializedName("payee")
    @Expose
    private int payee;
    @SerializedName("recipient")
    @Expose
    private int recipient;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("suggestedSessions")
    @Expose
    private int suggestedSessions;
    @SerializedName("suggestedHours")
    @Expose
    private int suggestedHours;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The dateFrom
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * @param dateFrom The dateFrom
     */
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    /**
     * @return The dateTo
     */
    public String getDateTo() {
        return dateTo;
    }

    /**
     * @param dateTo The dateTo
     */
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    /**
     * @return The payee
     */
    public int getPayee() {
        return payee;
    }

    /**
     * @param payee The payee
     */
    public void setPayee(int payee) {
        this.payee = payee;
    }

    /**
     * @return The recipient
     */
    public int getRecipient() {
        return recipient;
    }

    /**
     * @param recipient The recipient
     */
    public void setRecipient(int recipient) {
        this.recipient = recipient;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return The suggestedSessions
     */
    public int getSuggestedSessions() {
        return suggestedSessions;
    }

    /**
     * @param suggestedSessions The suggestedSessions
     */
    public void setSuggestedSessions(int suggestedSessions) {
        this.suggestedSessions = suggestedSessions;
    }

    /**
     * @return The suggestedHours
     */
    public int getSuggestedHours() {
        return suggestedHours;
    }

    /**
     * @param suggestedHours The suggestedHours
     */
    public void setSuggestedHours(int suggestedHours) {
        this.suggestedHours = suggestedHours;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

}