package faqulty.club.models;

import java.util.List;

/**
 * Created by admin on 3/1/17.
 */

public class HashTagsResponse extends BaseApi{
List<HashTag> data;

    public List<HashTag> getData() {
        return data;
    }

    public void setData(List<HashTag> data) {
        this.data = data;
    }
}
