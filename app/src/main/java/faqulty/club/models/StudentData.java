package faqulty.club.models;

/**
 * Created by Neeraj Yadav on 10/15/2016.
 */

public class StudentData {
    int userImage, icon1, icon2, userInvite;
    String name;

    public int getUserImage() {
        return userImage;
    }

    public void setUserImage(int userImage) {
        this.userImage = userImage;
    }

    public int getIcon1() {
        return icon1;
    }

    public void setIcon1(int icon1) {
        this.icon1 = icon1;
    }

    public int getIcon2() {
        return icon2;
    }

    public void setIcon2(int icon2) {
        this.icon2 = icon2;
    }

    public int getUserInvite() {
        return userInvite;
    }

    public void setUserInvite(int userInvite) {
        this.userInvite = userInvite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
