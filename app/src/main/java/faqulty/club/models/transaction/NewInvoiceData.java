package faqulty.club.models.transaction;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raghu on 9/1/17.
 */
public class NewInvoiceData implements Serializable{
    double outstanding;
    List<Transaction> transactions;

    public long getOutstanding() {
        return (long) outstanding;
    }

    public void setOutstanding(double outstanding) {
        this.outstanding = outstanding;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
