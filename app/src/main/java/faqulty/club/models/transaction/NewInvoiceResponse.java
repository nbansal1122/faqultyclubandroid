package faqulty.club.models.transaction;

import faqulty.club.models.BaseApi;

/**
 * Created by raghu on 9/1/17.
 */
public class NewInvoiceResponse extends BaseApi {
    NewInvoiceData data;

    public NewInvoiceData getData() {
        return data;
    }

    public void setData(NewInvoiceData data) {
        this.data = data;
    }
}
