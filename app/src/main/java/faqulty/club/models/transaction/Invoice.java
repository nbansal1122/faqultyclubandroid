package faqulty.club.models.transaction;

/**
 * Created by my on 17-10-2016.
 */

public class Invoice {
    String invoiceNum;
    String invoiceFrom;
    String invoiceTo;
    String invoicePrice;
    String invoicePaid;
    int imageNext;

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getInvoiceFrom() {
        return invoiceFrom;
    }

    public void setInvoiceFrom(String invoiceFrom) {
        this.invoiceFrom = invoiceFrom;
    }

    public String getInvoiceTo() {
        return invoiceTo;
    }

    public void setInvoiceTo(String invoiceTo) {
        this.invoiceTo = invoiceTo;
    }

    public String getInvoicePrice() {
        return invoicePrice;
    }

    public void setInvoicePrice(String invoicePrice) {
        this.invoicePrice = invoicePrice;
    }

    public String getInvoicePaid() {
        return invoicePaid;
    }

    public void setInvoicePaid(String invoicePaid) {
        this.invoicePaid = invoicePaid;
    }

    public int getImageNext() {
        return imageNext;
    }

    public void setImageNext(int imageNext) {
        this.imageNext = imageNext;
    }
}
