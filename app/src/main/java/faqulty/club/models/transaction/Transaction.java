package faqulty.club.models.transaction;

import java.io.Serializable;

/**
 * Created by raghu on 9/1/17.
 */
public class Transaction implements Serializable {
    private long id;
    private long studentId;
    private long tutorId;
    private String type;
    private String dateFrom;
    private String dateTo;
    private String suggestedSessions;
    private String suggestedHours;
    private long amount;
    private String createdOn;

    public String getSuggestedSessions() {
        return suggestedSessions;
    }

    public void setSuggestedSessions(String suggestedSessions) {
        this.suggestedSessions = suggestedSessions;
    }

    public String getSuggestedHours() {
        return suggestedHours;
    }

    public void setSuggestedHours(String suggestedHours) {
        this.suggestedHours = suggestedHours;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getTutorId() {
        return tutorId;
    }

    public void setTutorId(long tutorId) {
        this.tutorId = tutorId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
