package faqulty.club.models.transaction;

/**
 * Created by Neeraj Yadav on 12/6/2016.
 */

public class PaymentModel {
    String title;
    int logo;

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
