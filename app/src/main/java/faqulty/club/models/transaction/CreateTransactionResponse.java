package faqulty.club.models.transaction;

import faqulty.club.models.BaseApi;

/**
 * Created by admin on 3/15/17.
 */

public class CreateTransactionResponse extends BaseApi {
    private Transaction data;

    public Transaction getData() {
        return data;
    }

    public void setData(Transaction data) {
        this.data = data;
    }
}
