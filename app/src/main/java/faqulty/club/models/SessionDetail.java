package faqulty.club.models;

/**
 * Created by my on 19-10-2016.
 */

public class SessionDetail {
    String sessionTitle;
    String newSession;

    public String getSessionTitle() {
        return sessionTitle;
    }

    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    public String getNewSession() {
        return newSession;
    }

    public void setNewSession(String newSession) {
        this.newSession = newSession;
    }
}
