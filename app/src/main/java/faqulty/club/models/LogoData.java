package faqulty.club.models;

/**
 * Created by admin on 2/18/17.
 */
public class LogoData {
    private int id;
    private String logo;
    private String brandingText;
    private String watermark;
    private String userId;

    public int getId() {
        return id;
    }

    public String getLogo() {
        return logo;
    }

    public String getBrandingText() {
        return brandingText;
    }

    public String getWatermark() {
        return watermark;
    }

    public String getUserId() {
        return userId;
    }
}
