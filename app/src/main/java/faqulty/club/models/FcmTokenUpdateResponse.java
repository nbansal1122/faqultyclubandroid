package faqulty.club.models;

/**
 * Created by admin on 2/28/17.
 */
public class FcmTokenUpdateResponse extends BaseApi {
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
