package faqulty.club.models;

/**
 * Created by my on 18-10-2016.
 */

public class WebTemplate {
    String templateTitle;
    String templateSubtitle;
    int templateImage;
    int templateNext;

    public String getTemplateTitle() {
        return templateTitle;
    }

    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }

    public String getTemplateSubtitle() {
        return templateSubtitle;
    }

    public void setTemplateSubtitle(String templateSubtitle) {
        this.templateSubtitle = templateSubtitle;
    }

    public int getTemplateImage() {
        return templateImage;
    }

    public void setTemplateImage(int templateImage) {
        this.templateImage = templateImage;
    }

    public int getTemplateNext() {
        return templateNext;
    }

    public void setTemplateNext(int templateNext) {
        this.templateNext = templateNext;
    }
}
