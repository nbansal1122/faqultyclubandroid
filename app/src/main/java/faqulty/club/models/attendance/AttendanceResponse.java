package faqulty.club.models.attendance;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.Util;

public class AttendanceResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sessionId")
    @Expose
    private Integer sessionId;
    @SerializedName("studentId")
    @Expose
    private Integer studentId;
    @SerializedName("performance")
    @Expose
    private Integer performance = 3;
    @SerializedName("attendance")
    @Expose
    private Integer attendance;
    @SerializedName("session_id")
    @Expose
    private Integer session_id;
    @SerializedName("session")
    @Expose
    private Session session;
    @SerializedName("durationHours")
    @Expose
    private Integer durationHours;
    @SerializedName("durationMins")
    @Expose
    private Integer durationMins;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    private String sessionName;

    public Session getSession() {
        return session;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getSessionNameForViewAttendanceScreen() {
        if (session != null && !TextUtils.isEmpty(session.getName())) {
            String[] split = session.getName().split("Session ");
            if (split.length > 0) {
                return split[split.length - 1];
            }
        }
        return "1";
    }

    public Integer getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(Integer durationHours) {
        this.durationHours = durationHours;
    }

    public Integer getDurationMins() {
        return durationMins;
    }

    public void setDurationMins(Integer durationMins) {
        this.durationMins = durationMins;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The sessionId
     */
    public Integer getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId The sessionId
     */
    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return The studentId
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * @param studentId The studentId
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * @return The performance
     */
    public Integer getPerformance() {
        return performance;
    }

    /**
     * @param performance The performance
     */
    public void setPerformance(Integer performance) {
        this.performance = performance;
    }

    /**
     * @return The attendance
     */
    public Integer getAttendance() {
        return attendance;
    }

    /**
     * @param attendance The attendance
     */
    public void setAttendance(Integer attendance) {
        this.attendance = attendance;
    }

    /**
     * @return The session_id
     */
    public Integer getSession_id() {
        return session_id;
    }

    /**
     * @param session_id The session_id
     */
    public void setSession_id(Integer session_id) {
        this.session_id = session_id;
    }
//
//    /**
//     * @return The session
//     */
//    public Session getSession() {
//        return session;
//    }

    /**
     * @param session The session
     */
    public void setSession(Session session) {
        this.session = session;
    }

    public static List<AttendanceResponse> parseJson(String json) {
        Log.d("parse category", "parse json call");
        List<AttendanceResponse> filterItem = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(json);

            JSONArray jsonArray = obj.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                Gson gson = new Gson();
                AttendanceResponse filter = gson.fromJson(jsonObject.toString(), AttendanceResponse.class);
                String date = Util.convertDateFormat(filter.getCreatedOn(), Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.UI_ATTENDANCE_DATE_PATTERN);
                filter.setCreatedOn(date);
                filterItem.add(filter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return filterItem;
    }

}


