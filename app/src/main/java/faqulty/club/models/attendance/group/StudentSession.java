package faqulty.club.models.attendance.group;

import faqulty.club.models.tutorstudent.StudentBaseClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nbansal2211 on 13/11/16.
 */
public class StudentSession implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sessionId")
    @Expose
    private Integer sessionId;
    @SerializedName("studentId")
    @Expose
    private Integer studentId;
    @SerializedName("performance")
    @Expose
    private Integer performance;
    @SerializedName("attendance")
    @Expose
    private Integer attendance;
    @SerializedName("session_id")
    @Expose
    private Integer session_id;
    @SerializedName("student_id")
    @Expose
    private Integer student_id;
    @SerializedName("student")
    @Expose
    private StudentBaseClass student;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The sessionId
     */
    public Integer getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId The sessionId
     */
    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return The studentId
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * @param studentId The studentId
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * @return The performance
     */
    public Integer getPerformance() {
        return performance;
    }

    /**
     * @param performance The performance
     */
    public void setPerformance(Integer performance) {
        this.performance = performance;
    }

    /**
     * @return The attendance
     */
    public Integer getAttendance() {
        return attendance;
    }

    /**
     * @param attendance The attendance
     */
    public void setAttendance(Integer attendance) {
        this.attendance = attendance;
    }

    /**
     * @return The session_id
     */
    public Integer getSession_id() {
        return session_id;
    }

    /**
     * @param session_id The session_id
     */
    public void setSession_id(Integer session_id) {
        this.session_id = session_id;
    }

    /**
     * @return The student_id
     */
    public Integer getStudent_id() {
        return student_id;
    }

    /**
     * @param student_id The student_id
     */
    public void setStudent_id(Integer student_id) {
        this.student_id = student_id;
    }

    /**
     * @return The student
     */
    public StudentBaseClass getStudent() {
        return student;
    }

    /**
     * @param student The student
     */
    public void setStudent(StudentBaseClass student) {
        this.student = student;
    }

}
