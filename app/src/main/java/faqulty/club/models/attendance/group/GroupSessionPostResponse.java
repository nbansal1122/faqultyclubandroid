package faqulty.club.models.attendance.group;

import faqulty.club.models.BaseApi;

/**
 * Created by nbansal2211 on 26/12/16.
 */

public class GroupSessionPostResponse extends BaseApi {

    private GroupAttendance data;

    public GroupAttendance getData() {
        return data;
    }

    public void setData(GroupAttendance data) {
        this.data = data;
    }
}
