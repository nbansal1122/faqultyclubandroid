package faqulty.club.models.attendance;

import faqulty.club.models.BaseApi;

/**
 * Created by nbansal2211 on 10/02/17.
 */

public class PostAttendanceResponse extends BaseApi {
    private AttendanceResponse data;

    public AttendanceResponse getData() {
        return data;
    }

    public void setData(AttendanceResponse data) {
        this.data = data;
    }
}
