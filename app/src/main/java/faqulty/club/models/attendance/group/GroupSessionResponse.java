package faqulty.club.models.attendance.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 13/11/16.
 */
public class GroupSessionResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<GroupAttendance> data = new ArrayList<GroupAttendance>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<GroupAttendance> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<GroupAttendance> data) {
        this.data = data;
    }


    public static GroupSessionResponse parseJson(String json){
        GroupSessionResponse response = (GroupSessionResponse) JsonUtil.parseJson(json, GroupSessionResponse.class);
        if(response != null){
            for(GroupAttendance a : response.data){
                a.setCreatedOn(Util.convertDateFormat(a.getCreatedOn(), Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, Util.UI_ATTENDANCE_DATE_PATTERN));
            }
        }
        return response;
    }

}
