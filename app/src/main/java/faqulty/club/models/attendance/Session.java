package faqulty.club.models.attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhillon on 08-11-2016.
 */
public class Session {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("durationHours")
    @Expose
    private Integer durationHours;
    @SerializedName("durationMins")
    @Expose
    private Integer durationMins;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The durationHours
     */
    public Integer getDurationHours() {
        return durationHours;
    }

    /**
     * @param durationHours The durationHours
     */
    public void setDurationHours(Integer durationHours) {
        this.durationHours = durationHours;
    }

    /**
     * @return The durationMins
     */
    public Integer getDurationMins() {
        return durationMins;
    }

    /**
     * @param durationMins The durationMins
     */
    public void setDurationMins(Integer durationMins) {
        this.durationMins = durationMins;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

}
