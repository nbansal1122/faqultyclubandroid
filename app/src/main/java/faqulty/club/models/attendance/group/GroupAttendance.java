package faqulty.club.models.attendance.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GroupAttendance implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("durationHours")
    @Expose
    private Integer durationHours;
    @SerializedName("durationMins")
    @Expose
    private Integer durationMins;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("groupId")
    @Expose
    private Integer groupId;
    @SerializedName("studentSessions")
    @Expose
    private List<StudentSession> studentSessions = new ArrayList<StudentSession>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The durationHours
     */
    public Integer getDurationHours() {
        return durationHours;
    }

    /**
     * @param durationHours The durationHours
     */
    public void setDurationHours(Integer durationHours) {
        this.durationHours = durationHours;
    }

    /**
     * @return The durationMins
     */
    public Integer getDurationMins() {
        return durationMins;
    }

    /**
     * @param durationMins The durationMins
     */
    public void setDurationMins(Integer durationMins) {
        this.durationMins = durationMins;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId The groupId
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return The studentSessions
     */
    public List<StudentSession> getStudentSessions() {
        return studentSessions;
    }

    /**
     * @param studentSessions The studentSessions
     */
    public void setStudentSessions(List<StudentSession> studentSessions) {
        this.studentSessions = studentSessions;
    }

}

