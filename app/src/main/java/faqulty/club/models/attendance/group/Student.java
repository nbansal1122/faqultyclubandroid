package faqulty.club.models.attendance.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nbansal2211 on 13/11/16.
 */
public class Student {

    @SerializedName("invitationStatus")
    @Expose
    private Integer invitationStatus;
    @SerializedName("facultyProfileURI")
    @Expose
    private String facultyProfileURI;
    @SerializedName("userTypeString")
    @Expose
    private String userTypeString;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("image")
    @Expose
    private String image;

    /**
     * @return The invitationStatus
     */
    public Integer getInvitationStatus() {
        return invitationStatus;
    }

    /**
     * @param invitationStatus The invitationStatus
     */
    public void setInvitationStatus(Integer invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    /**
     * @return The facultyProfileURI
     */
    public String getFacultyProfileURI() {
        return facultyProfileURI;
    }

    /**
     * @param facultyProfileURI The facultyProfileURI
     */
    public void setFacultyProfileURI(String facultyProfileURI) {
        this.facultyProfileURI = facultyProfileURI;
    }

    /**
     * @return The userTypeString
     */
    public String getUserTypeString() {
        return userTypeString;
    }

    /**
     * @param userTypeString The userTypeString
     */
    public void setUserTypeString(String userTypeString) {
        this.userTypeString = userTypeString;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

}
