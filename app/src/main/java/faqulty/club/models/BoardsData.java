package faqulty.club.models;

/**
 * Created by saurabh on 15-09-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

public class BoardsData extends BaseAutoFilter implements Chip ,Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("board")
    @Expose
    private String board;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The board
     */
    public String getBoard() {
        return board;
    }

    /**
     * @param board The board
     */
    public void setBoard(String board) {
        this.board = board;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<BoardsData> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<BoardsData> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            BoardsData cityApi = (BoardsData) JsonUtil.parseJson(jsonObject.toString(), BoardsData.class);
            list.add(cityApi);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return board;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.board.toLowerCase().contains(text.toLowerCase());
    }

    @Override
    public String getText() {
        return board;
    }
}