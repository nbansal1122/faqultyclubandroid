package faqulty.club.models;

import faqulty.club.models.UserProfile.UserDetailsApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhillon on 29-09-2016.
 */

public class SocialLoginData{

    @SerializedName("user")
    @Expose
    private UserDetailsApi user;
    @SerializedName("token")
    @Expose
    private String token;

    /**
     *
     * @return
     * The user
     */
    public UserDetailsApi getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(UserDetailsApi user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }



}