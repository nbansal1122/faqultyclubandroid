package faqulty.club.models;

import java.io.Serializable;

/**
 * Created by saurabh on 13-09-2016.
 */
public class BaseApi implements Serializable{
    String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
