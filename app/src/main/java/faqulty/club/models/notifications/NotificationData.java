package faqulty.club.models.notifications;

import java.util.List;

/**
 * Created by raghu on 25/1/17.
 */

public class NotificationData {
    private List<Notification> notifications;
    int totalUnseenNotifications;

    public List<Notification> getNotifications() {
        return notifications;
    }

    public int getTotalUnseenNotifications() {
        return totalUnseenNotifications;
    }
}
