package faqulty.club.models;

/**
 * Created by saurabh on 15-09-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

public class TeachingModeApi extends BaseAutoFilter implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("mode")
    @Expose
    private String mode;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * @param mode The mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }


    public static List<TeachingModeApi> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<TeachingModeApi> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            TeachingModeApi teachingModeApi = (TeachingModeApi) JsonUtil.parseJson(jsonObject.toString(), TeachingModeApi.class);
            list.add(teachingModeApi);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return mode;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.mode.toLowerCase().contains(text.toLowerCase());
    }
}