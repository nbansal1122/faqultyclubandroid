package faqulty.club.models;

import com.google.gson.Gson;
import com.plumillonforge.android.chipview.Chip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raghu on 24/1/17.
 */
public class HashTag extends BaseAutoFilter implements Chip{
    int id;
    String tag;
    long score;
    String createdOn;

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTag() {
        return tag;
    }
    public static List<HashTag> parseJson(String json){
        List<HashTag> hashTagList=new ArrayList<>();
        try {
            Gson gson=new Gson();
            JSONArray jsonArray=new JSONArray(json);
            for(int x=0;x<jsonArray.length();x++){
                JSONObject jsonObject= jsonArray.getJSONObject(x);
                HashTag hashTag = gson.fromJson(jsonObject.toString(), HashTag.class);
                hashTagList.add(hashTag);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hashTagList;
    }

    @Override
    public String getDisplayString() {
        return tag;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.tag.toLowerCase().contains(text.toLowerCase());
    }

    @Override
    public String getText() {
        return tag;
    }


}
