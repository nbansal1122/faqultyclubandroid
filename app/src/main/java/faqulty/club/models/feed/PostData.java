package faqulty.club.models.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Neeraj Yadav on 12/17/2016.
 */

public class PostData {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tutorId")
    @Expose
    private String tutorId;
    @SerializedName("contentUrl")
    @Expose
    private String contentUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("numViews")
    @Expose
    private Integer numViews;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("mimetype")
    @Expose
    private String mimetype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTutorId() {
        return tutorId;
    }

    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumViews() {
        return numViews;
    }

    public void setNumViews(Integer numViews) {
        this.numViews = numViews;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

}
