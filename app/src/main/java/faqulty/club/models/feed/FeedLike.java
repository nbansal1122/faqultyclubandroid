package faqulty.club.models.feed;

/**
 * Created by nbansal2211 on 18/02/17.
 */

public class FeedLike {

    private User user = new User();

    public int getId() {
        return user.id;
    }

    public String getImage() {
        return user.image;
    }


    public String getName() {
        return user.name;
    }

    public String getUserType() {
        return user.userType;
    }


    public static class User {
        private String image;
        private String name;
        private String userType;
        private int id;
    }
}
