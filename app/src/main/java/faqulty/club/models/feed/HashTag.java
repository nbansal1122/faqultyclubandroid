package faqulty.club.models.feed;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.Gson;
import com.plumillonforge.android.chipview.Chip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raghu on 24/1/17.
 */
public class HashTag extends BaseAutoFilter implements Chip{
    int tag_count;
    String tag;

    public int getTag_count() {
        return tag_count;
    }

    public String getTag() {
        return tag;
    }
    public static List<HashTag> parseJson(String json){
        List<HashTag> hashTagList=new ArrayList<>();
        try {
            Gson gson=new Gson();
            JSONArray jsonArray=new JSONArray(json);
            for(int x=0;x<jsonArray.length();x++){
                JSONObject jsonObject= jsonArray.getJSONObject(x);
                HashTag hashTag = gson.fromJson(jsonObject.toString(), HashTag.class);
                hashTagList.add(hashTag);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hashTagList;
    }

    @Override
    public String getDisplayString() {
        return tag;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.tag.toLowerCase().contains(text.toLowerCase());
    }

    @Override
    public String getText() {
        return tag;
    }
}
