package faqulty.club.models.feed;

import faqulty.club.models.UserProfile.SubjectObject;
import simplifii.framework.requestmodels.BaseAdapterModel;

import faqulty.club.models.HashTag;
import faqulty.club.models.UserProfile.ClassObject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.requestmodels.SelectContent;
import simplifii.framework.utility.AppConstants;

public class Datum extends BaseAdapterModel implements Serializable {

  @SerializedName("tutor")
  @Expose
  private Tutor tutor;
  @SerializedName("likesCount")
  @Expose
  private Integer likesCount;
  @SerializedName("commentsCount")
  @Expose
  private Integer commentsCount;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("contentUrl")
  @Expose
  private String contentUrl;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("numViews")
  @Expose
  private Integer numViews;
  @SerializedName("updatedOn")
  @Expose
  private String updatedOn;
  @SerializedName("createdOn")
  @Expose
  private String createdOn;
  @SerializedName("content")
  @Expose
  private List<SelectContent> contentList = new ArrayList<>();
  @SerializedName("tutorId")
  @Expose
  private long tutorId;
  @SerializedName("tags")
  List<String> hashTags;
  List<SubjectObject> subjects;
  List<ClassObject> classes;
  private boolean hasLiked;

  public List<HashTag> gethashtagsList() {
    List<HashTag> hashTagList = new ArrayList<>();
    if (hashTagList != null) {
      for (String tag : hashTags) {
        HashTag hashTag = new HashTag();
        hashTag.setTag(tag);
        hashTagList.add(hashTag);
      }
    }
    return hashTagList;
  }

  public List<SubjectObject> getSubjects() {
    return subjects;
  }

  public List<ClassObject> getClasses() {
    return classes;
  }

  public List<String> getHashTags() {
    return hashTags;
  }

  public long getTutorId() {
    return tutorId;
  }

  public void setTutorId(long tutorId) {
    this.tutorId = tutorId;
  }

  public List<SelectContent> getContentList() {
    return contentList;
  }

  public void setContentList(List<SelectContent> contentList) {
    this.contentList = contentList;
  }

  public Tutor getTutor() {
    return tutor;
  }

  public void setTutor(Tutor tutor) {
    this.tutor = tutor;
  }

  public Integer getLikesCount() {
    return likesCount;
  }

  public void setLikesCount(Integer likesCount) {
    this.likesCount = likesCount;
  }

  public Integer getCommentsCount() {
    return commentsCount;
  }

  public void setCommentsCount(Integer commentsCount) {
    this.commentsCount = commentsCount;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getContentUrl() {
    return contentUrl;
  }

  public void setContentUrl(String contentUrl) {
    this.contentUrl = contentUrl;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getNumViews() {
    return numViews;
  }

  public void setNumViews(Integer numViews) {
    this.numViews = numViews;
  }

  public String getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(String updatedOn) {
    this.updatedOn = updatedOn;
  }

  public String getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(String createdOn) {
    this.createdOn = createdOn;
  }


  public int getViewType() {
    return AppConstants.VIEW_TYPE.FEED_DATA;
  }

  public boolean isHasLiked() {
    return hasLiked;
  }

  public void setHasLiked(boolean hasLiked) {
    this.hasLiked = hasLiked;
    if(likesCount!=null){
      if(hasLiked){
        likesCount++;
      }else {
        likesCount--;
      }
    }
  }
}