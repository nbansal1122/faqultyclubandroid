package faqulty.club.models.feed;

import faqulty.club.models.BaseApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbansal2211 on 18/02/17.
 */

public class FeedLikeResponse extends BaseApi {

    private List<FeedLike> data = new ArrayList<>();

    public List<FeedLike> getData() {
        return data;
    }

    public void setData(List<FeedLike> data) {
        this.data = data;
    }
}
