package faqulty.club.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassApi {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("createdOn")
    @Expose
    private Object createdOn;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The _class
     */
    public String getClass_() {
        return _class;
    }

    /**
     *
     * @param _class
     * The class
     */
    public void setClass_(String _class) {
        this._class = _class;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public Object getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The createdOn
     */
    public void setCreatedOn(Object createdOn) {
        this.createdOn = createdOn;
    }

}
