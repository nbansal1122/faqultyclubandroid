package faqulty.club.models;

import java.io.Serializable;

import simplifii.framework.requestmodels.BaseAdapterModel;

/**
 * Created by Neeraj Yadav on 12/5/2016.
 */

public class FeedData extends BaseAdapterModel implements Serializable {
    String name, time, desc,likeComment, nameComment, descComment, imageURI, videoUri, audioUri;
    int userPic, commentPic, ivFeedPost, moduleId;

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getImageURI() {

        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    public String getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(String videoUri) {
        this.videoUri = videoUri;
    }

    public String getAudioUri() {
        return audioUri;
    }

    public void setAudioUri(String audioUri) {
        this.audioUri = audioUri;
    }

    public int getIvFeedPost() {
        return ivFeedPost;
    }

    public void setIvFeedPost(int ivFeedPost) {
        this.ivFeedPost = ivFeedPost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLikeComment() {
        return likeComment;
    }

    public void setLikeComment(String likeComment) {
        this.likeComment = likeComment;
    }

    public String getNameComment() {
        return nameComment;
    }

    public void setNameComment(String nameComment) {
        this.nameComment = nameComment;
    }

    public String getDescComment() {
        return descComment;
    }

    public void setDescComment(String descComment) {
        this.descComment = descComment;
    }

    public int getUserPic() {
        return userPic;
    }

    public void setUserPic(int userPic) {
        this.userPic = userPic;
    }

    public int getCommentPic() {
        return commentPic;
    }

    public void setCommentPic(int commentPic) {
        this.commentPic = commentPic;
    }

//    @Override
//    public int getViewType() {
//        return AppConstants.VIEW_TYPE.FEED_DATA;
//    }
}
