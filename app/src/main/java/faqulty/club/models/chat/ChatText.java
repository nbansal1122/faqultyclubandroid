package faqulty.club.models.chat;

/**
 * Created by admin on 2/22/17.
 */

public class ChatText {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
