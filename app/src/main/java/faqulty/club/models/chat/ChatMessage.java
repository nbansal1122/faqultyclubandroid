package faqulty.club.models.chat;

import android.provider.BaseColumns;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import faqulty.club.models.UserProfile.UserDetailsApi;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by admin on 2/24/17.
 */
@Table(name = "ChatMessage", id = BaseColumns._ID)
public class ChatMessage extends Model {
    public static int INCOMING = 1;
    public static int OUTGOING = 0;

    public static String INDIVIDUAL = "INDIVIDUAL";
    public static String GROUP = "GROUP";

    @Column
    private String fromUserType;
    @Column
    private String fromUserName;
    @Column
    private String toUserType;
    @SerializedName("fromUserId")
    @Column
    private long fromUserID;
    @SerializedName("toUserId")
    @Column
    private long toUserID;
    @Column
    private long messageId;
    @Column
    private String chatType;
    @Column
    private String messageType;
    @SerializedName("metaInfo")
    @Column
    private String metaInfoString;
//    private MetaInfo metaInfo;


    public boolean isOutGoing() {
        if (Preferences.getUserId() == fromUserID) {
            return true;
        } else return false;
    }

    public String getFromUserName() {
        if(TextUtils.isEmpty(fromUserName)){
            return "Test User";
        }
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromUserType() {
        return fromUserType;
    }

    public void setFromUserType(String fromUserType) {
        this.fromUserType = fromUserType;
    }

    public String getToUserType() {
        return toUserType;
    }

    public void setToUserType(String toUserType) {
        this.toUserType = toUserType;
    }

    public long getFromUserID() {
        return fromUserID;
    }

    public void setFromUserID(int fromUserID) {
        this.fromUserID = fromUserID;
    }

    public long getToUserID() {
        return toUserID;
    }

    public void setToUserID(long toUserID) {
        this.toUserID = toUserID;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public void setMetaInfoString(String metaInfoString) {
        this.metaInfoString = metaInfoString;
    }

    public static ChatMessage getChatMessageToSend(String metaInfoString, String toUserType, long toUserId, String messageType, String chatType) {
        ChatMessage chatMessage = getComanData(toUserId, toUserType, messageType);
        chatMessage.metaInfoString = metaInfoString;
        chatMessage.chatType = chatType;
        return chatMessage;
    }

    private static ChatMessage getComanData(long toUserId, String toUserType, String messageType) {
        UserDetailsApi api = UserDetailsApi.getInstance();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.fromUserID = Preferences.getUserId();
        chatMessage.setToUserID(toUserId);
        chatMessage.messageType = messageType;
        chatMessage.fromUserType = api.isTutor() ? AppConstants.USER_TYPES.TUTOR : AppConstants.USER_TYPES.TCENTRE;
        chatMessage.toUserType = toUserType;
        chatMessage.messageId = System.currentTimeMillis();
        return chatMessage;
    }


    public static ChatMessage sendIndividualTextToStudent(String metaInfoString, long toUserId) {
        return ChatMessage.getChatMessageToSend(metaInfoString, AppConstants.USER_TYPES.STUDENT, toUserId, AppConstants.META_TYPES.TEXT, INDIVIDUAL);
    }

    public static ChatMessage sendIndividualFileToStudent(String metaInfoString, long toUserId) {
        return ChatMessage.getChatFileToSend(metaInfoString, AppConstants.USER_TYPES.STUDENT, toUserId, INDIVIDUAL);
    }

    public synchronized JSONObject getJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fromUserType", fromUserType);
            jsonObject.put("toUserType", toUserType);
            jsonObject.put("fromUserId", fromUserID);
            jsonObject.put("fromUserName",UserDetailsApi.getInstance().getName());
            jsonObject.put("toUserId", toUserID);
//            jsonObject.put("toUserId",469);
            jsonObject.put("messageId", messageId);
            jsonObject.put("chatType", chatType);
            jsonObject.put("messageType", messageType);
            jsonObject.put("metaInfo", metaInfoString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public int getItemViewType() {
        switch (messageType) {
            case AppConstants.META_TYPES.TEXT:
                if(isOutGoing()){
                    return AppConstants.VIEW_TYPE.CHAT_TYPES.TEXT_MESSAGE_SEND;
                }else {
                    return AppConstants.VIEW_TYPE.CHAT_TYPES.TEXT_MESSAGE_RECEIVE;
                }
            case AppConstants.META_TYPES.FILE:
                if(isOutGoing()){
                    return AppConstants.VIEW_TYPE.CHAT_TYPES.MEDIA_MESSAGE_SEND;
                }else {
                    return AppConstants.VIEW_TYPE.CHAT_TYPES.MEDIA_MESSAGE_RECEIVE;
                }
        }
        return AppConstants.VIEW_TYPE.CHAT_TYPES.TEXT_MESSAGE_SEND;
    }

    public String getMetaInfoString() {
        return metaInfoString;
    }

    private static ChatMessage getChatFileToSend(String metaInfoString, String toUserType, long toUserId, String chatType) {
        ChatMessage chatMessage = getComanData(toUserId, toUserType, AppConstants.META_TYPES.FILE);
        chatMessage.metaInfoString = metaInfoString;
        chatMessage.chatType = chatType;
        return chatMessage;
    }

    public static ChatMessage getFromJson(String s) {
        ChatMessage chatMessage=new ChatMessage();
        try {
            JSONObject jsonObject=new JSONObject(s);

            if(jsonObject.has("fromUserType")){
                chatMessage.setFromUserType(jsonObject.getString("fromUserType"));
            }
            if(jsonObject.has("toUserType")){
                chatMessage.setToUserType(jsonObject.getString("toUserType"));
            }
            if(jsonObject.has("fromUserId")){
                chatMessage.setFromUserID(jsonObject.getInt("fromUserId"));
            }
            if(jsonObject.has("fromUserName")){
                chatMessage.setFromUserName(jsonObject.getString("fromUserName"));
            }
            if(jsonObject.has("toUserId")){
                chatMessage.setToUserID(jsonObject.getLong("toUserId"));
            }
            if(jsonObject.has("messageId")){
                chatMessage.setMessageId(jsonObject.getLong("messageId"));
            }
            if(jsonObject.has("chatType")){
                chatMessage.setChatType(jsonObject.getString("chatType"));
            }
            if(jsonObject.has("messageType")){
                chatMessage.setMessageType(jsonObject.getString("messageType"));
            }
            if(jsonObject.has("metaInfo")){
                chatMessage.setMetaInfoString(jsonObject.getString("metaInfo"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chatMessage;
    }

    public String getTimeString() {
        long timeStamp = messageId;
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("hh:mm a");
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        String format = simpleDateFormat.format(calendar.getTime());
        return format;
    }
}
