package faqulty.club.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

public class LocationApi extends BaseAutoFilter {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The cityId
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * @param cityId The cityId
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    /**
     * @return The locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     * @param locality The locality
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static List<LocationApi> parseJson(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        List<LocationApi> list = new ArrayList<>();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(x);
            LocationApi loc = (LocationApi) JsonUtil.parseJson(jsonObject.toString(), LocationApi.class);
            list.add(loc);
        }
        return list;
    }

    @Override
    public String getDisplayString() {
        return locality;
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        return this.locality.toLowerCase().contains(text.toLowerCase());
    }
}