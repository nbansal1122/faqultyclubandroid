package faqulty.club.models;

import java.util.List;

/**
 * Created by Neeraj Yadav on 10/15/2016.
 */

public class StudentModel {
    String title;
    List<StudentData> studentDataList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<StudentData> getStudentDataList() {
        return studentDataList;
    }

    public void setStudentDataList(List<StudentData> studentDataList) {
        this.studentDataList = studentDataList;
    }
}
