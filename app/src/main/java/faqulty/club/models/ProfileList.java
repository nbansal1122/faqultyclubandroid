package faqulty.club.models;

/**
 * Created by Neeraj Yadav on 10/14/2016.
 */

public class ProfileList {
    String pTitle, pDescription;

    public String getpTitle() {
        return pTitle;
    }

    public void setpTitle(String pTitle) {
        this.pTitle = pTitle;
    }

    public String getpDescription() {
        return pDescription;
    }

    public void setpDescription(String pDescription) {
        this.pDescription = pDescription;
    }
}
