package faqulty.club.models;

import faqulty.club.models.UserProfile.UserDetailsApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by Dhillon on 29-09-2016.
 */

public class SocialLoginModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private SocialLoginData data;
    @SerializedName("msg")
    @Expose
    private String msg;

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public SocialLoginData getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(SocialLoginData data) {
        this.data = data;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static SocialLoginData parseJson(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        if(obj != null && obj.getBoolean("status")){
            JSONObject user = obj.getJSONObject("data").getJSONObject("user");
            UserDetailsApi api = UserDetailsApi.parseJson(user.toString());
            return (SocialLoginData) JsonUtil.parseJson(json, SocialLoginModel.class);
        }
        throw new JSONException("Manual Exception");
    }

}

