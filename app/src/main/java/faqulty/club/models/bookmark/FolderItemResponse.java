package faqulty.club.models.bookmark;

import faqulty.club.models.BaseApi;

/**
 * Created by nbansal2211 on 22/12/16.
 */

public class FolderItemResponse extends BaseApi {
    private Folder data;

    public Folder getData() {
        return data;
    }

    public void setData(Folder data) {
        this.data = data;
    }
}
