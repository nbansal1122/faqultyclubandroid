package faqulty.club.models.bookmark;

import android.text.TextUtils;

import simplifii.framework.utility.AppConstants;
//"id": 8,
//        "moduleId": 3,
//        "moduleType": "feed",
//        "name": "Sample assignment 1",
//        "description": "Class 5, B.A. (Advertising and Brand Management), chapter 1"
public class Bookmark extends BaseBookmark {
    private String bTitle;
    private int id;
    private int moduleId;
    private String moduleType;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleType() {
        return moduleType;
    }

    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    public String getName() {
        if (TextUtils.isEmpty(name)) {
            return "Feed";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getbTitle() {
        return bTitle;
    }

    public void setbTitle(String bTitle) {
        this.bTitle = bTitle;
    }

    @Override
    public Integer getView() {
        return AppConstants.VIEW_TYPE.BOOKMARK;
    }
}
