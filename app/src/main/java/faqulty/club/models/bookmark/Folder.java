package faqulty.club.models.bookmark;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 12/10/2016.
 */

public class Folder extends BaseBookmark {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    private List<Bookmark> bookmarks;

    public List<Bookmark> getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(List<Bookmark> bookmarks) {
        this.bookmarks = bookmarks;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     *
     * @param tutorId
     * The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public Integer getView() {
        return AppConstants.VIEW_TYPE.FOLDER;
    }
}
