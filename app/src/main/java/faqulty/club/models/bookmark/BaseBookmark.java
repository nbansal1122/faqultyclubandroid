package faqulty.club.models.bookmark;

import java.io.Serializable;

/**
 * Created by Neeraj Yadav on 12/10/2016.
 */

public class BaseBookmark implements Serializable {
    Integer View;

    public Integer getView() {
        return View;
    }

    public void setView(Integer view) {
        View = view;
    }
}
