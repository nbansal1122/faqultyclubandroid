package faqulty.club.models.bookmark;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Neeraj Yadav on 12/10/2016.
 */

public class NewBookmarkFolder {


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private NewBookmarkData data;
    @SerializedName("msg")
    @Expose
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The data
     */
    public NewBookmarkData getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(NewBookmarkData data) {
        this.data = data;
    }
}
