package faqulty.club.models.bookmark;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedBookmark {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("data")
@Expose
private FeedBookmarkData data;
@SerializedName("msg")
@Expose
private String msg;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public FeedBookmarkData getData() {
return data;
}

public void setData(FeedBookmarkData data) {
this.data = data;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

}