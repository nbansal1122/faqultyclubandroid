package faqulty.club.models.bookmark;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Neeraj Yadav on 12/10/2016.
 */

public class Data {


    @SerializedName("bookmarks")
    @Expose
    private List<Bookmark> bookmarks = null;
    @SerializedName("folders")
    @Expose
    private List<Folder> folders = null;

    /**
     *
     * @return
     * The bookmarks
     */
    public List<Bookmark> getBookmarks() {
        return bookmarks;
    }

    /**
     *
     * @param bookmarks
     * The bookmarks
     */
    public void setBookmarks(List<Bookmark> bookmarks) {
        this.bookmarks = bookmarks;
    }

    /**
     *
     * @return
     * The folders
     */
    public List<Folder> getFolders() {
        return folders;
    }

    /**
     *
     * @param folders
     * The folders
     */
    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

}
