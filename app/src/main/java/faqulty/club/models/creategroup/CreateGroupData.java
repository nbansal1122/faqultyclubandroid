package faqulty.club.models.creategroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhillon on 05-11-2016.
 */
public class CreateGroupData {

    @SerializedName("students")
    @Expose
    private List<PhoneContact> students = new ArrayList<>();
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("pupilInfoId")
    @Expose
    private Integer pupilInfoId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("pupil_info_id")
    @Expose
    private Integer pupil_info_id;
    @SerializedName("pupilInfo")
    @Expose
    private PupilInfo pupilInfo;

    /**
     * @return The students
     */
    public List<PhoneContact> getStudents() {
        return students;
    }

    /**
     * @param students The students
     */
    public void setStudents(List<PhoneContact> students) {
        this.students = students;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The pupilInfoId
     */
    public Integer getPupilInfoId() {
        return pupilInfoId;
    }

    /**
     * @param pupilInfoId The pupilInfoId
     */
    public void setPupilInfoId(Integer pupilInfoId) {
        this.pupilInfoId = pupilInfoId;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The pupil_info_id
     */
    public Integer getPupil_info_id() {
        return pupil_info_id;
    }

    /**
     * @param pupil_info_id The pupil_info_id
     */
    public void setPupil_info_id(Integer pupil_info_id) {
        this.pupil_info_id = pupil_info_id;
    }

    /**
     * @return The pupilInfo
     */
    public PupilInfo getPupilInfo() {
        return pupilInfo;
    }

    /**
     * @param pupilInfo The pupilInfo
     */
    public void setPupilInfo(PupilInfo pupilInfo) {
        this.pupilInfo = pupilInfo;
    }

}
