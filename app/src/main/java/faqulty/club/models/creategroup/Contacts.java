package faqulty.club.models.creategroup;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dhillon on 04-12-2016.
 */

public class Contacts extends PhoneContact implements Serializable{
    private ArrayList<PhoneContact> phoneContact;

    public ArrayList<PhoneContact> getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(ArrayList<PhoneContact> phoneContact) {
        this.phoneContact = phoneContact;
    }
}
