package faqulty.club.models.creategroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

public class CreateGroupResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private CreateGroupData createGroupData;

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The createGroupData
     */
    public CreateGroupData getCreateGroupData() {
        return createGroupData;
    }

    /**
     * @param createGroupData The createGroupData
     */
    public void setCreateGroupData(CreateGroupData createGroupData) {
        this.createGroupData = createGroupData;
    }

    public static CreateGroupResponse getInstance() {
        CreateGroupResponse group = null;
        String groupJson = Preferences.getData(Preferences.GROUP, "");
        if (groupJson != null) {
            group = parseJson(groupJson);
        }
        return group;
    }

    public static CreateGroupResponse parseJson(String json) {
        CreateGroupResponse createGroupResponse = (CreateGroupResponse) JsonUtil.parseJson(json, CreateGroupResponse.class);
        if (createGroupResponse != null) {
            Preferences.saveData(Preferences.GROUP, json);
        }
        return createGroupResponse;
    }
}


