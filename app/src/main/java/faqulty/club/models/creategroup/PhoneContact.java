package faqulty.club.models.creategroup;

import android.text.TextUtils;

import faqulty.club.models.BaseAutoFilter;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

import java.io.Serializable;

import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 07/11/16.
 */

public class PhoneContact extends BaseAutoFilter implements Chip, Serializable {
    private String name;
    @SerializedName("phone")
    private String number;
    private String email;
    private String photo;
    private boolean isSelected = false;
    private int performance = 3;
    private int id;


    public PhoneContact(String name, String number, String imageUri) {
        this.name = name;
        this.number = number;
        this.photo = imageUri;
    }

    public PhoneContact() {

    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPerformance() {
        return performance;
    }

    public void setPerformance(int performance) {
        this.performance = performance;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
//        if(number.startsWith("+91"))
//            number = number.substring(3);
//        else if(number.startsWith("0"))
//            number = number.substring(1);
//        number = number.replace(" ","");
//        number = number.replace("-","");
//        number = number.replace(")","");
//        number = number.replace(")","");
        return Util.getModifiedNumber(number);
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String getText() {
        if (TextUtils.isEmpty(name)) {
            return number;
        }
        return name;
    }

    public String getDisplayName() {
        if (TextUtils.isEmpty(name)) {
            return number;
        }
        return name;
    }

    @Override
    public String getDisplayString() {
        return getDisplayName();
    }

    @Override
    public boolean isMatchingConstraint(String text) {
        if (TextUtils.isEmpty(text)) return true;
        return getDisplayName().contains(text);
    }


}