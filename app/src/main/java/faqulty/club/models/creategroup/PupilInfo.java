package faqulty.club.models.creategroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhillon on 05-11-2016.
 */
public class PupilInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("tuitionType")
    @Expose
    private String tuitionType;
    @SerializedName("billingCycle")
    @Expose
    private String billingCycle;
    @SerializedName("fee")
    @Expose
    private Integer fee;
    @SerializedName("classDurationHours")
    @Expose
    private Integer classDurationHours;
    @SerializedName("classDurationMins")
    @Expose
    private Integer classDurationMins;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The tuitionType
     */
    public String getTuitionType() {
        return tuitionType;
    }

    /**
     * @param tuitionType The tuitionType
     */
    public void setTuitionType(String tuitionType) {
        this.tuitionType = tuitionType;
    }

    /**
     * @return The billingCycle
     */
    public String getBillingCycle() {
        return billingCycle;
    }

    /**
     * @param billingCycle The billingCycle
     */
    public void setBillingCycle(String billingCycle) {
        this.billingCycle = billingCycle;
    }

    /**
     * @return The fee
     */
    public Integer getFee() {
        return fee;
    }

    /**
     * @param fee The fee
     */
    public void setFee(Integer fee) {
        this.fee = fee;
    }

    /**
     * @return The classDurationHours
     */
    public Integer getClassDurationHours() {
        return classDurationHours;
    }

    /**
     * @param classDurationHours The classDurationHours
     */
    public void setClassDurationHours(Integer classDurationHours) {
        this.classDurationHours = classDurationHours;
    }

    /**
     * @return The classDurationMins
     */
    public Integer getClassDurationMins() {
        return classDurationMins;
    }

    /**
     * @param classDurationMins The classDurationMins
     */
    public void setClassDurationMins(Integer classDurationMins) {
        this.classDurationMins = classDurationMins;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

}
