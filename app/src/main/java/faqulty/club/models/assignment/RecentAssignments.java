package faqulty.club.models.assignment;

import faqulty.club.models.assignment.assignments.AssignmentData;

import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 28/01/17.
 */

public class RecentAssignments extends BaseAdapterModel{

    private AssignmentData assignment;

    public AssignmentData getAssignment() {
        return assignment;
    }

    public void setAssignment(AssignmentData assignment) {
        this.assignment = assignment;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.RECENT_ASSIGNMENT;
    }

    private boolean isFirst, isLast;

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }
}
