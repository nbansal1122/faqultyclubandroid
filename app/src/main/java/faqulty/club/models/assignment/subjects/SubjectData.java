package faqulty.club.models.assignment.subjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dhillon on 03-12-2016.
 */
public class SubjectData implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("degreeSubject")
    @Expose
    private String degreeSubject;
    @SerializedName("chapterCount")
    @Expose
    private Integer chapterCount;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The degreeSubject
     */
    public String getDegreeSubject() {
        return degreeSubject;
    }

    /**
     * @param degreeSubject The degreeSubject
     */
    public void setDegreeSubject(String degreeSubject) {
        this.degreeSubject = degreeSubject;
    }

    /**
     * @return The chapterCount
     */
    public Integer getChapterCount() {
        return chapterCount;
    }

    /**
     * @param chapterCount The chapterCount
     */
    public void setChapterCount(Integer chapterCount) {
        this.chapterCount = chapterCount;
    }

}
