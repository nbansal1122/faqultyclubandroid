package faqulty.club.models.assignment.subjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SubjectsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<SubjectData> data = new ArrayList<SubjectData>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<SubjectData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<SubjectData> data) {
        this.data = data;
    }

}


