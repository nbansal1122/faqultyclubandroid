package faqulty.club.models.assignment;

import faqulty.club.models.BaseApi;

import java.util.List;

/**
 * Created by nbansal2211 on 20/12/16.
 */

public class QuestionListResponse extends BaseApi {

    private List<QuestionInfo> data;

    public List<QuestionInfo> getData() {
        return data;
    }

    public void setData(List<QuestionInfo> data) {
        this.data = data;
    }
}
