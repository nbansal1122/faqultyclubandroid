package faqulty.club.models.assignment;

import faqulty.club.models.BaseApi;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by nbansal2211 on 28/01/17.
 */

public class RecentAssignmentResponse extends BaseApi {

    private List<RecentAssignments> data = new ArrayList<>();

    public List<RecentAssignments> getData() {
        return data;
    }

    public void setData(List<RecentAssignments> data) {
        this.data = data;
    }

    public static RecentAssignmentResponse parseJson(String json){
        RecentAssignmentResponse response = (RecentAssignmentResponse) JsonUtil.parseJson(json, RecentAssignmentResponse.class);
        if(response != null && response.getData() != null && response.getData().size()>0){
            List<RecentAssignments> list = response.getData();
            list.get(0).setFirst(true);
            list.get(list.size()-1).setLast(true);
        }
        return response;
    }
}
