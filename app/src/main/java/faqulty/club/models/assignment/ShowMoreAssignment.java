package faqulty.club.models.assignment;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;

/**
 * Created by admin on 4/4/17.
 */

public class ShowMoreAssignment extends BaseAdapterModel {
    private List<BaseAdapterModel> remaining=new ArrayList<>();
    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.SHOW_MORE;
    }
    public void addAll(List<RecentAssignments> remaining){
        this.remaining.addAll(remaining);
    }

    public List<BaseAdapterModel> getRemaining() {
        return remaining;
    }
}
