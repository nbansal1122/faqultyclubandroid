package faqulty.club.models.assignment.sharing;

/**
 * Created by nbansal2211 on 30/01/17.
 */

public class AssignmentLink {

    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
