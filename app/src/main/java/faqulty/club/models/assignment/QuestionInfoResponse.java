package faqulty.club.models.assignment;

import faqulty.club.models.BaseApi;

/**
 * Created by nbansal2211 on 20/12/16.
 */

public class QuestionInfoResponse extends BaseApi {
    private QuestionInfo data;

    public QuestionInfo getData() {
        return data;
    }

    public void setData(QuestionInfo data) {
        this.data = data;
    }
}
