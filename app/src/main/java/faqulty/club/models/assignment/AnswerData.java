package faqulty.club.models.assignment;

/**
 * Created by nbansal2211 on 20/12/16.
 */

public class AnswerData {
//    "answer": {
//        "id": 2,
//                "questionId": 2,
//                "answer": "&angle;OPQ = 90&deg; (OP is radius and PQ is tangent. Tangent is perpendicular to radius at the point of contact)<br/><img src=\"ImagesTemplate CirclesMA_C_L1_Q5_Sol_20161208184329292460_T.PNG\"  other =\"ImagesTemplate CirclesMA_C_L1_Q5_Sol_20161208184329292460_M.PNG\" /><br/>In rt&#9651;OPQ<br/>i. OP<sup>2</sup> &plus; PQ<sup>2</sup> = OQ<sup>2</sup><br/>ii. (5)<sup>2</sup> &plus; PQ<sup>2</sup> = (12)<sup>2</sup><br/>iii. PQ<sup>2</sup> = 144 &ndash; 25 = 119<br/>iv. PQ = &plusmn;<math> <semantics>  <mrow>   <msqrt>    <mrow>     <mn>119</mn></mrow>   </msqrt>   </mrow> </semantics></math>cm",
//                "createdOn": "2016-12-17T14:59:29.000Z",
//                "question_id": 2

    private int id;
    private int questionId;
    private String answer;
    private String createdOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
