package faqulty.club.models.assignment.chapters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ChaptersResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<ChapterData> data = new ArrayList<ChapterData>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<ChapterData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<ChapterData> data) {
        this.data = data;
    }

}


