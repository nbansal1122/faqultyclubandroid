package faqulty.club.models.assignment.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dhillon on 20-12-2016.
 */
public class QuestionSet implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("assignmentId")
    @Expose
    private Integer assignmentId;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("assignment_id")
    @Expose
    private Integer assignment_id;
    @SerializedName("answer")
    @Expose
    private Answer answer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Integer assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getAssignment_id() {
        return assignment_id;
    }

    public void setAssignment_id(Integer assignment_id) {
        this.assignment_id = assignment_id;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

}
