package faqulty.club.models.assignment;

import android.text.TextUtils;

import faqulty.club.models.assignment.chapters.ChapterData;
import faqulty.club.models.assignment.subjects.SubjectData;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

import java.util.HashSet;
import java.util.List;

/**
 * Created by nbansal2211 on 20/12/16.
 */

public class QuestionInfo implements Chip {
    private ClassModel classInfo;
    private SubjectData subjectInfo;
    private ChapterData chapterInfo;

    private int id;
    private String label;
    private String name;
    private String createdOn;
    @SerializedName("class")
    private int classId;
    private int subject;
    private int chapter;

    private HashSet<String> hashTagSet = new HashSet<>();

    private String displayLabelString = "";

    private List<QuestionData> questionSet;
    private boolean selected;

    public void initHashTagList() {
        if (!TextUtils.isEmpty(label)) {
            String[] array = label.split(",");
            if (array != null) {
                StringBuilder builder = new StringBuilder();
                for (String s : array) {
                    s = s.replaceAll("#", "").trim();
                    hashTagSet.add("#" + s);
                    builder.append("#" + s).append(", ");
                }
                if (builder.length() > 0) {
                    builder.deleteCharAt(builder.length() - 1);
                    builder.deleteCharAt(builder.length() - 1);
                }
                displayLabelString = builder.toString();
            }
        }
    }

    public HashSet<String> getHashTagSet() {
        return hashTagSet;
    }


    public String getDisplayHashTagString() {
        return displayLabelString;
    }

    public List<QuestionData> getQuestionSet() {
        return questionSet;
    }

    public void setQuestionSet(List<QuestionData> questionSet) {
        this.questionSet = questionSet;
    }

    public ClassModel getClassInfo() {
        return classInfo;
    }

    public void setClassInfo(ClassModel classInfo) {
        this.classInfo = classInfo;
    }

    public SubjectData getSubjectInfo() {
        return subjectInfo;
    }

    public void setSubjectInfo(SubjectData subjectInfo) {
        this.subjectInfo = subjectInfo;
    }

    public ChapterData getChapterInfo() {
        return chapterInfo;
    }

    public void setChapterInfo(ChapterData chapterInfo) {
        this.chapterInfo = chapterInfo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    @Override
    public String getText() {
        return label;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
