package faqulty.club.models.assignment;

/**
 * Created by nbansal2211 on 20/12/16.
 */

public class QuestionData {

    private int id;
    private int assignmentId;
    private String question;
    private String createdOn;
    private AnswerData answer;

    public AnswerData getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerData answer) {
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
