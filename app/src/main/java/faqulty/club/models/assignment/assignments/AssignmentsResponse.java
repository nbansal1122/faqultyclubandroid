package faqulty.club.models.assignment.assignments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssignmentsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<AssignmentData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<AssignmentData> getData() {
        return data;
    }

    public void setData(List<AssignmentData> data) {
        this.data = data;
    }

}
