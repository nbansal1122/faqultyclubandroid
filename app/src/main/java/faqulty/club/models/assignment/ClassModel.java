package faqulty.club.models.assignment;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 12/2/2016.
 */

public class ClassModel extends BaseAdapterModel implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("sortOrder")
    @Expose
    private Integer sortOrder;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The _class
     */
    public String getClass_() {
        return _class;
    }

    /**
     * @param _class The class
     */
    public void setClass_(String _class) {
        this._class = _class;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The sortOrder
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder The sortOrder
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }


    public static Object parseJson(String json) {
        Log.d("parse category", "parse json call");
        List<ClassModel> assignmentModelList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                Gson gson = new Gson();
                ClassModel assignmentModel = gson.fromJson(jsonObject.toString(), ClassModel.class);
                if (i == 0) {
                    assignmentModel.setFirst(true);
                }
                assignmentModelList.add(assignmentModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return assignmentModelList;
    }

    private boolean isFirst;

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.CLASSES;
    }
}
