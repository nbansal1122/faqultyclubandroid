package faqulty.club.models.assignment.chapters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dhillon on 03-12-2016.
 */
public class ChapterData implements Serializable{

    @SerializedName("chapterId")
    @Expose
    private Integer chapterId;
    @SerializedName("chapterName")
    @Expose
    private String chapterName;
    @SerializedName("assignmentCount")
    @Expose
    private Integer assignmentCount;

    /**
     * @return The chapterId
     */
    public Integer getChapterId() {
        return chapterId;
    }

    /**
     * @param chapterId The chapterId
     */
    public void setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
    }

    /**
     * @return The chapterName
     */
    public String getChapterName() {
        return chapterName;
    }

    /**
     * @param chapterName The chapterName
     */
    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    /**
     * @return The assignmentCount
     */
    public Integer getAssignmentCount() {
        return assignmentCount;
    }

    /**
     * @param assignmentCount The assignmentCount
     */
    public void setAssignmentCount(Integer assignmentCount) {
        this.assignmentCount = assignmentCount;
    }

}
