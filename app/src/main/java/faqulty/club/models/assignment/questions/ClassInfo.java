package faqulty.club.models.assignment.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dhillon on 20-12-2016.
 */
public class ClassInfo implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("createdOn")
    @Expose
    private Object createdOn;
    @SerializedName("sortOrder")
    @Expose
    private Integer sortOrder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public Object getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Object createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

}
