package faqulty.club.models.assignment.sharing;

import faqulty.club.models.BaseApi;

/**
 * Created by nbansal2211 on 30/01/17.
 */

public class PDFLinkResponse extends BaseApi {
    private AssignmentLink data;

    public AssignmentLink getData() {
        return data;
    }

    public void setData(AssignmentLink data) {
        this.data = data;
    }
}
