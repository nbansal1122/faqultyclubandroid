package faqulty.club.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by admin on 3/28/17.
 */

public class ProfileComments extends BaseApi {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_tution_center")
    @Expose
    private String isTutionCenter;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("reviews")
    @Expose
    private String reviews;
    @SerializedName("image")
    @Expose
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsTutionCenter() {
        return isTutionCenter;
    }

    public void setIsTutionCenter(String isTutionCenter) {
        this.isTutionCenter = isTutionCenter;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static List<ProfileComments> parseJson(String json) {
        List<ProfileComments> profileCommentses = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);
            Gson gson = new Gson();
            for (int x = 0; x < jsonArray.length(); x++) {
                ProfileComments profileComments = gson.fromJson(jsonArray.getJSONObject(x).toString(), ProfileComments.class);
                profileCommentses.add(profileComments);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (profileCommentses.size() > 0) {
            Preferences.saveData(AppConstants.PREF_KEYS.COMMENTS_COUNT, profileCommentses.size());
        }

        return profileCommentses;
    }
}
