package faqulty.club.models.invoice;

/**
 * Created by nbansal2211 on 17/11/16.
 */
//"noOfSessions": 7,
//        "noOfHours": 12
public class InvoiceData {
    private int noOfSessions;
    private int noOfHours;
    private int noOfMins;
    private double amount;

    public void setNoOfMins(int noOfMins) {
        this.noOfMins = noOfMins;
    }

    public long getAmount() {
        return (long) amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getNoOfMins() {
        return noOfMins;
    }

    public int getNoOfSessions() {
        return noOfSessions;
    }

    public void setNoOfSessions(int noOfSessions) {
        this.noOfSessions = noOfSessions;
    }

    public int getNoOfHours() {
        return noOfHours;
    }

    public void setNoOfHours(int noOfHours) {
        this.noOfHours = noOfHours;
    }
}
