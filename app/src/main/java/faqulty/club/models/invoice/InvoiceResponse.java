package faqulty.club.models.invoice;

import faqulty.club.models.BaseApi;

/**
 * Created by nbansal2211 on 17/11/16.
 */

public class InvoiceResponse extends BaseApi {

    private InvoiceData data;

    public InvoiceData getData() {
        return data;
    }

    public void setData(InvoiceData data) {
        this.data = data;
    }
}
