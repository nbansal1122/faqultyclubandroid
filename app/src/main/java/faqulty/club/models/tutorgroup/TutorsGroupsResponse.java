package faqulty.club.models.tutorgroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhillon on 07-11-2016.
 */
public class TutorsGroupsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<TutorGroupData> data = new ArrayList<TutorGroupData>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<TutorGroupData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<TutorGroupData> data) {
        this.data = data;
    }

}
