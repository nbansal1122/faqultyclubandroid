package faqulty.club.models.tutorgroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.plumillonforge.android.chipview.Chip;

import simplifii.framework.requestmodels.BaseAdapterModel;
import simplifii.framework.utility.AppConstants;

public class TutorGroupData extends BaseAdapterModel implements Chip {

    @SerializedName("isBdayToday")
    @Expose
    private Boolean isBdayToday;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("pupilInfoId")
    @Expose
    private Integer pupilInfoId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("pupil_info_id")
    @Expose
    private Integer pupil_info_id;


    /**
     * @return The isBdayToday
     */
    public Boolean getIsBdayToday() {
        return isBdayToday;
    }

    /**
     * @param isBdayToday The isBdayToday
     */
    public void setIsBdayToday(Boolean isBdayToday) {
        this.isBdayToday = isBdayToday;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The pupilInfoId
     */
    public Integer getPupilInfoId() {
        return pupilInfoId;
    }

    /**
     * @param pupilInfoId The pupilInfoId
     */
    public void setPupilInfoId(Integer pupilInfoId) {
        this.pupilInfoId = pupilInfoId;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The pupil_info_id
     */
    public Integer getPupil_info_id() {
        return pupil_info_id;
    }

    /**
     * @param pupil_info_id The pupil_info_id
     */
    public void setPupil_info_id(Integer pupil_info_id) {
        this.pupil_info_id = pupil_info_id;
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.GROUP;
    }
}


