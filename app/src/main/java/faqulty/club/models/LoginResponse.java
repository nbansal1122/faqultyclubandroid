package faqulty.club.models;

/**
 * Created by saurabh on 17-09-2016.
 */
public class LoginResponse extends BaseApi{
    String token;
    long userId;

    public String getToken() {
        return token;
    }

    public long getUserId() {
        return userId;
    }
}
