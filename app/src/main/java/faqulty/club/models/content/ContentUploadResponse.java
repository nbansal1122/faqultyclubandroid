package faqulty.club.models.content;

import faqulty.club.models.BaseApi;
import faqulty.club.models.UserProfile.FileUploadResponse;

/**
 * Created by nbansal2211 on 21/11/16.
 */

public class ContentUploadResponse extends BaseApi {
    private FileUploadResponse data;

    public FileUploadResponse getData() {
        return data;
    }

    public void setData(FileUploadResponse data) {
        this.data = data;
    }
}
