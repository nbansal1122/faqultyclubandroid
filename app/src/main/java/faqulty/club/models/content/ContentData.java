package faqulty.club.models.content;

import simplifii.framework.requestmodels.BaseAdapterModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.utility.AppConstants;

public class ContentData extends BaseAdapterModel implements Serializable {

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.CONTENT;
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tutorId")
    @Expose
    private Integer tutorId;
    @SerializedName("contentUrl")
    @Expose
    private String contentUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("numViews")
    @Expose
    private Integer numViews;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("tutor_id")
    @Expose
    private Integer tutor_id;
    @SerializedName("mimetype")
    @Expose
    private String mimeType;

    private boolean showPencil = true;

    public boolean isShowPencil() {
        return showPencil;
    }

    public void setShowPencil(boolean showPencil) {
        this.showPencil = showPencil;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The tutorId
     */
    public Integer getTutorId() {
        return tutorId;
    }

    /**
     * @param tutorId The tutorId
     */
    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * @return The contentUrl
     */
    public String getContentUrl() {
        return contentUrl;
    }

    /**
     * @param contentUrl The contentUrl
     */
    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The numViews
     */
    public Integer getNumViews() {
        return numViews;
    }

    /**
     * @param numViews The numViews
     */
    public void setNumViews(Integer numViews) {
        this.numViews = numViews;
    }

    /**
     * @return The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     * @param updatedOn The updatedOn
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The tutor_id
     */
    public Integer getTutor_id() {
        return tutor_id;
    }

    /**
     * @param tutor_id The tutor_id
     */
    public void setTutor_id(Integer tutor_id) {
        this.tutor_id = tutor_id;
    }

}