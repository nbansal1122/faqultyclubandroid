package faqulty.club.models;

import faqulty.club.models.pemphlets.UserPamphletsData;

/**
 * Created by admin on 2/18/17.
 */
public class GenerateTemplateResponse extends BaseApi{
    private boolean status;
    UserPamphletsData data;

    public boolean isStatus() {
        return status;
    }

    public UserPamphletsData getData() {
        return data;
    }
}
