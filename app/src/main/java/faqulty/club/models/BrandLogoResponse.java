package faqulty.club.models;

/**
 * Created by admin on 2/18/17.
 */
public class BrandLogoResponse extends BaseApi{
    private boolean status;
    private LogoData data;

    public boolean isStatus() {
        return status;
    }

    public LogoData getData() {
        return data;
    }
}
