package faqulty.club.models.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCommentApi {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("data")
@Expose
private String data;
@SerializedName("msg")
@Expose
private String msg;

/**
* 
* @return
* The status
*/
public Boolean getStatus() {
return status;
}

/**
* 
* @param status
* The status
*/
public void setStatus(Boolean status) {
this.status = status;
}

/**
* 
* @return
* The data
*/
public String getData() {
return data;
}

/**
* 
* @param data
* The data
*/
public void setData(String data) {
this.data = data;
}

/**
* 
* @return
* The msg
*/
public String getMsg() {
return msg;
}

/**
* 
* @param msg
* The msg
*/
public void setMsg(String msg) {
this.msg = msg;
}

}