package faqulty.club.models.comment;

import faqulty.club.models.feed.Tutor;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Neeraj Yadav on 12/13/2016.
 */

public class CommentData {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("moduleId")
    @Expose
    private Integer moduleId;
    @SerializedName("moduleType")
    @Expose
    private String moduleType;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;
    @SerializedName("module_id")
    @Expose
    private Integer module_id;
    @SerializedName("user_id")
    @Expose
    private Integer user_id;

    private Tutor user;

    public Tutor getUser() {
        return user;
    }

    public void setUser(Tutor user) {
        this.user = user;
    }

    private List<PostCommentData> postCommentDatas = new ArrayList<>();

    public List<PostCommentData> getPostCommentDatas() {
        return postCommentDatas;
    }

    public void setPostCommentDatas(List<PostCommentData> postCommentDatas) {
        this.postCommentDatas = postCommentDatas;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The moduleId
     */
    public Integer getModuleId() {
        return moduleId;
    }

    /**
     *
     * @param moduleId
     * The moduleId
     */
    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    /**
     *
     * @return
     * The moduleType
     */
    public String getModuleType() {
        return moduleType;
    }

    /**
     *
     * @param moduleType
     * The moduleType
     */
    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     *
     * @return
     * The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     *
     * @param updatedOn
     * The updatedOn
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     *
     * @return
     * The module_id
     */
    public Integer getModule_id() {
        return module_id;
    }

    /**
     *
     * @param module_id
     * The module_id
     */
    public void setModule_id(Integer module_id) {
        this.module_id = module_id;
    }

    /**
     *
     * @return
     * The user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     *
     * @param user_id
     * The user_id
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}
