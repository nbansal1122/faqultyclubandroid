package faqulty.club.models.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Neeraj Yadav on 12/13/2016.
 */

public class PostCommentData {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("moduleId")
    @Expose
    private Integer moduleId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("moduleType")
    @Expose
    private String moduleType;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("updatedOn")
    @Expose
    private String updatedOn;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The moduleId
     */
    public Integer getModuleId() {
        return moduleId;
    }

    /**
     *
     * @param moduleId
     * The moduleId
     */
    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The moduleType
     */
    public String getModuleType() {
        return moduleType;
    }

    /**
     *
     * @param moduleType
     * The moduleType
     */
    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     *
     * @return
     * The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     *
     * @param updatedOn
     * The updatedOn
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
}
