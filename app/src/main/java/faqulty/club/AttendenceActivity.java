package faqulty.club;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import faqulty.club.R;

import faqulty.club.fragments.attendance.SessionFragment;
import faqulty.club.models.SessionDetail;
import faqulty.club.models.attendance.AttendanceResponse;
import faqulty.club.models.tutorstudent.StudentBaseClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class AttendenceActivity extends BaseActivity {

    List<SessionDetail> sessionList = new ArrayList<>();
    private TextView tvNextDay, tvPreviousDay, tv_date;
    private SimpleDateFormat sdf;
    private Calendar c;
    private LinearLayout ll_session;
    private StudentBaseClass student;
    private Button btn_save;
    private HashMap<String, List<AttendanceResponse>> mapAttendanceData = new HashMap<>();
    private HashMap<String, List<SessionFragment>> mapAttendanceFragments = new HashMap<>();
    private List<AttendanceResponse> attendanceResponseList = new ArrayList<>();

    private String incomingDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);
        initToolBar(getString(R.string.title_mark_attendance));
        Bundle bundle = getIntent().getExtras();
        c = Calendar.getInstance();
        tvPreviousDay = (TextView) findViewById(R.id.tv_previous_day);
        tvNextDay = (TextView) findViewById(R.id.tv_next_day);
        ll_session = (LinearLayout) findViewById(R.id.ll_session);
        tv_date = (TextView) findViewById(R.id.tv_date);
        sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

        setOnClickListener(R.id.tv_previous_day, R.id.tv_next_day, R.id.btn_save_session, R.id.tv_new_session);
        if (bundle != null) {
            student = (StudentBaseClass) bundle.getSerializable(AppConstants.BUNDLE_KEYS.STUDENT);
            incomingDate = bundle.getString(AppConstants.BUNDLE_KEYS.SESSION_DATE);
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAttendanceData();
    }

    private void getAttendanceData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        String attendanceUrl = String.format(AppConstants.PAGE_URL.GETATTENDANCE, String.valueOf(student.getId()), String.valueOf(Preferences.getUserId()));
        httpParamObject.setUrl(attendanceUrl);
        httpParamObject.addParameter("offset", "0");
        httpParamObject.addParameter("count", "100");
        httpParamObject.setClassType(AttendanceResponse.class);
        executeTask(AppConstants.TASKCODES.GETATTENDANCE, httpParamObject);
    }

    private void setDataForGivenDate(String date) {
        try {
            Date d = sdf.parse(date);
            c.setTime(d);
            tv_date.setText(date);
            hideNextLabelIfRequired(date);
            setDataBasedOnDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GETATTENDANCE:
                if (response != null) {
                    attendanceResponseList = (List<AttendanceResponse>) response;
                    if (attendanceResponseList != null) {
                        createAttendanceMap();
                    }
                    if (!TextUtils.isEmpty(incomingDate)) {
                        setDataForGivenDate(incomingDate);
                    } else {
                        setDataForCurrentDate();
                    }
                }
                break;
            case AppConstants.TASKCODES.MARK_ATTENDANCE:
                setResult(RESULT_OK);
                showToast(R.string.sessions_updated_successfully);
                finish();
                break;

        }
    }

    private void setDataForCurrentDate() {
        String date = getDate(0);
        setDataBasedOnDate(date);
    }

    private String getDate(int dayAdditionValue) {
        c.add(Calendar.DAY_OF_MONTH, dayAdditionValue);
        String dateString = sdf.format(c.getTime());
        tv_date.setText(dateString);
        hideNextLabelIfRequired(dateString);
        return dateString;
    }

    private void hideNextLabelIfRequired(String selectedDateString) {
        Date currentDate = new Date();
        String dateString = sdf.format(currentDate);
        if (dateString.equalsIgnoreCase(selectedDateString)) {
            findViewById(R.id.tv_next_day).setVisibility(View.INVISIBLE);
            findViewById(R.id.tv_next_day).setOnClickListener(null);
        } else {
            findViewById(R.id.tv_next_day).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_next_day).setOnClickListener(this);
        }
    }

    private void createAttendanceMap() {
        for (int i = 0; i < attendanceResponseList.size(); i++) {
            AttendanceResponse response = attendanceResponseList.get(i);
            List<AttendanceResponse> mapList;
            if (mapAttendanceData.containsKey(response.getCreatedOn())) {
                mapList = mapAttendanceData.get(response.getCreatedOn());
            } else {
                mapList = new ArrayList<>();
            }
            mapList.add(response);
            mapAttendanceData.put(response.getCreatedOn(), mapList);
        }
    }

    private void setDataBasedOnDate(String date) {
        ll_session.removeAllViewsInLayout();
        if (mapAttendanceFragments.containsKey(date)) {
            List<SessionFragment> fragments = mapAttendanceFragments.get(date);
            List<SessionFragment> copyList = new ArrayList<>();
            for (SessionFragment f : fragments) {
                copyList.add(f.copyFragment());
            }
            mapAttendanceFragments.put(date, copyList);
            for (SessionFragment f : copyList) {
                addSessionFragment(f);
            }
        } else if (mapAttendanceData.containsKey(date)) {
            // Check if date exists in attendance coming from server
            List<AttendanceResponse> list = mapAttendanceData.get(date);
            for (int i = 0; i < list.size(); i++) {
                AttendanceResponse response = list.get(i);
                addSessionToLayout(response, date, (i == 0), i);
            }
        } else {
            addSessionToLayout(null, date, true, 0);
        }
    }


    private void addSessionFragment(SessionFragment fragment) {
//        FrameLayout layout = getFrame();
//        ll_session.addView(layout, ll_session.getChildCount());
//        if (fragment.isAdded()) {
//            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//        }
        getSupportFragmentManager().beginTransaction().add(R.id.ll_session, fragment).commitAllowingStateLoss();
//        ll_session.postInvalidate();
//        ll_session.requestLayout();
    }

    private FrameLayout getFrame() {
        return (FrameLayout) LayoutInflater.from(this).inflate(R.layout.row_session_frame, null);
    }

    private void addSessionToLayout(AttendanceResponse attendanceResponse, String date, boolean isNewSession, int counter) {
        SessionFragment f = SessionFragment.getInstance(attendanceResponse, date, isNewSession, counter, student
                .getId());
        List<SessionFragment> fragments = null;
        if (mapAttendanceFragments.containsKey(date)) {
            fragments = mapAttendanceFragments.get(date);
        } else {
            fragments = new ArrayList<>();
        }
        fragments.add(f);
        mapAttendanceFragments.put(date, fragments);
        addSessionFragment(f);
    }

    private void saveSessionForSelectedDate() {
        String date = getTextFromTV(R.id.tv_date);
        if (mapAttendanceFragments.containsKey(date)) {
            List<SessionFragment> fragments = mapAttendanceFragments.get(date);
            for (SessionFragment s : fragments) {
                if (!s.isValid()) {
                    return;
                }
            }
            saveSessions(fragments);
        } else {
            showToast("No New Session");
        }
    }

    private void saveSessions(List<SessionFragment> fragments) {
        if (fragments.size() == 0) return;
        List<HttpParamObject> objects = new ArrayList<>();
        for (SessionFragment f : fragments) {
//            HttpParamObject obj = new HttpParamObject();
//
//            if (f.attendanceResponse != null) {
//                obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE + "/" + f.attendanceResponse.getSessionId());
//                obj.setPutMethod();
//            } else {
//                obj.setUrl(AppConstants.PAGE_URL.ATTENDANCE);
//                obj.setPostMethod();
//            }
//
//            try {
//                obj.setJson(f.getSessionJson().toString());
//                obj.setJSONContentType();
//                objects.add(obj);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
            f.postOrUpdateSession();
        }

//        executeTask(AppConstants.TASKCODES.MARK_ATTENDANCE, objects);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_previous_day:
                setDataBasedOnDate(getDate(-1));
                break;
            case R.id.tv_next_day:
                setDataBasedOnDate(getDate(1));
                break;
            case R.id.btn_save_session:
                saveSessionForSelectedDate();
                break;
            case R.id.tv_new_session:
                String date = getDate(0);
                addSessionToLayout(null, date, true, mapAttendanceFragments.get(date).size());
                break;
        }
    }
}
