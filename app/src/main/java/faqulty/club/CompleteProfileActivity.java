package faqulty.club;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import faqulty.club.R;

import faqulty.club.activity.HomeActivity;
import faqulty.club.autocomplete.CityLocationSegment;
import faqulty.club.models.UserProfile.TutorLocation;
import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by saurabh on 13-09-2016.
 */
public class CompleteProfileActivity extends BaseActivity {
    CityLocationSegment f;
    EditText email, name;
    //    AutoCompleteTextView city, location;
//    Spinner gender;
    //    MultiAutoCompleteTextView courses;
    Button signup;
    LinearLayout cityLocationRow;
    CityLocationSegment citySegament;
    private UserDetailsApi user;
    private RadioButton selectedRadioButton;
    private String genderSelect;
    private RadioButton btnMaleGender, btnFemaleGender;
    private TextView tvFooter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complete_profile);
        Preferences.saveData(AppConstants.PREF_KEYS.BASIC_PROFILE_REQUIRED, true);
        initToolBar("Complete Your Profile");
        getHomeIcon(R.mipmap.arrows);

        user = UserDetailsApi.getInstance();
        tvFooter = (TextView) findViewById(R.id.base1);
        email = (EditText) findViewById(R.id.et_email_id);
        name = (EditText) findViewById(R.id.et_name);
        SpannableString spannableString = new SpannableString(getString(R.string.by_signing_up_you_agree_to_all));
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 32, 50, 0);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 54, 69, 0);
        tvFooter.setMovementMethod(LinkMovementMethod.getInstance());
        tvFooter.setText(spannableString);
//        city = (AutoCompleteTextView) findViewById(R.id.city);
//        location = (AutoCompleteTextView) findViewById(R.id.locality);
//        gender = (AppCompatSpinner) findViewById(R.id.gender_spinner);

        setUpGender();
//        courses = (MultiAutoCompleteTextView) findViewById(R.id.courses_spinner);
        signup = (Button) findViewById(R.id.btn_save_details);

        if (null != user) {
            if (user.getName() != null)
                name.setText(user.getName());
            if (user.getEmail() != null)
                email.setText(user.getEmail());
        }
        addTutorLocation(null);
        setOnClickListener(R.id.btn_save_details);
    }

    @Override
    protected void onHomePressed() {

    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.fc_icon;
    }

    private void setUpGender() {
        if (user == null) {
            return;
        }
        if (user.isTutor()) {
            btnMaleGender = (RadioButton) findViewById(R.id.rb_male);
//            btnMaleGender.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        genderSelect = "Male";
//                    } else {
//                        genderSelect = "Female";
//                    }
//                }
//            });
        } else {
            hideVisibility(R.id.ll_gender, R.id.tv_gender);
        }
    }

    private void addTutorLocation(TutorLocation location) {
        f = CityLocationSegment.getInstance(location, true);
        getSupportFragmentManager().beginTransaction().add(R.id.segament_city, f).commitAllowingStateLoss();
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_save_details:
                if (getEditText(R.id.et_email_id).length() < 256) {
                    if (Util.isValidEmail(getEditText(R.id.et_email_id))) {
                        if (isValidName()) {
                            completeProfile();
                        }
                    } else {
                        setError(R.id.et_email_id, "Enter a valid email id");
                    }
                } else
                    setError(R.id.et_email_id, "Email id can't exceed 256 characters");
        }
    }

    private boolean isValidName() {
        String name = getEditText(R.id.et_name);
        if (TextUtils.isEmpty(name)) {
            showToast(R.string.empty_name);
            return false;
        }
        return true;
    }


    @Override
    protected void setError(int editTextId, String error) {
        showToast(error);
    }

    private void completeProfile() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUserTypeUrl("basic");
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setContentType("application/json");
        httpParamObject.setJson(getJsonObject().toString());
        executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
    }

    private JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject j = new JSONObject();
        try {
            jsonObject.put("name", getEditText(R.id.et_name));
            jsonObject.put("email", getEditText(R.id.et_email_id));
            if (user.isTutor()) {
                jsonObject.put("userDetails", " ");
                if (btnMaleGender.isChecked()) {
                    jsonObject.put("gender", "Male");
                } else {
                    jsonObject.put("gender", "Female");
                }

            }
            j = f.getJsonObject();
            if (j != null) {
                jsonArray.put(j);
            }
            jsonObject.put("tutorLocation", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    Preferences.saveData(AppConstants.PREF_KEYS.BASIC_PROFILE_REQUIRED, false);
                    showToast("Profile successfully updated");
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.putExtra(AppConstants.BUNDLE_KEYS.TYPE, 1);
                    startActivity(intent);
                    finish();
                }
                break;
        }

    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }
}