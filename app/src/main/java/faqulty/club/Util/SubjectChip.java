package faqulty.club.Util;

import faqulty.club.models.UserProfile.SubjectObject;
import com.plumillonforge.android.chipview.Chip;

/**
 * Created by Dhillon on 16-12-2016.
 */

public class SubjectChip implements Chip {
    private SubjectObject subjectObject;

    public SubjectObject getSubjectObject() {
        return subjectObject;
    }

    public void setSubjectObject(SubjectObject subjectObject) {
        this.subjectObject = subjectObject;
    }

    @Override
    public String getText() {
        return subjectObject.getDisplayString();
    }
}
