package faqulty.club.Util;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import faqulty.club.R;

/**
 * Created by admin on 2/17/17.
 */

public class PamphletFieldView {
    private final LayoutInflater inflater;
    private String label;
    private int maxLimit;
    private String text;
    private String key;
    private Context context;
    private int inputType;

    public PamphletFieldView(String label, String key, int maxLimit, String text, int inputType, Context context) {
        this.label = label;
        this.key = key;
        this.maxLimit = maxLimit;
        this.text = text;
        this.context = context;
        this.inputType = inputType;
        inflater = LayoutInflater.from(context);
    }

    public View getView() {
        View view = inflater.inflate(R.layout.view_pamphlet, null);
        TextView tvLabel = (TextView) view.findViewById(R.id.tv_label);
        tvLabel.setText(label);
        final TextView tvCounter = (TextView) view.findViewById(R.id.tv_counter);
        tvCounter.setText("0/" + maxLimit);
        EditText editText = (EditText) view.findViewById(R.id.et_pamphlet);
        setEditTextMaxLength(maxLimit, editText);
        if (inputType == 1) {
            editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
//        if (inputType == 2) {
//            editText.setInputType(InputType.TYPE_CLASS_PHONE);
//        }
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvCounter.setText(charSequence.length() + "/" + maxLimit);
                text = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        if (!TextUtils.isEmpty(text)) {
            editText.setText(text);
        }
        return view;
    }

    public void setEditTextMaxLength(int length, EditText editText) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(FilterArray);
    }

    public String getText() {
        return text;
    }

    public boolean isValidate() {
        if (TextUtils.isEmpty(text)) {
            Toast.makeText(context, label + " cannot be empty !", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (inputType == 1) {
            if (!simplifii.framework.utility.Util.isValidEmail(text)) {
                Toast.makeText(context, context.getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
//        if(inputType==2){
//            if(text.length()!=10){
//                Toast.makeText(context, context.getString(R.string.invalid_phone_number), Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        }
        return true;
    }

    public String getKey() {
        return key;
    }
}
