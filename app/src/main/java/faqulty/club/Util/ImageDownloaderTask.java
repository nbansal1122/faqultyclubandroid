package faqulty.club.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import simplifii.framework.utility.Util;

public class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {
    private Context context;
    private ProgressDialog pd;
    private OnImageDownLoadedListener onImageDownLoadedListener;

    public ImageDownloaderTask(Context context, OnImageDownLoadedListener onImageDownLoadedListener) {
        this.context = context;
        this.onImageDownLoadedListener = onImageDownLoadedListener;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadBitmap(params[0]);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        showProgressDialog();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        hideProgressDialog();
        if(bitmap==null){
            onImageDownLoadedListener.onFailed();
        }else {
            try {
                File file = Util.getFile(bitmap, "Templets");
                if(file.exists()){
                    onImageDownLoadedListener.onLoaded(file.getPath());
                }
            } catch (Exception e) {
                e.printStackTrace();
                onImageDownLoadedListener.onFailed();
            }
        }
    }

    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();

            int statusCode = urlConnection.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        } catch (Exception e) {
            Log.d("URLCONNECTIONERROR", e.toString());
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            Log.w("ImageDownloader", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();

            }
        }
        return null;
    }

    private void showProgressDialog() {
        pd = new ProgressDialog(context);
        pd.setMessage("Please wait...");
        pd.show();
    }

    private void hideProgressDialog() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

    public interface OnImageDownLoadedListener {
        void onLoaded(String path);
        void onFailed();
    }
}