package faqulty.club.Util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import faqulty.club.models.creategroup.PhoneContact;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 11/11/16.
 */

public class AppUtil {

    public static PhoneContact getContactDataFromIntent(Intent data, Context context) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            String image = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = context.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int emailIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int imageUri = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            image = cursor.getString(imageUri);

            PhoneContact c = new PhoneContact(name, phoneNo, image);
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int parseStringToInt(String str) {
        if (TextUtils.isEmpty(str))
            return 0;
        int i = 0;
        i = Integer.parseInt(str);
        return i;
    }

    public static void setTabFont(PagerSlidingTabStrip tabStrip) {
        Typeface typeface = Typeface.createFromAsset(tabStrip.getContext().getAssets(), "fonts/" + AppConstants.DEF_REGULAR_FONT);
        List<TextView> textViews = getTextViews(tabStrip);
        for(TextView textView:textViews){
            textView.setTypeface(typeface);
        }
    }
    public static List<TextView> getTextViews(HorizontalScrollView rootView) {
        List<TextView> textViewList=new ArrayList<>();
        if(rootView.getChildCount()>0){
            LinearLayout linearLayout = (LinearLayout) rootView.getChildAt(0);
            for(int x=0;x<linearLayout.getChildCount();x++){
                View view = linearLayout.getChildAt(x);
                if(view instanceof TextView){
                    textViewList.add((TextView) view);
                }
            }
        }
        return textViewList;
    }

}
