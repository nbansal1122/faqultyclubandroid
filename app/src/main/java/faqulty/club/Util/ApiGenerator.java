package faqulty.club.Util;

import faqulty.club.models.BaseApi;
import faqulty.club.models.BasicResponse;
import faqulty.club.models.notifications.NotificationResponse;
import faqulty.club.models.transaction.CreateTransactionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by raghu on 24/1/17.
 */

public class ApiGenerator {
    public static HttpParamObject sendReminder(String type, List<String> modes, int studentId,String notificationType,long transactionId) {
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(String.format(AppConstants.PAGE_URL.GET_STUDENT_TRANSACTION_REMINDER, String.valueOf(Preferences.getUserId()), String.valueOf(studentId)));
        obj.setPostMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", type);
            jsonObject.put("notificationType", notificationType);
            jsonObject.put("transactionId", transactionId);
            JSONArray modeArray = new JSONArray();
            for (String s : modes) {
                modeArray.put(s);
            }
            jsonObject.put("modes", modeArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        obj.setJson(jsonObject.toString());
        obj.setJSONContentType();
        obj.setClassType(CreateTransactionResponse.class);
        obj.setClassType(BaseApi.class);
        return obj;
    }

    public static HttpParamObject getNotifications() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GET_NOTIFICATIONS, Preferences.getUserId()));
        httpParamObject.setClassType(NotificationResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject sareToEmail(int assignmentId, String moduaType, String subType) {
        HttpParamObject httpParamObject=new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EMAIL_SHARE);
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("userId",Preferences.getUserId());
            jsonObject.put("moduleId",assignmentId);
            jsonObject.put("moduleType",moduaType);
            jsonObject.put("subType",subType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BasicResponse.class);
        return httpParamObject;
    }
}
