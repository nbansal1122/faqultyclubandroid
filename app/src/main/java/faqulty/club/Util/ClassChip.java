package faqulty.club.Util;

import faqulty.club.models.UserProfile.ClassObject;
import com.plumillonforge.android.chipview.Chip;

import java.util.List;

/**
 * Created by Dhillon on 16-12-2016.
 */

public class ClassChip implements Chip {

    private boolean needToAppendClassString = true;

    public ClassChip() {

    }

    public ClassChip(boolean needToAppendClassString) {
        this.needToAppendClassString = needToAppendClassString;
    }

    public List<ClassObject> getClassObjectList() {
        return classObjectList;
    }

    public void setClassObjectList(List<ClassObject> classObjectList) {
        this.classObjectList = classObjectList;
    }

    private List<ClassObject> classObjectList;

    @Override
    public String getText() {
        if (classObjectList != null) {
            if (classObjectList.size() > 1)
                return classObjectList.get(0).getClassVal() + "-" + classObjectList.get(classObjectList.size() - 1).getClassVal();
            else {
                if (needToAppendClassString) {
                    return classObjectList.get(0).getDisplayString();
                } else {
                    return classObjectList.get(0).getClassVal();
                }
            }
        }
        return null;
    }
}
