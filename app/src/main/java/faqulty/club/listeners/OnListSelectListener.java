package faqulty.club.listeners;

import java.util.List;

import simplifii.framework.requestmodels.BaseAdapterModel;

/**
 * Created by raghu on 7/1/17.
 */

public interface OnListSelectListener {
    void onSelectListener(List<BaseAdapterModel> list);
}
