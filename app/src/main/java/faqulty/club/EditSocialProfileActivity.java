package faqulty.club;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;

import faqulty.club.R;

import faqulty.club.models.UserProfile.UserDetailsApi;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by saurabh on 06-09-2016.
 */
public class EditSocialProfileActivity extends BaseActivity {
    private Button saveChanges;
    private UserDetailsApi user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_social_profile);
        saveChanges = (Button) findViewById(R.id.btn_save_changes);

        initToolBar("Edit social profiles");
        getHomeIcon(R.mipmap.arrows);

        user = UserDetailsApi.getInstance();
        setDataInputTextLayout(R.id.til_linkedinURL, user.getLnlink());
        setDataInputTextLayout(R.id.til_twitterURL, user.getTwitterlink());
        setDataInputTextLayout(R.id.til_facebookURL, user.getFblink());
        setDataInputTextLayout(R.id.til_facultyclubURL, user.getFacultyProfileURI());
        findViewById(R.id.til_facultyclubURL).setEnabled(false);
        setOnClickListener(R.id.btn_save_changes, R.id.til_linkedinURL, R.id.til_twitterURL, R.id.til_facebookURL);
    }

    private int getHomeIcon(int arrows) {
        return arrows;
    }

    private void setDataInputTextLayout(int id, String link) {
        final TextInputLayout viewById = (TextInputLayout) findViewById(id);
        boolean auth = URLUtil.isValidUrl(link);
        if (auth) {
            viewById.getEditText().setText(link);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_save_changes:
                clearTilError(R.id.til_twitterURL);
                clearTilError(R.id.til_facebookURL);
                clearTilError(R.id.til_linkedinURL);
                URLUtil urlUtil = new URLUtil();
                String fbLink = "", twitterLink = "", lnLink = "";

                lnLink = getDataInputTextLayout(R.id.til_linkedinURL);
                twitterLink = getDataInputTextLayout(R.id.til_twitterURL);
                fbLink = getDataInputTextLayout(R.id.til_facebookURL);

                boolean isValidLnkdIn = checkValidity(lnLink, R.id.til_linkedinURL, R.string.error_linkedin_url, R.string.prefix_linkedin);
                boolean isValidTwitter = checkValidity(twitterLink, R.id.til_twitterURL, R.string.error_twitter_url, R.string.prefix_twitter);
                boolean isValidFB = checkValidity(fbLink, R.id.til_facebookURL, R.string.error_fb_link, R.string.prefix_facebook);

                if (isValidLnkdIn && isValidFB && isValidTwitter) {
                    updateSocialProfile(lnLink, twitterLink, fbLink);
                }
                break;
        }
    }

    private void updateSocialProfile(String lnLink, String twitterLink, String fbLink) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUserTypeUrl("basic");
        JSONObject jsonObject = new JSONObject();
        try {
            if(fbLink.trim().equals(getString(R.string.prefix_facebook))){
                jsonObject.put("fblink", null);
            }else {
                jsonObject.put("fblink", fbLink);
            }
            if(lnLink.trim().equals(getString(R.string.prefix_linkedin))){
                jsonObject.put("lnlink", null);
            }else {
                jsonObject.put("lnlink", lnLink);
            }
            if(twitterLink.trim().equals(getString(R.string.prefix_twitter))){
                jsonObject.put("twitterlink", null);
            }else {
                jsonObject.put("twitterlink", twitterLink);
            }
            jsonObject.put("name", user.getName());
            jsonObject.put("gender", user.getGender());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("userDetails", user.getUserDetails());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
    }

    private boolean checkValidity(String text, int tilId, int errorTextId, int prefixStringId) {
        if (TextUtils.isEmpty(text)) {
            return true;
        }
        if (text.startsWith(getString(prefixStringId))) {
            return true;
        } else {
            setTilError(tilId, errorTextId);
            return false;
        }

    }


    private String getDataInputTextLayout(int id) {
        TextInputLayout viewId = (TextInputLayout) findViewById(id);
        return viewId.getEditText().getText().toString();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    setResult(RESULT_OK);
                    showToast("Social Profile Update successfully");
                    finish();
                } else {
                    showToast("Social Profile could not be updated");
                }
        }
    }
}
