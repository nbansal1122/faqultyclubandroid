package faqulty.club;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import faqulty.club.R;

import faqulty.club.activity.LogoPreviewActivity;
import faqulty.club.fragments.AddImageFragment;
import faqulty.club.fragments.Image;
import faqulty.club.fragments.MediaFragment;
import faqulty.club.models.BrandLogoResponse;
import faqulty.club.models.LogoData;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.requestmodels.UploadImageData;
import simplifii.framework.requestmodels.UploadImageResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class CreateLogoActivity extends BaseActivity {

    private MediaFragment mediaFragment;
    private Bitmap logoBitmap;
    private ImageView imageView;
    private String logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_logo);
        initToolBar("Logo/Watermark/Branding");
        imageView = (ImageView) findViewById(R.id.iv_logo);
        setOnClickListener(R.id.iv_get_image, R.id.tv_preview, R.id.btn_genrete_pemphlet);
        mediaFragment = MediaFragment.init(getSupportFragmentManager());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getLogoFata();
    }

    private void getLogoFata() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GET_LOGO_DATA, Preferences.getUserId()));
        httpParamObject.setClassType(BrandLogoResponse.class);
        executeTask(AppConstants.TASKCODES.GET_LOGO, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_get_image:
                getLogoImage();
                break;
            case R.id.tv_preview:
                String brandName = getEditText(R.id.et_brand);
                if (TextUtils.isEmpty(brandName)) {
                    showToast(getString(R.string.error_empty_brand_name));
                    return;
                }
                String watermark = getEditText(R.id.et_watermark);
                PreviewData(brandName, watermark);
                break;
            case R.id.btn_genrete_pemphlet:
                String text = getEditText(R.id.et_brand);
                if (TextUtils.isEmpty(text)) {
                    showToast(getString(R.string.error_empty_brand_name));
                    return;
                }
                if (logoBitmap != null) {
                    uploadLogo();
                } else {
                    submitData("");
                }
                break;
        }
    }

    private void uploadLogo() {
        showProgressDialog();
        try {
            File file = Util.getFile(logoBitmap, "pamphlet logo");
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submitData(String logo) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.CERATE_LOGO, Preferences.getUserId()));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("logo", logo);
            jsonObject.put("brandingText", getEditText(R.id.et_brand));
            jsonObject.put("watermark", getEditText(R.id.et_watermark));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setPostMethod();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BrandLogoResponse.class);
        executeTask(AppConstants.TASKCODES.CREATE_LOGO, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.CREATE_LOGO:
                BrandLogoResponse brandLogoResponse = (BrandLogoResponse) response;
                if (brandLogoResponse != null) {
                    if (brandLogoResponse.isStatus()) {
                        showToast(brandLogoResponse.getMsg());
                        setResult(RESULT_OK);
                        finish();
                    }
                }
                break;
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    UploadImageData data = uploadImageResponse.getData();
                    if (data != null) {
                        submitData(data.getUri());
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_LOGO:
                BrandLogoResponse logoResponse = (BrandLogoResponse) response;
                if (logoResponse != null) {
                    LogoData data = logoResponse.getData();
                    if(data!=null){
                        setData(data);
                    }
                }
                break;
        }
    }

    private void setData(LogoData data) {
        setText(data.getBrandingText(),R.id.et_brand);
        setText(data.getWatermark(),R.id.et_watermark);
        logo = data.getLogo();
        if(!TextUtils.isEmpty(logo)){
            Picasso.with(this).load(Util.getCompleteUrl(logo)).into(imageView);
        }
    }

    private void PreviewData(String brandName, String watermark) {
        String logoPath = "";
        if (logoBitmap != null) {
            try {
                File file = Util.getFile(logoBitmap, getString(R.string.app_name));
                logoPath = file.getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LogoPreviewActivity.showPreview(brandName, watermark, logoPath,logo, this);
    }

    private void getLogoImage() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.upload_logo));
        mediaFragment.setImageListener(new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                logoBitmap = bitmap;
                imageView.setImageBitmap(Util.getResizeBitmap(bitmap, 1024));
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                if (images.size() > 0) {
                    String path = images.get(0).getFilePath();
                    logoBitmap = Util.getBitmapFromFilePath(path, CreateLogoActivity.this);
                    imageView.setImageBitmap(logoBitmap);
                }
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(mediaFragment);
        bottomSheetDialogFragment.setArguments(bundle);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }
}
