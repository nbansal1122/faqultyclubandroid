package faqulty.club.chat;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import faqulty.club.R;
import faqulty.club.autoadapters.BaseRecycleAdapter;
import faqulty.club.holder.receive.ReceiveAssignmentMessageHolder;
import faqulty.club.holder.receive.ReceiveBookmarkMessageHolder;
import faqulty.club.holder.receive.ReceiveMediaMessageHolder;
import faqulty.club.holder.receive.ReceiveTextMessageHolder;
import faqulty.club.holder.send.SendAssignmentMessageHolder;
import faqulty.club.holder.send.SendBookmarkMessageHolder;
import faqulty.club.holder.send.SendMediaMessageHolder;
import faqulty.club.holder.send.SendTextMessageHolder;
import faqulty.club.holders.BaseHolder;
import faqulty.club.models.chat.ChatMessage;

import java.util.List;

import simplifii.framework.fragments.TaskFragment;
import simplifii.framework.utility.AppConstants;


/**
 * Created by nbansal2211 on 25/08/16.
 */
public class ChatRecycleAdapter<T extends ChatMessage> extends BaseRecycleAdapter implements TaskFragment.AsyncTaskListener {

    public ChatRecycleAdapter(Context context, List<T> list) {
        super(context,list);
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        BaseHolder baseHolder;
        switch (viewType) {
            case AppConstants.VIEW_TYPE.CHAT_TYPES.TEXT_MESSAGE_RECEIVE:
                itemView = inflater.inflate(R.layout.row_chat_text_message_receive, parent, false);
                baseHolder=new ReceiveTextMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.TEXT_MESSAGE_SEND:
                itemView = inflater.inflate(R.layout.row_chat_text_message_send, parent, false);
                baseHolder=new SendTextMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.MEDIA_MESSAGE_SEND:
                itemView = inflater.inflate(R.layout.row_chat_media_message_send, parent, false);
                baseHolder=new SendMediaMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.MEDIA_MESSAGE_RECEIVE:
                itemView = inflater.inflate(R.layout.row_chat_media_message_receive, parent, false);
                baseHolder=new ReceiveMediaMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.BOOKMARK_MESSAGE_SEND:
                itemView = inflater.inflate(R.layout.row_bookmark_media_message_send, parent, false);
                baseHolder=new SendBookmarkMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.BOOKMARK_MESSAGE_RECEIVE:
                itemView = inflater.inflate(R.layout.row_bookmark_media_message_receive, parent, false);
                baseHolder=new ReceiveBookmarkMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.ASSIGNMENT_MESSAGE_SEND:
                itemView = inflater.inflate(R.layout.row_chat_assignment_message_send, parent, false);
                baseHolder=new SendAssignmentMessageHolder(itemView);
                return baseHolder;
            case AppConstants.VIEW_TYPE.CHAT_TYPES.ASSIGNMENT_MESSAGE_RECEIVE:
                itemView = inflater.inflate(R.layout.row_chat_assignment_message_receive, parent, false);
                baseHolder=new ReceiveAssignmentMessageHolder(itemView);
                return baseHolder;

        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage chatMessage = (ChatMessage) list.get(position);
        return chatMessage.getItemViewType();
    }
}

