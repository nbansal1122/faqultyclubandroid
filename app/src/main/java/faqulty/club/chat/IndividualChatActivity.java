package faqulty.club.chat;

import android.os.Bundle;

import faqulty.club.AppController;
import faqulty.club.chatutil.SocketUtil;
import faqulty.club.models.tutorstudent.StudentBaseClass;
import faqulty.club.models.chat.ChatMessage;
import faqulty.club.models.chat.MetaInfo;

import simplifii.framework.requestmodels.UploadImageResponse;
import simplifii.framework.utility.AppConstants;

public class IndividualChatActivity extends BaseChatActivity {
     private StudentBaseClass studentBaseClass;

    @Override
    protected void loadBundle(Bundle bundle) {
        studentBaseClass = (StudentBaseClass) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
    }

    @Override
    protected void refresh() {
        initToolBar(studentBaseClass.getName());
        initLoader(studentBaseClass.getId());
        AppController.chatUserId=studentBaseClass.getId();
    }

    @Override
    protected void sendTextMessage(String message) {
        super.sendTextMessage(message);
        if (studentBaseClass != null) {
            String metaInfoStrign = MetaInfo.getMetaInfoString(message);
            ChatMessage chatMessage = ChatMessage.sendIndividualTextToStudent(metaInfoStrign, studentBaseClass.getId());
            SocketUtil.sendChatMessage(chatMessage, this);
            refresh();
        }
    }

    @Override
    protected void sendFile(UploadImageResponse uploadImageResponse, String mediaType) {
        if (studentBaseClass != null) {
            String metaInfoStrign = MetaInfo.getMetaInfoFile(uploadImageResponse.getData());
            ChatMessage chatMessage = ChatMessage.sendIndividualFileToStudent(metaInfoStrign, studentBaseClass.getId());
            SocketUtil.sendChatMessage(chatMessage, this);
            refresh();
        }
    }

    @Override
    protected void onDestroy() {
        AppController.chatUserId=0;
        super.onDestroy();
    }
}
