package faqulty.club.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import faqulty.club.R;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by admin on 2/22/17.
 */

public class AttachmentFragment extends BaseFragment implements CustomListAdapterInterface {
    private OnAttachmentSelectListener onAttachmentSelectListener;
    private List<String> attachmentList=new ArrayList<>();

    public static AttachmentFragment getInstance(OnAttachmentSelectListener onAttachmentSelectListener) {
        AttachmentFragment attachmentFragment=new AttachmentFragment();
        attachmentFragment.onAttachmentSelectListener = onAttachmentSelectListener;
        return attachmentFragment;
    }

    @Override
    public void initViews() {
        initList();
        ListView listView= (ListView) findView(R.id.list_attachment);
        CustomListAdapter customListAdapter=new CustomListAdapter(getActivity(),R.layout.row_attachment,attachmentList,this);
        listView.setAdapter(customListAdapter);
    }

    private void initList() {
        attachmentList.add("Assignment");
        attachmentList.add("Bookmark");
        attachmentList.add("Gallery");
        attachmentList.add("Calender");
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_chat_attachment;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if(convertView==null){
            convertView=inflater.inflate(resourceID,null);
        }
        setText(R.id.tv_attachment_name,attachmentList.get(position),convertView);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAttachmentSelectListener.onAttachmentSelect(position);
            }
        });
        return convertView;
    }

    public interface OnAttachmentSelectListener{
        void onAttachmentSelect(int attachmentType);
    }
}
