
package faqulty.club;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import faqulty.club.R;

import faqulty.club.activity.AboutExampleActivity;
import faqulty.club.activity.ChangePasswordActivity;
import faqulty.club.fragments.AddFromGalleryFragment;
import faqulty.club.fragments.AddImageFragment;
import faqulty.club.fragments.Image;
import faqulty.club.fragments.MediaFragment;

import faqulty.club.models.UserProfile.UserDetailsApi;
import simplifii.framework.requestmodels.UploadImageResponse;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by saurabh on 06-09-2016.
 */
public class EditIntroActivity extends BaseActivity {
    private Button saveChanges;
    private TextInputLayout name, email;
    private EditText tagline, aboutyou;
    private UserDetailsApi user;
    private ImageView profilePic;
    private MediaFragment imagePicker;
    private Bitmap profileBitmap;
    private ArrayList<Object> arrayList = new ArrayList<>();
    String profilePicUri = "";
    boolean isTutor;
    private AddFromGalleryFragment.GetMediaListener getMediaListener;
    private String imagepath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_intro);
        user = UserDetailsApi.getInstance();
        isTutor = user.isTutor();
        getHomeIcon(R.mipmap.arrows);
        initToolBar(getString(R.string.edit_basic_details));

        saveChanges = (Button) findViewById(R.id.btn_save_changes);
        name = (TextInputLayout) findViewById(R.id.til_full_name);
        email = (TextInputLayout) findViewById(R.id.til_email);
        tagline = (EditText) findViewById(R.id.til_tagline);
        aboutyou = (EditText) findViewById(R.id.til_aboutyou);
        profilePic = (ImageView) findViewById(R.id.iv_profile_pic);

        setViewsIfTuitionCentre();

        setDataTextInputLayout(R.id.til_full_name, user.getName());
        setDataTextInputLayout(R.id.til_email, user.getEmail());
        setText(user.getTagline(), R.id.til_tagline);
        setText(user.getUserDetails(), R.id.til_aboutyou);

        imagePicker = new MediaFragment();
        imagePicker.isSquareShape = true;
        getSupportFragmentManager().beginTransaction().add(imagePicker, "Profile image").commit();

        setOnClickListener(R.id.btn_save_changes, R.id.iv_profile_pic, R.id.btn_edit_profile_pic, R.id.tv_about_me, R.id.tv_tag_line, R.id.btn_delete_profile_pic, R.id.lay_change_pass);

        if (!TextUtils.isEmpty(user.getImage())) {
            profilePicUri = user.getImage();
            Picasso.with(this).load(user.getCompleteImageUrl()).placeholder(R.mipmap.accountselected2x).into(profilePic);
        }

//        addTutorLocations();
    }

    private void setViewsIfTuitionCentre() {
        if (!isTutor) {
            setHintTextOfTIL(R.id.til_full_name, R.string.hint_name);
            setHintTextOfTIL(R.id.til_email, R.string.hint_email);
            setText(getString(R.string.hint_about_tuition_centre), R.id.tv_header_about_me);
            setText(getString(R.string.examples_about_t_center), R.id.tv_about_me);
            hideVisibility(R.id.ll_gender, R.id.tv_gender);
            setText(getString(R.string.basic_details), R.id.tv_heading_basic_details);
            setText(getString(R.string.institute_photo), R.id.lbl_faculty_photo);
        } else {
            setText(getString(R.string.profile_pic), R.id.lbl_faculty_photo);
            if ("male".equalsIgnoreCase(user.getGender())) {
                setRadioState(R.id.rb_male, true);
                setRadioState(R.id.rb_female, false);
            } else {
                setRadioState(R.id.rb_male, false);
                setRadioState(R.id.rb_female, true);
            }
            setText(getString(R.string.examples_about_me), R.id.tv_about_me);
        }
    }

    private void setHintTextOfTIL(int tilId, int hintTextId) {
        TextInputLayout layout = (TextInputLayout) findViewById(tilId);
        layout.setHint(getString(hintTextId));
    }


    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    private void setDataTextInputLayout(int id, String text) {
        TextInputLayout viewId = (TextInputLayout) findViewById(id);
        viewId.getEditText().setText(text);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_save_changes:
                if (isValid(name, email, aboutyou)) {

                } else {
                    return;
                }
                if (profileBitmap != null) {
                    uploadImageData();
                } else {
                    uploadUserData();
                }
                break;
            case R.id.iv_profile_pic:
                askPermissions();
                break;
            case R.id.btn_edit_profile_pic:
                askPermissions();
                break;
            case R.id.btn_delete_profile_pic:
                profilePicUri = "";
                profileBitmap = null;
                profilePic.setImageResource(0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    profilePic.setBackground(getDrawable(R.mipmap.accountselected2x));
                }
                break;
            case R.id.tv_about_me:
                Bundle b = new Bundle();
                if (user.isTutor()) {
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_ABOUT_TITLE, getString(R.string.title_example_about_me));
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_FILE_NAME, AppConstants.ASSETS_RESOURCES.ABOUT_TUTOR_JSON);
                } else {
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_ABOUT_TITLE, getString(R.string.title_example_about_t_center));
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_FILE_NAME, AppConstants.ASSETS_RESOURCES.ABOUT_TUTOR_JSON);
                }
                startNextActivityForResult(b, AboutExampleActivity.class, AppConstants.REQUEST_CODES.REQ_ABOUT_ME_EX);
                break;
            case R.id.tv_tag_line:
                Bundle b1 = new Bundle();
                b1.putString(AppConstants.BUNDLE_KEYS.KEY_ABOUT_TITLE, getString(R.string.title_example_tagline));
                b1.putString(AppConstants.BUNDLE_KEYS.KEY_FILE_NAME, AppConstants.ASSETS_RESOURCES.TAGLINE_EX_JSON);
                startNextActivityForResult(b1, AboutExampleActivity.class, AppConstants.REQUEST_CODES.REQ_TAGLINE_EX);
                break;
            case R.id.lay_change_pass:
                startNextActivity(ChangePasswordActivity.class);
                break;
        }
    }

    private boolean isValid(TextInputLayout til_full_name, TextInputLayout til_email, EditText til_aboutyou) {
        if (TextUtils.isEmpty(til_full_name.getEditText().getText().toString().trim())) {
            showToast(getString(R.string.empty_name));
            return false;
        }
        if (TextUtils.isEmpty(til_email.getEditText().getText().toString().trim())) {
            showToast(getString(R.string.error_empty_email));
            return false;
        }
        if (TextUtils.isEmpty(til_aboutyou.getText().toString().trim())) {
            if (user.isTutor()) {
                showToast(getString(R.string.empty_about_you));
            } else {
                showToast(getString(R.string.empty_about_tuition_center));
            }

            return false;
        } else {
            String text = til_aboutyou.getText().toString().trim();
            if (text.length() < 25) {
                if (user.isTutor()) {
                    showToast(getString(R.string.min_char_about_tuition_center, " about me"));
                } else {
                    showToast(getString(R.string.min_char_about_tuition_center, " about tuition center"));
                }
                return false;
            }
        }
        return true;

    }

    private void askPermissions() {
        selectImageViaPicker();
//
//        new TedPermission(this)
//                .setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                .setPermissionListener(new PermissionListener() {
//                    @Override
//                    public void onPermissionGranted() {
//                    }
//
//                    @Override
//                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
//
//                    }
//                }).check();
    }

    private void selectImageViaPicker() {
        Bundle b = new Bundle();
        b.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, "Select profile picture");
        b.putBoolean(AppConstants.BUNDLE_KEYS.POST_FEED, false);
        imagePicker.setImageListener(new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                onBitmap(bitmap, path);
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                onImageArray(images);
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(imagePicker);
        bottomSheetDialogFragment.setArguments(b);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    private void onImageArray(ArrayList<Image> images) {
        if (images != null && images.size() > 0) {
            for (Image image : images) {
                Bitmap bMap = Util.getBitmapFromFilePath(image.getFilePath(), this);
                if (bMap != null) {
                    this.profileBitmap = bMap;
                    profilePic.setImageBitmap(bMap);
                }
            }
        }
    }


    private void onBitmap(Bitmap bitmap, String path) {
        if (bitmap != null) {
            this.profileBitmap = bitmap;
            profilePic.setImageBitmap(bitmap);
            try {
                File feed = Util.getFile(bitmap, "Feed");
                imagepath = feed.getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void uploadUserData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUserTypeUrl("basic");
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setPutMethod();
        httpParamObject.setContentType("application/json");
        if (getTilText(R.id.til_email).length() < 256) {
            httpParamObject.setJson(getUpdatedJson().toString());
            executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
        } else
            setError(R.id.et_email_id, "Email id can't exceed 256 characters");
    }

    private void uploadImageData() {
        try {
            File file = Util.getFile(profileBitmap, "ProfilePicFaqultyClub");
            FileParamObject fileParamObject = null;
            if (profileBitmap == null)
                fileParamObject = new FileParamObject(null, profilePicUri, "file");
            else
                fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getGender() {
        if (isRadioChecked(R.id.rb_male)) {
            return "Male";
        } else {
            return "Female";
        }
    }

    private boolean isRadioChecked(int radioButtonId) {
        RadioButton btn = (RadioButton) findViewById(radioButtonId);
        return btn.isChecked();
    }

    private void setRadioState(int radioButtonId, boolean isChecked) {
        RadioButton btn = (RadioButton) findViewById(radioButtonId);
        btn.setChecked(isChecked);
    }


    private JSONObject getUpdatedJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getTilText(R.id.til_full_name));
            if (isTutor) {
                jsonObject.put("gender", getGender());
            }
            jsonObject.put("email", getTilText(R.id.til_email));
            jsonObject.put("tagline", getEditText(R.id.til_tagline));
            jsonObject.put("userDetails", getEditText(R.id.til_aboutyou));
            jsonObject.put("image", profilePicUri);
            //TODO put id of selected city in put

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    if (!TextUtils.isEmpty(uploadImageResponse.getData().getUri()))
                        profilePicUri = uploadImageResponse.getData().getUri();
                    uploadUserData();
                }
                break;
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    showToast(R.string.basic_profile_updated_successfully);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
           /* case AppConstants.REQUEST_CODES.REQ_PICK_IMAGE:
                ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
                if (images != null && images.size() == 1) {
                    Log.d(TAG, "Path :" + images.get(0).getPath());
                    File f = new File(images.get(0).getPath());
                    if (f != null)
                        startActivityToCrop(Uri.fromFile(f));
//                    Bitmap bMap = getBitmapFromFilePath(images.get(0).getPath());
//                    if (bMap != null) {
//                        profilePic.setImageBitmap(bMap);
//                        profileBitmap = bMap;
//                    }
                }
                break;*/
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                afterCrop(resultUri);
                break;
            case AppConstants.REQUEST_CODES.REQ_ABOUT_ME_EX:
                setText(data.getExtras().getString(AppConstants.BUNDLE_KEYS.KEY_EXAMPLE_TEXT), R.id.til_aboutyou);
                break;
            case AppConstants.REQUEST_CODES.REQ_TAGLINE_EX:
                setText(data.getExtras().getString(AppConstants.BUNDLE_KEYS.KEY_EXAMPLE_TEXT), R.id.til_tagline);
                break;

        }
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                Uri resultUri = result.getUri();
//                afterCrop(resultUri);
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//            }
//        }
    }

    private void afterCrop(Uri imageUri) {
        Bitmap bitmap = Util.getBitmapFromUri(this, imageUri);
        if (bitmap != null) {
            profilePic.setImageBitmap(bitmap);
            profileBitmap = bitmap;
        }
    }

    private void startActivityToCrop(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }
}
