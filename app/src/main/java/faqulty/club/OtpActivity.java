package faqulty.club;

import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import faqulty.club.R;

import faqulty.club.models.BaseApi;
import faqulty.club.models.LoginResponse;
import faqulty.club.models.UserProfile.UserDetailsApi;
import faqulty.club.receivers.SMSListener;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by saurabh on 13-09-2016.
 */
public class OtpActivity extends BaseActivity implements SMSListener.OTPCallback {
    private Button submit;
    private EditText otp;
    private TextView mobno, resendOTP;
    private String mobile, userType, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        initToolBar("Enter OTP");
        getHomeIcon(R.mipmap.arrows);

        submit = (Button) findViewById(R.id.btn_submit);
        otp = (EditText) findViewById(R.id.otp);
        mobno = (TextView) findViewById(R.id.tv_mob_no);
        resendOTP = (TextView) findViewById(R.id.resend_otp);
        setOnClickListener(R.id.btn_submit, R.id.resend_otp);
        mobno.setText(Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, "0"));
        askPermissionToReceiveSMS();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        mobile = bundle.getString(AppConstants.BUNDLE_KEYS.PHONE);
        userType = bundle.getString(AppConstants.BUNDLE_KEYS.USERTYPE);
        password = bundle.getString(AppConstants.BUNDLE_KEYS.PASSWORD);
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_submit:
                verifyOTP();
                break;
            case R.id.resend_otp:
                resendOTP();
                break;
        }
    }

    private void verifyOTP() {
        if (isValidate(R.id.otp)) {
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.OTP_VERIFY);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", otp.getText().toString());
                jsonObject.put("phone", Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            httpParamObject.setPostMethod();
            httpParamObject.setJson(jsonObject.toString());
            httpParamObject.setContentType("application/json");
            executeTask(AppConstants.TASKCODES.OTP, httpParamObject);

        }
    }

    public void resendOTP() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.RESENDOTP);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", mobile);
//            jsonObject.put("password", password);
//            jsonObject.put("confirmPassword", password);
//            jsonObject.put("source", "local");
//            jsonObject.put("userType", userType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setClassType(BaseApi.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setContentType("application/json");
        executeTask(AppConstants.TASKCODES.RESEND_OTP, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.OTP:
                Bundle extra = getIntent().getExtras();
                if (extra != null) {
                    if (extra.getBoolean(AppConstants.BUNDLE_KEYS.SOCIALSIGNUP)) {
                        setResult(RESULT_OK);
                        finish();
                        return;
                    }
                }
                loginUser();
                break;
            case AppConstants.TASKCODES.RESEND_OTP:
                BaseApi baseApi = (BaseApi) response;
                if (null != baseApi)
                    showToast(baseApi.getMsg());
                break;
            case AppConstants.TASKCODES.LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    LoginActivity.saveUserData(loginResponse);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    private void loginUser() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.LOGIN);
        httpParamObject.setClassType(LoginResponse.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(getJsonData().toString());
        httpParamObject.setContentType("application/json");
        executeTask(AppConstants.TASKCODES.LOGIN, httpParamObject);
    }

    private JSONObject getJsonData() {
        String mobileNumber = Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", mobileNumber);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void getUserData(LoginResponse loginResponse) {
        UserDetailsApi user;
        HttpParamObject obj = new HttpParamObject();
        obj.setUrl(AppConstants.PAGE_URL.GET_USER + loginResponse.getUserId());
        obj.setClassType(UserDetailsApi.class);
        obj.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, obj);

    }

    private void askPermissionToReceiveSMS() {
        new TedPermission(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        registerOTPReceiver();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> arrayList) {
//                        registerReceiver();
                    }
                })
                .setDeniedMessage("Permission required by Faqulty Club to verify otp")
                .setPermissions(android.Manifest.permission.RECEIVE_SMS)
                .setRationaleMessage("Permission required by Faqulty Club to verify otp")
                .check();
    }

    private SMSListener smsListener;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (smsListener != null) {
            unregisterReceiver(smsListener);
        }
    }

    private void registerOTPReceiver() {
        if (smsListener == null) {
            smsListener = new SMSListener(this);
        }
        registerReceiver(smsListener, getFilter());
    }

    private IntentFilter getFilter() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        return filter;
    }


    @Override
    public void onOTPReceived(String otpString) {
//        otp.setText(otpString);
//        verifyOTP();
    }
}
